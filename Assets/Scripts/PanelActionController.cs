﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelActionController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
    void Update()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                transform.Translate(0, 0, 8);
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                transform.Translate(0, 0, -8);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                transform.Translate(-8, 0, 0);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                transform.Translate(8, 0, 0);
            }
        if (Input.GetKeyDown(KeyCode.W))
        {
            transform.Translate(0, 0, 8);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            transform.Translate(0, 0, -8);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            transform.Translate(-8, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.Translate(8, 0, 0);
        }
    }
    }

