﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// PauseMenu
struct PauseMenu_t3916167947;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t1232140387;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.GUIText
struct GUIText_t402233326;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t2571361770;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.SpringJoint
struct SpringJoint_t1912369980;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t2999697109;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WheelCollider
struct WheelCollider_t2307365979;
// UnityEngine.WheelCollider[]
struct WheelColliderU5BU5D_t483547098;
// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct ParticleSystemMultiplier_t2770350653;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
struct DemoParticleSystem_t1195446716;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
struct DemoParticleSystemList_t4288938153;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[]
struct DemoParticleSystemU5BU5D_t58401749;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t2596446823;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_t1887104210;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t219844479;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t1600652016;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t639177103;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t558680695;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t3168066469;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t2725803170;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t3574483607;
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t445075330;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t2584574554;
// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings
struct AdvancedSetttings_t3817742043;
// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface[]
struct ControlSurfaceU5BU5D_t233403270;
// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController
struct AeroplaneController_t857819489;
// UnityStandardAssets.Vehicles.Car.CarController
struct CarController_t4228304488;
// UnityStandardAssets.Vehicles.Car.SkidTrail
struct SkidTrail_t3884084597;
// UnityStandardAssets.Vehicles.Car.WheelEffects
struct WheelEffects_t1559845116;
// UnityStandardAssets.Vehicles.Car.WheelEffects[]
struct WheelEffectsU5BU5D_t3384186773;




#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef DEMOPARTICLESYSTEMLIST_T4288938153_H
#define DEMOPARTICLESYSTEMLIST_T4288938153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
struct  DemoParticleSystemList_t4288938153  : public RuntimeObject
{
public:
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[] UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::items
	DemoParticleSystemU5BU5D_t58401749* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(DemoParticleSystemList_t4288938153, ___items_0)); }
	inline DemoParticleSystemU5BU5D_t58401749* get_items_0() const { return ___items_0; }
	inline DemoParticleSystemU5BU5D_t58401749** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(DemoParticleSystemU5BU5D_t58401749* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOPARTICLESYSTEMLIST_T4288938153_H
#ifndef REPLACEMENTDEFINITION_T2693741842_H
#define REPLACEMENTDEFINITION_T2693741842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t2693741842  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t4151988712 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t4151988712 * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___original_0)); }
	inline Shader_t4151988712 * get_original_0() const { return ___original_0; }
	inline Shader_t4151988712 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_t4151988712 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___replacement_1)); }
	inline Shader_t4151988712 * get_replacement_1() const { return ___replacement_1; }
	inline Shader_t4151988712 ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_t4151988712 * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_T2693741842_H
#ifndef REPLACEMENTLIST_T1887104210_H
#define REPLACEMENTLIST_T1887104210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_t1887104210  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t2596446823* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_t1887104210, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t2596446823* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t2596446823** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t2596446823* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_T1887104210_H
#ifndef FOVKICK_T120370150_H
#define FOVKICK_T120370150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t120370150  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t4157153871 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_t3046754366 * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___IncreaseCurve_5)); }
	inline AnimationCurve_t3046754366 * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_t3046754366 * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T120370150_H
#ifndef U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#define U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1
struct  U3CFOVKickDownU3Ec__Iterator1_t1440840980  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifndef U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#define U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0
struct  U3CFOVKickUpU3Ec__Iterator0_t3738408313  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifndef LERPCONTROLLEDBOB_T1895875871_H
#define LERPCONTROLLEDBOB_T1895875871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t1895875871  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T1895875871_H
#ifndef U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#define U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0
struct  U3CDoBobCycleU3Ec__Iterator0_t1149538828  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$this
	LerpControlledBob_t1895875871 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24this_1)); }
	inline LerpControlledBob_t1895875871 * get_U24this_1() const { return ___U24this_1; }
	inline LerpControlledBob_t1895875871 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LerpControlledBob_t1895875871 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifndef U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#define U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0
struct  U3CResetCoroutineU3Ec__Iterator0_t3232105836  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::delay
	float ___delay_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar0
	TransformU5BU5D_t807237628* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$this
	ObjectResetter_t639177103 * ___U24this_3;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar0_1)); }
	inline TransformU5BU5D_t807237628* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline TransformU5BU5D_t807237628** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(TransformU5BU5D_t807237628* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24this_3)); }
	inline ObjectResetter_t639177103 * get_U24this_3() const { return ___U24this_3; }
	inline ObjectResetter_t639177103 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ObjectResetter_t639177103 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifndef U3CSTARTU3EC__ITERATOR0_T980021917_H
#define U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t980021917  : public RuntimeObject
{
public:
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<systems>__0
	ParticleSystemU5BU5D_t3089334924* ___U3CsystemsU3E__0_0;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar0
	ParticleSystemU5BU5D_t3089334924* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<stopTime>__0
	float ___U3CstopTimeU3E__0_3;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar2
	ParticleSystemU5BU5D_t3089334924* ___U24locvar2_4;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar3
	int32_t ___U24locvar3_5;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$this
	ParticleSystemDestroyer_t558680695 * ___U24this_6;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CsystemsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CsystemsU3E__0_0)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U3CsystemsU3E__0_0() const { return ___U3CsystemsU3E__0_0; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U3CsystemsU3E__0_0() { return &___U3CsystemsU3E__0_0; }
	inline void set_U3CsystemsU3E__0_0(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U3CsystemsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar0_1)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CstopTimeU3E__0_3)); }
	inline float get_U3CstopTimeU3E__0_3() const { return ___U3CstopTimeU3E__0_3; }
	inline float* get_address_of_U3CstopTimeU3E__0_3() { return &___U3CstopTimeU3E__0_3; }
	inline void set_U3CstopTimeU3E__0_3(float value)
	{
		___U3CstopTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar2_4)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar2_4() const { return ___U24locvar2_4; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar2_4() { return &___U24locvar2_4; }
	inline void set_U24locvar2_4(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_4), value);
	}

	inline static int32_t get_offset_of_U24locvar3_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar3_5)); }
	inline int32_t get_U24locvar3_5() const { return ___U24locvar3_5; }
	inline int32_t* get_address_of_U24locvar3_5() { return &___U24locvar3_5; }
	inline void set_U24locvar3_5(int32_t value)
	{
		___U24locvar3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24this_6)); }
	inline ParticleSystemDestroyer_t558680695 * get_U24this_6() const { return ___U24this_6; }
	inline ParticleSystemDestroyer_t558680695 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ParticleSystemDestroyer_t558680695 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifndef U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#define U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0
struct  U3CActivateU3Ec__Iterator0_t2664723090  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#ifndef U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#define U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1
struct  U3CDeactivateU3Ec__Iterator1_t730025274  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#ifndef U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#define U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2
struct  U3CReloadLevelU3Ec__Iterator2_t2784493974  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#ifndef ENTRIES_T3168066469_H
#define ENTRIES_T3168066469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t3168066469  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t3574483607* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(Entries_t3168066469, ___entries_0)); }
	inline EntryU5BU5D_t3574483607* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t3574483607** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t3574483607* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRIES_T3168066469_H
#ifndef WAYPOINTLIST_T2584574554_H
#define WAYPOINTLIST_T2584574554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t2584574554  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_t445075330 * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t807237628* ___items_1;

public:
	inline static int32_t get_offset_of_circuit_0() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___circuit_0)); }
	inline WaypointCircuit_t445075330 * get_circuit_0() const { return ___circuit_0; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_0() { return &___circuit_0; }
	inline void set_circuit_0(WaypointCircuit_t445075330 * value)
	{
		___circuit_0 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___items_1)); }
	inline TransformU5BU5D_t807237628* get_items_1() const { return ___items_1; }
	inline TransformU5BU5D_t807237628** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(TransformU5BU5D_t807237628* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTLIST_T2584574554_H
#ifndef ADVANCEDSETTTINGS_T3817742043_H
#define ADVANCEDSETTTINGS_T3817742043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings
struct  AdvancedSetttings_t3817742043  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::engineMinDistance
	float ___engineMinDistance_0;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::engineMaxDistance
	float ___engineMaxDistance_1;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::engineDopplerLevel
	float ___engineDopplerLevel_2;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::engineMasterVolume
	float ___engineMasterVolume_3;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::windMinDistance
	float ___windMinDistance_4;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::windMaxDistance
	float ___windMaxDistance_5;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::windDopplerLevel
	float ___windDopplerLevel_6;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::windMasterVolume
	float ___windMasterVolume_7;

public:
	inline static int32_t get_offset_of_engineMinDistance_0() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___engineMinDistance_0)); }
	inline float get_engineMinDistance_0() const { return ___engineMinDistance_0; }
	inline float* get_address_of_engineMinDistance_0() { return &___engineMinDistance_0; }
	inline void set_engineMinDistance_0(float value)
	{
		___engineMinDistance_0 = value;
	}

	inline static int32_t get_offset_of_engineMaxDistance_1() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___engineMaxDistance_1)); }
	inline float get_engineMaxDistance_1() const { return ___engineMaxDistance_1; }
	inline float* get_address_of_engineMaxDistance_1() { return &___engineMaxDistance_1; }
	inline void set_engineMaxDistance_1(float value)
	{
		___engineMaxDistance_1 = value;
	}

	inline static int32_t get_offset_of_engineDopplerLevel_2() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___engineDopplerLevel_2)); }
	inline float get_engineDopplerLevel_2() const { return ___engineDopplerLevel_2; }
	inline float* get_address_of_engineDopplerLevel_2() { return &___engineDopplerLevel_2; }
	inline void set_engineDopplerLevel_2(float value)
	{
		___engineDopplerLevel_2 = value;
	}

	inline static int32_t get_offset_of_engineMasterVolume_3() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___engineMasterVolume_3)); }
	inline float get_engineMasterVolume_3() const { return ___engineMasterVolume_3; }
	inline float* get_address_of_engineMasterVolume_3() { return &___engineMasterVolume_3; }
	inline void set_engineMasterVolume_3(float value)
	{
		___engineMasterVolume_3 = value;
	}

	inline static int32_t get_offset_of_windMinDistance_4() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___windMinDistance_4)); }
	inline float get_windMinDistance_4() const { return ___windMinDistance_4; }
	inline float* get_address_of_windMinDistance_4() { return &___windMinDistance_4; }
	inline void set_windMinDistance_4(float value)
	{
		___windMinDistance_4 = value;
	}

	inline static int32_t get_offset_of_windMaxDistance_5() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___windMaxDistance_5)); }
	inline float get_windMaxDistance_5() const { return ___windMaxDistance_5; }
	inline float* get_address_of_windMaxDistance_5() { return &___windMaxDistance_5; }
	inline void set_windMaxDistance_5(float value)
	{
		___windMaxDistance_5 = value;
	}

	inline static int32_t get_offset_of_windDopplerLevel_6() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___windDopplerLevel_6)); }
	inline float get_windDopplerLevel_6() const { return ___windDopplerLevel_6; }
	inline float* get_address_of_windDopplerLevel_6() { return &___windDopplerLevel_6; }
	inline void set_windDopplerLevel_6(float value)
	{
		___windDopplerLevel_6 = value;
	}

	inline static int32_t get_offset_of_windMasterVolume_7() { return static_cast<int32_t>(offsetof(AdvancedSetttings_t3817742043, ___windMasterVolume_7)); }
	inline float get_windMasterVolume_7() const { return ___windMasterVolume_7; }
	inline float* get_address_of_windMasterVolume_7() { return &___windMasterVolume_7; }
	inline void set_windMasterVolume_7(float value)
	{
		___windMasterVolume_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDSETTTINGS_T3817742043_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3590021378_H
#define U3CSTARTU3EC__ITERATOR0_T3590021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3590021378  : public RuntimeObject
{
public:
	// UnityStandardAssets.Vehicles.Car.SkidTrail UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::$this
	SkidTrail_t3884084597 * ___U24this_0;
	// System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3590021378, ___U24this_0)); }
	inline SkidTrail_t3884084597 * get_U24this_0() const { return ___U24this_0; }
	inline SkidTrail_t3884084597 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SkidTrail_t3884084597 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3590021378, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3590021378, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3590021378, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3590021378_H
#ifndef U3CSTARTSKIDTRAILU3EC__ITERATOR0_T912754801_H
#define U3CSTARTSKIDTRAILU3EC__ITERATOR0_T912754801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator0
struct  U3CStartSkidTrailU3Ec__Iterator0_t912754801  : public RuntimeObject
{
public:
	// UnityStandardAssets.Vehicles.Car.WheelEffects UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator0::$this
	WheelEffects_t1559845116 * ___U24this_0;
	// System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartSkidTrailU3Ec__Iterator0_t912754801, ___U24this_0)); }
	inline WheelEffects_t1559845116 * get_U24this_0() const { return ___U24this_0; }
	inline WheelEffects_t1559845116 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(WheelEffects_t1559845116 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartSkidTrailU3Ec__Iterator0_t912754801, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartSkidTrailU3Ec__Iterator0_t912754801, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartSkidTrailU3Ec__Iterator0_t912754801, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTSKIDTRAILU3EC__ITERATOR0_T912754801_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef ALIGNMODE_T432429434_H
#define ALIGNMODE_T432429434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
struct  AlignMode_t432429434 
{
public:
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AlignMode_t432429434, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMODE_T432429434_H
#ifndef MODE_T2106834709_H
#define MODE_T2106834709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
struct  Mode_t2106834709 
{
public:
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t2106834709, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2106834709_H
#ifndef MODE_T3024470803_H
#define MODE_T3024470803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t3024470803 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3024470803, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3024470803_H
#ifndef CAMERAREFOCUS_T4263235746_H
#define CAMERAREFOCUS_T4263235746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t4263235746  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t4157153871 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_t3722313464  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_t3600365921 * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_t3722313464  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Lookatpoint_1)); }
	inline Vector3_t3722313464  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_t3722313464 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_t3722313464  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Parent_2)); }
	inline Transform_t3600365921 * get_Parent_2() const { return ___Parent_2; }
	inline Transform_t3600365921 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_t3600365921 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_OrigCameraPos_3)); }
	inline Vector3_t3722313464  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_t3722313464 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_t3722313464  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T4263235746_H
#ifndef CURVECONTROLLEDBOB_T2679313829_H
#define CURVECONTROLLEDBOB_T2679313829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t2679313829  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_t3046754366 * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___Bobcurve_2)); }
	inline AnimationCurve_t3046754366 * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_t3046754366 * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T2679313829_H
#ifndef BUILDTARGETGROUP_T72322187_H
#define BUILDTARGETGROUP_T72322187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t72322187 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t72322187, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T72322187_H
#ifndef ACTION_T837364808_H
#define ACTION_T837364808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t837364808 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t837364808, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T837364808_H
#ifndef ROUTEPOINT_T3880028948_H
#define ROUTEPOINT_T3880028948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct  RoutePoint_t3880028948 
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::direction
	Vector3_t3722313464  ___direction_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___direction_1)); }
	inline Vector3_t3722313464  get_direction_1() const { return ___direction_1; }
	inline Vector3_t3722313464 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_t3722313464  value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTEPOINT_T3880028948_H
#ifndef PROGRESSSTYLE_T3254572979_H
#define PROGRESSSTYLE_T3254572979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
struct  ProgressStyle_t3254572979 
{
public:
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProgressStyle_t3254572979, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSSTYLE_T3254572979_H
#ifndef TYPE_T2783176643_H
#define TYPE_T2783176643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface/Type
struct  Type_t2783176643 
{
public:
	// System.Int32 UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2783176643, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2783176643_H
#ifndef GEARSTATE_T3611444568_H
#define GEARSTATE_T3611444568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.LandingGear/GearState
struct  GearState_t3611444568 
{
public:
	// System.Int32 UnityStandardAssets.Vehicles.Aeroplane.LandingGear/GearState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GearState_t3611444568, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEARSTATE_T3611444568_H
#ifndef BRAKECONDITION_T1083170144_H
#define BRAKECONDITION_T1083170144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarAIControl/BrakeCondition
struct  BrakeCondition_t1083170144 
{
public:
	// System.Int32 UnityStandardAssets.Vehicles.Car.CarAIControl/BrakeCondition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BrakeCondition_t1083170144, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAKECONDITION_T1083170144_H
#ifndef ENGINEAUDIOOPTIONS_T2675404656_H
#define ENGINEAUDIOOPTIONS_T2675404656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarAudio/EngineAudioOptions
struct  EngineAudioOptions_t2675404656 
{
public:
	// System.Int32 UnityStandardAssets.Vehicles.Car.CarAudio/EngineAudioOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EngineAudioOptions_t2675404656, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENGINEAUDIOOPTIONS_T2675404656_H
#ifndef CARDRIVETYPE_T1984246879_H
#define CARDRIVETYPE_T1984246879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarDriveType
struct  CarDriveType_t1984246879 
{
public:
	// System.Int32 UnityStandardAssets.Vehicles.Car.CarDriveType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CarDriveType_t1984246879, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARDRIVETYPE_T1984246879_H
#ifndef SPEEDTYPE_T3478026278_H
#define SPEEDTYPE_T3478026278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.SpeedType
struct  SpeedType_t3478026278 
{
public:
	// System.Int32 UnityStandardAssets.Vehicles.Car.SpeedType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpeedType_t3478026278, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDTYPE_T3478026278_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DEMOPARTICLESYSTEM_T1195446716_H
#define DEMOPARTICLESYSTEM_T1195446716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
struct  DemoParticleSystem_t1195446716  : public RuntimeObject
{
public:
	// UnityEngine.Transform UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::transform
	Transform_t3600365921 * ___transform_0;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::mode
	int32_t ___mode_1;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::align
	int32_t ___align_2;
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::maxCount
	int32_t ___maxCount_3;
	// System.Single UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::minDist
	float ___minDist_4;
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::camOffset
	int32_t ___camOffset_5;
	// System.String UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::instructionText
	String_t* ___instructionText_6;

public:
	inline static int32_t get_offset_of_transform_0() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___transform_0)); }
	inline Transform_t3600365921 * get_transform_0() const { return ___transform_0; }
	inline Transform_t3600365921 ** get_address_of_transform_0() { return &___transform_0; }
	inline void set_transform_0(Transform_t3600365921 * value)
	{
		___transform_0 = value;
		Il2CppCodeGenWriteBarrier((&___transform_0), value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_align_2() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___align_2)); }
	inline int32_t get_align_2() const { return ___align_2; }
	inline int32_t* get_address_of_align_2() { return &___align_2; }
	inline void set_align_2(int32_t value)
	{
		___align_2 = value;
	}

	inline static int32_t get_offset_of_maxCount_3() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___maxCount_3)); }
	inline int32_t get_maxCount_3() const { return ___maxCount_3; }
	inline int32_t* get_address_of_maxCount_3() { return &___maxCount_3; }
	inline void set_maxCount_3(int32_t value)
	{
		___maxCount_3 = value;
	}

	inline static int32_t get_offset_of_minDist_4() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___minDist_4)); }
	inline float get_minDist_4() const { return ___minDist_4; }
	inline float* get_address_of_minDist_4() { return &___minDist_4; }
	inline void set_minDist_4(float value)
	{
		___minDist_4 = value;
	}

	inline static int32_t get_offset_of_camOffset_5() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___camOffset_5)); }
	inline int32_t get_camOffset_5() const { return ___camOffset_5; }
	inline int32_t* get_address_of_camOffset_5() { return &___camOffset_5; }
	inline void set_camOffset_5(int32_t value)
	{
		___camOffset_5 = value;
	}

	inline static int32_t get_offset_of_instructionText_6() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___instructionText_6)); }
	inline String_t* get_instructionText_6() const { return ___instructionText_6; }
	inline String_t** get_address_of_instructionText_6() { return &___instructionText_6; }
	inline void set_instructionText_6(String_t* value)
	{
		___instructionText_6 = value;
		Il2CppCodeGenWriteBarrier((&___instructionText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOPARTICLESYSTEM_T1195446716_H
#ifndef VECTOR3ANDSPACE_T219844479_H
#define VECTOR3ANDSPACE_T219844479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t219844479  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_t3722313464  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T219844479_H
#ifndef U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#define U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0
struct  U3CDragObjectU3Ec__Iterator0_t4151609119  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldDrag>__0
	float ___U3ColdDragU3E__0_0;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldAngularDrag>__0
	float ___U3ColdAngularDragU3E__0_1;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<mainCamera>__0
	Camera_t4157153871 * ___U3CmainCameraU3E__0_2;
	// UnityEngine.Ray UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<ray>__1
	Ray_t3785851493  ___U3CrayU3E__1_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::distance
	float ___distance_4;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$this
	DragRigidbody_t1600652016 * ___U24this_5;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3ColdDragU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdDragU3E__0_0)); }
	inline float get_U3ColdDragU3E__0_0() const { return ___U3ColdDragU3E__0_0; }
	inline float* get_address_of_U3ColdDragU3E__0_0() { return &___U3ColdDragU3E__0_0; }
	inline void set_U3ColdDragU3E__0_0(float value)
	{
		___U3ColdDragU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdAngularDragU3E__0_1)); }
	inline float get_U3ColdAngularDragU3E__0_1() const { return ___U3ColdAngularDragU3E__0_1; }
	inline float* get_address_of_U3ColdAngularDragU3E__0_1() { return &___U3ColdAngularDragU3E__0_1; }
	inline void set_U3ColdAngularDragU3E__0_1(float value)
	{
		___U3ColdAngularDragU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CmainCameraU3E__0_2)); }
	inline Camera_t4157153871 * get_U3CmainCameraU3E__0_2() const { return ___U3CmainCameraU3E__0_2; }
	inline Camera_t4157153871 ** get_address_of_U3CmainCameraU3E__0_2() { return &___U3CmainCameraU3E__0_2; }
	inline void set_U3CmainCameraU3E__0_2(Camera_t4157153871 * value)
	{
		___U3CmainCameraU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrayU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CrayU3E__1_3)); }
	inline Ray_t3785851493  get_U3CrayU3E__1_3() const { return ___U3CrayU3E__1_3; }
	inline Ray_t3785851493 * get_address_of_U3CrayU3E__1_3() { return &___U3CrayU3E__1_3; }
	inline void set_U3CrayU3E__1_3(Ray_t3785851493  value)
	{
		___U3CrayU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24this_5)); }
	inline DragRigidbody_t1600652016 * get_U24this_5() const { return ___U24this_5; }
	inline DragRigidbody_t1600652016 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(DragRigidbody_t1600652016 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifndef ENTRY_T2725803170_H
#define ENTRY_T2725803170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct  Entry_t2725803170  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Utility.TimedObjectActivator/Entry::target
	GameObject_t1113636619 * ___target_0;
	// UnityStandardAssets.Utility.TimedObjectActivator/Action UnityStandardAssets.Utility.TimedObjectActivator/Entry::action
	int32_t ___action_1;
	// System.Single UnityStandardAssets.Utility.TimedObjectActivator/Entry::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___target_0)); }
	inline GameObject_t1113636619 * get_target_0() const { return ___target_0; }
	inline GameObject_t1113636619 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1113636619 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2725803170_H
#ifndef CONTROLSURFACE_T2158797087_H
#define CONTROLSURFACE_T2158797087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface
struct  ControlSurface_t2158797087  : public RuntimeObject
{
public:
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface::transform
	Transform_t3600365921 * ___transform_0;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface::amount
	float ___amount_1;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface/Type UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface::type
	int32_t ___type_2;
	// UnityEngine.Quaternion UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface::originalLocalRotation
	Quaternion_t2301928331  ___originalLocalRotation_3;

public:
	inline static int32_t get_offset_of_transform_0() { return static_cast<int32_t>(offsetof(ControlSurface_t2158797087, ___transform_0)); }
	inline Transform_t3600365921 * get_transform_0() const { return ___transform_0; }
	inline Transform_t3600365921 ** get_address_of_transform_0() { return &___transform_0; }
	inline void set_transform_0(Transform_t3600365921 * value)
	{
		___transform_0 = value;
		Il2CppCodeGenWriteBarrier((&___transform_0), value);
	}

	inline static int32_t get_offset_of_amount_1() { return static_cast<int32_t>(offsetof(ControlSurface_t2158797087, ___amount_1)); }
	inline float get_amount_1() const { return ___amount_1; }
	inline float* get_address_of_amount_1() { return &___amount_1; }
	inline void set_amount_1(float value)
	{
		___amount_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(ControlSurface_t2158797087, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_originalLocalRotation_3() { return static_cast<int32_t>(offsetof(ControlSurface_t2158797087, ___originalLocalRotation_3)); }
	inline Quaternion_t2301928331  get_originalLocalRotation_3() const { return ___originalLocalRotation_3; }
	inline Quaternion_t2301928331 * get_address_of_originalLocalRotation_3() { return &___originalLocalRotation_3; }
	inline void set_originalLocalRotation_3(Quaternion_t2301928331  value)
	{
		___originalLocalRotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSURFACE_T2158797087_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ALPHABUTTONCLICKMASK_T141136539_H
#define ALPHABUTTONCLICKMASK_T141136539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlphaButtonClickMask
struct  AlphaButtonClickMask_t141136539  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image AlphaButtonClickMask::_image
	Image_t2670269651 * ____image_4;

public:
	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(AlphaButtonClickMask_t141136539, ____image_4)); }
	inline Image_t2670269651 * get__image_4() const { return ____image_4; }
	inline Image_t2670269651 ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(Image_t2670269651 * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHABUTTONCLICKMASK_T141136539_H
#ifndef CAMERASWITCH_T735968501_H
#define CAMERASWITCH_T735968501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSwitch
struct  CameraSwitch_t735968501  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CameraSwitch::objects
	GameObjectU5BU5D_t3328599146* ___objects_4;
	// UnityEngine.UI.Text CameraSwitch::text
	Text_t1901882714 * ___text_5;
	// System.Int32 CameraSwitch::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_6;

public:
	inline static int32_t get_offset_of_objects_4() { return static_cast<int32_t>(offsetof(CameraSwitch_t735968501, ___objects_4)); }
	inline GameObjectU5BU5D_t3328599146* get_objects_4() const { return ___objects_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objects_4() { return &___objects_4; }
	inline void set_objects_4(GameObjectU5BU5D_t3328599146* value)
	{
		___objects_4 = value;
		Il2CppCodeGenWriteBarrier((&___objects_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(CameraSwitch_t735968501, ___text_5)); }
	inline Text_t1901882714 * get_text_5() const { return ___text_5; }
	inline Text_t1901882714 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t1901882714 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_6() { return static_cast<int32_t>(offsetof(CameraSwitch_t735968501, ___m_CurrentActiveObject_6)); }
	inline int32_t get_m_CurrentActiveObject_6() const { return ___m_CurrentActiveObject_6; }
	inline int32_t* get_address_of_m_CurrentActiveObject_6() { return &___m_CurrentActiveObject_6; }
	inline void set_m_CurrentActiveObject_6(int32_t value)
	{
		___m_CurrentActiveObject_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASWITCH_T735968501_H
#ifndef EVENTSYSTEMCHECKER_T1882757729_H
#define EVENTSYSTEMCHECKER_T1882757729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventSystemChecker
struct  EventSystemChecker_t1882757729  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMCHECKER_T1882757729_H
#ifndef FORCEDRESET_T301124368_H
#define FORCEDRESET_T301124368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t301124368  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T301124368_H
#ifndef LEVELRESET_T1558959187_H
#define LEVELRESET_T1558959187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelReset
struct  LevelReset_t1558959187  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELRESET_T1558959187_H
#ifndef MENUSCENELOADER_T2183500486_H
#define MENUSCENELOADER_T2183500486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuSceneLoader
struct  MenuSceneLoader_t2183500486  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MenuSceneLoader::menuUI
	GameObject_t1113636619 * ___menuUI_4;
	// UnityEngine.GameObject MenuSceneLoader::m_Go
	GameObject_t1113636619 * ___m_Go_5;

public:
	inline static int32_t get_offset_of_menuUI_4() { return static_cast<int32_t>(offsetof(MenuSceneLoader_t2183500486, ___menuUI_4)); }
	inline GameObject_t1113636619 * get_menuUI_4() const { return ___menuUI_4; }
	inline GameObject_t1113636619 ** get_address_of_menuUI_4() { return &___menuUI_4; }
	inline void set_menuUI_4(GameObject_t1113636619 * value)
	{
		___menuUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuUI_4), value);
	}

	inline static int32_t get_offset_of_m_Go_5() { return static_cast<int32_t>(offsetof(MenuSceneLoader_t2183500486, ___m_Go_5)); }
	inline GameObject_t1113636619 * get_m_Go_5() const { return ___m_Go_5; }
	inline GameObject_t1113636619 ** get_address_of_m_Go_5() { return &___m_Go_5; }
	inline void set_m_Go_5(GameObject_t1113636619 * value)
	{
		___m_Go_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Go_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUSCENELOADER_T2183500486_H
#ifndef PAUSEMENU_T3916167947_H
#define PAUSEMENU_T3916167947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu
struct  PauseMenu_t3916167947  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle PauseMenu::m_MenuToggle
	Toggle_t2735377061 * ___m_MenuToggle_4;
	// System.Single PauseMenu::m_TimeScaleRef
	float ___m_TimeScaleRef_5;
	// System.Single PauseMenu::m_VolumeRef
	float ___m_VolumeRef_6;
	// System.Boolean PauseMenu::m_Paused
	bool ___m_Paused_7;

public:
	inline static int32_t get_offset_of_m_MenuToggle_4() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_MenuToggle_4)); }
	inline Toggle_t2735377061 * get_m_MenuToggle_4() const { return ___m_MenuToggle_4; }
	inline Toggle_t2735377061 ** get_address_of_m_MenuToggle_4() { return &___m_MenuToggle_4; }
	inline void set_m_MenuToggle_4(Toggle_t2735377061 * value)
	{
		___m_MenuToggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MenuToggle_4), value);
	}

	inline static int32_t get_offset_of_m_TimeScaleRef_5() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_TimeScaleRef_5)); }
	inline float get_m_TimeScaleRef_5() const { return ___m_TimeScaleRef_5; }
	inline float* get_address_of_m_TimeScaleRef_5() { return &___m_TimeScaleRef_5; }
	inline void set_m_TimeScaleRef_5(float value)
	{
		___m_TimeScaleRef_5 = value;
	}

	inline static int32_t get_offset_of_m_VolumeRef_6() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_VolumeRef_6)); }
	inline float get_m_VolumeRef_6() const { return ___m_VolumeRef_6; }
	inline float* get_address_of_m_VolumeRef_6() { return &___m_VolumeRef_6; }
	inline void set_m_VolumeRef_6(float value)
	{
		___m_VolumeRef_6 = value;
	}

	inline static int32_t get_offset_of_m_Paused_7() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_Paused_7)); }
	inline bool get_m_Paused_7() const { return ___m_Paused_7; }
	inline bool* get_address_of_m_Paused_7() { return &___m_Paused_7; }
	inline void set_m_Paused_7(bool value)
	{
		___m_Paused_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_T3916167947_H
#ifndef SCENEANDURLLOADER_T3512401881_H
#define SCENEANDURLLOADER_T3512401881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneAndURLLoader
struct  SceneAndURLLoader_t3512401881  : public MonoBehaviour_t3962482529
{
public:
	// PauseMenu SceneAndURLLoader::m_PauseMenu
	PauseMenu_t3916167947 * ___m_PauseMenu_4;

public:
	inline static int32_t get_offset_of_m_PauseMenu_4() { return static_cast<int32_t>(offsetof(SceneAndURLLoader_t3512401881, ___m_PauseMenu_4)); }
	inline PauseMenu_t3916167947 * get_m_PauseMenu_4() const { return ___m_PauseMenu_4; }
	inline PauseMenu_t3916167947 ** get_address_of_m_PauseMenu_4() { return &___m_PauseMenu_4; }
	inline void set_m_PauseMenu_4(PauseMenu_t3916167947 * value)
	{
		___m_PauseMenu_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PauseMenu_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEANDURLLOADER_T3512401881_H
#ifndef WATERHOSEPARTICLES_T1340502520_H
#define WATERHOSEPARTICLES_T1340502520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.WaterHoseParticles
struct  WaterHoseParticles_t1340502520  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::force
	float ___force_5;
	// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent> UnityStandardAssets.Effects.WaterHoseParticles::m_CollisionEvents
	List_1_t1232140387 * ___m_CollisionEvents_6;
	// UnityEngine.ParticleSystem UnityStandardAssets.Effects.WaterHoseParticles::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_7;

public:
	inline static int32_t get_offset_of_force_5() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520, ___force_5)); }
	inline float get_force_5() const { return ___force_5; }
	inline float* get_address_of_force_5() { return &___force_5; }
	inline void set_force_5(float value)
	{
		___force_5 = value;
	}

	inline static int32_t get_offset_of_m_CollisionEvents_6() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520, ___m_CollisionEvents_6)); }
	inline List_1_t1232140387 * get_m_CollisionEvents_6() const { return ___m_CollisionEvents_6; }
	inline List_1_t1232140387 ** get_address_of_m_CollisionEvents_6() { return &___m_CollisionEvents_6; }
	inline void set_m_CollisionEvents_6(List_1_t1232140387 * value)
	{
		___m_CollisionEvents_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollisionEvents_6), value);
	}

	inline static int32_t get_offset_of_m_ParticleSystem_7() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520, ___m_ParticleSystem_7)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_7() const { return ___m_ParticleSystem_7; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_7() { return &___m_ParticleSystem_7; }
	inline void set_m_ParticleSystem_7(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_7), value);
	}
};

struct WaterHoseParticles_t1340502520_StaticFields
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::lastSoundTime
	float ___lastSoundTime_4;

public:
	inline static int32_t get_offset_of_lastSoundTime_4() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520_StaticFields, ___lastSoundTime_4)); }
	inline float get_lastSoundTime_4() const { return ___lastSoundTime_4; }
	inline float* get_address_of_lastSoundTime_4() { return &___lastSoundTime_4; }
	inline void set_lastSoundTime_4(float value)
	{
		___lastSoundTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERHOSEPARTICLES_T1340502520_H
#ifndef PARTICLESCENECONTROLS_T306931203_H
#define PARTICLESCENECONTROLS_T306931203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls
struct  ParticleSceneControls_t306931203  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList UnityStandardAssets.SceneUtils.ParticleSceneControls::demoParticles
	DemoParticleSystemList_t4288938153 * ___demoParticles_4;
	// System.Single UnityStandardAssets.SceneUtils.ParticleSceneControls::spawnOffset
	float ___spawnOffset_5;
	// System.Single UnityStandardAssets.SceneUtils.ParticleSceneControls::multiply
	float ___multiply_6;
	// System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::clearOnChange
	bool ___clearOnChange_7;
	// UnityEngine.UI.Text UnityStandardAssets.SceneUtils.ParticleSceneControls::titleText
	Text_t1901882714 * ___titleText_8;
	// UnityEngine.Transform UnityStandardAssets.SceneUtils.ParticleSceneControls::sceneCamera
	Transform_t3600365921 * ___sceneCamera_9;
	// UnityEngine.UI.Text UnityStandardAssets.SceneUtils.ParticleSceneControls::instructionText
	Text_t1901882714 * ___instructionText_10;
	// UnityEngine.UI.Button UnityStandardAssets.SceneUtils.ParticleSceneControls::previousButton
	Button_t4055032469 * ___previousButton_11;
	// UnityEngine.UI.Button UnityStandardAssets.SceneUtils.ParticleSceneControls::nextButton
	Button_t4055032469 * ___nextButton_12;
	// UnityEngine.UI.GraphicRaycaster UnityStandardAssets.SceneUtils.ParticleSceneControls::graphicRaycaster
	GraphicRaycaster_t2999697109 * ___graphicRaycaster_13;
	// UnityEngine.EventSystems.EventSystem UnityStandardAssets.SceneUtils.ParticleSceneControls::eventSystem
	EventSystem_t1003666588 * ___eventSystem_14;
	// UnityStandardAssets.Effects.ParticleSystemMultiplier UnityStandardAssets.SceneUtils.ParticleSceneControls::m_ParticleMultiplier
	ParticleSystemMultiplier_t2770350653 * ___m_ParticleMultiplier_15;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.SceneUtils.ParticleSceneControls::m_CurrentParticleList
	List_1_t777473367 * ___m_CurrentParticleList_16;
	// UnityEngine.Transform UnityStandardAssets.SceneUtils.ParticleSceneControls::m_Instance
	Transform_t3600365921 * ___m_Instance_17;
	// UnityEngine.Vector3 UnityStandardAssets.SceneUtils.ParticleSceneControls::m_CamOffsetVelocity
	Vector3_t3722313464  ___m_CamOffsetVelocity_19;
	// UnityEngine.Vector3 UnityStandardAssets.SceneUtils.ParticleSceneControls::m_LastPos
	Vector3_t3722313464  ___m_LastPos_20;

public:
	inline static int32_t get_offset_of_demoParticles_4() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___demoParticles_4)); }
	inline DemoParticleSystemList_t4288938153 * get_demoParticles_4() const { return ___demoParticles_4; }
	inline DemoParticleSystemList_t4288938153 ** get_address_of_demoParticles_4() { return &___demoParticles_4; }
	inline void set_demoParticles_4(DemoParticleSystemList_t4288938153 * value)
	{
		___demoParticles_4 = value;
		Il2CppCodeGenWriteBarrier((&___demoParticles_4), value);
	}

	inline static int32_t get_offset_of_spawnOffset_5() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___spawnOffset_5)); }
	inline float get_spawnOffset_5() const { return ___spawnOffset_5; }
	inline float* get_address_of_spawnOffset_5() { return &___spawnOffset_5; }
	inline void set_spawnOffset_5(float value)
	{
		___spawnOffset_5 = value;
	}

	inline static int32_t get_offset_of_multiply_6() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___multiply_6)); }
	inline float get_multiply_6() const { return ___multiply_6; }
	inline float* get_address_of_multiply_6() { return &___multiply_6; }
	inline void set_multiply_6(float value)
	{
		___multiply_6 = value;
	}

	inline static int32_t get_offset_of_clearOnChange_7() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___clearOnChange_7)); }
	inline bool get_clearOnChange_7() const { return ___clearOnChange_7; }
	inline bool* get_address_of_clearOnChange_7() { return &___clearOnChange_7; }
	inline void set_clearOnChange_7(bool value)
	{
		___clearOnChange_7 = value;
	}

	inline static int32_t get_offset_of_titleText_8() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___titleText_8)); }
	inline Text_t1901882714 * get_titleText_8() const { return ___titleText_8; }
	inline Text_t1901882714 ** get_address_of_titleText_8() { return &___titleText_8; }
	inline void set_titleText_8(Text_t1901882714 * value)
	{
		___titleText_8 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_8), value);
	}

	inline static int32_t get_offset_of_sceneCamera_9() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___sceneCamera_9)); }
	inline Transform_t3600365921 * get_sceneCamera_9() const { return ___sceneCamera_9; }
	inline Transform_t3600365921 ** get_address_of_sceneCamera_9() { return &___sceneCamera_9; }
	inline void set_sceneCamera_9(Transform_t3600365921 * value)
	{
		___sceneCamera_9 = value;
		Il2CppCodeGenWriteBarrier((&___sceneCamera_9), value);
	}

	inline static int32_t get_offset_of_instructionText_10() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___instructionText_10)); }
	inline Text_t1901882714 * get_instructionText_10() const { return ___instructionText_10; }
	inline Text_t1901882714 ** get_address_of_instructionText_10() { return &___instructionText_10; }
	inline void set_instructionText_10(Text_t1901882714 * value)
	{
		___instructionText_10 = value;
		Il2CppCodeGenWriteBarrier((&___instructionText_10), value);
	}

	inline static int32_t get_offset_of_previousButton_11() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___previousButton_11)); }
	inline Button_t4055032469 * get_previousButton_11() const { return ___previousButton_11; }
	inline Button_t4055032469 ** get_address_of_previousButton_11() { return &___previousButton_11; }
	inline void set_previousButton_11(Button_t4055032469 * value)
	{
		___previousButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___previousButton_11), value);
	}

	inline static int32_t get_offset_of_nextButton_12() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___nextButton_12)); }
	inline Button_t4055032469 * get_nextButton_12() const { return ___nextButton_12; }
	inline Button_t4055032469 ** get_address_of_nextButton_12() { return &___nextButton_12; }
	inline void set_nextButton_12(Button_t4055032469 * value)
	{
		___nextButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_12), value);
	}

	inline static int32_t get_offset_of_graphicRaycaster_13() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___graphicRaycaster_13)); }
	inline GraphicRaycaster_t2999697109 * get_graphicRaycaster_13() const { return ___graphicRaycaster_13; }
	inline GraphicRaycaster_t2999697109 ** get_address_of_graphicRaycaster_13() { return &___graphicRaycaster_13; }
	inline void set_graphicRaycaster_13(GraphicRaycaster_t2999697109 * value)
	{
		___graphicRaycaster_13 = value;
		Il2CppCodeGenWriteBarrier((&___graphicRaycaster_13), value);
	}

	inline static int32_t get_offset_of_eventSystem_14() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___eventSystem_14)); }
	inline EventSystem_t1003666588 * get_eventSystem_14() const { return ___eventSystem_14; }
	inline EventSystem_t1003666588 ** get_address_of_eventSystem_14() { return &___eventSystem_14; }
	inline void set_eventSystem_14(EventSystem_t1003666588 * value)
	{
		___eventSystem_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_14), value);
	}

	inline static int32_t get_offset_of_m_ParticleMultiplier_15() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_ParticleMultiplier_15)); }
	inline ParticleSystemMultiplier_t2770350653 * get_m_ParticleMultiplier_15() const { return ___m_ParticleMultiplier_15; }
	inline ParticleSystemMultiplier_t2770350653 ** get_address_of_m_ParticleMultiplier_15() { return &___m_ParticleMultiplier_15; }
	inline void set_m_ParticleMultiplier_15(ParticleSystemMultiplier_t2770350653 * value)
	{
		___m_ParticleMultiplier_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleMultiplier_15), value);
	}

	inline static int32_t get_offset_of_m_CurrentParticleList_16() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_CurrentParticleList_16)); }
	inline List_1_t777473367 * get_m_CurrentParticleList_16() const { return ___m_CurrentParticleList_16; }
	inline List_1_t777473367 ** get_address_of_m_CurrentParticleList_16() { return &___m_CurrentParticleList_16; }
	inline void set_m_CurrentParticleList_16(List_1_t777473367 * value)
	{
		___m_CurrentParticleList_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentParticleList_16), value);
	}

	inline static int32_t get_offset_of_m_Instance_17() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_Instance_17)); }
	inline Transform_t3600365921 * get_m_Instance_17() const { return ___m_Instance_17; }
	inline Transform_t3600365921 ** get_address_of_m_Instance_17() { return &___m_Instance_17; }
	inline void set_m_Instance_17(Transform_t3600365921 * value)
	{
		___m_Instance_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_17), value);
	}

	inline static int32_t get_offset_of_m_CamOffsetVelocity_19() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_CamOffsetVelocity_19)); }
	inline Vector3_t3722313464  get_m_CamOffsetVelocity_19() const { return ___m_CamOffsetVelocity_19; }
	inline Vector3_t3722313464 * get_address_of_m_CamOffsetVelocity_19() { return &___m_CamOffsetVelocity_19; }
	inline void set_m_CamOffsetVelocity_19(Vector3_t3722313464  value)
	{
		___m_CamOffsetVelocity_19 = value;
	}

	inline static int32_t get_offset_of_m_LastPos_20() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_LastPos_20)); }
	inline Vector3_t3722313464  get_m_LastPos_20() const { return ___m_LastPos_20; }
	inline Vector3_t3722313464 * get_address_of_m_LastPos_20() { return &___m_LastPos_20; }
	inline void set_m_LastPos_20(Vector3_t3722313464  value)
	{
		___m_LastPos_20 = value;
	}
};

struct ParticleSceneControls_t306931203_StaticFields
{
public:
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls::s_SelectedIndex
	int32_t ___s_SelectedIndex_18;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem UnityStandardAssets.SceneUtils.ParticleSceneControls::s_Selected
	DemoParticleSystem_t1195446716 * ___s_Selected_21;

public:
	inline static int32_t get_offset_of_s_SelectedIndex_18() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203_StaticFields, ___s_SelectedIndex_18)); }
	inline int32_t get_s_SelectedIndex_18() const { return ___s_SelectedIndex_18; }
	inline int32_t* get_address_of_s_SelectedIndex_18() { return &___s_SelectedIndex_18; }
	inline void set_s_SelectedIndex_18(int32_t value)
	{
		___s_SelectedIndex_18 = value;
	}

	inline static int32_t get_offset_of_s_Selected_21() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203_StaticFields, ___s_Selected_21)); }
	inline DemoParticleSystem_t1195446716 * get_s_Selected_21() const { return ___s_Selected_21; }
	inline DemoParticleSystem_t1195446716 ** get_address_of_s_Selected_21() { return &___s_Selected_21; }
	inline void set_s_Selected_21(DemoParticleSystem_t1195446716 * value)
	{
		___s_Selected_21 = value;
		Il2CppCodeGenWriteBarrier((&___s_Selected_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESCENECONTROLS_T306931203_H
#ifndef PLACETARGETWITHMOUSE_T3823755269_H
#define PLACETARGETWITHMOUSE_T3823755269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
struct  PlaceTargetWithMouse_t3823755269  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::surfaceOffset
	float ___surfaceOffset_4;
	// UnityEngine.GameObject UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::setTargetOn
	GameObject_t1113636619 * ___setTargetOn_5;

public:
	inline static int32_t get_offset_of_surfaceOffset_4() { return static_cast<int32_t>(offsetof(PlaceTargetWithMouse_t3823755269, ___surfaceOffset_4)); }
	inline float get_surfaceOffset_4() const { return ___surfaceOffset_4; }
	inline float* get_address_of_surfaceOffset_4() { return &___surfaceOffset_4; }
	inline void set_surfaceOffset_4(float value)
	{
		___surfaceOffset_4 = value;
	}

	inline static int32_t get_offset_of_setTargetOn_5() { return static_cast<int32_t>(offsetof(PlaceTargetWithMouse_t3823755269, ___setTargetOn_5)); }
	inline GameObject_t1113636619 * get_setTargetOn_5() const { return ___setTargetOn_5; }
	inline GameObject_t1113636619 ** get_address_of_setTargetOn_5() { return &___setTargetOn_5; }
	inline void set_setTargetOn_5(GameObject_t1113636619 * value)
	{
		___setTargetOn_5 = value;
		Il2CppCodeGenWriteBarrier((&___setTargetOn_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACETARGETWITHMOUSE_T3823755269_H
#ifndef SLOWMOBUTTON_T57138989_H
#define SLOWMOBUTTON_T57138989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.SlowMoButton
struct  SlowMoButton_t57138989  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite UnityStandardAssets.SceneUtils.SlowMoButton::FullSpeedTex
	Sprite_t280657092 * ___FullSpeedTex_4;
	// UnityEngine.Sprite UnityStandardAssets.SceneUtils.SlowMoButton::SlowSpeedTex
	Sprite_t280657092 * ___SlowSpeedTex_5;
	// System.Single UnityStandardAssets.SceneUtils.SlowMoButton::fullSpeed
	float ___fullSpeed_6;
	// System.Single UnityStandardAssets.SceneUtils.SlowMoButton::slowSpeed
	float ___slowSpeed_7;
	// UnityEngine.UI.Button UnityStandardAssets.SceneUtils.SlowMoButton::button
	Button_t4055032469 * ___button_8;
	// System.Boolean UnityStandardAssets.SceneUtils.SlowMoButton::m_SlowMo
	bool ___m_SlowMo_9;

public:
	inline static int32_t get_offset_of_FullSpeedTex_4() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___FullSpeedTex_4)); }
	inline Sprite_t280657092 * get_FullSpeedTex_4() const { return ___FullSpeedTex_4; }
	inline Sprite_t280657092 ** get_address_of_FullSpeedTex_4() { return &___FullSpeedTex_4; }
	inline void set_FullSpeedTex_4(Sprite_t280657092 * value)
	{
		___FullSpeedTex_4 = value;
		Il2CppCodeGenWriteBarrier((&___FullSpeedTex_4), value);
	}

	inline static int32_t get_offset_of_SlowSpeedTex_5() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___SlowSpeedTex_5)); }
	inline Sprite_t280657092 * get_SlowSpeedTex_5() const { return ___SlowSpeedTex_5; }
	inline Sprite_t280657092 ** get_address_of_SlowSpeedTex_5() { return &___SlowSpeedTex_5; }
	inline void set_SlowSpeedTex_5(Sprite_t280657092 * value)
	{
		___SlowSpeedTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___SlowSpeedTex_5), value);
	}

	inline static int32_t get_offset_of_fullSpeed_6() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___fullSpeed_6)); }
	inline float get_fullSpeed_6() const { return ___fullSpeed_6; }
	inline float* get_address_of_fullSpeed_6() { return &___fullSpeed_6; }
	inline void set_fullSpeed_6(float value)
	{
		___fullSpeed_6 = value;
	}

	inline static int32_t get_offset_of_slowSpeed_7() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___slowSpeed_7)); }
	inline float get_slowSpeed_7() const { return ___slowSpeed_7; }
	inline float* get_address_of_slowSpeed_7() { return &___slowSpeed_7; }
	inline void set_slowSpeed_7(float value)
	{
		___slowSpeed_7 = value;
	}

	inline static int32_t get_offset_of_button_8() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___button_8)); }
	inline Button_t4055032469 * get_button_8() const { return ___button_8; }
	inline Button_t4055032469 ** get_address_of_button_8() { return &___button_8; }
	inline void set_button_8(Button_t4055032469 * value)
	{
		___button_8 = value;
		Il2CppCodeGenWriteBarrier((&___button_8), value);
	}

	inline static int32_t get_offset_of_m_SlowMo_9() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___m_SlowMo_9)); }
	inline bool get_m_SlowMo_9() const { return ___m_SlowMo_9; }
	inline bool* get_address_of_m_SlowMo_9() { return &___m_SlowMo_9; }
	inline void set_m_SlowMo_9(bool value)
	{
		___m_SlowMo_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOWMOBUTTON_T57138989_H
#ifndef ACTIVATETRIGGER_T3349759092_H
#define ACTIVATETRIGGER_T3349759092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_t3349759092  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_4;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_t631007953 * ___target_5;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_t1113636619 * ___source_6;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_7;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_8;

public:
	inline static int32_t get_offset_of_action_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___action_4)); }
	inline int32_t get_action_4() const { return ___action_4; }
	inline int32_t* get_address_of_action_4() { return &___action_4; }
	inline void set_action_4(int32_t value)
	{
		___action_4 = value;
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___target_5)); }
	inline Object_t631007953 * get_target_5() const { return ___target_5; }
	inline Object_t631007953 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Object_t631007953 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___source_6)); }
	inline GameObject_t1113636619 * get_source_6() const { return ___source_6; }
	inline GameObject_t1113636619 ** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(GameObject_t1113636619 * value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier((&___source_6), value);
	}

	inline static int32_t get_offset_of_triggerCount_7() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___triggerCount_7)); }
	inline int32_t get_triggerCount_7() const { return ___triggerCount_7; }
	inline int32_t* get_address_of_triggerCount_7() { return &___triggerCount_7; }
	inline void set_triggerCount_7(int32_t value)
	{
		___triggerCount_7 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_8() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___repeatTrigger_8)); }
	inline bool get_repeatTrigger_8() const { return ___repeatTrigger_8; }
	inline bool* get_address_of_repeatTrigger_8() { return &___repeatTrigger_8; }
	inline void set_repeatTrigger_8(bool value)
	{
		___repeatTrigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_T3349759092_H
#ifndef AUTOMOBILESHADERSWITCH_T568447889_H
#define AUTOMOBILESHADERSWITCH_T568447889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_t568447889  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_t1887104210 * ___m_ReplacementList_4;

public:
	inline static int32_t get_offset_of_m_ReplacementList_4() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_t568447889, ___m_ReplacementList_4)); }
	inline ReplacementList_t1887104210 * get_m_ReplacementList_4() const { return ___m_ReplacementList_4; }
	inline ReplacementList_t1887104210 ** get_address_of_m_ReplacementList_4() { return &___m_ReplacementList_4; }
	inline void set_m_ReplacementList_4(ReplacementList_t1887104210 * value)
	{
		___m_ReplacementList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_T568447889_H
#ifndef AUTOMOVEANDROTATE_T2437913015_H
#define AUTOMOVEANDROTATE_T2437913015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t2437913015  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t219844479 * ___moveUnitsPerSecond_4;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t219844479 * ___rotateDegreesPerSecond_5;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_6;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_7;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___moveUnitsPerSecond_4)); }
	inline Vector3andSpace_t219844479 * get_moveUnitsPerSecond_4() const { return ___moveUnitsPerSecond_4; }
	inline Vector3andSpace_t219844479 ** get_address_of_moveUnitsPerSecond_4() { return &___moveUnitsPerSecond_4; }
	inline void set_moveUnitsPerSecond_4(Vector3andSpace_t219844479 * value)
	{
		___moveUnitsPerSecond_4 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_4), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___rotateDegreesPerSecond_5)); }
	inline Vector3andSpace_t219844479 * get_rotateDegreesPerSecond_5() const { return ___rotateDegreesPerSecond_5; }
	inline Vector3andSpace_t219844479 ** get_address_of_rotateDegreesPerSecond_5() { return &___rotateDegreesPerSecond_5; }
	inline void set_rotateDegreesPerSecond_5(Vector3andSpace_t219844479 * value)
	{
		___rotateDegreesPerSecond_5 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_5), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_6() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___ignoreTimescale_6)); }
	inline bool get_ignoreTimescale_6() const { return ___ignoreTimescale_6; }
	inline bool* get_address_of_ignoreTimescale_6() { return &___ignoreTimescale_6; }
	inline void set_ignoreTimescale_6(bool value)
	{
		___ignoreTimescale_6 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_7() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___m_LastRealTime_7)); }
	inline float get_m_LastRealTime_7() const { return ___m_LastRealTime_7; }
	inline float* get_address_of_m_LastRealTime_7() { return &___m_LastRealTime_7; }
	inline void set_m_LastRealTime_7(float value)
	{
		___m_LastRealTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T2437913015_H
#ifndef DRAGRIGIDBODY_T1600652016_H
#define DRAGRIGIDBODY_T1600652016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t1600652016  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t1912369980 * ___m_SpringJoint_10;

public:
	inline static int32_t get_offset_of_m_SpringJoint_10() { return static_cast<int32_t>(offsetof(DragRigidbody_t1600652016, ___m_SpringJoint_10)); }
	inline SpringJoint_t1912369980 * get_m_SpringJoint_10() const { return ___m_SpringJoint_10; }
	inline SpringJoint_t1912369980 ** get_address_of_m_SpringJoint_10() { return &___m_SpringJoint_10; }
	inline void set_m_SpringJoint_10(SpringJoint_t1912369980 * value)
	{
		___m_SpringJoint_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_T1600652016_H
#ifndef DYNAMICSHADOWSETTINGS_T59119858_H
#define DYNAMICSHADOWSETTINGS_T59119858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t59119858  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_t3756812086 * ___sunLight_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_12;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_13;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_14;

public:
	inline static int32_t get_offset_of_sunLight_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___sunLight_4)); }
	inline Light_t3756812086 * get_sunLight_4() const { return ___sunLight_4; }
	inline Light_t3756812086 ** get_address_of_sunLight_4() { return &___sunLight_4; }
	inline void set_sunLight_4(Light_t3756812086 * value)
	{
		___sunLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_4), value);
	}

	inline static int32_t get_offset_of_minHeight_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minHeight_5)); }
	inline float get_minHeight_5() const { return ___minHeight_5; }
	inline float* get_address_of_minHeight_5() { return &___minHeight_5; }
	inline void set_minHeight_5(float value)
	{
		___minHeight_5 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowDistance_6)); }
	inline float get_minShadowDistance_6() const { return ___minShadowDistance_6; }
	inline float* get_address_of_minShadowDistance_6() { return &___minShadowDistance_6; }
	inline void set_minShadowDistance_6(float value)
	{
		___minShadowDistance_6 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowBias_7)); }
	inline float get_minShadowBias_7() const { return ___minShadowBias_7; }
	inline float* get_address_of_minShadowBias_7() { return &___minShadowBias_7; }
	inline void set_minShadowBias_7(float value)
	{
		___minShadowBias_7 = value;
	}

	inline static int32_t get_offset_of_maxHeight_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxHeight_8)); }
	inline float get_maxHeight_8() const { return ___maxHeight_8; }
	inline float* get_address_of_maxHeight_8() { return &___maxHeight_8; }
	inline void set_maxHeight_8(float value)
	{
		___maxHeight_8 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowDistance_9)); }
	inline float get_maxShadowDistance_9() const { return ___maxShadowDistance_9; }
	inline float* get_address_of_maxShadowDistance_9() { return &___maxShadowDistance_9; }
	inline void set_maxShadowDistance_9(float value)
	{
		___maxShadowDistance_9 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowBias_10)); }
	inline float get_maxShadowBias_10() const { return ___maxShadowBias_10; }
	inline float* get_address_of_maxShadowBias_10() { return &___maxShadowBias_10; }
	inline void set_maxShadowBias_10(float value)
	{
		___maxShadowBias_10 = value;
	}

	inline static int32_t get_offset_of_adaptTime_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___adaptTime_11)); }
	inline float get_adaptTime_11() const { return ___adaptTime_11; }
	inline float* get_address_of_adaptTime_11() { return &___adaptTime_11; }
	inline void set_adaptTime_11(float value)
	{
		___adaptTime_11 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_SmoothHeight_12)); }
	inline float get_m_SmoothHeight_12() const { return ___m_SmoothHeight_12; }
	inline float* get_address_of_m_SmoothHeight_12() { return &___m_SmoothHeight_12; }
	inline void set_m_SmoothHeight_12(float value)
	{
		___m_SmoothHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_13() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_ChangeSpeed_13)); }
	inline float get_m_ChangeSpeed_13() const { return ___m_ChangeSpeed_13; }
	inline float* get_address_of_m_ChangeSpeed_13() { return &___m_ChangeSpeed_13; }
	inline void set_m_ChangeSpeed_13(float value)
	{
		___m_ChangeSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_14() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_OriginalStrength_14)); }
	inline float get_m_OriginalStrength_14() const { return ___m_OriginalStrength_14; }
	inline float* get_address_of_m_OriginalStrength_14() { return &___m_OriginalStrength_14; }
	inline void set_m_OriginalStrength_14(float value)
	{
		___m_OriginalStrength_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T59119858_H
#ifndef FPSCOUNTER_T2351221284_H
#define FPSCOUNTER_T2351221284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t2351221284  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_5;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_6;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_7;
	// UnityEngine.UI.Text UnityStandardAssets.Utility.FPSCounter::m_Text
	Text_t1901882714 * ___m_Text_9;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_5() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsAccumulator_5)); }
	inline int32_t get_m_FpsAccumulator_5() const { return ___m_FpsAccumulator_5; }
	inline int32_t* get_address_of_m_FpsAccumulator_5() { return &___m_FpsAccumulator_5; }
	inline void set_m_FpsAccumulator_5(int32_t value)
	{
		___m_FpsAccumulator_5 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_6() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsNextPeriod_6)); }
	inline float get_m_FpsNextPeriod_6() const { return ___m_FpsNextPeriod_6; }
	inline float* get_address_of_m_FpsNextPeriod_6() { return &___m_FpsNextPeriod_6; }
	inline void set_m_FpsNextPeriod_6(float value)
	{
		___m_FpsNextPeriod_6 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_7() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_CurrentFps_7)); }
	inline int32_t get_m_CurrentFps_7() const { return ___m_CurrentFps_7; }
	inline int32_t* get_address_of_m_CurrentFps_7() { return &___m_CurrentFps_7; }
	inline void set_m_CurrentFps_7(int32_t value)
	{
		___m_CurrentFps_7 = value;
	}

	inline static int32_t get_offset_of_m_Text_9() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_Text_9)); }
	inline Text_t1901882714 * get_m_Text_9() const { return ___m_Text_9; }
	inline Text_t1901882714 ** get_address_of_m_Text_9() { return &___m_Text_9; }
	inline void set_m_Text_9(Text_t1901882714 * value)
	{
		___m_Text_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2351221284_H
#ifndef FOLLOWTARGET_T166153614_H
#define FOLLOWTARGET_T166153614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t166153614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t3600365921 * ___target_4;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t3722313464  ___offset_5;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___offset_5)); }
	inline Vector3_t3722313464  get_offset_5() const { return ___offset_5; }
	inline Vector3_t3722313464 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_t3722313464  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T166153614_H
#ifndef OBJECTRESETTER_T639177103_H
#define OBJECTRESETTER_T639177103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_t639177103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_t3722313464  ___originalPosition_4;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t2301928331  ___originalRotation_5;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t777473367 * ___originalStructure_6;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_t3916780224 * ___Rigidbody_7;

public:
	inline static int32_t get_offset_of_originalPosition_4() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalPosition_4)); }
	inline Vector3_t3722313464  get_originalPosition_4() const { return ___originalPosition_4; }
	inline Vector3_t3722313464 * get_address_of_originalPosition_4() { return &___originalPosition_4; }
	inline void set_originalPosition_4(Vector3_t3722313464  value)
	{
		___originalPosition_4 = value;
	}

	inline static int32_t get_offset_of_originalRotation_5() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalRotation_5)); }
	inline Quaternion_t2301928331  get_originalRotation_5() const { return ___originalRotation_5; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_5() { return &___originalRotation_5; }
	inline void set_originalRotation_5(Quaternion_t2301928331  value)
	{
		___originalRotation_5 = value;
	}

	inline static int32_t get_offset_of_originalStructure_6() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalStructure_6)); }
	inline List_1_t777473367 * get_originalStructure_6() const { return ___originalStructure_6; }
	inline List_1_t777473367 ** get_address_of_originalStructure_6() { return &___originalStructure_6; }
	inline void set_originalStructure_6(List_1_t777473367 * value)
	{
		___originalStructure_6 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_6), value);
	}

	inline static int32_t get_offset_of_Rigidbody_7() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___Rigidbody_7)); }
	inline Rigidbody_t3916780224 * get_Rigidbody_7() const { return ___Rigidbody_7; }
	inline Rigidbody_t3916780224 ** get_address_of_Rigidbody_7() { return &___Rigidbody_7; }
	inline void set_Rigidbody_7(Rigidbody_t3916780224 * value)
	{
		___Rigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_T639177103_H
#ifndef PARTICLESYSTEMDESTROYER_T558680695_H
#define PARTICLESYSTEMDESTROYER_T558680695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t558680695  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_4;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_5;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_6;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_7;

public:
	inline static int32_t get_offset_of_minDuration_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___minDuration_4)); }
	inline float get_minDuration_4() const { return ___minDuration_4; }
	inline float* get_address_of_minDuration_4() { return &___minDuration_4; }
	inline void set_minDuration_4(float value)
	{
		___minDuration_4 = value;
	}

	inline static int32_t get_offset_of_maxDuration_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___maxDuration_5)); }
	inline float get_maxDuration_5() const { return ___maxDuration_5; }
	inline float* get_address_of_maxDuration_5() { return &___maxDuration_5; }
	inline void set_maxDuration_5(float value)
	{
		___maxDuration_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_6() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_MaxLifetime_6)); }
	inline float get_m_MaxLifetime_6() const { return ___m_MaxLifetime_6; }
	inline float* get_address_of_m_MaxLifetime_6() { return &___m_MaxLifetime_6; }
	inline void set_m_MaxLifetime_6(float value)
	{
		___m_MaxLifetime_6 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_7() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_EarlyStop_7)); }
	inline bool get_m_EarlyStop_7() const { return ___m_EarlyStop_7; }
	inline bool* get_address_of_m_EarlyStop_7() { return &___m_EarlyStop_7; }
	inline void set_m_EarlyStop_7(bool value)
	{
		___m_EarlyStop_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T558680695_H
#ifndef PLATFORMSPECIFICCONTENT_T1404549723_H
#define PLATFORMSPECIFICCONTENT_T1404549723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t1404549723  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_4;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_t3328599146* ___m_Content_5;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___m_MonoBehaviours_6;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_7;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_BuildTargetGroup_4)); }
	inline int32_t get_m_BuildTargetGroup_4() const { return ___m_BuildTargetGroup_4; }
	inline int32_t* get_address_of_m_BuildTargetGroup_4() { return &___m_BuildTargetGroup_4; }
	inline void set_m_BuildTargetGroup_4(int32_t value)
	{
		___m_BuildTargetGroup_4 = value;
	}

	inline static int32_t get_offset_of_m_Content_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_Content_5)); }
	inline GameObjectU5BU5D_t3328599146* get_m_Content_5() const { return ___m_Content_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_Content_5() { return &___m_Content_5; }
	inline void set_m_Content_5(GameObjectU5BU5D_t3328599146* value)
	{
		___m_Content_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_5), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_6() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_MonoBehaviours_6)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_m_MonoBehaviours_6() const { return ___m_MonoBehaviours_6; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_m_MonoBehaviours_6() { return &___m_MonoBehaviours_6; }
	inline void set_m_MonoBehaviours_6(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___m_MonoBehaviours_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_6), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_7() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_ChildrenOfThisObject_7)); }
	inline bool get_m_ChildrenOfThisObject_7() const { return ___m_ChildrenOfThisObject_7; }
	inline bool* get_address_of_m_ChildrenOfThisObject_7() { return &___m_ChildrenOfThisObject_7; }
	inline void set_m_ChildrenOfThisObject_7(bool value)
	{
		___m_ChildrenOfThisObject_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T1404549723_H
#ifndef SIMPLEACTIVATORMENU_T1387811551_H
#define SIMPLEACTIVATORMENU_T1387811551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t1387811551  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t402233326 * ___camSwitchButton_4;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_t3328599146* ___objects_5;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_6;

public:
	inline static int32_t get_offset_of_camSwitchButton_4() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___camSwitchButton_4)); }
	inline GUIText_t402233326 * get_camSwitchButton_4() const { return ___camSwitchButton_4; }
	inline GUIText_t402233326 ** get_address_of_camSwitchButton_4() { return &___camSwitchButton_4; }
	inline void set_camSwitchButton_4(GUIText_t402233326 * value)
	{
		___camSwitchButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___camSwitchButton_4), value);
	}

	inline static int32_t get_offset_of_objects_5() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___objects_5)); }
	inline GameObjectU5BU5D_t3328599146* get_objects_5() const { return ___objects_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objects_5() { return &___objects_5; }
	inline void set_objects_5(GameObjectU5BU5D_t3328599146* value)
	{
		___objects_5 = value;
		Il2CppCodeGenWriteBarrier((&___objects_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_6() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___m_CurrentActiveObject_6)); }
	inline int32_t get_m_CurrentActiveObject_6() const { return ___m_CurrentActiveObject_6; }
	inline int32_t* get_address_of_m_CurrentActiveObject_6() { return &___m_CurrentActiveObject_6; }
	inline void set_m_CurrentActiveObject_6(int32_t value)
	{
		___m_CurrentActiveObject_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIVATORMENU_T1387811551_H
#ifndef SIMPLEMOUSEROTATOR_T2364742953_H
#define SIMPLEMOUSEROTATOR_T2364742953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleMouseRotator
struct  SimpleMouseRotator_t2364742953  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Utility.SimpleMouseRotator::rotationRange
	Vector2_t2156229523  ___rotationRange_4;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::dampingTime
	float ___dampingTime_6;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroVerticalOnMobile
	bool ___autoZeroVerticalOnMobile_7;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroHorizontalOnMobile
	bool ___autoZeroHorizontalOnMobile_8;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::relative
	bool ___relative_9;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_TargetAngles
	Vector3_t3722313464  ___m_TargetAngles_10;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowAngles
	Vector3_t3722313464  ___m_FollowAngles_11;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowVelocity
	Vector3_t3722313464  ___m_FollowVelocity_12;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.SimpleMouseRotator::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_13;

public:
	inline static int32_t get_offset_of_rotationRange_4() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationRange_4)); }
	inline Vector2_t2156229523  get_rotationRange_4() const { return ___rotationRange_4; }
	inline Vector2_t2156229523 * get_address_of_rotationRange_4() { return &___rotationRange_4; }
	inline void set_rotationRange_4(Vector2_t2156229523  value)
	{
		___rotationRange_4 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_dampingTime_6() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___dampingTime_6)); }
	inline float get_dampingTime_6() const { return ___dampingTime_6; }
	inline float* get_address_of_dampingTime_6() { return &___dampingTime_6; }
	inline void set_dampingTime_6(float value)
	{
		___dampingTime_6 = value;
	}

	inline static int32_t get_offset_of_autoZeroVerticalOnMobile_7() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroVerticalOnMobile_7)); }
	inline bool get_autoZeroVerticalOnMobile_7() const { return ___autoZeroVerticalOnMobile_7; }
	inline bool* get_address_of_autoZeroVerticalOnMobile_7() { return &___autoZeroVerticalOnMobile_7; }
	inline void set_autoZeroVerticalOnMobile_7(bool value)
	{
		___autoZeroVerticalOnMobile_7 = value;
	}

	inline static int32_t get_offset_of_autoZeroHorizontalOnMobile_8() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroHorizontalOnMobile_8)); }
	inline bool get_autoZeroHorizontalOnMobile_8() const { return ___autoZeroHorizontalOnMobile_8; }
	inline bool* get_address_of_autoZeroHorizontalOnMobile_8() { return &___autoZeroHorizontalOnMobile_8; }
	inline void set_autoZeroHorizontalOnMobile_8(bool value)
	{
		___autoZeroHorizontalOnMobile_8 = value;
	}

	inline static int32_t get_offset_of_relative_9() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___relative_9)); }
	inline bool get_relative_9() const { return ___relative_9; }
	inline bool* get_address_of_relative_9() { return &___relative_9; }
	inline void set_relative_9(bool value)
	{
		___relative_9 = value;
	}

	inline static int32_t get_offset_of_m_TargetAngles_10() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_TargetAngles_10)); }
	inline Vector3_t3722313464  get_m_TargetAngles_10() const { return ___m_TargetAngles_10; }
	inline Vector3_t3722313464 * get_address_of_m_TargetAngles_10() { return &___m_TargetAngles_10; }
	inline void set_m_TargetAngles_10(Vector3_t3722313464  value)
	{
		___m_TargetAngles_10 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_11() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowAngles_11)); }
	inline Vector3_t3722313464  get_m_FollowAngles_11() const { return ___m_FollowAngles_11; }
	inline Vector3_t3722313464 * get_address_of_m_FollowAngles_11() { return &___m_FollowAngles_11; }
	inline void set_m_FollowAngles_11(Vector3_t3722313464  value)
	{
		___m_FollowAngles_11 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_12() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowVelocity_12)); }
	inline Vector3_t3722313464  get_m_FollowVelocity_12() const { return ___m_FollowVelocity_12; }
	inline Vector3_t3722313464 * get_address_of_m_FollowVelocity_12() { return &___m_FollowVelocity_12; }
	inline void set_m_FollowVelocity_12(Vector3_t3722313464  value)
	{
		___m_FollowVelocity_12 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_13() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_OriginalRotation_13)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_13() const { return ___m_OriginalRotation_13; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_13() { return &___m_OriginalRotation_13; }
	inline void set_m_OriginalRotation_13(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMOUSEROTATOR_T2364742953_H
#ifndef SMOOTHFOLLOW_T4204731361_H
#define SMOOTHFOLLOW_T4204731361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SmoothFollow
struct  SmoothFollow_t4204731361  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.SmoothFollow::target
	Transform_t3600365921 * ___target_4;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::distance
	float ___distance_5;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::height
	float ___height_6;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::rotationDamping
	float ___rotationDamping_7;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::heightDamping
	float ___heightDamping_8;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_7() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___rotationDamping_7)); }
	inline float get_rotationDamping_7() const { return ___rotationDamping_7; }
	inline float* get_address_of_rotationDamping_7() { return &___rotationDamping_7; }
	inline void set_rotationDamping_7(float value)
	{
		___rotationDamping_7 = value;
	}

	inline static int32_t get_offset_of_heightDamping_8() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___heightDamping_8)); }
	inline float get_heightDamping_8() const { return ___heightDamping_8; }
	inline float* get_address_of_heightDamping_8() { return &___heightDamping_8; }
	inline void set_heightDamping_8(float value)
	{
		___heightDamping_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T4204731361_H
#ifndef TIMEDOBJECTACTIVATOR_T1846709985_H
#define TIMEDOBJECTACTIVATOR_T1846709985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t1846709985  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t3168066469 * ___entries_4;

public:
	inline static int32_t get_offset_of_entries_4() { return static_cast<int32_t>(offsetof(TimedObjectActivator_t1846709985, ___entries_4)); }
	inline Entries_t3168066469 * get_entries_4() const { return ___entries_4; }
	inline Entries_t3168066469 ** get_address_of_entries_4() { return &___entries_4; }
	inline void set_entries_4(Entries_t3168066469 * value)
	{
		___entries_4 = value;
		Il2CppCodeGenWriteBarrier((&___entries_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTACTIVATOR_T1846709985_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3438860414_H
#define TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectDestructor
struct  TimedObjectDestructor_t3438860414  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.TimedObjectDestructor::m_TimeOut
	float ___m_TimeOut_4;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectDestructor::m_DetachChildren
	bool ___m_DetachChildren_5;

public:
	inline static int32_t get_offset_of_m_TimeOut_4() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_TimeOut_4)); }
	inline float get_m_TimeOut_4() const { return ___m_TimeOut_4; }
	inline float* get_address_of_m_TimeOut_4() { return &___m_TimeOut_4; }
	inline void set_m_TimeOut_4(float value)
	{
		___m_TimeOut_4 = value;
	}

	inline static int32_t get_offset_of_m_DetachChildren_5() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_DetachChildren_5)); }
	inline bool get_m_DetachChildren_5() const { return ___m_DetachChildren_5; }
	inline bool* get_address_of_m_DetachChildren_5() { return &___m_DetachChildren_5; }
	inline void set_m_DetachChildren_5(bool value)
	{
		___m_DetachChildren_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifndef WAYPOINTCIRCUIT_T445075330_H
#define WAYPOINTCIRCUIT_T445075330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit
struct  WaypointCircuit_t445075330  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit/WaypointList UnityStandardAssets.Utility.WaypointCircuit::waypointList
	WaypointList_t2584574554 * ___waypointList_4;
	// System.Boolean UnityStandardAssets.Utility.WaypointCircuit::smoothRoute
	bool ___smoothRoute_5;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::numPoints
	int32_t ___numPoints_6;
	// UnityEngine.Vector3[] UnityStandardAssets.Utility.WaypointCircuit::points
	Vector3U5BU5D_t1718750761* ___points_7;
	// System.Single[] UnityStandardAssets.Utility.WaypointCircuit::distances
	SingleU5BU5D_t1444911251* ___distances_8;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::editorVisualisationSubsteps
	float ___editorVisualisationSubsteps_9;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_10;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p0n
	int32_t ___p0n_11;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p1n
	int32_t ___p1n_12;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p2n
	int32_t ___p2n_13;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p3n
	int32_t ___p3n_14;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::i
	float ___i_15;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P0
	Vector3_t3722313464  ___P0_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P1
	Vector3_t3722313464  ___P1_17;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P2
	Vector3_t3722313464  ___P2_18;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P3
	Vector3_t3722313464  ___P3_19;

public:
	inline static int32_t get_offset_of_waypointList_4() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___waypointList_4)); }
	inline WaypointList_t2584574554 * get_waypointList_4() const { return ___waypointList_4; }
	inline WaypointList_t2584574554 ** get_address_of_waypointList_4() { return &___waypointList_4; }
	inline void set_waypointList_4(WaypointList_t2584574554 * value)
	{
		___waypointList_4 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_4), value);
	}

	inline static int32_t get_offset_of_smoothRoute_5() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___smoothRoute_5)); }
	inline bool get_smoothRoute_5() const { return ___smoothRoute_5; }
	inline bool* get_address_of_smoothRoute_5() { return &___smoothRoute_5; }
	inline void set_smoothRoute_5(bool value)
	{
		___smoothRoute_5 = value;
	}

	inline static int32_t get_offset_of_numPoints_6() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___numPoints_6)); }
	inline int32_t get_numPoints_6() const { return ___numPoints_6; }
	inline int32_t* get_address_of_numPoints_6() { return &___numPoints_6; }
	inline void set_numPoints_6(int32_t value)
	{
		___numPoints_6 = value;
	}

	inline static int32_t get_offset_of_points_7() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___points_7)); }
	inline Vector3U5BU5D_t1718750761* get_points_7() const { return ___points_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_points_7() { return &___points_7; }
	inline void set_points_7(Vector3U5BU5D_t1718750761* value)
	{
		___points_7 = value;
		Il2CppCodeGenWriteBarrier((&___points_7), value);
	}

	inline static int32_t get_offset_of_distances_8() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___distances_8)); }
	inline SingleU5BU5D_t1444911251* get_distances_8() const { return ___distances_8; }
	inline SingleU5BU5D_t1444911251** get_address_of_distances_8() { return &___distances_8; }
	inline void set_distances_8(SingleU5BU5D_t1444911251* value)
	{
		___distances_8 = value;
		Il2CppCodeGenWriteBarrier((&___distances_8), value);
	}

	inline static int32_t get_offset_of_editorVisualisationSubsteps_9() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___editorVisualisationSubsteps_9)); }
	inline float get_editorVisualisationSubsteps_9() const { return ___editorVisualisationSubsteps_9; }
	inline float* get_address_of_editorVisualisationSubsteps_9() { return &___editorVisualisationSubsteps_9; }
	inline void set_editorVisualisationSubsteps_9(float value)
	{
		___editorVisualisationSubsteps_9 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___U3CLengthU3Ek__BackingField_10)); }
	inline float get_U3CLengthU3Ek__BackingField_10() const { return ___U3CLengthU3Ek__BackingField_10; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_10() { return &___U3CLengthU3Ek__BackingField_10; }
	inline void set_U3CLengthU3Ek__BackingField_10(float value)
	{
		___U3CLengthU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_p0n_11() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p0n_11)); }
	inline int32_t get_p0n_11() const { return ___p0n_11; }
	inline int32_t* get_address_of_p0n_11() { return &___p0n_11; }
	inline void set_p0n_11(int32_t value)
	{
		___p0n_11 = value;
	}

	inline static int32_t get_offset_of_p1n_12() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p1n_12)); }
	inline int32_t get_p1n_12() const { return ___p1n_12; }
	inline int32_t* get_address_of_p1n_12() { return &___p1n_12; }
	inline void set_p1n_12(int32_t value)
	{
		___p1n_12 = value;
	}

	inline static int32_t get_offset_of_p2n_13() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p2n_13)); }
	inline int32_t get_p2n_13() const { return ___p2n_13; }
	inline int32_t* get_address_of_p2n_13() { return &___p2n_13; }
	inline void set_p2n_13(int32_t value)
	{
		___p2n_13 = value;
	}

	inline static int32_t get_offset_of_p3n_14() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p3n_14)); }
	inline int32_t get_p3n_14() const { return ___p3n_14; }
	inline int32_t* get_address_of_p3n_14() { return &___p3n_14; }
	inline void set_p3n_14(int32_t value)
	{
		___p3n_14 = value;
	}

	inline static int32_t get_offset_of_i_15() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___i_15)); }
	inline float get_i_15() const { return ___i_15; }
	inline float* get_address_of_i_15() { return &___i_15; }
	inline void set_i_15(float value)
	{
		___i_15 = value;
	}

	inline static int32_t get_offset_of_P0_16() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P0_16)); }
	inline Vector3_t3722313464  get_P0_16() const { return ___P0_16; }
	inline Vector3_t3722313464 * get_address_of_P0_16() { return &___P0_16; }
	inline void set_P0_16(Vector3_t3722313464  value)
	{
		___P0_16 = value;
	}

	inline static int32_t get_offset_of_P1_17() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P1_17)); }
	inline Vector3_t3722313464  get_P1_17() const { return ___P1_17; }
	inline Vector3_t3722313464 * get_address_of_P1_17() { return &___P1_17; }
	inline void set_P1_17(Vector3_t3722313464  value)
	{
		___P1_17 = value;
	}

	inline static int32_t get_offset_of_P2_18() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P2_18)); }
	inline Vector3_t3722313464  get_P2_18() const { return ___P2_18; }
	inline Vector3_t3722313464 * get_address_of_P2_18() { return &___P2_18; }
	inline void set_P2_18(Vector3_t3722313464  value)
	{
		___P2_18 = value;
	}

	inline static int32_t get_offset_of_P3_19() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P3_19)); }
	inline Vector3_t3722313464  get_P3_19() const { return ___P3_19; }
	inline Vector3_t3722313464 * get_address_of_P3_19() { return &___P3_19; }
	inline void set_P3_19(Vector3_t3722313464  value)
	{
		___P3_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCIRCUIT_T445075330_H
#ifndef WAYPOINTPROGRESSTRACKER_T1841386251_H
#define WAYPOINTPROGRESSTRACKER_T1841386251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker
struct  WaypointProgressTracker_t1841386251  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointProgressTracker::circuit
	WaypointCircuit_t445075330 * ___circuit_4;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetOffset
	float ___lookAheadForTargetOffset_5;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetFactor
	float ___lookAheadForTargetFactor_6;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedOffset
	float ___lookAheadForSpeedOffset_7;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedFactor
	float ___lookAheadForSpeedFactor_8;
	// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle UnityStandardAssets.Utility.WaypointProgressTracker::progressStyle
	int32_t ___progressStyle_9;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::pointToPointThreshold
	float ___pointToPointThreshold_10;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<targetPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CtargetPointU3Ek__BackingField_11;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<speedPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CspeedPointU3Ek__BackingField_12;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<progressPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CprogressPointU3Ek__BackingField_13;
	// UnityEngine.Transform UnityStandardAssets.Utility.WaypointProgressTracker::target
	Transform_t3600365921 * ___target_14;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::progressDistance
	float ___progressDistance_15;
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker::progressNum
	int32_t ___progressNum_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointProgressTracker::lastPosition
	Vector3_t3722313464  ___lastPosition_17;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::speed
	float ___speed_18;

public:
	inline static int32_t get_offset_of_circuit_4() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___circuit_4)); }
	inline WaypointCircuit_t445075330 * get_circuit_4() const { return ___circuit_4; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_4() { return &___circuit_4; }
	inline void set_circuit_4(WaypointCircuit_t445075330 * value)
	{
		___circuit_4 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_4), value);
	}

	inline static int32_t get_offset_of_lookAheadForTargetOffset_5() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetOffset_5)); }
	inline float get_lookAheadForTargetOffset_5() const { return ___lookAheadForTargetOffset_5; }
	inline float* get_address_of_lookAheadForTargetOffset_5() { return &___lookAheadForTargetOffset_5; }
	inline void set_lookAheadForTargetOffset_5(float value)
	{
		___lookAheadForTargetOffset_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadForTargetFactor_6() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetFactor_6)); }
	inline float get_lookAheadForTargetFactor_6() const { return ___lookAheadForTargetFactor_6; }
	inline float* get_address_of_lookAheadForTargetFactor_6() { return &___lookAheadForTargetFactor_6; }
	inline void set_lookAheadForTargetFactor_6(float value)
	{
		___lookAheadForTargetFactor_6 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedOffset_7() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedOffset_7)); }
	inline float get_lookAheadForSpeedOffset_7() const { return ___lookAheadForSpeedOffset_7; }
	inline float* get_address_of_lookAheadForSpeedOffset_7() { return &___lookAheadForSpeedOffset_7; }
	inline void set_lookAheadForSpeedOffset_7(float value)
	{
		___lookAheadForSpeedOffset_7 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedFactor_8() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedFactor_8)); }
	inline float get_lookAheadForSpeedFactor_8() const { return ___lookAheadForSpeedFactor_8; }
	inline float* get_address_of_lookAheadForSpeedFactor_8() { return &___lookAheadForSpeedFactor_8; }
	inline void set_lookAheadForSpeedFactor_8(float value)
	{
		___lookAheadForSpeedFactor_8 = value;
	}

	inline static int32_t get_offset_of_progressStyle_9() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressStyle_9)); }
	inline int32_t get_progressStyle_9() const { return ___progressStyle_9; }
	inline int32_t* get_address_of_progressStyle_9() { return &___progressStyle_9; }
	inline void set_progressStyle_9(int32_t value)
	{
		___progressStyle_9 = value;
	}

	inline static int32_t get_offset_of_pointToPointThreshold_10() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___pointToPointThreshold_10)); }
	inline float get_pointToPointThreshold_10() const { return ___pointToPointThreshold_10; }
	inline float* get_address_of_pointToPointThreshold_10() { return &___pointToPointThreshold_10; }
	inline void set_pointToPointThreshold_10(float value)
	{
		___pointToPointThreshold_10 = value;
	}

	inline static int32_t get_offset_of_U3CtargetPointU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CtargetPointU3Ek__BackingField_11)); }
	inline RoutePoint_t3880028948  get_U3CtargetPointU3Ek__BackingField_11() const { return ___U3CtargetPointU3Ek__BackingField_11; }
	inline RoutePoint_t3880028948 * get_address_of_U3CtargetPointU3Ek__BackingField_11() { return &___U3CtargetPointU3Ek__BackingField_11; }
	inline void set_U3CtargetPointU3Ek__BackingField_11(RoutePoint_t3880028948  value)
	{
		___U3CtargetPointU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPointU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CspeedPointU3Ek__BackingField_12)); }
	inline RoutePoint_t3880028948  get_U3CspeedPointU3Ek__BackingField_12() const { return ___U3CspeedPointU3Ek__BackingField_12; }
	inline RoutePoint_t3880028948 * get_address_of_U3CspeedPointU3Ek__BackingField_12() { return &___U3CspeedPointU3Ek__BackingField_12; }
	inline void set_U3CspeedPointU3Ek__BackingField_12(RoutePoint_t3880028948  value)
	{
		___U3CspeedPointU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CprogressPointU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CprogressPointU3Ek__BackingField_13)); }
	inline RoutePoint_t3880028948  get_U3CprogressPointU3Ek__BackingField_13() const { return ___U3CprogressPointU3Ek__BackingField_13; }
	inline RoutePoint_t3880028948 * get_address_of_U3CprogressPointU3Ek__BackingField_13() { return &___U3CprogressPointU3Ek__BackingField_13; }
	inline void set_U3CprogressPointU3Ek__BackingField_13(RoutePoint_t3880028948  value)
	{
		___U3CprogressPointU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_target_14() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___target_14)); }
	inline Transform_t3600365921 * get_target_14() const { return ___target_14; }
	inline Transform_t3600365921 ** get_address_of_target_14() { return &___target_14; }
	inline void set_target_14(Transform_t3600365921 * value)
	{
		___target_14 = value;
		Il2CppCodeGenWriteBarrier((&___target_14), value);
	}

	inline static int32_t get_offset_of_progressDistance_15() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressDistance_15)); }
	inline float get_progressDistance_15() const { return ___progressDistance_15; }
	inline float* get_address_of_progressDistance_15() { return &___progressDistance_15; }
	inline void set_progressDistance_15(float value)
	{
		___progressDistance_15 = value;
	}

	inline static int32_t get_offset_of_progressNum_16() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressNum_16)); }
	inline int32_t get_progressNum_16() const { return ___progressNum_16; }
	inline int32_t* get_address_of_progressNum_16() { return &___progressNum_16; }
	inline void set_progressNum_16(int32_t value)
	{
		___progressNum_16 = value;
	}

	inline static int32_t get_offset_of_lastPosition_17() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lastPosition_17)); }
	inline Vector3_t3722313464  get_lastPosition_17() const { return ___lastPosition_17; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_17() { return &___lastPosition_17; }
	inline void set_lastPosition_17(Vector3_t3722313464  value)
	{
		___lastPosition_17 = value;
	}

	inline static int32_t get_offset_of_speed_18() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___speed_18)); }
	inline float get_speed_18() const { return ___speed_18; }
	inline float* get_address_of_speed_18() { return &___speed_18; }
	inline void set_speed_18(float value)
	{
		___speed_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTPROGRESSTRACKER_T1841386251_H
#ifndef AEROPLANEAICONTROL_T3513225471_H
#define AEROPLANEAICONTROL_T3513225471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl
struct  AeroplaneAiControl_t3513225471  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_RollSensitivity
	float ___m_RollSensitivity_4;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_PitchSensitivity
	float ___m_PitchSensitivity_5;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_LateralWanderDistance
	float ___m_LateralWanderDistance_6;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_LateralWanderSpeed
	float ___m_LateralWanderSpeed_7;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_MaxClimbAngle
	float ___m_MaxClimbAngle_8;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_MaxRollAngle
	float ___m_MaxRollAngle_9;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_SpeedEffect
	float ___m_SpeedEffect_10;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_TakeoffHeight
	float ___m_TakeoffHeight_11;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_Target
	Transform_t3600365921 * ___m_Target_12;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_AeroplaneController
	AeroplaneController_t857819489 * ___m_AeroplaneController_13;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_RandomPerlin
	float ___m_RandomPerlin_14;
	// System.Boolean UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::m_TakenOff
	bool ___m_TakenOff_15;

public:
	inline static int32_t get_offset_of_m_RollSensitivity_4() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_RollSensitivity_4)); }
	inline float get_m_RollSensitivity_4() const { return ___m_RollSensitivity_4; }
	inline float* get_address_of_m_RollSensitivity_4() { return &___m_RollSensitivity_4; }
	inline void set_m_RollSensitivity_4(float value)
	{
		___m_RollSensitivity_4 = value;
	}

	inline static int32_t get_offset_of_m_PitchSensitivity_5() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_PitchSensitivity_5)); }
	inline float get_m_PitchSensitivity_5() const { return ___m_PitchSensitivity_5; }
	inline float* get_address_of_m_PitchSensitivity_5() { return &___m_PitchSensitivity_5; }
	inline void set_m_PitchSensitivity_5(float value)
	{
		___m_PitchSensitivity_5 = value;
	}

	inline static int32_t get_offset_of_m_LateralWanderDistance_6() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_LateralWanderDistance_6)); }
	inline float get_m_LateralWanderDistance_6() const { return ___m_LateralWanderDistance_6; }
	inline float* get_address_of_m_LateralWanderDistance_6() { return &___m_LateralWanderDistance_6; }
	inline void set_m_LateralWanderDistance_6(float value)
	{
		___m_LateralWanderDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_LateralWanderSpeed_7() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_LateralWanderSpeed_7)); }
	inline float get_m_LateralWanderSpeed_7() const { return ___m_LateralWanderSpeed_7; }
	inline float* get_address_of_m_LateralWanderSpeed_7() { return &___m_LateralWanderSpeed_7; }
	inline void set_m_LateralWanderSpeed_7(float value)
	{
		___m_LateralWanderSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxClimbAngle_8() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_MaxClimbAngle_8)); }
	inline float get_m_MaxClimbAngle_8() const { return ___m_MaxClimbAngle_8; }
	inline float* get_address_of_m_MaxClimbAngle_8() { return &___m_MaxClimbAngle_8; }
	inline void set_m_MaxClimbAngle_8(float value)
	{
		___m_MaxClimbAngle_8 = value;
	}

	inline static int32_t get_offset_of_m_MaxRollAngle_9() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_MaxRollAngle_9)); }
	inline float get_m_MaxRollAngle_9() const { return ___m_MaxRollAngle_9; }
	inline float* get_address_of_m_MaxRollAngle_9() { return &___m_MaxRollAngle_9; }
	inline void set_m_MaxRollAngle_9(float value)
	{
		___m_MaxRollAngle_9 = value;
	}

	inline static int32_t get_offset_of_m_SpeedEffect_10() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_SpeedEffect_10)); }
	inline float get_m_SpeedEffect_10() const { return ___m_SpeedEffect_10; }
	inline float* get_address_of_m_SpeedEffect_10() { return &___m_SpeedEffect_10; }
	inline void set_m_SpeedEffect_10(float value)
	{
		___m_SpeedEffect_10 = value;
	}

	inline static int32_t get_offset_of_m_TakeoffHeight_11() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_TakeoffHeight_11)); }
	inline float get_m_TakeoffHeight_11() const { return ___m_TakeoffHeight_11; }
	inline float* get_address_of_m_TakeoffHeight_11() { return &___m_TakeoffHeight_11; }
	inline void set_m_TakeoffHeight_11(float value)
	{
		___m_TakeoffHeight_11 = value;
	}

	inline static int32_t get_offset_of_m_Target_12() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_Target_12)); }
	inline Transform_t3600365921 * get_m_Target_12() const { return ___m_Target_12; }
	inline Transform_t3600365921 ** get_address_of_m_Target_12() { return &___m_Target_12; }
	inline void set_m_Target_12(Transform_t3600365921 * value)
	{
		___m_Target_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_12), value);
	}

	inline static int32_t get_offset_of_m_AeroplaneController_13() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_AeroplaneController_13)); }
	inline AeroplaneController_t857819489 * get_m_AeroplaneController_13() const { return ___m_AeroplaneController_13; }
	inline AeroplaneController_t857819489 ** get_address_of_m_AeroplaneController_13() { return &___m_AeroplaneController_13; }
	inline void set_m_AeroplaneController_13(AeroplaneController_t857819489 * value)
	{
		___m_AeroplaneController_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_AeroplaneController_13), value);
	}

	inline static int32_t get_offset_of_m_RandomPerlin_14() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_RandomPerlin_14)); }
	inline float get_m_RandomPerlin_14() const { return ___m_RandomPerlin_14; }
	inline float* get_address_of_m_RandomPerlin_14() { return &___m_RandomPerlin_14; }
	inline void set_m_RandomPerlin_14(float value)
	{
		___m_RandomPerlin_14 = value;
	}

	inline static int32_t get_offset_of_m_TakenOff_15() { return static_cast<int32_t>(offsetof(AeroplaneAiControl_t3513225471, ___m_TakenOff_15)); }
	inline bool get_m_TakenOff_15() const { return ___m_TakenOff_15; }
	inline bool* get_address_of_m_TakenOff_15() { return &___m_TakenOff_15; }
	inline void set_m_TakenOff_15(bool value)
	{
		___m_TakenOff_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEROPLANEAICONTROL_T3513225471_H
#ifndef AEROPLANEAUDIO_T418183782_H
#define AEROPLANEAUDIO_T418183782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio
struct  AeroplaneAudio_t418183782  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_EngineSound
	AudioClip_t3680889665 * ___m_EngineSound_4;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_EngineMinThrottlePitch
	float ___m_EngineMinThrottlePitch_5;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_EngineMaxThrottlePitch
	float ___m_EngineMaxThrottlePitch_6;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_EngineFwdSpeedMultiplier
	float ___m_EngineFwdSpeedMultiplier_7;
	// UnityEngine.AudioClip UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_WindSound
	AudioClip_t3680889665 * ___m_WindSound_8;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_WindBasePitch
	float ___m_WindBasePitch_9;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_WindSpeedPitchFactor
	float ___m_WindSpeedPitchFactor_10;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_WindMaxSpeedVolume
	float ___m_WindMaxSpeedVolume_11;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_AdvancedSetttings
	AdvancedSetttings_t3817742043 * ___m_AdvancedSetttings_12;
	// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_EngineSoundSource
	AudioSource_t3935305588 * ___m_EngineSoundSource_13;
	// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_WindSoundSource
	AudioSource_t3935305588 * ___m_WindSoundSource_14;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_Plane
	AeroplaneController_t857819489 * ___m_Plane_15;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_16;

public:
	inline static int32_t get_offset_of_m_EngineSound_4() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_EngineSound_4)); }
	inline AudioClip_t3680889665 * get_m_EngineSound_4() const { return ___m_EngineSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_m_EngineSound_4() { return &___m_EngineSound_4; }
	inline void set_m_EngineSound_4(AudioClip_t3680889665 * value)
	{
		___m_EngineSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EngineSound_4), value);
	}

	inline static int32_t get_offset_of_m_EngineMinThrottlePitch_5() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_EngineMinThrottlePitch_5)); }
	inline float get_m_EngineMinThrottlePitch_5() const { return ___m_EngineMinThrottlePitch_5; }
	inline float* get_address_of_m_EngineMinThrottlePitch_5() { return &___m_EngineMinThrottlePitch_5; }
	inline void set_m_EngineMinThrottlePitch_5(float value)
	{
		___m_EngineMinThrottlePitch_5 = value;
	}

	inline static int32_t get_offset_of_m_EngineMaxThrottlePitch_6() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_EngineMaxThrottlePitch_6)); }
	inline float get_m_EngineMaxThrottlePitch_6() const { return ___m_EngineMaxThrottlePitch_6; }
	inline float* get_address_of_m_EngineMaxThrottlePitch_6() { return &___m_EngineMaxThrottlePitch_6; }
	inline void set_m_EngineMaxThrottlePitch_6(float value)
	{
		___m_EngineMaxThrottlePitch_6 = value;
	}

	inline static int32_t get_offset_of_m_EngineFwdSpeedMultiplier_7() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_EngineFwdSpeedMultiplier_7)); }
	inline float get_m_EngineFwdSpeedMultiplier_7() const { return ___m_EngineFwdSpeedMultiplier_7; }
	inline float* get_address_of_m_EngineFwdSpeedMultiplier_7() { return &___m_EngineFwdSpeedMultiplier_7; }
	inline void set_m_EngineFwdSpeedMultiplier_7(float value)
	{
		___m_EngineFwdSpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_WindSound_8() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_WindSound_8)); }
	inline AudioClip_t3680889665 * get_m_WindSound_8() const { return ___m_WindSound_8; }
	inline AudioClip_t3680889665 ** get_address_of_m_WindSound_8() { return &___m_WindSound_8; }
	inline void set_m_WindSound_8(AudioClip_t3680889665 * value)
	{
		___m_WindSound_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_WindSound_8), value);
	}

	inline static int32_t get_offset_of_m_WindBasePitch_9() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_WindBasePitch_9)); }
	inline float get_m_WindBasePitch_9() const { return ___m_WindBasePitch_9; }
	inline float* get_address_of_m_WindBasePitch_9() { return &___m_WindBasePitch_9; }
	inline void set_m_WindBasePitch_9(float value)
	{
		___m_WindBasePitch_9 = value;
	}

	inline static int32_t get_offset_of_m_WindSpeedPitchFactor_10() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_WindSpeedPitchFactor_10)); }
	inline float get_m_WindSpeedPitchFactor_10() const { return ___m_WindSpeedPitchFactor_10; }
	inline float* get_address_of_m_WindSpeedPitchFactor_10() { return &___m_WindSpeedPitchFactor_10; }
	inline void set_m_WindSpeedPitchFactor_10(float value)
	{
		___m_WindSpeedPitchFactor_10 = value;
	}

	inline static int32_t get_offset_of_m_WindMaxSpeedVolume_11() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_WindMaxSpeedVolume_11)); }
	inline float get_m_WindMaxSpeedVolume_11() const { return ___m_WindMaxSpeedVolume_11; }
	inline float* get_address_of_m_WindMaxSpeedVolume_11() { return &___m_WindMaxSpeedVolume_11; }
	inline void set_m_WindMaxSpeedVolume_11(float value)
	{
		___m_WindMaxSpeedVolume_11 = value;
	}

	inline static int32_t get_offset_of_m_AdvancedSetttings_12() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_AdvancedSetttings_12)); }
	inline AdvancedSetttings_t3817742043 * get_m_AdvancedSetttings_12() const { return ___m_AdvancedSetttings_12; }
	inline AdvancedSetttings_t3817742043 ** get_address_of_m_AdvancedSetttings_12() { return &___m_AdvancedSetttings_12; }
	inline void set_m_AdvancedSetttings_12(AdvancedSetttings_t3817742043 * value)
	{
		___m_AdvancedSetttings_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_AdvancedSetttings_12), value);
	}

	inline static int32_t get_offset_of_m_EngineSoundSource_13() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_EngineSoundSource_13)); }
	inline AudioSource_t3935305588 * get_m_EngineSoundSource_13() const { return ___m_EngineSoundSource_13; }
	inline AudioSource_t3935305588 ** get_address_of_m_EngineSoundSource_13() { return &___m_EngineSoundSource_13; }
	inline void set_m_EngineSoundSource_13(AudioSource_t3935305588 * value)
	{
		___m_EngineSoundSource_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_EngineSoundSource_13), value);
	}

	inline static int32_t get_offset_of_m_WindSoundSource_14() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_WindSoundSource_14)); }
	inline AudioSource_t3935305588 * get_m_WindSoundSource_14() const { return ___m_WindSoundSource_14; }
	inline AudioSource_t3935305588 ** get_address_of_m_WindSoundSource_14() { return &___m_WindSoundSource_14; }
	inline void set_m_WindSoundSource_14(AudioSource_t3935305588 * value)
	{
		___m_WindSoundSource_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_WindSoundSource_14), value);
	}

	inline static int32_t get_offset_of_m_Plane_15() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_Plane_15)); }
	inline AeroplaneController_t857819489 * get_m_Plane_15() const { return ___m_Plane_15; }
	inline AeroplaneController_t857819489 ** get_address_of_m_Plane_15() { return &___m_Plane_15; }
	inline void set_m_Plane_15(AeroplaneController_t857819489 * value)
	{
		___m_Plane_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_15), value);
	}

	inline static int32_t get_offset_of_m_Rigidbody_16() { return static_cast<int32_t>(offsetof(AeroplaneAudio_t418183782, ___m_Rigidbody_16)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_16() const { return ___m_Rigidbody_16; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_16() { return &___m_Rigidbody_16; }
	inline void set_m_Rigidbody_16(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEROPLANEAUDIO_T418183782_H
#ifndef AEROPLANECONTROLSURFACEANIMATOR_T2833473099_H
#define AEROPLANECONTROLSURFACEANIMATOR_T2833473099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator
struct  AeroplaneControlSurfaceAnimator_t2833473099  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::m_Smoothing
	float ___m_Smoothing_4;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface[] UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::m_ControlSurfaces
	ControlSurfaceU5BU5D_t233403270* ___m_ControlSurfaces_5;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::m_Plane
	AeroplaneController_t857819489 * ___m_Plane_6;

public:
	inline static int32_t get_offset_of_m_Smoothing_4() { return static_cast<int32_t>(offsetof(AeroplaneControlSurfaceAnimator_t2833473099, ___m_Smoothing_4)); }
	inline float get_m_Smoothing_4() const { return ___m_Smoothing_4; }
	inline float* get_address_of_m_Smoothing_4() { return &___m_Smoothing_4; }
	inline void set_m_Smoothing_4(float value)
	{
		___m_Smoothing_4 = value;
	}

	inline static int32_t get_offset_of_m_ControlSurfaces_5() { return static_cast<int32_t>(offsetof(AeroplaneControlSurfaceAnimator_t2833473099, ___m_ControlSurfaces_5)); }
	inline ControlSurfaceU5BU5D_t233403270* get_m_ControlSurfaces_5() const { return ___m_ControlSurfaces_5; }
	inline ControlSurfaceU5BU5D_t233403270** get_address_of_m_ControlSurfaces_5() { return &___m_ControlSurfaces_5; }
	inline void set_m_ControlSurfaces_5(ControlSurfaceU5BU5D_t233403270* value)
	{
		___m_ControlSurfaces_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlSurfaces_5), value);
	}

	inline static int32_t get_offset_of_m_Plane_6() { return static_cast<int32_t>(offsetof(AeroplaneControlSurfaceAnimator_t2833473099, ___m_Plane_6)); }
	inline AeroplaneController_t857819489 * get_m_Plane_6() const { return ___m_Plane_6; }
	inline AeroplaneController_t857819489 ** get_address_of_m_Plane_6() { return &___m_Plane_6; }
	inline void set_m_Plane_6(AeroplaneController_t857819489 * value)
	{
		___m_Plane_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEROPLANECONTROLSURFACEANIMATOR_T2833473099_H
#ifndef AEROPLANECONTROLLER_T857819489_H
#define AEROPLANECONTROLLER_T857819489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController
struct  AeroplaneController_t857819489  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_MaxEnginePower
	float ___m_MaxEnginePower_4;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_Lift
	float ___m_Lift_5;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_ZeroLiftSpeed
	float ___m_ZeroLiftSpeed_6;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_RollEffect
	float ___m_RollEffect_7;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_PitchEffect
	float ___m_PitchEffect_8;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_YawEffect
	float ___m_YawEffect_9;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_BankedTurnEffect
	float ___m_BankedTurnEffect_10;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_AerodynamicEffect
	float ___m_AerodynamicEffect_11;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_AutoTurnPitch
	float ___m_AutoTurnPitch_12;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_AutoRollLevel
	float ___m_AutoRollLevel_13;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_AutoPitchLevel
	float ___m_AutoPitchLevel_14;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_AirBrakesEffect
	float ___m_AirBrakesEffect_15;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_ThrottleChangeSpeed
	float ___m_ThrottleChangeSpeed_16;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_DragIncreaseFactor
	float ___m_DragIncreaseFactor_17;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<Altitude>k__BackingField
	float ___U3CAltitudeU3Ek__BackingField_18;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<Throttle>k__BackingField
	float ___U3CThrottleU3Ek__BackingField_19;
	// System.Boolean UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<AirBrakes>k__BackingField
	bool ___U3CAirBrakesU3Ek__BackingField_20;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<ForwardSpeed>k__BackingField
	float ___U3CForwardSpeedU3Ek__BackingField_21;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<EnginePower>k__BackingField
	float ___U3CEnginePowerU3Ek__BackingField_22;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<RollAngle>k__BackingField
	float ___U3CRollAngleU3Ek__BackingField_23;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<PitchAngle>k__BackingField
	float ___U3CPitchAngleU3Ek__BackingField_24;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<RollInput>k__BackingField
	float ___U3CRollInputU3Ek__BackingField_25;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<PitchInput>k__BackingField
	float ___U3CPitchInputU3Ek__BackingField_26;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<YawInput>k__BackingField
	float ___U3CYawInputU3Ek__BackingField_27;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::<ThrottleInput>k__BackingField
	float ___U3CThrottleInputU3Ek__BackingField_28;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_OriginalDrag
	float ___m_OriginalDrag_29;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_OriginalAngularDrag
	float ___m_OriginalAngularDrag_30;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_AeroFactor
	float ___m_AeroFactor_31;
	// System.Boolean UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_Immobilized
	bool ___m_Immobilized_32;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_BankedTurnAmount
	float ___m_BankedTurnAmount_33;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_34;
	// UnityEngine.WheelCollider[] UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::m_WheelColliders
	WheelColliderU5BU5D_t483547098* ___m_WheelColliders_35;

public:
	inline static int32_t get_offset_of_m_MaxEnginePower_4() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_MaxEnginePower_4)); }
	inline float get_m_MaxEnginePower_4() const { return ___m_MaxEnginePower_4; }
	inline float* get_address_of_m_MaxEnginePower_4() { return &___m_MaxEnginePower_4; }
	inline void set_m_MaxEnginePower_4(float value)
	{
		___m_MaxEnginePower_4 = value;
	}

	inline static int32_t get_offset_of_m_Lift_5() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_Lift_5)); }
	inline float get_m_Lift_5() const { return ___m_Lift_5; }
	inline float* get_address_of_m_Lift_5() { return &___m_Lift_5; }
	inline void set_m_Lift_5(float value)
	{
		___m_Lift_5 = value;
	}

	inline static int32_t get_offset_of_m_ZeroLiftSpeed_6() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_ZeroLiftSpeed_6)); }
	inline float get_m_ZeroLiftSpeed_6() const { return ___m_ZeroLiftSpeed_6; }
	inline float* get_address_of_m_ZeroLiftSpeed_6() { return &___m_ZeroLiftSpeed_6; }
	inline void set_m_ZeroLiftSpeed_6(float value)
	{
		___m_ZeroLiftSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_RollEffect_7() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_RollEffect_7)); }
	inline float get_m_RollEffect_7() const { return ___m_RollEffect_7; }
	inline float* get_address_of_m_RollEffect_7() { return &___m_RollEffect_7; }
	inline void set_m_RollEffect_7(float value)
	{
		___m_RollEffect_7 = value;
	}

	inline static int32_t get_offset_of_m_PitchEffect_8() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_PitchEffect_8)); }
	inline float get_m_PitchEffect_8() const { return ___m_PitchEffect_8; }
	inline float* get_address_of_m_PitchEffect_8() { return &___m_PitchEffect_8; }
	inline void set_m_PitchEffect_8(float value)
	{
		___m_PitchEffect_8 = value;
	}

	inline static int32_t get_offset_of_m_YawEffect_9() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_YawEffect_9)); }
	inline float get_m_YawEffect_9() const { return ___m_YawEffect_9; }
	inline float* get_address_of_m_YawEffect_9() { return &___m_YawEffect_9; }
	inline void set_m_YawEffect_9(float value)
	{
		___m_YawEffect_9 = value;
	}

	inline static int32_t get_offset_of_m_BankedTurnEffect_10() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_BankedTurnEffect_10)); }
	inline float get_m_BankedTurnEffect_10() const { return ___m_BankedTurnEffect_10; }
	inline float* get_address_of_m_BankedTurnEffect_10() { return &___m_BankedTurnEffect_10; }
	inline void set_m_BankedTurnEffect_10(float value)
	{
		___m_BankedTurnEffect_10 = value;
	}

	inline static int32_t get_offset_of_m_AerodynamicEffect_11() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_AerodynamicEffect_11)); }
	inline float get_m_AerodynamicEffect_11() const { return ___m_AerodynamicEffect_11; }
	inline float* get_address_of_m_AerodynamicEffect_11() { return &___m_AerodynamicEffect_11; }
	inline void set_m_AerodynamicEffect_11(float value)
	{
		___m_AerodynamicEffect_11 = value;
	}

	inline static int32_t get_offset_of_m_AutoTurnPitch_12() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_AutoTurnPitch_12)); }
	inline float get_m_AutoTurnPitch_12() const { return ___m_AutoTurnPitch_12; }
	inline float* get_address_of_m_AutoTurnPitch_12() { return &___m_AutoTurnPitch_12; }
	inline void set_m_AutoTurnPitch_12(float value)
	{
		___m_AutoTurnPitch_12 = value;
	}

	inline static int32_t get_offset_of_m_AutoRollLevel_13() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_AutoRollLevel_13)); }
	inline float get_m_AutoRollLevel_13() const { return ___m_AutoRollLevel_13; }
	inline float* get_address_of_m_AutoRollLevel_13() { return &___m_AutoRollLevel_13; }
	inline void set_m_AutoRollLevel_13(float value)
	{
		___m_AutoRollLevel_13 = value;
	}

	inline static int32_t get_offset_of_m_AutoPitchLevel_14() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_AutoPitchLevel_14)); }
	inline float get_m_AutoPitchLevel_14() const { return ___m_AutoPitchLevel_14; }
	inline float* get_address_of_m_AutoPitchLevel_14() { return &___m_AutoPitchLevel_14; }
	inline void set_m_AutoPitchLevel_14(float value)
	{
		___m_AutoPitchLevel_14 = value;
	}

	inline static int32_t get_offset_of_m_AirBrakesEffect_15() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_AirBrakesEffect_15)); }
	inline float get_m_AirBrakesEffect_15() const { return ___m_AirBrakesEffect_15; }
	inline float* get_address_of_m_AirBrakesEffect_15() { return &___m_AirBrakesEffect_15; }
	inline void set_m_AirBrakesEffect_15(float value)
	{
		___m_AirBrakesEffect_15 = value;
	}

	inline static int32_t get_offset_of_m_ThrottleChangeSpeed_16() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_ThrottleChangeSpeed_16)); }
	inline float get_m_ThrottleChangeSpeed_16() const { return ___m_ThrottleChangeSpeed_16; }
	inline float* get_address_of_m_ThrottleChangeSpeed_16() { return &___m_ThrottleChangeSpeed_16; }
	inline void set_m_ThrottleChangeSpeed_16(float value)
	{
		___m_ThrottleChangeSpeed_16 = value;
	}

	inline static int32_t get_offset_of_m_DragIncreaseFactor_17() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_DragIncreaseFactor_17)); }
	inline float get_m_DragIncreaseFactor_17() const { return ___m_DragIncreaseFactor_17; }
	inline float* get_address_of_m_DragIncreaseFactor_17() { return &___m_DragIncreaseFactor_17; }
	inline void set_m_DragIncreaseFactor_17(float value)
	{
		___m_DragIncreaseFactor_17 = value;
	}

	inline static int32_t get_offset_of_U3CAltitudeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CAltitudeU3Ek__BackingField_18)); }
	inline float get_U3CAltitudeU3Ek__BackingField_18() const { return ___U3CAltitudeU3Ek__BackingField_18; }
	inline float* get_address_of_U3CAltitudeU3Ek__BackingField_18() { return &___U3CAltitudeU3Ek__BackingField_18; }
	inline void set_U3CAltitudeU3Ek__BackingField_18(float value)
	{
		___U3CAltitudeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CThrottleU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CThrottleU3Ek__BackingField_19)); }
	inline float get_U3CThrottleU3Ek__BackingField_19() const { return ___U3CThrottleU3Ek__BackingField_19; }
	inline float* get_address_of_U3CThrottleU3Ek__BackingField_19() { return &___U3CThrottleU3Ek__BackingField_19; }
	inline void set_U3CThrottleU3Ek__BackingField_19(float value)
	{
		___U3CThrottleU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CAirBrakesU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CAirBrakesU3Ek__BackingField_20)); }
	inline bool get_U3CAirBrakesU3Ek__BackingField_20() const { return ___U3CAirBrakesU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CAirBrakesU3Ek__BackingField_20() { return &___U3CAirBrakesU3Ek__BackingField_20; }
	inline void set_U3CAirBrakesU3Ek__BackingField_20(bool value)
	{
		___U3CAirBrakesU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CForwardSpeedU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CForwardSpeedU3Ek__BackingField_21)); }
	inline float get_U3CForwardSpeedU3Ek__BackingField_21() const { return ___U3CForwardSpeedU3Ek__BackingField_21; }
	inline float* get_address_of_U3CForwardSpeedU3Ek__BackingField_21() { return &___U3CForwardSpeedU3Ek__BackingField_21; }
	inline void set_U3CForwardSpeedU3Ek__BackingField_21(float value)
	{
		___U3CForwardSpeedU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CEnginePowerU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CEnginePowerU3Ek__BackingField_22)); }
	inline float get_U3CEnginePowerU3Ek__BackingField_22() const { return ___U3CEnginePowerU3Ek__BackingField_22; }
	inline float* get_address_of_U3CEnginePowerU3Ek__BackingField_22() { return &___U3CEnginePowerU3Ek__BackingField_22; }
	inline void set_U3CEnginePowerU3Ek__BackingField_22(float value)
	{
		___U3CEnginePowerU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CRollAngleU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CRollAngleU3Ek__BackingField_23)); }
	inline float get_U3CRollAngleU3Ek__BackingField_23() const { return ___U3CRollAngleU3Ek__BackingField_23; }
	inline float* get_address_of_U3CRollAngleU3Ek__BackingField_23() { return &___U3CRollAngleU3Ek__BackingField_23; }
	inline void set_U3CRollAngleU3Ek__BackingField_23(float value)
	{
		___U3CRollAngleU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CPitchAngleU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CPitchAngleU3Ek__BackingField_24)); }
	inline float get_U3CPitchAngleU3Ek__BackingField_24() const { return ___U3CPitchAngleU3Ek__BackingField_24; }
	inline float* get_address_of_U3CPitchAngleU3Ek__BackingField_24() { return &___U3CPitchAngleU3Ek__BackingField_24; }
	inline void set_U3CPitchAngleU3Ek__BackingField_24(float value)
	{
		___U3CPitchAngleU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CRollInputU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CRollInputU3Ek__BackingField_25)); }
	inline float get_U3CRollInputU3Ek__BackingField_25() const { return ___U3CRollInputU3Ek__BackingField_25; }
	inline float* get_address_of_U3CRollInputU3Ek__BackingField_25() { return &___U3CRollInputU3Ek__BackingField_25; }
	inline void set_U3CRollInputU3Ek__BackingField_25(float value)
	{
		___U3CRollInputU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CPitchInputU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CPitchInputU3Ek__BackingField_26)); }
	inline float get_U3CPitchInputU3Ek__BackingField_26() const { return ___U3CPitchInputU3Ek__BackingField_26; }
	inline float* get_address_of_U3CPitchInputU3Ek__BackingField_26() { return &___U3CPitchInputU3Ek__BackingField_26; }
	inline void set_U3CPitchInputU3Ek__BackingField_26(float value)
	{
		___U3CPitchInputU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CYawInputU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CYawInputU3Ek__BackingField_27)); }
	inline float get_U3CYawInputU3Ek__BackingField_27() const { return ___U3CYawInputU3Ek__BackingField_27; }
	inline float* get_address_of_U3CYawInputU3Ek__BackingField_27() { return &___U3CYawInputU3Ek__BackingField_27; }
	inline void set_U3CYawInputU3Ek__BackingField_27(float value)
	{
		___U3CYawInputU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CThrottleInputU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___U3CThrottleInputU3Ek__BackingField_28)); }
	inline float get_U3CThrottleInputU3Ek__BackingField_28() const { return ___U3CThrottleInputU3Ek__BackingField_28; }
	inline float* get_address_of_U3CThrottleInputU3Ek__BackingField_28() { return &___U3CThrottleInputU3Ek__BackingField_28; }
	inline void set_U3CThrottleInputU3Ek__BackingField_28(float value)
	{
		___U3CThrottleInputU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_m_OriginalDrag_29() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_OriginalDrag_29)); }
	inline float get_m_OriginalDrag_29() const { return ___m_OriginalDrag_29; }
	inline float* get_address_of_m_OriginalDrag_29() { return &___m_OriginalDrag_29; }
	inline void set_m_OriginalDrag_29(float value)
	{
		___m_OriginalDrag_29 = value;
	}

	inline static int32_t get_offset_of_m_OriginalAngularDrag_30() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_OriginalAngularDrag_30)); }
	inline float get_m_OriginalAngularDrag_30() const { return ___m_OriginalAngularDrag_30; }
	inline float* get_address_of_m_OriginalAngularDrag_30() { return &___m_OriginalAngularDrag_30; }
	inline void set_m_OriginalAngularDrag_30(float value)
	{
		___m_OriginalAngularDrag_30 = value;
	}

	inline static int32_t get_offset_of_m_AeroFactor_31() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_AeroFactor_31)); }
	inline float get_m_AeroFactor_31() const { return ___m_AeroFactor_31; }
	inline float* get_address_of_m_AeroFactor_31() { return &___m_AeroFactor_31; }
	inline void set_m_AeroFactor_31(float value)
	{
		___m_AeroFactor_31 = value;
	}

	inline static int32_t get_offset_of_m_Immobilized_32() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_Immobilized_32)); }
	inline bool get_m_Immobilized_32() const { return ___m_Immobilized_32; }
	inline bool* get_address_of_m_Immobilized_32() { return &___m_Immobilized_32; }
	inline void set_m_Immobilized_32(bool value)
	{
		___m_Immobilized_32 = value;
	}

	inline static int32_t get_offset_of_m_BankedTurnAmount_33() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_BankedTurnAmount_33)); }
	inline float get_m_BankedTurnAmount_33() const { return ___m_BankedTurnAmount_33; }
	inline float* get_address_of_m_BankedTurnAmount_33() { return &___m_BankedTurnAmount_33; }
	inline void set_m_BankedTurnAmount_33(float value)
	{
		___m_BankedTurnAmount_33 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_34() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_Rigidbody_34)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_34() const { return ___m_Rigidbody_34; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_34() { return &___m_Rigidbody_34; }
	inline void set_m_Rigidbody_34(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_34), value);
	}

	inline static int32_t get_offset_of_m_WheelColliders_35() { return static_cast<int32_t>(offsetof(AeroplaneController_t857819489, ___m_WheelColliders_35)); }
	inline WheelColliderU5BU5D_t483547098* get_m_WheelColliders_35() const { return ___m_WheelColliders_35; }
	inline WheelColliderU5BU5D_t483547098** get_address_of_m_WheelColliders_35() { return &___m_WheelColliders_35; }
	inline void set_m_WheelColliders_35(WheelColliderU5BU5D_t483547098* value)
	{
		___m_WheelColliders_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_WheelColliders_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEROPLANECONTROLLER_T857819489_H
#ifndef AEROPLANEPROPELLERANIMATOR_T2205181023_H
#define AEROPLANEPROPELLERANIMATOR_T2205181023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator
struct  AeroplanePropellerAnimator_t2205181023  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_PropellorModel
	Transform_t3600365921 * ___m_PropellorModel_4;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_PropellorBlur
	Transform_t3600365921 * ___m_PropellorBlur_5;
	// UnityEngine.Texture2D[] UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_PropellorBlurTextures
	Texture2DU5BU5D_t149664596* ___m_PropellorBlurTextures_6;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_ThrottleBlurStart
	float ___m_ThrottleBlurStart_7;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_ThrottleBlurEnd
	float ___m_ThrottleBlurEnd_8;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_MaxRpm
	float ___m_MaxRpm_9;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_Plane
	AeroplaneController_t857819489 * ___m_Plane_10;
	// System.Int32 UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_PropellorBlurState
	int32_t ___m_PropellorBlurState_11;
	// UnityEngine.Renderer UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_PropellorModelRenderer
	Renderer_t2627027031 * ___m_PropellorModelRenderer_13;
	// UnityEngine.Renderer UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::m_PropellorBlurRenderer
	Renderer_t2627027031 * ___m_PropellorBlurRenderer_14;

public:
	inline static int32_t get_offset_of_m_PropellorModel_4() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_PropellorModel_4)); }
	inline Transform_t3600365921 * get_m_PropellorModel_4() const { return ___m_PropellorModel_4; }
	inline Transform_t3600365921 ** get_address_of_m_PropellorModel_4() { return &___m_PropellorModel_4; }
	inline void set_m_PropellorModel_4(Transform_t3600365921 * value)
	{
		___m_PropellorModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropellorModel_4), value);
	}

	inline static int32_t get_offset_of_m_PropellorBlur_5() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_PropellorBlur_5)); }
	inline Transform_t3600365921 * get_m_PropellorBlur_5() const { return ___m_PropellorBlur_5; }
	inline Transform_t3600365921 ** get_address_of_m_PropellorBlur_5() { return &___m_PropellorBlur_5; }
	inline void set_m_PropellorBlur_5(Transform_t3600365921 * value)
	{
		___m_PropellorBlur_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropellorBlur_5), value);
	}

	inline static int32_t get_offset_of_m_PropellorBlurTextures_6() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_PropellorBlurTextures_6)); }
	inline Texture2DU5BU5D_t149664596* get_m_PropellorBlurTextures_6() const { return ___m_PropellorBlurTextures_6; }
	inline Texture2DU5BU5D_t149664596** get_address_of_m_PropellorBlurTextures_6() { return &___m_PropellorBlurTextures_6; }
	inline void set_m_PropellorBlurTextures_6(Texture2DU5BU5D_t149664596* value)
	{
		___m_PropellorBlurTextures_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropellorBlurTextures_6), value);
	}

	inline static int32_t get_offset_of_m_ThrottleBlurStart_7() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_ThrottleBlurStart_7)); }
	inline float get_m_ThrottleBlurStart_7() const { return ___m_ThrottleBlurStart_7; }
	inline float* get_address_of_m_ThrottleBlurStart_7() { return &___m_ThrottleBlurStart_7; }
	inline void set_m_ThrottleBlurStart_7(float value)
	{
		___m_ThrottleBlurStart_7 = value;
	}

	inline static int32_t get_offset_of_m_ThrottleBlurEnd_8() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_ThrottleBlurEnd_8)); }
	inline float get_m_ThrottleBlurEnd_8() const { return ___m_ThrottleBlurEnd_8; }
	inline float* get_address_of_m_ThrottleBlurEnd_8() { return &___m_ThrottleBlurEnd_8; }
	inline void set_m_ThrottleBlurEnd_8(float value)
	{
		___m_ThrottleBlurEnd_8 = value;
	}

	inline static int32_t get_offset_of_m_MaxRpm_9() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_MaxRpm_9)); }
	inline float get_m_MaxRpm_9() const { return ___m_MaxRpm_9; }
	inline float* get_address_of_m_MaxRpm_9() { return &___m_MaxRpm_9; }
	inline void set_m_MaxRpm_9(float value)
	{
		___m_MaxRpm_9 = value;
	}

	inline static int32_t get_offset_of_m_Plane_10() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_Plane_10)); }
	inline AeroplaneController_t857819489 * get_m_Plane_10() const { return ___m_Plane_10; }
	inline AeroplaneController_t857819489 ** get_address_of_m_Plane_10() { return &___m_Plane_10; }
	inline void set_m_Plane_10(AeroplaneController_t857819489 * value)
	{
		___m_Plane_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_10), value);
	}

	inline static int32_t get_offset_of_m_PropellorBlurState_11() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_PropellorBlurState_11)); }
	inline int32_t get_m_PropellorBlurState_11() const { return ___m_PropellorBlurState_11; }
	inline int32_t* get_address_of_m_PropellorBlurState_11() { return &___m_PropellorBlurState_11; }
	inline void set_m_PropellorBlurState_11(int32_t value)
	{
		___m_PropellorBlurState_11 = value;
	}

	inline static int32_t get_offset_of_m_PropellorModelRenderer_13() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_PropellorModelRenderer_13)); }
	inline Renderer_t2627027031 * get_m_PropellorModelRenderer_13() const { return ___m_PropellorModelRenderer_13; }
	inline Renderer_t2627027031 ** get_address_of_m_PropellorModelRenderer_13() { return &___m_PropellorModelRenderer_13; }
	inline void set_m_PropellorModelRenderer_13(Renderer_t2627027031 * value)
	{
		___m_PropellorModelRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropellorModelRenderer_13), value);
	}

	inline static int32_t get_offset_of_m_PropellorBlurRenderer_14() { return static_cast<int32_t>(offsetof(AeroplanePropellerAnimator_t2205181023, ___m_PropellorBlurRenderer_14)); }
	inline Renderer_t2627027031 * get_m_PropellorBlurRenderer_14() const { return ___m_PropellorBlurRenderer_14; }
	inline Renderer_t2627027031 ** get_address_of_m_PropellorBlurRenderer_14() { return &___m_PropellorBlurRenderer_14; }
	inline void set_m_PropellorBlurRenderer_14(Renderer_t2627027031 * value)
	{
		___m_PropellorBlurRenderer_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropellorBlurRenderer_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEROPLANEPROPELLERANIMATOR_T2205181023_H
#ifndef AEROPLANEUSERCONTROL2AXIS_T599233410_H
#define AEROPLANEUSERCONTROL2AXIS_T599233410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis
struct  AeroplaneUserControl2Axis_t599233410  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::maxRollAngle
	float ___maxRollAngle_4;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::maxPitchAngle
	float ___maxPitchAngle_5;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::m_Aeroplane
	AeroplaneController_t857819489 * ___m_Aeroplane_6;

public:
	inline static int32_t get_offset_of_maxRollAngle_4() { return static_cast<int32_t>(offsetof(AeroplaneUserControl2Axis_t599233410, ___maxRollAngle_4)); }
	inline float get_maxRollAngle_4() const { return ___maxRollAngle_4; }
	inline float* get_address_of_maxRollAngle_4() { return &___maxRollAngle_4; }
	inline void set_maxRollAngle_4(float value)
	{
		___maxRollAngle_4 = value;
	}

	inline static int32_t get_offset_of_maxPitchAngle_5() { return static_cast<int32_t>(offsetof(AeroplaneUserControl2Axis_t599233410, ___maxPitchAngle_5)); }
	inline float get_maxPitchAngle_5() const { return ___maxPitchAngle_5; }
	inline float* get_address_of_maxPitchAngle_5() { return &___maxPitchAngle_5; }
	inline void set_maxPitchAngle_5(float value)
	{
		___maxPitchAngle_5 = value;
	}

	inline static int32_t get_offset_of_m_Aeroplane_6() { return static_cast<int32_t>(offsetof(AeroplaneUserControl2Axis_t599233410, ___m_Aeroplane_6)); }
	inline AeroplaneController_t857819489 * get_m_Aeroplane_6() const { return ___m_Aeroplane_6; }
	inline AeroplaneController_t857819489 ** get_address_of_m_Aeroplane_6() { return &___m_Aeroplane_6; }
	inline void set_m_Aeroplane_6(AeroplaneController_t857819489 * value)
	{
		___m_Aeroplane_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Aeroplane_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEROPLANEUSERCONTROL2AXIS_T599233410_H
#ifndef AEROPLANEUSERCONTROL4AXIS_T4090860418_H
#define AEROPLANEUSERCONTROL4AXIS_T4090860418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis
struct  AeroplaneUserControl4Axis_t4090860418  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::maxRollAngle
	float ___maxRollAngle_4;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::maxPitchAngle
	float ___maxPitchAngle_5;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::m_Aeroplane
	AeroplaneController_t857819489 * ___m_Aeroplane_6;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::m_Throttle
	float ___m_Throttle_7;
	// System.Boolean UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::m_AirBrakes
	bool ___m_AirBrakes_8;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::m_Yaw
	float ___m_Yaw_9;

public:
	inline static int32_t get_offset_of_maxRollAngle_4() { return static_cast<int32_t>(offsetof(AeroplaneUserControl4Axis_t4090860418, ___maxRollAngle_4)); }
	inline float get_maxRollAngle_4() const { return ___maxRollAngle_4; }
	inline float* get_address_of_maxRollAngle_4() { return &___maxRollAngle_4; }
	inline void set_maxRollAngle_4(float value)
	{
		___maxRollAngle_4 = value;
	}

	inline static int32_t get_offset_of_maxPitchAngle_5() { return static_cast<int32_t>(offsetof(AeroplaneUserControl4Axis_t4090860418, ___maxPitchAngle_5)); }
	inline float get_maxPitchAngle_5() const { return ___maxPitchAngle_5; }
	inline float* get_address_of_maxPitchAngle_5() { return &___maxPitchAngle_5; }
	inline void set_maxPitchAngle_5(float value)
	{
		___maxPitchAngle_5 = value;
	}

	inline static int32_t get_offset_of_m_Aeroplane_6() { return static_cast<int32_t>(offsetof(AeroplaneUserControl4Axis_t4090860418, ___m_Aeroplane_6)); }
	inline AeroplaneController_t857819489 * get_m_Aeroplane_6() const { return ___m_Aeroplane_6; }
	inline AeroplaneController_t857819489 ** get_address_of_m_Aeroplane_6() { return &___m_Aeroplane_6; }
	inline void set_m_Aeroplane_6(AeroplaneController_t857819489 * value)
	{
		___m_Aeroplane_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Aeroplane_6), value);
	}

	inline static int32_t get_offset_of_m_Throttle_7() { return static_cast<int32_t>(offsetof(AeroplaneUserControl4Axis_t4090860418, ___m_Throttle_7)); }
	inline float get_m_Throttle_7() const { return ___m_Throttle_7; }
	inline float* get_address_of_m_Throttle_7() { return &___m_Throttle_7; }
	inline void set_m_Throttle_7(float value)
	{
		___m_Throttle_7 = value;
	}

	inline static int32_t get_offset_of_m_AirBrakes_8() { return static_cast<int32_t>(offsetof(AeroplaneUserControl4Axis_t4090860418, ___m_AirBrakes_8)); }
	inline bool get_m_AirBrakes_8() const { return ___m_AirBrakes_8; }
	inline bool* get_address_of_m_AirBrakes_8() { return &___m_AirBrakes_8; }
	inline void set_m_AirBrakes_8(bool value)
	{
		___m_AirBrakes_8 = value;
	}

	inline static int32_t get_offset_of_m_Yaw_9() { return static_cast<int32_t>(offsetof(AeroplaneUserControl4Axis_t4090860418, ___m_Yaw_9)); }
	inline float get_m_Yaw_9() const { return ___m_Yaw_9; }
	inline float* get_address_of_m_Yaw_9() { return &___m_Yaw_9; }
	inline void set_m_Yaw_9(float value)
	{
		___m_Yaw_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEROPLANEUSERCONTROL4AXIS_T4090860418_H
#ifndef JETPARTICLEEFFECT_T772215299_H
#define JETPARTICLEEFFECT_T772215299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect
struct  JetParticleEffect_t772215299  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::minColour
	Color_t2555686324  ___minColour_4;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::m_Jet
	AeroplaneController_t857819489 * ___m_Jet_5;
	// UnityEngine.ParticleSystem UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::m_System
	ParticleSystem_t1800779281 * ___m_System_6;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::m_OriginalStartSize
	float ___m_OriginalStartSize_7;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::m_OriginalLifetime
	float ___m_OriginalLifetime_8;
	// UnityEngine.Color UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::m_OriginalStartColor
	Color_t2555686324  ___m_OriginalStartColor_9;

public:
	inline static int32_t get_offset_of_minColour_4() { return static_cast<int32_t>(offsetof(JetParticleEffect_t772215299, ___minColour_4)); }
	inline Color_t2555686324  get_minColour_4() const { return ___minColour_4; }
	inline Color_t2555686324 * get_address_of_minColour_4() { return &___minColour_4; }
	inline void set_minColour_4(Color_t2555686324  value)
	{
		___minColour_4 = value;
	}

	inline static int32_t get_offset_of_m_Jet_5() { return static_cast<int32_t>(offsetof(JetParticleEffect_t772215299, ___m_Jet_5)); }
	inline AeroplaneController_t857819489 * get_m_Jet_5() const { return ___m_Jet_5; }
	inline AeroplaneController_t857819489 ** get_address_of_m_Jet_5() { return &___m_Jet_5; }
	inline void set_m_Jet_5(AeroplaneController_t857819489 * value)
	{
		___m_Jet_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Jet_5), value);
	}

	inline static int32_t get_offset_of_m_System_6() { return static_cast<int32_t>(offsetof(JetParticleEffect_t772215299, ___m_System_6)); }
	inline ParticleSystem_t1800779281 * get_m_System_6() const { return ___m_System_6; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_System_6() { return &___m_System_6; }
	inline void set_m_System_6(ParticleSystem_t1800779281 * value)
	{
		___m_System_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_System_6), value);
	}

	inline static int32_t get_offset_of_m_OriginalStartSize_7() { return static_cast<int32_t>(offsetof(JetParticleEffect_t772215299, ___m_OriginalStartSize_7)); }
	inline float get_m_OriginalStartSize_7() const { return ___m_OriginalStartSize_7; }
	inline float* get_address_of_m_OriginalStartSize_7() { return &___m_OriginalStartSize_7; }
	inline void set_m_OriginalStartSize_7(float value)
	{
		___m_OriginalStartSize_7 = value;
	}

	inline static int32_t get_offset_of_m_OriginalLifetime_8() { return static_cast<int32_t>(offsetof(JetParticleEffect_t772215299, ___m_OriginalLifetime_8)); }
	inline float get_m_OriginalLifetime_8() const { return ___m_OriginalLifetime_8; }
	inline float* get_address_of_m_OriginalLifetime_8() { return &___m_OriginalLifetime_8; }
	inline void set_m_OriginalLifetime_8(float value)
	{
		___m_OriginalLifetime_8 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStartColor_9() { return static_cast<int32_t>(offsetof(JetParticleEffect_t772215299, ___m_OriginalStartColor_9)); }
	inline Color_t2555686324  get_m_OriginalStartColor_9() const { return ___m_OriginalStartColor_9; }
	inline Color_t2555686324 * get_address_of_m_OriginalStartColor_9() { return &___m_OriginalStartColor_9; }
	inline void set_m_OriginalStartColor_9(Color_t2555686324  value)
	{
		___m_OriginalStartColor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JETPARTICLEEFFECT_T772215299_H
#ifndef LANDINGGEAR_T786871954_H
#define LANDINGGEAR_T786871954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Aeroplane.LandingGear
struct  LandingGear_t786871954  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.LandingGear::raiseAtAltitude
	float ___raiseAtAltitude_4;
	// System.Single UnityStandardAssets.Vehicles.Aeroplane.LandingGear::lowerAtAltitude
	float ___lowerAtAltitude_5;
	// UnityStandardAssets.Vehicles.Aeroplane.LandingGear/GearState UnityStandardAssets.Vehicles.Aeroplane.LandingGear::m_State
	int32_t ___m_State_6;
	// UnityEngine.Animator UnityStandardAssets.Vehicles.Aeroplane.LandingGear::m_Animator
	Animator_t434523843 * ___m_Animator_7;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Aeroplane.LandingGear::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_8;
	// UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.LandingGear::m_Plane
	AeroplaneController_t857819489 * ___m_Plane_9;

public:
	inline static int32_t get_offset_of_raiseAtAltitude_4() { return static_cast<int32_t>(offsetof(LandingGear_t786871954, ___raiseAtAltitude_4)); }
	inline float get_raiseAtAltitude_4() const { return ___raiseAtAltitude_4; }
	inline float* get_address_of_raiseAtAltitude_4() { return &___raiseAtAltitude_4; }
	inline void set_raiseAtAltitude_4(float value)
	{
		___raiseAtAltitude_4 = value;
	}

	inline static int32_t get_offset_of_lowerAtAltitude_5() { return static_cast<int32_t>(offsetof(LandingGear_t786871954, ___lowerAtAltitude_5)); }
	inline float get_lowerAtAltitude_5() const { return ___lowerAtAltitude_5; }
	inline float* get_address_of_lowerAtAltitude_5() { return &___lowerAtAltitude_5; }
	inline void set_lowerAtAltitude_5(float value)
	{
		___lowerAtAltitude_5 = value;
	}

	inline static int32_t get_offset_of_m_State_6() { return static_cast<int32_t>(offsetof(LandingGear_t786871954, ___m_State_6)); }
	inline int32_t get_m_State_6() const { return ___m_State_6; }
	inline int32_t* get_address_of_m_State_6() { return &___m_State_6; }
	inline void set_m_State_6(int32_t value)
	{
		___m_State_6 = value;
	}

	inline static int32_t get_offset_of_m_Animator_7() { return static_cast<int32_t>(offsetof(LandingGear_t786871954, ___m_Animator_7)); }
	inline Animator_t434523843 * get_m_Animator_7() const { return ___m_Animator_7; }
	inline Animator_t434523843 ** get_address_of_m_Animator_7() { return &___m_Animator_7; }
	inline void set_m_Animator_7(Animator_t434523843 * value)
	{
		___m_Animator_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_7), value);
	}

	inline static int32_t get_offset_of_m_Rigidbody_8() { return static_cast<int32_t>(offsetof(LandingGear_t786871954, ___m_Rigidbody_8)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_8() const { return ___m_Rigidbody_8; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_8() { return &___m_Rigidbody_8; }
	inline void set_m_Rigidbody_8(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_8), value);
	}

	inline static int32_t get_offset_of_m_Plane_9() { return static_cast<int32_t>(offsetof(LandingGear_t786871954, ___m_Plane_9)); }
	inline AeroplaneController_t857819489 * get_m_Plane_9() const { return ___m_Plane_9; }
	inline AeroplaneController_t857819489 ** get_address_of_m_Plane_9() { return &___m_Plane_9; }
	inline void set_m_Plane_9(AeroplaneController_t857819489 * value)
	{
		___m_Plane_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANDINGGEAR_T786871954_H
#ifndef BRAKELIGHT_T2229864552_H
#define BRAKELIGHT_T2229864552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.BrakeLight
struct  BrakeLight_t2229864552  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Car.CarController UnityStandardAssets.Vehicles.Car.BrakeLight::car
	CarController_t4228304488 * ___car_4;
	// UnityEngine.Renderer UnityStandardAssets.Vehicles.Car.BrakeLight::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_5;

public:
	inline static int32_t get_offset_of_car_4() { return static_cast<int32_t>(offsetof(BrakeLight_t2229864552, ___car_4)); }
	inline CarController_t4228304488 * get_car_4() const { return ___car_4; }
	inline CarController_t4228304488 ** get_address_of_car_4() { return &___car_4; }
	inline void set_car_4(CarController_t4228304488 * value)
	{
		___car_4 = value;
		Il2CppCodeGenWriteBarrier((&___car_4), value);
	}

	inline static int32_t get_offset_of_m_Renderer_5() { return static_cast<int32_t>(offsetof(BrakeLight_t2229864552, ___m_Renderer_5)); }
	inline Renderer_t2627027031 * get_m_Renderer_5() const { return ___m_Renderer_5; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_5() { return &___m_Renderer_5; }
	inline void set_m_Renderer_5(Renderer_t2627027031 * value)
	{
		___m_Renderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAKELIGHT_T2229864552_H
#ifndef CARAICONTROL_T1541705461_H
#define CARAICONTROL_T1541705461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarAIControl
struct  CarAIControl_t1541705461  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_CautiousSpeedFactor
	float ___m_CautiousSpeedFactor_4;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_CautiousMaxAngle
	float ___m_CautiousMaxAngle_5;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_CautiousMaxDistance
	float ___m_CautiousMaxDistance_6;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_CautiousAngularVelocityFactor
	float ___m_CautiousAngularVelocityFactor_7;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_SteerSensitivity
	float ___m_SteerSensitivity_8;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_AccelSensitivity
	float ___m_AccelSensitivity_9;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_BrakeSensitivity
	float ___m_BrakeSensitivity_10;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_LateralWanderDistance
	float ___m_LateralWanderDistance_11;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_LateralWanderSpeed
	float ___m_LateralWanderSpeed_12;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_AccelWanderAmount
	float ___m_AccelWanderAmount_13;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_AccelWanderSpeed
	float ___m_AccelWanderSpeed_14;
	// UnityStandardAssets.Vehicles.Car.CarAIControl/BrakeCondition UnityStandardAssets.Vehicles.Car.CarAIControl::m_BrakeCondition
	int32_t ___m_BrakeCondition_15;
	// System.Boolean UnityStandardAssets.Vehicles.Car.CarAIControl::m_Driving
	bool ___m_Driving_16;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Car.CarAIControl::m_Target
	Transform_t3600365921 * ___m_Target_17;
	// System.Boolean UnityStandardAssets.Vehicles.Car.CarAIControl::m_StopWhenTargetReached
	bool ___m_StopWhenTargetReached_18;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_ReachTargetThreshold
	float ___m_ReachTargetThreshold_19;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_RandomPerlin
	float ___m_RandomPerlin_20;
	// UnityStandardAssets.Vehicles.Car.CarController UnityStandardAssets.Vehicles.Car.CarAIControl::m_CarController
	CarController_t4228304488 * ___m_CarController_21;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_AvoidOtherCarTime
	float ___m_AvoidOtherCarTime_22;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_AvoidOtherCarSlowdown
	float ___m_AvoidOtherCarSlowdown_23;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAIControl::m_AvoidPathOffset
	float ___m_AvoidPathOffset_24;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Car.CarAIControl::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_25;

public:
	inline static int32_t get_offset_of_m_CautiousSpeedFactor_4() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_CautiousSpeedFactor_4)); }
	inline float get_m_CautiousSpeedFactor_4() const { return ___m_CautiousSpeedFactor_4; }
	inline float* get_address_of_m_CautiousSpeedFactor_4() { return &___m_CautiousSpeedFactor_4; }
	inline void set_m_CautiousSpeedFactor_4(float value)
	{
		___m_CautiousSpeedFactor_4 = value;
	}

	inline static int32_t get_offset_of_m_CautiousMaxAngle_5() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_CautiousMaxAngle_5)); }
	inline float get_m_CautiousMaxAngle_5() const { return ___m_CautiousMaxAngle_5; }
	inline float* get_address_of_m_CautiousMaxAngle_5() { return &___m_CautiousMaxAngle_5; }
	inline void set_m_CautiousMaxAngle_5(float value)
	{
		___m_CautiousMaxAngle_5 = value;
	}

	inline static int32_t get_offset_of_m_CautiousMaxDistance_6() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_CautiousMaxDistance_6)); }
	inline float get_m_CautiousMaxDistance_6() const { return ___m_CautiousMaxDistance_6; }
	inline float* get_address_of_m_CautiousMaxDistance_6() { return &___m_CautiousMaxDistance_6; }
	inline void set_m_CautiousMaxDistance_6(float value)
	{
		___m_CautiousMaxDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_CautiousAngularVelocityFactor_7() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_CautiousAngularVelocityFactor_7)); }
	inline float get_m_CautiousAngularVelocityFactor_7() const { return ___m_CautiousAngularVelocityFactor_7; }
	inline float* get_address_of_m_CautiousAngularVelocityFactor_7() { return &___m_CautiousAngularVelocityFactor_7; }
	inline void set_m_CautiousAngularVelocityFactor_7(float value)
	{
		___m_CautiousAngularVelocityFactor_7 = value;
	}

	inline static int32_t get_offset_of_m_SteerSensitivity_8() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_SteerSensitivity_8)); }
	inline float get_m_SteerSensitivity_8() const { return ___m_SteerSensitivity_8; }
	inline float* get_address_of_m_SteerSensitivity_8() { return &___m_SteerSensitivity_8; }
	inline void set_m_SteerSensitivity_8(float value)
	{
		___m_SteerSensitivity_8 = value;
	}

	inline static int32_t get_offset_of_m_AccelSensitivity_9() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_AccelSensitivity_9)); }
	inline float get_m_AccelSensitivity_9() const { return ___m_AccelSensitivity_9; }
	inline float* get_address_of_m_AccelSensitivity_9() { return &___m_AccelSensitivity_9; }
	inline void set_m_AccelSensitivity_9(float value)
	{
		___m_AccelSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_BrakeSensitivity_10() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_BrakeSensitivity_10)); }
	inline float get_m_BrakeSensitivity_10() const { return ___m_BrakeSensitivity_10; }
	inline float* get_address_of_m_BrakeSensitivity_10() { return &___m_BrakeSensitivity_10; }
	inline void set_m_BrakeSensitivity_10(float value)
	{
		___m_BrakeSensitivity_10 = value;
	}

	inline static int32_t get_offset_of_m_LateralWanderDistance_11() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_LateralWanderDistance_11)); }
	inline float get_m_LateralWanderDistance_11() const { return ___m_LateralWanderDistance_11; }
	inline float* get_address_of_m_LateralWanderDistance_11() { return &___m_LateralWanderDistance_11; }
	inline void set_m_LateralWanderDistance_11(float value)
	{
		___m_LateralWanderDistance_11 = value;
	}

	inline static int32_t get_offset_of_m_LateralWanderSpeed_12() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_LateralWanderSpeed_12)); }
	inline float get_m_LateralWanderSpeed_12() const { return ___m_LateralWanderSpeed_12; }
	inline float* get_address_of_m_LateralWanderSpeed_12() { return &___m_LateralWanderSpeed_12; }
	inline void set_m_LateralWanderSpeed_12(float value)
	{
		___m_LateralWanderSpeed_12 = value;
	}

	inline static int32_t get_offset_of_m_AccelWanderAmount_13() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_AccelWanderAmount_13)); }
	inline float get_m_AccelWanderAmount_13() const { return ___m_AccelWanderAmount_13; }
	inline float* get_address_of_m_AccelWanderAmount_13() { return &___m_AccelWanderAmount_13; }
	inline void set_m_AccelWanderAmount_13(float value)
	{
		___m_AccelWanderAmount_13 = value;
	}

	inline static int32_t get_offset_of_m_AccelWanderSpeed_14() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_AccelWanderSpeed_14)); }
	inline float get_m_AccelWanderSpeed_14() const { return ___m_AccelWanderSpeed_14; }
	inline float* get_address_of_m_AccelWanderSpeed_14() { return &___m_AccelWanderSpeed_14; }
	inline void set_m_AccelWanderSpeed_14(float value)
	{
		___m_AccelWanderSpeed_14 = value;
	}

	inline static int32_t get_offset_of_m_BrakeCondition_15() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_BrakeCondition_15)); }
	inline int32_t get_m_BrakeCondition_15() const { return ___m_BrakeCondition_15; }
	inline int32_t* get_address_of_m_BrakeCondition_15() { return &___m_BrakeCondition_15; }
	inline void set_m_BrakeCondition_15(int32_t value)
	{
		___m_BrakeCondition_15 = value;
	}

	inline static int32_t get_offset_of_m_Driving_16() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_Driving_16)); }
	inline bool get_m_Driving_16() const { return ___m_Driving_16; }
	inline bool* get_address_of_m_Driving_16() { return &___m_Driving_16; }
	inline void set_m_Driving_16(bool value)
	{
		___m_Driving_16 = value;
	}

	inline static int32_t get_offset_of_m_Target_17() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_Target_17)); }
	inline Transform_t3600365921 * get_m_Target_17() const { return ___m_Target_17; }
	inline Transform_t3600365921 ** get_address_of_m_Target_17() { return &___m_Target_17; }
	inline void set_m_Target_17(Transform_t3600365921 * value)
	{
		___m_Target_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_17), value);
	}

	inline static int32_t get_offset_of_m_StopWhenTargetReached_18() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_StopWhenTargetReached_18)); }
	inline bool get_m_StopWhenTargetReached_18() const { return ___m_StopWhenTargetReached_18; }
	inline bool* get_address_of_m_StopWhenTargetReached_18() { return &___m_StopWhenTargetReached_18; }
	inline void set_m_StopWhenTargetReached_18(bool value)
	{
		___m_StopWhenTargetReached_18 = value;
	}

	inline static int32_t get_offset_of_m_ReachTargetThreshold_19() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_ReachTargetThreshold_19)); }
	inline float get_m_ReachTargetThreshold_19() const { return ___m_ReachTargetThreshold_19; }
	inline float* get_address_of_m_ReachTargetThreshold_19() { return &___m_ReachTargetThreshold_19; }
	inline void set_m_ReachTargetThreshold_19(float value)
	{
		___m_ReachTargetThreshold_19 = value;
	}

	inline static int32_t get_offset_of_m_RandomPerlin_20() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_RandomPerlin_20)); }
	inline float get_m_RandomPerlin_20() const { return ___m_RandomPerlin_20; }
	inline float* get_address_of_m_RandomPerlin_20() { return &___m_RandomPerlin_20; }
	inline void set_m_RandomPerlin_20(float value)
	{
		___m_RandomPerlin_20 = value;
	}

	inline static int32_t get_offset_of_m_CarController_21() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_CarController_21)); }
	inline CarController_t4228304488 * get_m_CarController_21() const { return ___m_CarController_21; }
	inline CarController_t4228304488 ** get_address_of_m_CarController_21() { return &___m_CarController_21; }
	inline void set_m_CarController_21(CarController_t4228304488 * value)
	{
		___m_CarController_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_CarController_21), value);
	}

	inline static int32_t get_offset_of_m_AvoidOtherCarTime_22() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_AvoidOtherCarTime_22)); }
	inline float get_m_AvoidOtherCarTime_22() const { return ___m_AvoidOtherCarTime_22; }
	inline float* get_address_of_m_AvoidOtherCarTime_22() { return &___m_AvoidOtherCarTime_22; }
	inline void set_m_AvoidOtherCarTime_22(float value)
	{
		___m_AvoidOtherCarTime_22 = value;
	}

	inline static int32_t get_offset_of_m_AvoidOtherCarSlowdown_23() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_AvoidOtherCarSlowdown_23)); }
	inline float get_m_AvoidOtherCarSlowdown_23() const { return ___m_AvoidOtherCarSlowdown_23; }
	inline float* get_address_of_m_AvoidOtherCarSlowdown_23() { return &___m_AvoidOtherCarSlowdown_23; }
	inline void set_m_AvoidOtherCarSlowdown_23(float value)
	{
		___m_AvoidOtherCarSlowdown_23 = value;
	}

	inline static int32_t get_offset_of_m_AvoidPathOffset_24() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_AvoidPathOffset_24)); }
	inline float get_m_AvoidPathOffset_24() const { return ___m_AvoidPathOffset_24; }
	inline float* get_address_of_m_AvoidPathOffset_24() { return &___m_AvoidPathOffset_24; }
	inline void set_m_AvoidPathOffset_24(float value)
	{
		___m_AvoidPathOffset_24 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_25() { return static_cast<int32_t>(offsetof(CarAIControl_t1541705461, ___m_Rigidbody_25)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_25() const { return ___m_Rigidbody_25; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_25() { return &___m_Rigidbody_25; }
	inline void set_m_Rigidbody_25(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARAICONTROL_T1541705461_H
#ifndef CARAUDIO_T499525394_H
#define CARAUDIO_T499525394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarAudio
struct  CarAudio_t499525394  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Car.CarAudio/EngineAudioOptions UnityStandardAssets.Vehicles.Car.CarAudio::engineSoundStyle
	int32_t ___engineSoundStyle_4;
	// UnityEngine.AudioClip UnityStandardAssets.Vehicles.Car.CarAudio::lowAccelClip
	AudioClip_t3680889665 * ___lowAccelClip_5;
	// UnityEngine.AudioClip UnityStandardAssets.Vehicles.Car.CarAudio::lowDecelClip
	AudioClip_t3680889665 * ___lowDecelClip_6;
	// UnityEngine.AudioClip UnityStandardAssets.Vehicles.Car.CarAudio::highAccelClip
	AudioClip_t3680889665 * ___highAccelClip_7;
	// UnityEngine.AudioClip UnityStandardAssets.Vehicles.Car.CarAudio::highDecelClip
	AudioClip_t3680889665 * ___highDecelClip_8;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAudio::pitchMultiplier
	float ___pitchMultiplier_9;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAudio::lowPitchMin
	float ___lowPitchMin_10;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAudio::lowPitchMax
	float ___lowPitchMax_11;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAudio::highPitchMultiplier
	float ___highPitchMultiplier_12;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAudio::maxRolloffDistance
	float ___maxRolloffDistance_13;
	// System.Single UnityStandardAssets.Vehicles.Car.CarAudio::dopplerLevel
	float ___dopplerLevel_14;
	// System.Boolean UnityStandardAssets.Vehicles.Car.CarAudio::useDoppler
	bool ___useDoppler_15;
	// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.CarAudio::m_LowAccel
	AudioSource_t3935305588 * ___m_LowAccel_16;
	// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.CarAudio::m_LowDecel
	AudioSource_t3935305588 * ___m_LowDecel_17;
	// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.CarAudio::m_HighAccel
	AudioSource_t3935305588 * ___m_HighAccel_18;
	// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.CarAudio::m_HighDecel
	AudioSource_t3935305588 * ___m_HighDecel_19;
	// System.Boolean UnityStandardAssets.Vehicles.Car.CarAudio::m_StartedSound
	bool ___m_StartedSound_20;
	// UnityStandardAssets.Vehicles.Car.CarController UnityStandardAssets.Vehicles.Car.CarAudio::m_CarController
	CarController_t4228304488 * ___m_CarController_21;

public:
	inline static int32_t get_offset_of_engineSoundStyle_4() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___engineSoundStyle_4)); }
	inline int32_t get_engineSoundStyle_4() const { return ___engineSoundStyle_4; }
	inline int32_t* get_address_of_engineSoundStyle_4() { return &___engineSoundStyle_4; }
	inline void set_engineSoundStyle_4(int32_t value)
	{
		___engineSoundStyle_4 = value;
	}

	inline static int32_t get_offset_of_lowAccelClip_5() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___lowAccelClip_5)); }
	inline AudioClip_t3680889665 * get_lowAccelClip_5() const { return ___lowAccelClip_5; }
	inline AudioClip_t3680889665 ** get_address_of_lowAccelClip_5() { return &___lowAccelClip_5; }
	inline void set_lowAccelClip_5(AudioClip_t3680889665 * value)
	{
		___lowAccelClip_5 = value;
		Il2CppCodeGenWriteBarrier((&___lowAccelClip_5), value);
	}

	inline static int32_t get_offset_of_lowDecelClip_6() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___lowDecelClip_6)); }
	inline AudioClip_t3680889665 * get_lowDecelClip_6() const { return ___lowDecelClip_6; }
	inline AudioClip_t3680889665 ** get_address_of_lowDecelClip_6() { return &___lowDecelClip_6; }
	inline void set_lowDecelClip_6(AudioClip_t3680889665 * value)
	{
		___lowDecelClip_6 = value;
		Il2CppCodeGenWriteBarrier((&___lowDecelClip_6), value);
	}

	inline static int32_t get_offset_of_highAccelClip_7() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___highAccelClip_7)); }
	inline AudioClip_t3680889665 * get_highAccelClip_7() const { return ___highAccelClip_7; }
	inline AudioClip_t3680889665 ** get_address_of_highAccelClip_7() { return &___highAccelClip_7; }
	inline void set_highAccelClip_7(AudioClip_t3680889665 * value)
	{
		___highAccelClip_7 = value;
		Il2CppCodeGenWriteBarrier((&___highAccelClip_7), value);
	}

	inline static int32_t get_offset_of_highDecelClip_8() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___highDecelClip_8)); }
	inline AudioClip_t3680889665 * get_highDecelClip_8() const { return ___highDecelClip_8; }
	inline AudioClip_t3680889665 ** get_address_of_highDecelClip_8() { return &___highDecelClip_8; }
	inline void set_highDecelClip_8(AudioClip_t3680889665 * value)
	{
		___highDecelClip_8 = value;
		Il2CppCodeGenWriteBarrier((&___highDecelClip_8), value);
	}

	inline static int32_t get_offset_of_pitchMultiplier_9() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___pitchMultiplier_9)); }
	inline float get_pitchMultiplier_9() const { return ___pitchMultiplier_9; }
	inline float* get_address_of_pitchMultiplier_9() { return &___pitchMultiplier_9; }
	inline void set_pitchMultiplier_9(float value)
	{
		___pitchMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_lowPitchMin_10() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___lowPitchMin_10)); }
	inline float get_lowPitchMin_10() const { return ___lowPitchMin_10; }
	inline float* get_address_of_lowPitchMin_10() { return &___lowPitchMin_10; }
	inline void set_lowPitchMin_10(float value)
	{
		___lowPitchMin_10 = value;
	}

	inline static int32_t get_offset_of_lowPitchMax_11() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___lowPitchMax_11)); }
	inline float get_lowPitchMax_11() const { return ___lowPitchMax_11; }
	inline float* get_address_of_lowPitchMax_11() { return &___lowPitchMax_11; }
	inline void set_lowPitchMax_11(float value)
	{
		___lowPitchMax_11 = value;
	}

	inline static int32_t get_offset_of_highPitchMultiplier_12() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___highPitchMultiplier_12)); }
	inline float get_highPitchMultiplier_12() const { return ___highPitchMultiplier_12; }
	inline float* get_address_of_highPitchMultiplier_12() { return &___highPitchMultiplier_12; }
	inline void set_highPitchMultiplier_12(float value)
	{
		___highPitchMultiplier_12 = value;
	}

	inline static int32_t get_offset_of_maxRolloffDistance_13() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___maxRolloffDistance_13)); }
	inline float get_maxRolloffDistance_13() const { return ___maxRolloffDistance_13; }
	inline float* get_address_of_maxRolloffDistance_13() { return &___maxRolloffDistance_13; }
	inline void set_maxRolloffDistance_13(float value)
	{
		___maxRolloffDistance_13 = value;
	}

	inline static int32_t get_offset_of_dopplerLevel_14() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___dopplerLevel_14)); }
	inline float get_dopplerLevel_14() const { return ___dopplerLevel_14; }
	inline float* get_address_of_dopplerLevel_14() { return &___dopplerLevel_14; }
	inline void set_dopplerLevel_14(float value)
	{
		___dopplerLevel_14 = value;
	}

	inline static int32_t get_offset_of_useDoppler_15() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___useDoppler_15)); }
	inline bool get_useDoppler_15() const { return ___useDoppler_15; }
	inline bool* get_address_of_useDoppler_15() { return &___useDoppler_15; }
	inline void set_useDoppler_15(bool value)
	{
		___useDoppler_15 = value;
	}

	inline static int32_t get_offset_of_m_LowAccel_16() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___m_LowAccel_16)); }
	inline AudioSource_t3935305588 * get_m_LowAccel_16() const { return ___m_LowAccel_16; }
	inline AudioSource_t3935305588 ** get_address_of_m_LowAccel_16() { return &___m_LowAccel_16; }
	inline void set_m_LowAccel_16(AudioSource_t3935305588 * value)
	{
		___m_LowAccel_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_LowAccel_16), value);
	}

	inline static int32_t get_offset_of_m_LowDecel_17() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___m_LowDecel_17)); }
	inline AudioSource_t3935305588 * get_m_LowDecel_17() const { return ___m_LowDecel_17; }
	inline AudioSource_t3935305588 ** get_address_of_m_LowDecel_17() { return &___m_LowDecel_17; }
	inline void set_m_LowDecel_17(AudioSource_t3935305588 * value)
	{
		___m_LowDecel_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_LowDecel_17), value);
	}

	inline static int32_t get_offset_of_m_HighAccel_18() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___m_HighAccel_18)); }
	inline AudioSource_t3935305588 * get_m_HighAccel_18() const { return ___m_HighAccel_18; }
	inline AudioSource_t3935305588 ** get_address_of_m_HighAccel_18() { return &___m_HighAccel_18; }
	inline void set_m_HighAccel_18(AudioSource_t3935305588 * value)
	{
		___m_HighAccel_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighAccel_18), value);
	}

	inline static int32_t get_offset_of_m_HighDecel_19() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___m_HighDecel_19)); }
	inline AudioSource_t3935305588 * get_m_HighDecel_19() const { return ___m_HighDecel_19; }
	inline AudioSource_t3935305588 ** get_address_of_m_HighDecel_19() { return &___m_HighDecel_19; }
	inline void set_m_HighDecel_19(AudioSource_t3935305588 * value)
	{
		___m_HighDecel_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighDecel_19), value);
	}

	inline static int32_t get_offset_of_m_StartedSound_20() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___m_StartedSound_20)); }
	inline bool get_m_StartedSound_20() const { return ___m_StartedSound_20; }
	inline bool* get_address_of_m_StartedSound_20() { return &___m_StartedSound_20; }
	inline void set_m_StartedSound_20(bool value)
	{
		___m_StartedSound_20 = value;
	}

	inline static int32_t get_offset_of_m_CarController_21() { return static_cast<int32_t>(offsetof(CarAudio_t499525394, ___m_CarController_21)); }
	inline CarController_t4228304488 * get_m_CarController_21() const { return ___m_CarController_21; }
	inline CarController_t4228304488 ** get_address_of_m_CarController_21() { return &___m_CarController_21; }
	inline void set_m_CarController_21(CarController_t4228304488 * value)
	{
		___m_CarController_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_CarController_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARAUDIO_T499525394_H
#ifndef CARCONTROLLER_T4228304488_H
#define CARCONTROLLER_T4228304488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarController
struct  CarController_t4228304488  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Car.CarDriveType UnityStandardAssets.Vehicles.Car.CarController::m_CarDriveType
	int32_t ___m_CarDriveType_4;
	// UnityEngine.WheelCollider[] UnityStandardAssets.Vehicles.Car.CarController::m_WheelColliders
	WheelColliderU5BU5D_t483547098* ___m_WheelColliders_5;
	// UnityEngine.GameObject[] UnityStandardAssets.Vehicles.Car.CarController::m_WheelMeshes
	GameObjectU5BU5D_t3328599146* ___m_WheelMeshes_6;
	// UnityStandardAssets.Vehicles.Car.WheelEffects[] UnityStandardAssets.Vehicles.Car.CarController::m_WheelEffects
	WheelEffectsU5BU5D_t3384186773* ___m_WheelEffects_7;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Car.CarController::m_CentreOfMassOffset
	Vector3_t3722313464  ___m_CentreOfMassOffset_8;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_MaximumSteerAngle
	float ___m_MaximumSteerAngle_9;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_SteerHelper
	float ___m_SteerHelper_10;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_TractionControl
	float ___m_TractionControl_11;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_FullTorqueOverAllWheels
	float ___m_FullTorqueOverAllWheels_12;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_ReverseTorque
	float ___m_ReverseTorque_13;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_MaxHandbrakeTorque
	float ___m_MaxHandbrakeTorque_14;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_Downforce
	float ___m_Downforce_15;
	// UnityStandardAssets.Vehicles.Car.SpeedType UnityStandardAssets.Vehicles.Car.CarController::m_SpeedType
	int32_t ___m_SpeedType_16;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_Topspeed
	float ___m_Topspeed_17;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_RevRangeBoundary
	float ___m_RevRangeBoundary_19;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_SlipLimit
	float ___m_SlipLimit_20;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_BrakeTorque
	float ___m_BrakeTorque_21;
	// UnityEngine.Quaternion[] UnityStandardAssets.Vehicles.Car.CarController::m_WheelMeshLocalRotations
	QuaternionU5BU5D_t2571361770* ___m_WheelMeshLocalRotations_22;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Car.CarController::m_Prevpos
	Vector3_t3722313464  ___m_Prevpos_23;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Car.CarController::m_Pos
	Vector3_t3722313464  ___m_Pos_24;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_SteerAngle
	float ___m_SteerAngle_25;
	// System.Int32 UnityStandardAssets.Vehicles.Car.CarController::m_GearNum
	int32_t ___m_GearNum_26;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_GearFactor
	float ___m_GearFactor_27;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_OldRotation
	float ___m_OldRotation_28;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::m_CurrentTorque
	float ___m_CurrentTorque_29;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Car.CarController::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_30;
	// System.Boolean UnityStandardAssets.Vehicles.Car.CarController::<Skidding>k__BackingField
	bool ___U3CSkiddingU3Ek__BackingField_32;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::<BrakeInput>k__BackingField
	float ___U3CBrakeInputU3Ek__BackingField_33;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::<Revs>k__BackingField
	float ___U3CRevsU3Ek__BackingField_34;
	// System.Single UnityStandardAssets.Vehicles.Car.CarController::<AccelInput>k__BackingField
	float ___U3CAccelInputU3Ek__BackingField_35;

public:
	inline static int32_t get_offset_of_m_CarDriveType_4() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_CarDriveType_4)); }
	inline int32_t get_m_CarDriveType_4() const { return ___m_CarDriveType_4; }
	inline int32_t* get_address_of_m_CarDriveType_4() { return &___m_CarDriveType_4; }
	inline void set_m_CarDriveType_4(int32_t value)
	{
		___m_CarDriveType_4 = value;
	}

	inline static int32_t get_offset_of_m_WheelColliders_5() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_WheelColliders_5)); }
	inline WheelColliderU5BU5D_t483547098* get_m_WheelColliders_5() const { return ___m_WheelColliders_5; }
	inline WheelColliderU5BU5D_t483547098** get_address_of_m_WheelColliders_5() { return &___m_WheelColliders_5; }
	inline void set_m_WheelColliders_5(WheelColliderU5BU5D_t483547098* value)
	{
		___m_WheelColliders_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_WheelColliders_5), value);
	}

	inline static int32_t get_offset_of_m_WheelMeshes_6() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_WheelMeshes_6)); }
	inline GameObjectU5BU5D_t3328599146* get_m_WheelMeshes_6() const { return ___m_WheelMeshes_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_WheelMeshes_6() { return &___m_WheelMeshes_6; }
	inline void set_m_WheelMeshes_6(GameObjectU5BU5D_t3328599146* value)
	{
		___m_WheelMeshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_WheelMeshes_6), value);
	}

	inline static int32_t get_offset_of_m_WheelEffects_7() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_WheelEffects_7)); }
	inline WheelEffectsU5BU5D_t3384186773* get_m_WheelEffects_7() const { return ___m_WheelEffects_7; }
	inline WheelEffectsU5BU5D_t3384186773** get_address_of_m_WheelEffects_7() { return &___m_WheelEffects_7; }
	inline void set_m_WheelEffects_7(WheelEffectsU5BU5D_t3384186773* value)
	{
		___m_WheelEffects_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_WheelEffects_7), value);
	}

	inline static int32_t get_offset_of_m_CentreOfMassOffset_8() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_CentreOfMassOffset_8)); }
	inline Vector3_t3722313464  get_m_CentreOfMassOffset_8() const { return ___m_CentreOfMassOffset_8; }
	inline Vector3_t3722313464 * get_address_of_m_CentreOfMassOffset_8() { return &___m_CentreOfMassOffset_8; }
	inline void set_m_CentreOfMassOffset_8(Vector3_t3722313464  value)
	{
		___m_CentreOfMassOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_MaximumSteerAngle_9() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_MaximumSteerAngle_9)); }
	inline float get_m_MaximumSteerAngle_9() const { return ___m_MaximumSteerAngle_9; }
	inline float* get_address_of_m_MaximumSteerAngle_9() { return &___m_MaximumSteerAngle_9; }
	inline void set_m_MaximumSteerAngle_9(float value)
	{
		___m_MaximumSteerAngle_9 = value;
	}

	inline static int32_t get_offset_of_m_SteerHelper_10() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_SteerHelper_10)); }
	inline float get_m_SteerHelper_10() const { return ___m_SteerHelper_10; }
	inline float* get_address_of_m_SteerHelper_10() { return &___m_SteerHelper_10; }
	inline void set_m_SteerHelper_10(float value)
	{
		___m_SteerHelper_10 = value;
	}

	inline static int32_t get_offset_of_m_TractionControl_11() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_TractionControl_11)); }
	inline float get_m_TractionControl_11() const { return ___m_TractionControl_11; }
	inline float* get_address_of_m_TractionControl_11() { return &___m_TractionControl_11; }
	inline void set_m_TractionControl_11(float value)
	{
		___m_TractionControl_11 = value;
	}

	inline static int32_t get_offset_of_m_FullTorqueOverAllWheels_12() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_FullTorqueOverAllWheels_12)); }
	inline float get_m_FullTorqueOverAllWheels_12() const { return ___m_FullTorqueOverAllWheels_12; }
	inline float* get_address_of_m_FullTorqueOverAllWheels_12() { return &___m_FullTorqueOverAllWheels_12; }
	inline void set_m_FullTorqueOverAllWheels_12(float value)
	{
		___m_FullTorqueOverAllWheels_12 = value;
	}

	inline static int32_t get_offset_of_m_ReverseTorque_13() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_ReverseTorque_13)); }
	inline float get_m_ReverseTorque_13() const { return ___m_ReverseTorque_13; }
	inline float* get_address_of_m_ReverseTorque_13() { return &___m_ReverseTorque_13; }
	inline void set_m_ReverseTorque_13(float value)
	{
		___m_ReverseTorque_13 = value;
	}

	inline static int32_t get_offset_of_m_MaxHandbrakeTorque_14() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_MaxHandbrakeTorque_14)); }
	inline float get_m_MaxHandbrakeTorque_14() const { return ___m_MaxHandbrakeTorque_14; }
	inline float* get_address_of_m_MaxHandbrakeTorque_14() { return &___m_MaxHandbrakeTorque_14; }
	inline void set_m_MaxHandbrakeTorque_14(float value)
	{
		___m_MaxHandbrakeTorque_14 = value;
	}

	inline static int32_t get_offset_of_m_Downforce_15() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_Downforce_15)); }
	inline float get_m_Downforce_15() const { return ___m_Downforce_15; }
	inline float* get_address_of_m_Downforce_15() { return &___m_Downforce_15; }
	inline void set_m_Downforce_15(float value)
	{
		___m_Downforce_15 = value;
	}

	inline static int32_t get_offset_of_m_SpeedType_16() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_SpeedType_16)); }
	inline int32_t get_m_SpeedType_16() const { return ___m_SpeedType_16; }
	inline int32_t* get_address_of_m_SpeedType_16() { return &___m_SpeedType_16; }
	inline void set_m_SpeedType_16(int32_t value)
	{
		___m_SpeedType_16 = value;
	}

	inline static int32_t get_offset_of_m_Topspeed_17() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_Topspeed_17)); }
	inline float get_m_Topspeed_17() const { return ___m_Topspeed_17; }
	inline float* get_address_of_m_Topspeed_17() { return &___m_Topspeed_17; }
	inline void set_m_Topspeed_17(float value)
	{
		___m_Topspeed_17 = value;
	}

	inline static int32_t get_offset_of_m_RevRangeBoundary_19() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_RevRangeBoundary_19)); }
	inline float get_m_RevRangeBoundary_19() const { return ___m_RevRangeBoundary_19; }
	inline float* get_address_of_m_RevRangeBoundary_19() { return &___m_RevRangeBoundary_19; }
	inline void set_m_RevRangeBoundary_19(float value)
	{
		___m_RevRangeBoundary_19 = value;
	}

	inline static int32_t get_offset_of_m_SlipLimit_20() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_SlipLimit_20)); }
	inline float get_m_SlipLimit_20() const { return ___m_SlipLimit_20; }
	inline float* get_address_of_m_SlipLimit_20() { return &___m_SlipLimit_20; }
	inline void set_m_SlipLimit_20(float value)
	{
		___m_SlipLimit_20 = value;
	}

	inline static int32_t get_offset_of_m_BrakeTorque_21() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_BrakeTorque_21)); }
	inline float get_m_BrakeTorque_21() const { return ___m_BrakeTorque_21; }
	inline float* get_address_of_m_BrakeTorque_21() { return &___m_BrakeTorque_21; }
	inline void set_m_BrakeTorque_21(float value)
	{
		___m_BrakeTorque_21 = value;
	}

	inline static int32_t get_offset_of_m_WheelMeshLocalRotations_22() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_WheelMeshLocalRotations_22)); }
	inline QuaternionU5BU5D_t2571361770* get_m_WheelMeshLocalRotations_22() const { return ___m_WheelMeshLocalRotations_22; }
	inline QuaternionU5BU5D_t2571361770** get_address_of_m_WheelMeshLocalRotations_22() { return &___m_WheelMeshLocalRotations_22; }
	inline void set_m_WheelMeshLocalRotations_22(QuaternionU5BU5D_t2571361770* value)
	{
		___m_WheelMeshLocalRotations_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_WheelMeshLocalRotations_22), value);
	}

	inline static int32_t get_offset_of_m_Prevpos_23() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_Prevpos_23)); }
	inline Vector3_t3722313464  get_m_Prevpos_23() const { return ___m_Prevpos_23; }
	inline Vector3_t3722313464 * get_address_of_m_Prevpos_23() { return &___m_Prevpos_23; }
	inline void set_m_Prevpos_23(Vector3_t3722313464  value)
	{
		___m_Prevpos_23 = value;
	}

	inline static int32_t get_offset_of_m_Pos_24() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_Pos_24)); }
	inline Vector3_t3722313464  get_m_Pos_24() const { return ___m_Pos_24; }
	inline Vector3_t3722313464 * get_address_of_m_Pos_24() { return &___m_Pos_24; }
	inline void set_m_Pos_24(Vector3_t3722313464  value)
	{
		___m_Pos_24 = value;
	}

	inline static int32_t get_offset_of_m_SteerAngle_25() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_SteerAngle_25)); }
	inline float get_m_SteerAngle_25() const { return ___m_SteerAngle_25; }
	inline float* get_address_of_m_SteerAngle_25() { return &___m_SteerAngle_25; }
	inline void set_m_SteerAngle_25(float value)
	{
		___m_SteerAngle_25 = value;
	}

	inline static int32_t get_offset_of_m_GearNum_26() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_GearNum_26)); }
	inline int32_t get_m_GearNum_26() const { return ___m_GearNum_26; }
	inline int32_t* get_address_of_m_GearNum_26() { return &___m_GearNum_26; }
	inline void set_m_GearNum_26(int32_t value)
	{
		___m_GearNum_26 = value;
	}

	inline static int32_t get_offset_of_m_GearFactor_27() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_GearFactor_27)); }
	inline float get_m_GearFactor_27() const { return ___m_GearFactor_27; }
	inline float* get_address_of_m_GearFactor_27() { return &___m_GearFactor_27; }
	inline void set_m_GearFactor_27(float value)
	{
		___m_GearFactor_27 = value;
	}

	inline static int32_t get_offset_of_m_OldRotation_28() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_OldRotation_28)); }
	inline float get_m_OldRotation_28() const { return ___m_OldRotation_28; }
	inline float* get_address_of_m_OldRotation_28() { return &___m_OldRotation_28; }
	inline void set_m_OldRotation_28(float value)
	{
		___m_OldRotation_28 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTorque_29() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_CurrentTorque_29)); }
	inline float get_m_CurrentTorque_29() const { return ___m_CurrentTorque_29; }
	inline float* get_address_of_m_CurrentTorque_29() { return &___m_CurrentTorque_29; }
	inline void set_m_CurrentTorque_29(float value)
	{
		___m_CurrentTorque_29 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_30() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___m_Rigidbody_30)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_30() const { return ___m_Rigidbody_30; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_30() { return &___m_Rigidbody_30; }
	inline void set_m_Rigidbody_30(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_30), value);
	}

	inline static int32_t get_offset_of_U3CSkiddingU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___U3CSkiddingU3Ek__BackingField_32)); }
	inline bool get_U3CSkiddingU3Ek__BackingField_32() const { return ___U3CSkiddingU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CSkiddingU3Ek__BackingField_32() { return &___U3CSkiddingU3Ek__BackingField_32; }
	inline void set_U3CSkiddingU3Ek__BackingField_32(bool value)
	{
		___U3CSkiddingU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CBrakeInputU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___U3CBrakeInputU3Ek__BackingField_33)); }
	inline float get_U3CBrakeInputU3Ek__BackingField_33() const { return ___U3CBrakeInputU3Ek__BackingField_33; }
	inline float* get_address_of_U3CBrakeInputU3Ek__BackingField_33() { return &___U3CBrakeInputU3Ek__BackingField_33; }
	inline void set_U3CBrakeInputU3Ek__BackingField_33(float value)
	{
		___U3CBrakeInputU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CRevsU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___U3CRevsU3Ek__BackingField_34)); }
	inline float get_U3CRevsU3Ek__BackingField_34() const { return ___U3CRevsU3Ek__BackingField_34; }
	inline float* get_address_of_U3CRevsU3Ek__BackingField_34() { return &___U3CRevsU3Ek__BackingField_34; }
	inline void set_U3CRevsU3Ek__BackingField_34(float value)
	{
		___U3CRevsU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CAccelInputU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(CarController_t4228304488, ___U3CAccelInputU3Ek__BackingField_35)); }
	inline float get_U3CAccelInputU3Ek__BackingField_35() const { return ___U3CAccelInputU3Ek__BackingField_35; }
	inline float* get_address_of_U3CAccelInputU3Ek__BackingField_35() { return &___U3CAccelInputU3Ek__BackingField_35; }
	inline void set_U3CAccelInputU3Ek__BackingField_35(float value)
	{
		___U3CAccelInputU3Ek__BackingField_35 = value;
	}
};

struct CarController_t4228304488_StaticFields
{
public:
	// System.Int32 UnityStandardAssets.Vehicles.Car.CarController::NoOfGears
	int32_t ___NoOfGears_18;

public:
	inline static int32_t get_offset_of_NoOfGears_18() { return static_cast<int32_t>(offsetof(CarController_t4228304488_StaticFields, ___NoOfGears_18)); }
	inline int32_t get_NoOfGears_18() const { return ___NoOfGears_18; }
	inline int32_t* get_address_of_NoOfGears_18() { return &___NoOfGears_18; }
	inline void set_NoOfGears_18(int32_t value)
	{
		___NoOfGears_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARCONTROLLER_T4228304488_H
#ifndef CARSELFRIGHTING_T887373655_H
#define CARSELFRIGHTING_T887373655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarSelfRighting
struct  CarSelfRighting_t887373655  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_WaitTime
	float ___m_WaitTime_4;
	// System.Single UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_VelocityThreshold
	float ___m_VelocityThreshold_5;
	// System.Single UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_LastOkTime
	float ___m_LastOkTime_6;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_7;

public:
	inline static int32_t get_offset_of_m_WaitTime_4() { return static_cast<int32_t>(offsetof(CarSelfRighting_t887373655, ___m_WaitTime_4)); }
	inline float get_m_WaitTime_4() const { return ___m_WaitTime_4; }
	inline float* get_address_of_m_WaitTime_4() { return &___m_WaitTime_4; }
	inline void set_m_WaitTime_4(float value)
	{
		___m_WaitTime_4 = value;
	}

	inline static int32_t get_offset_of_m_VelocityThreshold_5() { return static_cast<int32_t>(offsetof(CarSelfRighting_t887373655, ___m_VelocityThreshold_5)); }
	inline float get_m_VelocityThreshold_5() const { return ___m_VelocityThreshold_5; }
	inline float* get_address_of_m_VelocityThreshold_5() { return &___m_VelocityThreshold_5; }
	inline void set_m_VelocityThreshold_5(float value)
	{
		___m_VelocityThreshold_5 = value;
	}

	inline static int32_t get_offset_of_m_LastOkTime_6() { return static_cast<int32_t>(offsetof(CarSelfRighting_t887373655, ___m_LastOkTime_6)); }
	inline float get_m_LastOkTime_6() const { return ___m_LastOkTime_6; }
	inline float* get_address_of_m_LastOkTime_6() { return &___m_LastOkTime_6; }
	inline void set_m_LastOkTime_6(float value)
	{
		___m_LastOkTime_6 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_7() { return static_cast<int32_t>(offsetof(CarSelfRighting_t887373655, ___m_Rigidbody_7)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_7() const { return ___m_Rigidbody_7; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_7() { return &___m_Rigidbody_7; }
	inline void set_m_Rigidbody_7(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARSELFRIGHTING_T887373655_H
#ifndef CARUSERCONTROL_T4236640283_H
#define CARUSERCONTROL_T4236640283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.CarUserControl
struct  CarUserControl_t4236640283  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Car.CarController UnityStandardAssets.Vehicles.Car.CarUserControl::m_Car
	CarController_t4228304488 * ___m_Car_4;

public:
	inline static int32_t get_offset_of_m_Car_4() { return static_cast<int32_t>(offsetof(CarUserControl_t4236640283, ___m_Car_4)); }
	inline CarController_t4228304488 * get_m_Car_4() const { return ___m_Car_4; }
	inline CarController_t4228304488 ** get_address_of_m_Car_4() { return &___m_Car_4; }
	inline void set_m_Car_4(CarController_t4228304488 * value)
	{
		___m_Car_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Car_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARUSERCONTROL_T4236640283_H
#ifndef MUDGUARD_T1728581668_H
#define MUDGUARD_T1728581668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.Mudguard
struct  Mudguard_t1728581668  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Car.CarController UnityStandardAssets.Vehicles.Car.Mudguard::carController
	CarController_t4228304488 * ___carController_4;
	// UnityEngine.Quaternion UnityStandardAssets.Vehicles.Car.Mudguard::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_5;

public:
	inline static int32_t get_offset_of_carController_4() { return static_cast<int32_t>(offsetof(Mudguard_t1728581668, ___carController_4)); }
	inline CarController_t4228304488 * get_carController_4() const { return ___carController_4; }
	inline CarController_t4228304488 ** get_address_of_carController_4() { return &___carController_4; }
	inline void set_carController_4(CarController_t4228304488 * value)
	{
		___carController_4 = value;
		Il2CppCodeGenWriteBarrier((&___carController_4), value);
	}

	inline static int32_t get_offset_of_m_OriginalRotation_5() { return static_cast<int32_t>(offsetof(Mudguard_t1728581668, ___m_OriginalRotation_5)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_5() const { return ___m_OriginalRotation_5; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_5() { return &___m_OriginalRotation_5; }
	inline void set_m_OriginalRotation_5(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUDGUARD_T1728581668_H
#ifndef SKIDTRAIL_T3884084597_H
#define SKIDTRAIL_T3884084597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.SkidTrail
struct  SkidTrail_t3884084597  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Car.SkidTrail::m_PersistTime
	float ___m_PersistTime_4;

public:
	inline static int32_t get_offset_of_m_PersistTime_4() { return static_cast<int32_t>(offsetof(SkidTrail_t3884084597, ___m_PersistTime_4)); }
	inline float get_m_PersistTime_4() const { return ___m_PersistTime_4; }
	inline float* get_address_of_m_PersistTime_4() { return &___m_PersistTime_4; }
	inline void set_m_PersistTime_4(float value)
	{
		___m_PersistTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIDTRAIL_T3884084597_H
#ifndef SUSPENSION_T171091506_H
#define SUSPENSION_T171091506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.Suspension
struct  Suspension_t171091506  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Vehicles.Car.Suspension::wheel
	GameObject_t1113636619 * ___wheel_4;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Car.Suspension::m_TargetOriginalPosition
	Vector3_t3722313464  ___m_TargetOriginalPosition_5;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Car.Suspension::m_Origin
	Vector3_t3722313464  ___m_Origin_6;

public:
	inline static int32_t get_offset_of_wheel_4() { return static_cast<int32_t>(offsetof(Suspension_t171091506, ___wheel_4)); }
	inline GameObject_t1113636619 * get_wheel_4() const { return ___wheel_4; }
	inline GameObject_t1113636619 ** get_address_of_wheel_4() { return &___wheel_4; }
	inline void set_wheel_4(GameObject_t1113636619 * value)
	{
		___wheel_4 = value;
		Il2CppCodeGenWriteBarrier((&___wheel_4), value);
	}

	inline static int32_t get_offset_of_m_TargetOriginalPosition_5() { return static_cast<int32_t>(offsetof(Suspension_t171091506, ___m_TargetOriginalPosition_5)); }
	inline Vector3_t3722313464  get_m_TargetOriginalPosition_5() const { return ___m_TargetOriginalPosition_5; }
	inline Vector3_t3722313464 * get_address_of_m_TargetOriginalPosition_5() { return &___m_TargetOriginalPosition_5; }
	inline void set_m_TargetOriginalPosition_5(Vector3_t3722313464  value)
	{
		___m_TargetOriginalPosition_5 = value;
	}

	inline static int32_t get_offset_of_m_Origin_6() { return static_cast<int32_t>(offsetof(Suspension_t171091506, ___m_Origin_6)); }
	inline Vector3_t3722313464  get_m_Origin_6() const { return ___m_Origin_6; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_6() { return &___m_Origin_6; }
	inline void set_m_Origin_6(Vector3_t3722313464  value)
	{
		___m_Origin_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUSPENSION_T171091506_H
#ifndef WHEELEFFECTS_T1559845116_H
#define WHEELEFFECTS_T1559845116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Car.WheelEffects
struct  WheelEffects_t1559845116  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Car.WheelEffects::SkidTrailPrefab
	Transform_t3600365921 * ___SkidTrailPrefab_4;
	// UnityEngine.ParticleSystem UnityStandardAssets.Vehicles.Car.WheelEffects::skidParticles
	ParticleSystem_t1800779281 * ___skidParticles_6;
	// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::<skidding>k__BackingField
	bool ___U3CskiddingU3Ek__BackingField_7;
	// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::<PlayingAudio>k__BackingField
	bool ___U3CPlayingAudioU3Ek__BackingField_8;
	// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.WheelEffects::m_AudioSource
	AudioSource_t3935305588 * ___m_AudioSource_9;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Car.WheelEffects::m_SkidTrail
	Transform_t3600365921 * ___m_SkidTrail_10;
	// UnityEngine.WheelCollider UnityStandardAssets.Vehicles.Car.WheelEffects::m_WheelCollider
	WheelCollider_t2307365979 * ___m_WheelCollider_11;

public:
	inline static int32_t get_offset_of_SkidTrailPrefab_4() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116, ___SkidTrailPrefab_4)); }
	inline Transform_t3600365921 * get_SkidTrailPrefab_4() const { return ___SkidTrailPrefab_4; }
	inline Transform_t3600365921 ** get_address_of_SkidTrailPrefab_4() { return &___SkidTrailPrefab_4; }
	inline void set_SkidTrailPrefab_4(Transform_t3600365921 * value)
	{
		___SkidTrailPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___SkidTrailPrefab_4), value);
	}

	inline static int32_t get_offset_of_skidParticles_6() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116, ___skidParticles_6)); }
	inline ParticleSystem_t1800779281 * get_skidParticles_6() const { return ___skidParticles_6; }
	inline ParticleSystem_t1800779281 ** get_address_of_skidParticles_6() { return &___skidParticles_6; }
	inline void set_skidParticles_6(ParticleSystem_t1800779281 * value)
	{
		___skidParticles_6 = value;
		Il2CppCodeGenWriteBarrier((&___skidParticles_6), value);
	}

	inline static int32_t get_offset_of_U3CskiddingU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116, ___U3CskiddingU3Ek__BackingField_7)); }
	inline bool get_U3CskiddingU3Ek__BackingField_7() const { return ___U3CskiddingU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CskiddingU3Ek__BackingField_7() { return &___U3CskiddingU3Ek__BackingField_7; }
	inline void set_U3CskiddingU3Ek__BackingField_7(bool value)
	{
		___U3CskiddingU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CPlayingAudioU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116, ___U3CPlayingAudioU3Ek__BackingField_8)); }
	inline bool get_U3CPlayingAudioU3Ek__BackingField_8() const { return ___U3CPlayingAudioU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CPlayingAudioU3Ek__BackingField_8() { return &___U3CPlayingAudioU3Ek__BackingField_8; }
	inline void set_U3CPlayingAudioU3Ek__BackingField_8(bool value)
	{
		___U3CPlayingAudioU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_m_AudioSource_9() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116, ___m_AudioSource_9)); }
	inline AudioSource_t3935305588 * get_m_AudioSource_9() const { return ___m_AudioSource_9; }
	inline AudioSource_t3935305588 ** get_address_of_m_AudioSource_9() { return &___m_AudioSource_9; }
	inline void set_m_AudioSource_9(AudioSource_t3935305588 * value)
	{
		___m_AudioSource_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioSource_9), value);
	}

	inline static int32_t get_offset_of_m_SkidTrail_10() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116, ___m_SkidTrail_10)); }
	inline Transform_t3600365921 * get_m_SkidTrail_10() const { return ___m_SkidTrail_10; }
	inline Transform_t3600365921 ** get_address_of_m_SkidTrail_10() { return &___m_SkidTrail_10; }
	inline void set_m_SkidTrail_10(Transform_t3600365921 * value)
	{
		___m_SkidTrail_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkidTrail_10), value);
	}

	inline static int32_t get_offset_of_m_WheelCollider_11() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116, ___m_WheelCollider_11)); }
	inline WheelCollider_t2307365979 * get_m_WheelCollider_11() const { return ___m_WheelCollider_11; }
	inline WheelCollider_t2307365979 ** get_address_of_m_WheelCollider_11() { return &___m_WheelCollider_11; }
	inline void set_m_WheelCollider_11(WheelCollider_t2307365979 * value)
	{
		___m_WheelCollider_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_WheelCollider_11), value);
	}
};

struct WheelEffects_t1559845116_StaticFields
{
public:
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Car.WheelEffects::skidTrailsDetachedParent
	Transform_t3600365921 * ___skidTrailsDetachedParent_5;

public:
	inline static int32_t get_offset_of_skidTrailsDetachedParent_5() { return static_cast<int32_t>(offsetof(WheelEffects_t1559845116_StaticFields, ___skidTrailsDetachedParent_5)); }
	inline Transform_t3600365921 * get_skidTrailsDetachedParent_5() const { return ___skidTrailsDetachedParent_5; }
	inline Transform_t3600365921 ** get_address_of_skidTrailsDetachedParent_5() { return &___skidTrailsDetachedParent_5; }
	inline void set_skidTrailsDetachedParent_5(Transform_t3600365921 * value)
	{
		___skidTrailsDetachedParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___skidTrailsDetachedParent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEELEFFECTS_T1559845116_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (WaterHoseParticles_t1340502520), -1, sizeof(WaterHoseParticles_t1340502520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2200[4] = 
{
	WaterHoseParticles_t1340502520_StaticFields::get_offset_of_lastSoundTime_4(),
	WaterHoseParticles_t1340502520::get_offset_of_force_5(),
	WaterHoseParticles_t1340502520::get_offset_of_m_CollisionEvents_6(),
	WaterHoseParticles_t1340502520::get_offset_of_m_ParticleSystem_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (ActivateTrigger_t3349759092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[5] = 
{
	ActivateTrigger_t3349759092::get_offset_of_action_4(),
	ActivateTrigger_t3349759092::get_offset_of_target_5(),
	ActivateTrigger_t3349759092::get_offset_of_source_6(),
	ActivateTrigger_t3349759092::get_offset_of_triggerCount_7(),
	ActivateTrigger_t3349759092::get_offset_of_repeatTrigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (Mode_t3024470803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2202[7] = 
{
	Mode_t3024470803::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (AlphaButtonClickMask_t141136539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[1] = 
{
	AlphaButtonClickMask_t141136539::get_offset_of__image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (AutoMobileShaderSwitch_t568447889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[1] = 
{
	AutoMobileShaderSwitch_t568447889::get_offset_of_m_ReplacementList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (ReplacementDefinition_t2693741842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[2] = 
{
	ReplacementDefinition_t2693741842::get_offset_of_original_0(),
	ReplacementDefinition_t2693741842::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (ReplacementList_t1887104210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[1] = 
{
	ReplacementList_t1887104210::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (AutoMoveAndRotate_t2437913015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[4] = 
{
	AutoMoveAndRotate_t2437913015::get_offset_of_moveUnitsPerSecond_4(),
	AutoMoveAndRotate_t2437913015::get_offset_of_rotateDegreesPerSecond_5(),
	AutoMoveAndRotate_t2437913015::get_offset_of_ignoreTimescale_6(),
	AutoMoveAndRotate_t2437913015::get_offset_of_m_LastRealTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (Vector3andSpace_t219844479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[2] = 
{
	Vector3andSpace_t219844479::get_offset_of_value_0(),
	Vector3andSpace_t219844479::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (CameraRefocus_t4263235746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[5] = 
{
	CameraRefocus_t4263235746::get_offset_of_Camera_0(),
	CameraRefocus_t4263235746::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t4263235746::get_offset_of_Parent_2(),
	CameraRefocus_t4263235746::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t4263235746::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (CurveControlledBob_t2679313829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[9] = 
{
	CurveControlledBob_t2679313829::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2679313829::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2679313829::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2679313829::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2679313829::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2679313829::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2679313829::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (DragRigidbody_t1600652016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t1600652016::get_offset_of_m_SpringJoint_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (U3CDragObjectU3Ec__Iterator0_t4151609119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[9] = 
{
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdDragU3E__0_0(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdAngularDragU3E__0_1(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CmainCameraU3E__0_2(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CrayU3E__1_3(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_distance_4(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24this_5(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24current_6(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24disposing_7(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (DynamicShadowSettings_t59119858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[11] = 
{
	DynamicShadowSettings_t59119858::get_offset_of_sunLight_4(),
	DynamicShadowSettings_t59119858::get_offset_of_minHeight_5(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowDistance_6(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowBias_7(),
	DynamicShadowSettings_t59119858::get_offset_of_maxHeight_8(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowDistance_9(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowBias_10(),
	DynamicShadowSettings_t59119858::get_offset_of_adaptTime_11(),
	DynamicShadowSettings_t59119858::get_offset_of_m_SmoothHeight_12(),
	DynamicShadowSettings_t59119858::get_offset_of_m_ChangeSpeed_13(),
	DynamicShadowSettings_t59119858::get_offset_of_m_OriginalStrength_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (EventSystemChecker_t1882757729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (FollowTarget_t166153614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[2] = 
{
	FollowTarget_t166153614::get_offset_of_target_4(),
	FollowTarget_t166153614::get_offset_of_offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (ForcedReset_t301124368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (FOVKick_t120370150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[6] = 
{
	FOVKick_t120370150::get_offset_of_Camera_0(),
	FOVKick_t120370150::get_offset_of_originalFov_1(),
	FOVKick_t120370150::get_offset_of_FOVIncrease_2(),
	FOVKick_t120370150::get_offset_of_TimeToIncrease_3(),
	FOVKick_t120370150::get_offset_of_TimeToDecrease_4(),
	FOVKick_t120370150::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (U3CFOVKickUpU3Ec__Iterator0_t3738408313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[5] = 
{
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24this_1(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24current_2(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24disposing_3(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (U3CFOVKickDownU3Ec__Iterator1_t1440840980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[5] = 
{
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24this_1(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24current_2(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24disposing_3(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (FPSCounter_t2351221284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[6] = 
{
	0,
	FPSCounter_t2351221284::get_offset_of_m_FpsAccumulator_5(),
	FPSCounter_t2351221284::get_offset_of_m_FpsNextPeriod_6(),
	FPSCounter_t2351221284::get_offset_of_m_CurrentFps_7(),
	0,
	FPSCounter_t2351221284::get_offset_of_m_Text_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (LerpControlledBob_t1895875871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[3] = 
{
	LerpControlledBob_t1895875871::get_offset_of_BobDuration_0(),
	LerpControlledBob_t1895875871::get_offset_of_BobAmount_1(),
	LerpControlledBob_t1895875871::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (U3CDoBobCycleU3Ec__Iterator0_t1149538828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[5] = 
{
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U3CtU3E__0_0(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24this_1(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24current_2(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24disposing_3(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (ObjectResetter_t639177103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[4] = 
{
	ObjectResetter_t639177103::get_offset_of_originalPosition_4(),
	ObjectResetter_t639177103::get_offset_of_originalRotation_5(),
	ObjectResetter_t639177103::get_offset_of_originalStructure_6(),
	ObjectResetter_t639177103::get_offset_of_Rigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (U3CResetCoroutineU3Ec__Iterator0_t3232105836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[7] = 
{
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_delay_0(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar0_1(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar1_2(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24this_3(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24current_4(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24disposing_5(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (ParticleSystemDestroyer_t558680695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[4] = 
{
	ParticleSystemDestroyer_t558680695::get_offset_of_minDuration_4(),
	ParticleSystemDestroyer_t558680695::get_offset_of_maxDuration_5(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_MaxLifetime_6(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_EarlyStop_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CStartU3Ec__Iterator0_t980021917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[10] = 
{
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CsystemsU3E__0_0(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar1_2(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CstopTimeU3E__0_3(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar2_4(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar3_5(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (PlatformSpecificContent_t1404549723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	PlatformSpecificContent_t1404549723::get_offset_of_m_BuildTargetGroup_4(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_Content_5(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_MonoBehaviours_6(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_ChildrenOfThisObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (BuildTargetGroup_t72322187)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2228[3] = 
{
	BuildTargetGroup_t72322187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (SimpleActivatorMenu_t1387811551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[3] = 
{
	SimpleActivatorMenu_t1387811551::get_offset_of_camSwitchButton_4(),
	SimpleActivatorMenu_t1387811551::get_offset_of_objects_5(),
	SimpleActivatorMenu_t1387811551::get_offset_of_m_CurrentActiveObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (SimpleMouseRotator_t2364742953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[10] = 
{
	SimpleMouseRotator_t2364742953::get_offset_of_rotationRange_4(),
	SimpleMouseRotator_t2364742953::get_offset_of_rotationSpeed_5(),
	SimpleMouseRotator_t2364742953::get_offset_of_dampingTime_6(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroVerticalOnMobile_7(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroHorizontalOnMobile_8(),
	SimpleMouseRotator_t2364742953::get_offset_of_relative_9(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_TargetAngles_10(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowAngles_11(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowVelocity_12(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_OriginalRotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (SmoothFollow_t4204731361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[5] = 
{
	SmoothFollow_t4204731361::get_offset_of_target_4(),
	SmoothFollow_t4204731361::get_offset_of_distance_5(),
	SmoothFollow_t4204731361::get_offset_of_height_6(),
	SmoothFollow_t4204731361::get_offset_of_rotationDamping_7(),
	SmoothFollow_t4204731361::get_offset_of_heightDamping_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (TimedObjectActivator_t1846709985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[1] = 
{
	TimedObjectActivator_t1846709985::get_offset_of_entries_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (Action_t837364808)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[6] = 
{
	Action_t837364808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (Entry_t2725803170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[3] = 
{
	Entry_t2725803170::get_offset_of_target_0(),
	Entry_t2725803170::get_offset_of_action_1(),
	Entry_t2725803170::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (Entries_t3168066469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[1] = 
{
	Entries_t3168066469::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U3CActivateU3Ec__Iterator0_t2664723090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[4] = 
{
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_entry_0(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24current_1(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24disposing_2(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (U3CDeactivateU3Ec__Iterator1_t730025274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[4] = 
{
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_entry_0(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24current_1(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24disposing_2(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (U3CReloadLevelU3Ec__Iterator2_t2784493974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[4] = 
{
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_entry_0(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24current_1(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24disposing_2(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (TimedObjectDestructor_t3438860414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[2] = 
{
	TimedObjectDestructor_t3438860414::get_offset_of_m_TimeOut_4(),
	TimedObjectDestructor_t3438860414::get_offset_of_m_DetachChildren_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (WaypointCircuit_t445075330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[16] = 
{
	WaypointCircuit_t445075330::get_offset_of_waypointList_4(),
	WaypointCircuit_t445075330::get_offset_of_smoothRoute_5(),
	WaypointCircuit_t445075330::get_offset_of_numPoints_6(),
	WaypointCircuit_t445075330::get_offset_of_points_7(),
	WaypointCircuit_t445075330::get_offset_of_distances_8(),
	WaypointCircuit_t445075330::get_offset_of_editorVisualisationSubsteps_9(),
	WaypointCircuit_t445075330::get_offset_of_U3CLengthU3Ek__BackingField_10(),
	WaypointCircuit_t445075330::get_offset_of_p0n_11(),
	WaypointCircuit_t445075330::get_offset_of_p1n_12(),
	WaypointCircuit_t445075330::get_offset_of_p2n_13(),
	WaypointCircuit_t445075330::get_offset_of_p3n_14(),
	WaypointCircuit_t445075330::get_offset_of_i_15(),
	WaypointCircuit_t445075330::get_offset_of_P0_16(),
	WaypointCircuit_t445075330::get_offset_of_P1_17(),
	WaypointCircuit_t445075330::get_offset_of_P2_18(),
	WaypointCircuit_t445075330::get_offset_of_P3_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (WaypointList_t2584574554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	WaypointList_t2584574554::get_offset_of_circuit_0(),
	WaypointList_t2584574554::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (RoutePoint_t3880028948)+ sizeof (RuntimeObject), sizeof(RoutePoint_t3880028948 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2242[2] = 
{
	RoutePoint_t3880028948::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoutePoint_t3880028948::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (WaypointProgressTracker_t1841386251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[15] = 
{
	WaypointProgressTracker_t1841386251::get_offset_of_circuit_4(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetOffset_5(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetFactor_6(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedOffset_7(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedFactor_8(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressStyle_9(),
	WaypointProgressTracker_t1841386251::get_offset_of_pointToPointThreshold_10(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CtargetPointU3Ek__BackingField_11(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CspeedPointU3Ek__BackingField_12(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CprogressPointU3Ek__BackingField_13(),
	WaypointProgressTracker_t1841386251::get_offset_of_target_14(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressDistance_15(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressNum_16(),
	WaypointProgressTracker_t1841386251::get_offset_of_lastPosition_17(),
	WaypointProgressTracker_t1841386251::get_offset_of_speed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (ProgressStyle_t3254572979)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2244[3] = 
{
	ProgressStyle_t3254572979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (AeroplaneAiControl_t3513225471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[12] = 
{
	AeroplaneAiControl_t3513225471::get_offset_of_m_RollSensitivity_4(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_PitchSensitivity_5(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_LateralWanderDistance_6(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_LateralWanderSpeed_7(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_MaxClimbAngle_8(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_MaxRollAngle_9(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_SpeedEffect_10(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_TakeoffHeight_11(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_Target_12(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_AeroplaneController_13(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_RandomPerlin_14(),
	AeroplaneAiControl_t3513225471::get_offset_of_m_TakenOff_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (AeroplaneAudio_t418183782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[13] = 
{
	AeroplaneAudio_t418183782::get_offset_of_m_EngineSound_4(),
	AeroplaneAudio_t418183782::get_offset_of_m_EngineMinThrottlePitch_5(),
	AeroplaneAudio_t418183782::get_offset_of_m_EngineMaxThrottlePitch_6(),
	AeroplaneAudio_t418183782::get_offset_of_m_EngineFwdSpeedMultiplier_7(),
	AeroplaneAudio_t418183782::get_offset_of_m_WindSound_8(),
	AeroplaneAudio_t418183782::get_offset_of_m_WindBasePitch_9(),
	AeroplaneAudio_t418183782::get_offset_of_m_WindSpeedPitchFactor_10(),
	AeroplaneAudio_t418183782::get_offset_of_m_WindMaxSpeedVolume_11(),
	AeroplaneAudio_t418183782::get_offset_of_m_AdvancedSetttings_12(),
	AeroplaneAudio_t418183782::get_offset_of_m_EngineSoundSource_13(),
	AeroplaneAudio_t418183782::get_offset_of_m_WindSoundSource_14(),
	AeroplaneAudio_t418183782::get_offset_of_m_Plane_15(),
	AeroplaneAudio_t418183782::get_offset_of_m_Rigidbody_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (AdvancedSetttings_t3817742043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[8] = 
{
	AdvancedSetttings_t3817742043::get_offset_of_engineMinDistance_0(),
	AdvancedSetttings_t3817742043::get_offset_of_engineMaxDistance_1(),
	AdvancedSetttings_t3817742043::get_offset_of_engineDopplerLevel_2(),
	AdvancedSetttings_t3817742043::get_offset_of_engineMasterVolume_3(),
	AdvancedSetttings_t3817742043::get_offset_of_windMinDistance_4(),
	AdvancedSetttings_t3817742043::get_offset_of_windMaxDistance_5(),
	AdvancedSetttings_t3817742043::get_offset_of_windDopplerLevel_6(),
	AdvancedSetttings_t3817742043::get_offset_of_windMasterVolume_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (AeroplaneController_t857819489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[32] = 
{
	AeroplaneController_t857819489::get_offset_of_m_MaxEnginePower_4(),
	AeroplaneController_t857819489::get_offset_of_m_Lift_5(),
	AeroplaneController_t857819489::get_offset_of_m_ZeroLiftSpeed_6(),
	AeroplaneController_t857819489::get_offset_of_m_RollEffect_7(),
	AeroplaneController_t857819489::get_offset_of_m_PitchEffect_8(),
	AeroplaneController_t857819489::get_offset_of_m_YawEffect_9(),
	AeroplaneController_t857819489::get_offset_of_m_BankedTurnEffect_10(),
	AeroplaneController_t857819489::get_offset_of_m_AerodynamicEffect_11(),
	AeroplaneController_t857819489::get_offset_of_m_AutoTurnPitch_12(),
	AeroplaneController_t857819489::get_offset_of_m_AutoRollLevel_13(),
	AeroplaneController_t857819489::get_offset_of_m_AutoPitchLevel_14(),
	AeroplaneController_t857819489::get_offset_of_m_AirBrakesEffect_15(),
	AeroplaneController_t857819489::get_offset_of_m_ThrottleChangeSpeed_16(),
	AeroplaneController_t857819489::get_offset_of_m_DragIncreaseFactor_17(),
	AeroplaneController_t857819489::get_offset_of_U3CAltitudeU3Ek__BackingField_18(),
	AeroplaneController_t857819489::get_offset_of_U3CThrottleU3Ek__BackingField_19(),
	AeroplaneController_t857819489::get_offset_of_U3CAirBrakesU3Ek__BackingField_20(),
	AeroplaneController_t857819489::get_offset_of_U3CForwardSpeedU3Ek__BackingField_21(),
	AeroplaneController_t857819489::get_offset_of_U3CEnginePowerU3Ek__BackingField_22(),
	AeroplaneController_t857819489::get_offset_of_U3CRollAngleU3Ek__BackingField_23(),
	AeroplaneController_t857819489::get_offset_of_U3CPitchAngleU3Ek__BackingField_24(),
	AeroplaneController_t857819489::get_offset_of_U3CRollInputU3Ek__BackingField_25(),
	AeroplaneController_t857819489::get_offset_of_U3CPitchInputU3Ek__BackingField_26(),
	AeroplaneController_t857819489::get_offset_of_U3CYawInputU3Ek__BackingField_27(),
	AeroplaneController_t857819489::get_offset_of_U3CThrottleInputU3Ek__BackingField_28(),
	AeroplaneController_t857819489::get_offset_of_m_OriginalDrag_29(),
	AeroplaneController_t857819489::get_offset_of_m_OriginalAngularDrag_30(),
	AeroplaneController_t857819489::get_offset_of_m_AeroFactor_31(),
	AeroplaneController_t857819489::get_offset_of_m_Immobilized_32(),
	AeroplaneController_t857819489::get_offset_of_m_BankedTurnAmount_33(),
	AeroplaneController_t857819489::get_offset_of_m_Rigidbody_34(),
	AeroplaneController_t857819489::get_offset_of_m_WheelColliders_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (AeroplaneControlSurfaceAnimator_t2833473099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[3] = 
{
	AeroplaneControlSurfaceAnimator_t2833473099::get_offset_of_m_Smoothing_4(),
	AeroplaneControlSurfaceAnimator_t2833473099::get_offset_of_m_ControlSurfaces_5(),
	AeroplaneControlSurfaceAnimator_t2833473099::get_offset_of_m_Plane_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (ControlSurface_t2158797087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[4] = 
{
	ControlSurface_t2158797087::get_offset_of_transform_0(),
	ControlSurface_t2158797087::get_offset_of_amount_1(),
	ControlSurface_t2158797087::get_offset_of_type_2(),
	ControlSurface_t2158797087::get_offset_of_originalLocalRotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (Type_t2783176643)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2251[6] = 
{
	Type_t2783176643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (AeroplanePropellerAnimator_t2205181023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[11] = 
{
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_PropellorModel_4(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_PropellorBlur_5(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_PropellorBlurTextures_6(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_ThrottleBlurStart_7(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_ThrottleBlurEnd_8(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_MaxRpm_9(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_Plane_10(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_PropellorBlurState_11(),
	0,
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_PropellorModelRenderer_13(),
	AeroplanePropellerAnimator_t2205181023::get_offset_of_m_PropellorBlurRenderer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (AeroplaneUserControl2Axis_t599233410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[3] = 
{
	AeroplaneUserControl2Axis_t599233410::get_offset_of_maxRollAngle_4(),
	AeroplaneUserControl2Axis_t599233410::get_offset_of_maxPitchAngle_5(),
	AeroplaneUserControl2Axis_t599233410::get_offset_of_m_Aeroplane_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (AeroplaneUserControl4Axis_t4090860418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[6] = 
{
	AeroplaneUserControl4Axis_t4090860418::get_offset_of_maxRollAngle_4(),
	AeroplaneUserControl4Axis_t4090860418::get_offset_of_maxPitchAngle_5(),
	AeroplaneUserControl4Axis_t4090860418::get_offset_of_m_Aeroplane_6(),
	AeroplaneUserControl4Axis_t4090860418::get_offset_of_m_Throttle_7(),
	AeroplaneUserControl4Axis_t4090860418::get_offset_of_m_AirBrakes_8(),
	AeroplaneUserControl4Axis_t4090860418::get_offset_of_m_Yaw_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (JetParticleEffect_t772215299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[6] = 
{
	JetParticleEffect_t772215299::get_offset_of_minColour_4(),
	JetParticleEffect_t772215299::get_offset_of_m_Jet_5(),
	JetParticleEffect_t772215299::get_offset_of_m_System_6(),
	JetParticleEffect_t772215299::get_offset_of_m_OriginalStartSize_7(),
	JetParticleEffect_t772215299::get_offset_of_m_OriginalLifetime_8(),
	JetParticleEffect_t772215299::get_offset_of_m_OriginalStartColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (LandingGear_t786871954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[6] = 
{
	LandingGear_t786871954::get_offset_of_raiseAtAltitude_4(),
	LandingGear_t786871954::get_offset_of_lowerAtAltitude_5(),
	LandingGear_t786871954::get_offset_of_m_State_6(),
	LandingGear_t786871954::get_offset_of_m_Animator_7(),
	LandingGear_t786871954::get_offset_of_m_Rigidbody_8(),
	LandingGear_t786871954::get_offset_of_m_Plane_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (GearState_t3611444568)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2257[3] = 
{
	GearState_t3611444568::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (BrakeLight_t2229864552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[2] = 
{
	BrakeLight_t2229864552::get_offset_of_car_4(),
	BrakeLight_t2229864552::get_offset_of_m_Renderer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (CarAIControl_t1541705461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[22] = 
{
	CarAIControl_t1541705461::get_offset_of_m_CautiousSpeedFactor_4(),
	CarAIControl_t1541705461::get_offset_of_m_CautiousMaxAngle_5(),
	CarAIControl_t1541705461::get_offset_of_m_CautiousMaxDistance_6(),
	CarAIControl_t1541705461::get_offset_of_m_CautiousAngularVelocityFactor_7(),
	CarAIControl_t1541705461::get_offset_of_m_SteerSensitivity_8(),
	CarAIControl_t1541705461::get_offset_of_m_AccelSensitivity_9(),
	CarAIControl_t1541705461::get_offset_of_m_BrakeSensitivity_10(),
	CarAIControl_t1541705461::get_offset_of_m_LateralWanderDistance_11(),
	CarAIControl_t1541705461::get_offset_of_m_LateralWanderSpeed_12(),
	CarAIControl_t1541705461::get_offset_of_m_AccelWanderAmount_13(),
	CarAIControl_t1541705461::get_offset_of_m_AccelWanderSpeed_14(),
	CarAIControl_t1541705461::get_offset_of_m_BrakeCondition_15(),
	CarAIControl_t1541705461::get_offset_of_m_Driving_16(),
	CarAIControl_t1541705461::get_offset_of_m_Target_17(),
	CarAIControl_t1541705461::get_offset_of_m_StopWhenTargetReached_18(),
	CarAIControl_t1541705461::get_offset_of_m_ReachTargetThreshold_19(),
	CarAIControl_t1541705461::get_offset_of_m_RandomPerlin_20(),
	CarAIControl_t1541705461::get_offset_of_m_CarController_21(),
	CarAIControl_t1541705461::get_offset_of_m_AvoidOtherCarTime_22(),
	CarAIControl_t1541705461::get_offset_of_m_AvoidOtherCarSlowdown_23(),
	CarAIControl_t1541705461::get_offset_of_m_AvoidPathOffset_24(),
	CarAIControl_t1541705461::get_offset_of_m_Rigidbody_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (BrakeCondition_t1083170144)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2260[4] = 
{
	BrakeCondition_t1083170144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (CarAudio_t499525394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[18] = 
{
	CarAudio_t499525394::get_offset_of_engineSoundStyle_4(),
	CarAudio_t499525394::get_offset_of_lowAccelClip_5(),
	CarAudio_t499525394::get_offset_of_lowDecelClip_6(),
	CarAudio_t499525394::get_offset_of_highAccelClip_7(),
	CarAudio_t499525394::get_offset_of_highDecelClip_8(),
	CarAudio_t499525394::get_offset_of_pitchMultiplier_9(),
	CarAudio_t499525394::get_offset_of_lowPitchMin_10(),
	CarAudio_t499525394::get_offset_of_lowPitchMax_11(),
	CarAudio_t499525394::get_offset_of_highPitchMultiplier_12(),
	CarAudio_t499525394::get_offset_of_maxRolloffDistance_13(),
	CarAudio_t499525394::get_offset_of_dopplerLevel_14(),
	CarAudio_t499525394::get_offset_of_useDoppler_15(),
	CarAudio_t499525394::get_offset_of_m_LowAccel_16(),
	CarAudio_t499525394::get_offset_of_m_LowDecel_17(),
	CarAudio_t499525394::get_offset_of_m_HighAccel_18(),
	CarAudio_t499525394::get_offset_of_m_HighDecel_19(),
	CarAudio_t499525394::get_offset_of_m_StartedSound_20(),
	CarAudio_t499525394::get_offset_of_m_CarController_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (EngineAudioOptions_t2675404656)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2262[3] = 
{
	EngineAudioOptions_t2675404656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (CarDriveType_t1984246879)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[4] = 
{
	CarDriveType_t1984246879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (SpeedType_t3478026278)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[3] = 
{
	SpeedType_t3478026278::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (CarController_t4228304488), -1, sizeof(CarController_t4228304488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2265[32] = 
{
	CarController_t4228304488::get_offset_of_m_CarDriveType_4(),
	CarController_t4228304488::get_offset_of_m_WheelColliders_5(),
	CarController_t4228304488::get_offset_of_m_WheelMeshes_6(),
	CarController_t4228304488::get_offset_of_m_WheelEffects_7(),
	CarController_t4228304488::get_offset_of_m_CentreOfMassOffset_8(),
	CarController_t4228304488::get_offset_of_m_MaximumSteerAngle_9(),
	CarController_t4228304488::get_offset_of_m_SteerHelper_10(),
	CarController_t4228304488::get_offset_of_m_TractionControl_11(),
	CarController_t4228304488::get_offset_of_m_FullTorqueOverAllWheels_12(),
	CarController_t4228304488::get_offset_of_m_ReverseTorque_13(),
	CarController_t4228304488::get_offset_of_m_MaxHandbrakeTorque_14(),
	CarController_t4228304488::get_offset_of_m_Downforce_15(),
	CarController_t4228304488::get_offset_of_m_SpeedType_16(),
	CarController_t4228304488::get_offset_of_m_Topspeed_17(),
	CarController_t4228304488_StaticFields::get_offset_of_NoOfGears_18(),
	CarController_t4228304488::get_offset_of_m_RevRangeBoundary_19(),
	CarController_t4228304488::get_offset_of_m_SlipLimit_20(),
	CarController_t4228304488::get_offset_of_m_BrakeTorque_21(),
	CarController_t4228304488::get_offset_of_m_WheelMeshLocalRotations_22(),
	CarController_t4228304488::get_offset_of_m_Prevpos_23(),
	CarController_t4228304488::get_offset_of_m_Pos_24(),
	CarController_t4228304488::get_offset_of_m_SteerAngle_25(),
	CarController_t4228304488::get_offset_of_m_GearNum_26(),
	CarController_t4228304488::get_offset_of_m_GearFactor_27(),
	CarController_t4228304488::get_offset_of_m_OldRotation_28(),
	CarController_t4228304488::get_offset_of_m_CurrentTorque_29(),
	CarController_t4228304488::get_offset_of_m_Rigidbody_30(),
	0,
	CarController_t4228304488::get_offset_of_U3CSkiddingU3Ek__BackingField_32(),
	CarController_t4228304488::get_offset_of_U3CBrakeInputU3Ek__BackingField_33(),
	CarController_t4228304488::get_offset_of_U3CRevsU3Ek__BackingField_34(),
	CarController_t4228304488::get_offset_of_U3CAccelInputU3Ek__BackingField_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (CarSelfRighting_t887373655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[4] = 
{
	CarSelfRighting_t887373655::get_offset_of_m_WaitTime_4(),
	CarSelfRighting_t887373655::get_offset_of_m_VelocityThreshold_5(),
	CarSelfRighting_t887373655::get_offset_of_m_LastOkTime_6(),
	CarSelfRighting_t887373655::get_offset_of_m_Rigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (CarUserControl_t4236640283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[1] = 
{
	CarUserControl_t4236640283::get_offset_of_m_Car_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (Mudguard_t1728581668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[2] = 
{
	Mudguard_t1728581668::get_offset_of_carController_4(),
	Mudguard_t1728581668::get_offset_of_m_OriginalRotation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (SkidTrail_t3884084597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[1] = 
{
	SkidTrail_t3884084597::get_offset_of_m_PersistTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (U3CStartU3Ec__Iterator0_t3590021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[4] = 
{
	U3CStartU3Ec__Iterator0_t3590021378::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3590021378::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3590021378::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3590021378::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (Suspension_t171091506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[3] = 
{
	Suspension_t171091506::get_offset_of_wheel_4(),
	Suspension_t171091506::get_offset_of_m_TargetOriginalPosition_5(),
	Suspension_t171091506::get_offset_of_m_Origin_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (WheelEffects_t1559845116), -1, sizeof(WheelEffects_t1559845116_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2272[8] = 
{
	WheelEffects_t1559845116::get_offset_of_SkidTrailPrefab_4(),
	WheelEffects_t1559845116_StaticFields::get_offset_of_skidTrailsDetachedParent_5(),
	WheelEffects_t1559845116::get_offset_of_skidParticles_6(),
	WheelEffects_t1559845116::get_offset_of_U3CskiddingU3Ek__BackingField_7(),
	WheelEffects_t1559845116::get_offset_of_U3CPlayingAudioU3Ek__BackingField_8(),
	WheelEffects_t1559845116::get_offset_of_m_AudioSource_9(),
	WheelEffects_t1559845116::get_offset_of_m_SkidTrail_10(),
	WheelEffects_t1559845116::get_offset_of_m_WheelCollider_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (U3CStartSkidTrailU3Ec__Iterator0_t912754801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[4] = 
{
	U3CStartSkidTrailU3Ec__Iterator0_t912754801::get_offset_of_U24this_0(),
	U3CStartSkidTrailU3Ec__Iterator0_t912754801::get_offset_of_U24current_1(),
	U3CStartSkidTrailU3Ec__Iterator0_t912754801::get_offset_of_U24disposing_2(),
	U3CStartSkidTrailU3Ec__Iterator0_t912754801::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (MenuSceneLoader_t2183500486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[2] = 
{
	MenuSceneLoader_t2183500486::get_offset_of_menuUI_4(),
	MenuSceneLoader_t2183500486::get_offset_of_m_Go_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (PauseMenu_t3916167947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[4] = 
{
	PauseMenu_t3916167947::get_offset_of_m_MenuToggle_4(),
	PauseMenu_t3916167947::get_offset_of_m_TimeScaleRef_5(),
	PauseMenu_t3916167947::get_offset_of_m_VolumeRef_6(),
	PauseMenu_t3916167947::get_offset_of_m_Paused_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (SceneAndURLLoader_t3512401881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[1] = 
{
	SceneAndURLLoader_t3512401881::get_offset_of_m_PauseMenu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (CameraSwitch_t735968501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	CameraSwitch_t735968501::get_offset_of_objects_4(),
	CameraSwitch_t735968501::get_offset_of_text_5(),
	CameraSwitch_t735968501::get_offset_of_m_CurrentActiveObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (LevelReset_t1558959187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (ParticleSceneControls_t306931203), -1, sizeof(ParticleSceneControls_t306931203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2280[18] = 
{
	ParticleSceneControls_t306931203::get_offset_of_demoParticles_4(),
	ParticleSceneControls_t306931203::get_offset_of_spawnOffset_5(),
	ParticleSceneControls_t306931203::get_offset_of_multiply_6(),
	ParticleSceneControls_t306931203::get_offset_of_clearOnChange_7(),
	ParticleSceneControls_t306931203::get_offset_of_titleText_8(),
	ParticleSceneControls_t306931203::get_offset_of_sceneCamera_9(),
	ParticleSceneControls_t306931203::get_offset_of_instructionText_10(),
	ParticleSceneControls_t306931203::get_offset_of_previousButton_11(),
	ParticleSceneControls_t306931203::get_offset_of_nextButton_12(),
	ParticleSceneControls_t306931203::get_offset_of_graphicRaycaster_13(),
	ParticleSceneControls_t306931203::get_offset_of_eventSystem_14(),
	ParticleSceneControls_t306931203::get_offset_of_m_ParticleMultiplier_15(),
	ParticleSceneControls_t306931203::get_offset_of_m_CurrentParticleList_16(),
	ParticleSceneControls_t306931203::get_offset_of_m_Instance_17(),
	ParticleSceneControls_t306931203_StaticFields::get_offset_of_s_SelectedIndex_18(),
	ParticleSceneControls_t306931203::get_offset_of_m_CamOffsetVelocity_19(),
	ParticleSceneControls_t306931203::get_offset_of_m_LastPos_20(),
	ParticleSceneControls_t306931203_StaticFields::get_offset_of_s_Selected_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (Mode_t2106834709)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2281[4] = 
{
	Mode_t2106834709::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (AlignMode_t432429434)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2282[3] = 
{
	AlignMode_t432429434::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (DemoParticleSystem_t1195446716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[7] = 
{
	DemoParticleSystem_t1195446716::get_offset_of_transform_0(),
	DemoParticleSystem_t1195446716::get_offset_of_mode_1(),
	DemoParticleSystem_t1195446716::get_offset_of_align_2(),
	DemoParticleSystem_t1195446716::get_offset_of_maxCount_3(),
	DemoParticleSystem_t1195446716::get_offset_of_minDist_4(),
	DemoParticleSystem_t1195446716::get_offset_of_camOffset_5(),
	DemoParticleSystem_t1195446716::get_offset_of_instructionText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (DemoParticleSystemList_t4288938153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[1] = 
{
	DemoParticleSystemList_t4288938153::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (PlaceTargetWithMouse_t3823755269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[2] = 
{
	PlaceTargetWithMouse_t3823755269::get_offset_of_surfaceOffset_4(),
	PlaceTargetWithMouse_t3823755269::get_offset_of_setTargetOn_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (SlowMoButton_t57138989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[6] = 
{
	SlowMoButton_t57138989::get_offset_of_FullSpeedTex_4(),
	SlowMoButton_t57138989::get_offset_of_SlowSpeedTex_5(),
	SlowMoButton_t57138989::get_offset_of_fullSpeed_6(),
	SlowMoButton_t57138989::get_offset_of_slowSpeed_7(),
	SlowMoButton_t57138989::get_offset_of_button_8(),
	SlowMoButton_t57138989::get_offset_of_m_SlowMo_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
