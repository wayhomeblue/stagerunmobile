﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t3872604895;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t2541822629;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct Dictionary_2_t310742658;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct Dictionary_2_t75641268;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct List_1_t1093887670;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t197597763;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487;
// UnityEngine.Collision
struct Collision_t4262080450;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.SphereCollider
struct SphereCollider_t2077223608;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
struct RayHitComparer_t2205555946;
// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct MouseLook_t2859678661;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct RigidbodyFirstPersonController_t1207297146;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct AdvancedSettings_t778418834;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct MovementSettings_t1096092444;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t1711070432;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t4087348596;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t3982445645;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t2597455733;
// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct ExplosionFireAndDebris_t2411343565;
// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct ExplosionPhysicsForce_t3982641844;
// UnityStandardAssets.Effects.Explosive
struct Explosive_t792321375;
// UnityStandardAssets.Utility.CurveControlledBob
struct CurveControlledBob_t2679313829;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t639177103;
// UnityStandardAssets.Vehicles.Ball.Ball
struct Ball_t2378314638;
// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t439636033;
// UnityStandardAssets.Water.WaterBase
struct WaterBase_t3883863317;
// UnityStandardAssets._2D.PlatformerCharacter2D
struct PlatformerCharacter2D_t675295753;




#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef RAYHITCOMPARER_T2205555946_H
#define RAYHITCOMPARER_T2205555946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
struct  RayHitComparer_t2205555946  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYHITCOMPARER_T2205555946_H
#ifndef ADVANCEDSETTINGS_T778418834_H
#define ADVANCEDSETTINGS_T778418834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct  AdvancedSettings_t778418834  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::groundCheckDistance
	float ___groundCheckDistance_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::stickToGroundHelperDistance
	float ___stickToGroundHelperDistance_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::slowDownRate
	float ___slowDownRate_2;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::airControl
	bool ___airControl_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::shellOffset
	float ___shellOffset_4;

public:
	inline static int32_t get_offset_of_groundCheckDistance_0() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___groundCheckDistance_0)); }
	inline float get_groundCheckDistance_0() const { return ___groundCheckDistance_0; }
	inline float* get_address_of_groundCheckDistance_0() { return &___groundCheckDistance_0; }
	inline void set_groundCheckDistance_0(float value)
	{
		___groundCheckDistance_0 = value;
	}

	inline static int32_t get_offset_of_stickToGroundHelperDistance_1() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___stickToGroundHelperDistance_1)); }
	inline float get_stickToGroundHelperDistance_1() const { return ___stickToGroundHelperDistance_1; }
	inline float* get_address_of_stickToGroundHelperDistance_1() { return &___stickToGroundHelperDistance_1; }
	inline void set_stickToGroundHelperDistance_1(float value)
	{
		___stickToGroundHelperDistance_1 = value;
	}

	inline static int32_t get_offset_of_slowDownRate_2() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___slowDownRate_2)); }
	inline float get_slowDownRate_2() const { return ___slowDownRate_2; }
	inline float* get_address_of_slowDownRate_2() { return &___slowDownRate_2; }
	inline void set_slowDownRate_2(float value)
	{
		___slowDownRate_2 = value;
	}

	inline static int32_t get_offset_of_airControl_3() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___airControl_3)); }
	inline bool get_airControl_3() const { return ___airControl_3; }
	inline bool* get_address_of_airControl_3() { return &___airControl_3; }
	inline void set_airControl_3(bool value)
	{
		___airControl_3 = value;
	}

	inline static int32_t get_offset_of_shellOffset_4() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___shellOffset_4)); }
	inline float get_shellOffset_4() const { return ___shellOffset_4; }
	inline float* get_address_of_shellOffset_4() { return &___shellOffset_4; }
	inline void set_shellOffset_4(float value)
	{
		___shellOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDSETTINGS_T778418834_H
#ifndef CROSSPLATFORMINPUTMANAGER_T191731427_H
#define CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t191731427  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t191731427_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t2597455733 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t2597455733 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t2597455733 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t2597455733 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t2597455733 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t2597455733 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t2597455733 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t2597455733 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t2597455733 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t2597455733 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t2597455733 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t2597455733 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifndef VIRTUALAXIS_T4087348596_H
#define VIRTUALAXIS_T4087348596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_t4087348596  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_T4087348596_H
#ifndef VIRTUALBUTTON_T2756566330_H
#define VIRTUALBUTTON_T2756566330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_t2756566330  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T2756566330_H
#ifndef U3CSTARTU3EC__ITERATOR0_T150375094_H
#define U3CSTARTU3EC__ITERATOR0_T150375094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t150375094  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<multiplier>__0
	float ___U3CmultiplierU3E__0_0;
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<r>__0
	float ___U3CrU3E__0_1;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<cols>__0
	ColliderU5BU5D_t4234922487* ___U3CcolsU3E__0_2;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$locvar0
	ColliderU5BU5D_t4234922487* ___U24locvar0_3;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$locvar1
	int32_t ___U24locvar1_4;
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<testR>__0
	float ___U3CtestRU3E__0_5;
	// UnityStandardAssets.Effects.ExplosionFireAndDebris UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$this
	ExplosionFireAndDebris_t2411343565 * ___U24this_6;
	// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CmultiplierU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U3CmultiplierU3E__0_0)); }
	inline float get_U3CmultiplierU3E__0_0() const { return ___U3CmultiplierU3E__0_0; }
	inline float* get_address_of_U3CmultiplierU3E__0_0() { return &___U3CmultiplierU3E__0_0; }
	inline void set_U3CmultiplierU3E__0_0(float value)
	{
		___U3CmultiplierU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CrU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U3CrU3E__0_1)); }
	inline float get_U3CrU3E__0_1() const { return ___U3CrU3E__0_1; }
	inline float* get_address_of_U3CrU3E__0_1() { return &___U3CrU3E__0_1; }
	inline void set_U3CrU3E__0_1(float value)
	{
		___U3CrU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CcolsU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U3CcolsU3E__0_2)); }
	inline ColliderU5BU5D_t4234922487* get_U3CcolsU3E__0_2() const { return ___U3CcolsU3E__0_2; }
	inline ColliderU5BU5D_t4234922487** get_address_of_U3CcolsU3E__0_2() { return &___U3CcolsU3E__0_2; }
	inline void set_U3CcolsU3E__0_2(ColliderU5BU5D_t4234922487* value)
	{
		___U3CcolsU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolsU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U24locvar0_3)); }
	inline ColliderU5BU5D_t4234922487* get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline ColliderU5BU5D_t4234922487** get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(ColliderU5BU5D_t4234922487* value)
	{
		___U24locvar0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_3), value);
	}

	inline static int32_t get_offset_of_U24locvar1_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U24locvar1_4)); }
	inline int32_t get_U24locvar1_4() const { return ___U24locvar1_4; }
	inline int32_t* get_address_of_U24locvar1_4() { return &___U24locvar1_4; }
	inline void set_U24locvar1_4(int32_t value)
	{
		___U24locvar1_4 = value;
	}

	inline static int32_t get_offset_of_U3CtestRU3E__0_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U3CtestRU3E__0_5)); }
	inline float get_U3CtestRU3E__0_5() const { return ___U3CtestRU3E__0_5; }
	inline float* get_address_of_U3CtestRU3E__0_5() { return &___U3CtestRU3E__0_5; }
	inline void set_U3CtestRU3E__0_5(float value)
	{
		___U3CtestRU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U24this_6)); }
	inline ExplosionFireAndDebris_t2411343565 * get_U24this_6() const { return ___U24this_6; }
	inline ExplosionFireAndDebris_t2411343565 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ExplosionFireAndDebris_t2411343565 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t150375094, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T150375094_H
#ifndef U3CONCOLLISIONENTERU3EC__ITERATOR0_T2695961835_H
#define U3CONCOLLISIONENTERU3EC__ITERATOR0_T2695961835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator0
struct  U3COnCollisionEnterU3Ec__Iterator0_t2695961835  : public RuntimeObject
{
public:
	// UnityEngine.Collision UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator0::col
	Collision_t4262080450 * ___col_0;
	// UnityStandardAssets.Effects.Explosive UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator0::$this
	Explosive_t792321375 * ___U24this_1;
	// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_col_0() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ec__Iterator0_t2695961835, ___col_0)); }
	inline Collision_t4262080450 * get_col_0() const { return ___col_0; }
	inline Collision_t4262080450 ** get_address_of_col_0() { return &___col_0; }
	inline void set_col_0(Collision_t4262080450 * value)
	{
		___col_0 = value;
		Il2CppCodeGenWriteBarrier((&___col_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ec__Iterator0_t2695961835, ___U24this_1)); }
	inline Explosive_t792321375 * get_U24this_1() const { return ___U24this_1; }
	inline Explosive_t792321375 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Explosive_t792321375 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ec__Iterator0_t2695961835, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ec__Iterator0_t2695961835, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ec__Iterator0_t2695961835, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONCOLLISIONENTERU3EC__ITERATOR0_T2695961835_H
#ifndef MESHCONTAINER_T140041298_H
#define MESHCONTAINER_T140041298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.MeshContainer
struct  MeshContainer_t140041298  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityStandardAssets.Water.MeshContainer::mesh
	Mesh_t3648964284 * ___mesh_0;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::vertices
	Vector3U5BU5D_t1718750761* ___vertices_1;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::normals
	Vector3U5BU5D_t1718750761* ___normals_2;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___mesh_0)); }
	inline Mesh_t3648964284 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t3648964284 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t3648964284 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___vertices_1)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_1() const { return ___vertices_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}

	inline static int32_t get_offset_of_normals_2() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___normals_2)); }
	inline Vector3U5BU5D_t1718750761* get_normals_2() const { return ___normals_2; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_2() { return &___normals_2; }
	inline void set_normals_2(Vector3U5BU5D_t1718750761* value)
	{
		___normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___normals_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCONTAINER_T140041298_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef ENUMERATOR_T2983131547_H
#define ENUMERATOR_T2983131547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>
struct  Enumerator_t2983131547 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1093887670 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Rigidbody_t3916780224 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2983131547, ___l_0)); }
	inline List_1_t1093887670 * get_l_0() const { return ___l_0; }
	inline List_1_t1093887670 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1093887670 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2983131547, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2983131547, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2983131547, ___current_3)); }
	inline Rigidbody_t3916780224 * get_current_3() const { return ___current_3; }
	inline Rigidbody_t3916780224 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Rigidbody_t3916780224 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2983131547_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef COLLISIONFLAGS_T1776808576_H
#define COLLISIONFLAGS_T1776808576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t1776808576 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t1776808576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T1776808576_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef UPDATETYPE_T2449601881_H
#define UPDATETYPE_T2449601881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
struct  UpdateType_t2449601881 
{
public:
	// System.Int32 UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t2449601881, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T2449601881_H
#ifndef MOUSELOOK_T2859678661_H
#define MOUSELOOK_T2859678661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct  MouseLook_t2859678661  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::XSensitivity
	float ___XSensitivity_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::YSensitivity
	float ___YSensitivity_1;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::clampVerticalRotation
	bool ___clampVerticalRotation_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MinimumX
	float ___MinimumX_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MaximumX
	float ___MaximumX_4;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::smooth
	bool ___smooth_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::smoothTime
	float ___smoothTime_6;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::lockCursor
	bool ___lockCursor_7;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CharacterTargetRot
	Quaternion_t2301928331  ___m_CharacterTargetRot_8;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CameraTargetRot
	Quaternion_t2301928331  ___m_CameraTargetRot_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::m_cursorIsLocked
	bool ___m_cursorIsLocked_10;

public:
	inline static int32_t get_offset_of_XSensitivity_0() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___XSensitivity_0)); }
	inline float get_XSensitivity_0() const { return ___XSensitivity_0; }
	inline float* get_address_of_XSensitivity_0() { return &___XSensitivity_0; }
	inline void set_XSensitivity_0(float value)
	{
		___XSensitivity_0 = value;
	}

	inline static int32_t get_offset_of_YSensitivity_1() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___YSensitivity_1)); }
	inline float get_YSensitivity_1() const { return ___YSensitivity_1; }
	inline float* get_address_of_YSensitivity_1() { return &___YSensitivity_1; }
	inline void set_YSensitivity_1(float value)
	{
		___YSensitivity_1 = value;
	}

	inline static int32_t get_offset_of_clampVerticalRotation_2() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___clampVerticalRotation_2)); }
	inline bool get_clampVerticalRotation_2() const { return ___clampVerticalRotation_2; }
	inline bool* get_address_of_clampVerticalRotation_2() { return &___clampVerticalRotation_2; }
	inline void set_clampVerticalRotation_2(bool value)
	{
		___clampVerticalRotation_2 = value;
	}

	inline static int32_t get_offset_of_MinimumX_3() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___MinimumX_3)); }
	inline float get_MinimumX_3() const { return ___MinimumX_3; }
	inline float* get_address_of_MinimumX_3() { return &___MinimumX_3; }
	inline void set_MinimumX_3(float value)
	{
		___MinimumX_3 = value;
	}

	inline static int32_t get_offset_of_MaximumX_4() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___MaximumX_4)); }
	inline float get_MaximumX_4() const { return ___MaximumX_4; }
	inline float* get_address_of_MaximumX_4() { return &___MaximumX_4; }
	inline void set_MaximumX_4(float value)
	{
		___MaximumX_4 = value;
	}

	inline static int32_t get_offset_of_smooth_5() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___smooth_5)); }
	inline bool get_smooth_5() const { return ___smooth_5; }
	inline bool* get_address_of_smooth_5() { return &___smooth_5; }
	inline void set_smooth_5(bool value)
	{
		___smooth_5 = value;
	}

	inline static int32_t get_offset_of_smoothTime_6() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___smoothTime_6)); }
	inline float get_smoothTime_6() const { return ___smoothTime_6; }
	inline float* get_address_of_smoothTime_6() { return &___smoothTime_6; }
	inline void set_smoothTime_6(float value)
	{
		___smoothTime_6 = value;
	}

	inline static int32_t get_offset_of_lockCursor_7() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___lockCursor_7)); }
	inline bool get_lockCursor_7() const { return ___lockCursor_7; }
	inline bool* get_address_of_lockCursor_7() { return &___lockCursor_7; }
	inline void set_lockCursor_7(bool value)
	{
		___lockCursor_7 = value;
	}

	inline static int32_t get_offset_of_m_CharacterTargetRot_8() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_CharacterTargetRot_8)); }
	inline Quaternion_t2301928331  get_m_CharacterTargetRot_8() const { return ___m_CharacterTargetRot_8; }
	inline Quaternion_t2301928331 * get_address_of_m_CharacterTargetRot_8() { return &___m_CharacterTargetRot_8; }
	inline void set_m_CharacterTargetRot_8(Quaternion_t2301928331  value)
	{
		___m_CharacterTargetRot_8 = value;
	}

	inline static int32_t get_offset_of_m_CameraTargetRot_9() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_CameraTargetRot_9)); }
	inline Quaternion_t2301928331  get_m_CameraTargetRot_9() const { return ___m_CameraTargetRot_9; }
	inline Quaternion_t2301928331 * get_address_of_m_CameraTargetRot_9() { return &___m_CameraTargetRot_9; }
	inline void set_m_CameraTargetRot_9(Quaternion_t2301928331  value)
	{
		___m_CameraTargetRot_9 = value;
	}

	inline static int32_t get_offset_of_m_cursorIsLocked_10() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_cursorIsLocked_10)); }
	inline bool get_m_cursorIsLocked_10() const { return ___m_cursorIsLocked_10; }
	inline bool* get_address_of_m_cursorIsLocked_10() { return &___m_cursorIsLocked_10; }
	inline void set_m_cursorIsLocked_10(bool value)
	{
		___m_cursorIsLocked_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSELOOK_T2859678661_H
#ifndef ACTIVEINPUTMETHOD_T139315314_H
#define ACTIVEINPUTMETHOD_T139315314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t139315314 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t139315314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T139315314_H
#ifndef AXISOPTION_T3128671669_H
#define AXISOPTION_T3128671669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t3128671669 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t3128671669, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T3128671669_H
#ifndef MAPPINGTYPE_T2039944511_H
#define MAPPINGTYPE_T2039944511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_t2039944511 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MappingType_t2039944511, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T2039944511_H
#ifndef AXISOPTIONS_T3101732129_H
#define AXISOPTIONS_T3101732129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t3101732129 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOptions_t3101732129, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T3101732129_H
#ifndef AXISOPTION_T1372819835_H
#define AXISOPTION_T1372819835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t1372819835 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t1372819835, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1372819835_H
#ifndef CONTROLSTYLE_T1372986211_H
#define CONTROLSTYLE_T1372986211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t1372986211 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlStyle_t1372986211, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T1372986211_H
#ifndef VIRTUALINPUT_T2597455733_H
#define VIRTUALINPUT_T2597455733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t2597455733  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_t3722313464  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t3872604895 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t2541822629 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_t3319525431 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t3872604895 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t3872604895 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t3872604895 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t2541822629 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t2541822629 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t2541822629 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_AlwaysUseVirtual_3)); }
	inline List_1_t3319525431 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_t3319525431 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_t3319525431 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T2597455733_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3271890998_H
#define U3CSTARTU3EC__ITERATOR0_T3271890998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3271890998  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::<multiplier>__0
	float ___U3CmultiplierU3E__0_0;
	// System.Single UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::<r>__0
	float ___U3CrU3E__0_1;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::<cols>__0
	ColliderU5BU5D_t4234922487* ___U3CcolsU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody> UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::<rigidbodies>__0
	List_1_t1093887670 * ___U3CrigidbodiesU3E__0_3;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::$locvar0
	ColliderU5BU5D_t4234922487* ___U24locvar0_4;
	// System.Int32 UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::$locvar1
	int32_t ___U24locvar1_5;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody> UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::$locvar2
	Enumerator_t2983131547  ___U24locvar2_6;
	// UnityStandardAssets.Effects.ExplosionPhysicsForce UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::$this
	ExplosionPhysicsForce_t3982641844 * ___U24this_7;
	// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CmultiplierU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U3CmultiplierU3E__0_0)); }
	inline float get_U3CmultiplierU3E__0_0() const { return ___U3CmultiplierU3E__0_0; }
	inline float* get_address_of_U3CmultiplierU3E__0_0() { return &___U3CmultiplierU3E__0_0; }
	inline void set_U3CmultiplierU3E__0_0(float value)
	{
		___U3CmultiplierU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CrU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U3CrU3E__0_1)); }
	inline float get_U3CrU3E__0_1() const { return ___U3CrU3E__0_1; }
	inline float* get_address_of_U3CrU3E__0_1() { return &___U3CrU3E__0_1; }
	inline void set_U3CrU3E__0_1(float value)
	{
		___U3CrU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CcolsU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U3CcolsU3E__0_2)); }
	inline ColliderU5BU5D_t4234922487* get_U3CcolsU3E__0_2() const { return ___U3CcolsU3E__0_2; }
	inline ColliderU5BU5D_t4234922487** get_address_of_U3CcolsU3E__0_2() { return &___U3CcolsU3E__0_2; }
	inline void set_U3CcolsU3E__0_2(ColliderU5BU5D_t4234922487* value)
	{
		___U3CcolsU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolsU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrigidbodiesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U3CrigidbodiesU3E__0_3)); }
	inline List_1_t1093887670 * get_U3CrigidbodiesU3E__0_3() const { return ___U3CrigidbodiesU3E__0_3; }
	inline List_1_t1093887670 ** get_address_of_U3CrigidbodiesU3E__0_3() { return &___U3CrigidbodiesU3E__0_3; }
	inline void set_U3CrigidbodiesU3E__0_3(List_1_t1093887670 * value)
	{
		___U3CrigidbodiesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrigidbodiesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U24locvar0_4)); }
	inline ColliderU5BU5D_t4234922487* get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline ColliderU5BU5D_t4234922487** get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(ColliderU5BU5D_t4234922487* value)
	{
		___U24locvar0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_4), value);
	}

	inline static int32_t get_offset_of_U24locvar1_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U24locvar1_5)); }
	inline int32_t get_U24locvar1_5() const { return ___U24locvar1_5; }
	inline int32_t* get_address_of_U24locvar1_5() { return &___U24locvar1_5; }
	inline void set_U24locvar1_5(int32_t value)
	{
		___U24locvar1_5 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U24locvar2_6)); }
	inline Enumerator_t2983131547  get_U24locvar2_6() const { return ___U24locvar2_6; }
	inline Enumerator_t2983131547 * get_address_of_U24locvar2_6() { return &___U24locvar2_6; }
	inline void set_U24locvar2_6(Enumerator_t2983131547  value)
	{
		___U24locvar2_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U24this_7)); }
	inline ExplosionPhysicsForce_t3982641844 * get_U24this_7() const { return ___U24this_7; }
	inline ExplosionPhysicsForce_t3982641844 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(ExplosionPhysicsForce_t3982641844 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3271890998, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3271890998_H
#ifndef WATERMODE_T293960580_H
#define WATERMODE_T293960580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water/WaterMode
struct  WaterMode_t293960580 
{
public:
	// System.Int32 UnityStandardAssets.Water.Water/WaterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterMode_t293960580, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERMODE_T293960580_H
#ifndef WATERQUALITY_T1541080576_H
#define WATERQUALITY_T1541080576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterQuality
struct  WaterQuality_t1541080576 
{
public:
	// System.Int32 UnityStandardAssets.Water.WaterQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterQuality_t1541080576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERQUALITY_T1541080576_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MOVEMENTSETTINGS_T1096092444_H
#define MOVEMENTSETTINGS_T1096092444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct  MovementSettings_t1096092444  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::ForwardSpeed
	float ___ForwardSpeed_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::BackwardSpeed
	float ___BackwardSpeed_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::StrafeSpeed
	float ___StrafeSpeed_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunMultiplier
	float ___RunMultiplier_3;
	// UnityEngine.KeyCode UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunKey
	int32_t ___RunKey_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::JumpForce
	float ___JumpForce_5;
	// UnityEngine.AnimationCurve UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::SlopeCurveModifier
	AnimationCurve_t3046754366 * ___SlopeCurveModifier_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::CurrentTargetSpeed
	float ___CurrentTargetSpeed_7;

public:
	inline static int32_t get_offset_of_ForwardSpeed_0() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___ForwardSpeed_0)); }
	inline float get_ForwardSpeed_0() const { return ___ForwardSpeed_0; }
	inline float* get_address_of_ForwardSpeed_0() { return &___ForwardSpeed_0; }
	inline void set_ForwardSpeed_0(float value)
	{
		___ForwardSpeed_0 = value;
	}

	inline static int32_t get_offset_of_BackwardSpeed_1() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___BackwardSpeed_1)); }
	inline float get_BackwardSpeed_1() const { return ___BackwardSpeed_1; }
	inline float* get_address_of_BackwardSpeed_1() { return &___BackwardSpeed_1; }
	inline void set_BackwardSpeed_1(float value)
	{
		___BackwardSpeed_1 = value;
	}

	inline static int32_t get_offset_of_StrafeSpeed_2() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___StrafeSpeed_2)); }
	inline float get_StrafeSpeed_2() const { return ___StrafeSpeed_2; }
	inline float* get_address_of_StrafeSpeed_2() { return &___StrafeSpeed_2; }
	inline void set_StrafeSpeed_2(float value)
	{
		___StrafeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RunMultiplier_3() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___RunMultiplier_3)); }
	inline float get_RunMultiplier_3() const { return ___RunMultiplier_3; }
	inline float* get_address_of_RunMultiplier_3() { return &___RunMultiplier_3; }
	inline void set_RunMultiplier_3(float value)
	{
		___RunMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_RunKey_4() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___RunKey_4)); }
	inline int32_t get_RunKey_4() const { return ___RunKey_4; }
	inline int32_t* get_address_of_RunKey_4() { return &___RunKey_4; }
	inline void set_RunKey_4(int32_t value)
	{
		___RunKey_4 = value;
	}

	inline static int32_t get_offset_of_JumpForce_5() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___JumpForce_5)); }
	inline float get_JumpForce_5() const { return ___JumpForce_5; }
	inline float* get_address_of_JumpForce_5() { return &___JumpForce_5; }
	inline void set_JumpForce_5(float value)
	{
		___JumpForce_5 = value;
	}

	inline static int32_t get_offset_of_SlopeCurveModifier_6() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___SlopeCurveModifier_6)); }
	inline AnimationCurve_t3046754366 * get_SlopeCurveModifier_6() const { return ___SlopeCurveModifier_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_SlopeCurveModifier_6() { return &___SlopeCurveModifier_6; }
	inline void set_SlopeCurveModifier_6(AnimationCurve_t3046754366 * value)
	{
		___SlopeCurveModifier_6 = value;
		Il2CppCodeGenWriteBarrier((&___SlopeCurveModifier_6), value);
	}

	inline static int32_t get_offset_of_CurrentTargetSpeed_7() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___CurrentTargetSpeed_7)); }
	inline float get_CurrentTargetSpeed_7() const { return ___CurrentTargetSpeed_7; }
	inline float* get_address_of_CurrentTargetSpeed_7() { return &___CurrentTargetSpeed_7; }
	inline void set_CurrentTargetSpeed_7(float value)
	{
		___CurrentTargetSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTSETTINGS_T1096092444_H
#ifndef MOBILEINPUT_T2025745297_H
#define MOBILEINPUT_T2025745297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t2025745297  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T2025745297_H
#ifndef STANDALONEINPUT_T1343950252_H
#define STANDALONEINPUT_T1343950252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t1343950252  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T1343950252_H
#ifndef AXISMAPPING_T3982445645_H
#define AXISMAPPING_T3982445645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t3982445645  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T3982445645_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_4;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_5;

public:
	inline static int32_t get_offset_of_m_Trigger_4() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_4)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_4() const { return ___m_Trigger_4; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_4() { return &___m_Trigger_4; }
	inline void set_m_Trigger_4(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_4), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_5() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_5)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_5() const { return ___m_EventPayload_5; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_5() { return &___m_EventPayload_5; }
	inline void set_m_EventPayload_5(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_5;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_6;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_7;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_8;

public:
	inline static int32_t get_offset_of_m_EventName_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_4)); }
	inline String_t* get_m_EventName_4() const { return ___m_EventName_4; }
	inline String_t** get_address_of_m_EventName_4() { return &___m_EventName_4; }
	inline void set_m_EventName_4(String_t* value)
	{
		___m_EventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_4), value);
	}

	inline static int32_t get_offset_of_m_Dict_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_5)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_5() const { return ___m_Dict_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_5() { return &___m_Dict_5; }
	inline void set_m_Dict_5(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_5), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_6)); }
	inline int32_t get_m_PrevDictHash_6() const { return ___m_PrevDictHash_6; }
	inline int32_t* get_address_of_m_PrevDictHash_6() { return &___m_PrevDictHash_6; }
	inline void set_m_PrevDictHash_6(int32_t value)
	{
		___m_PrevDictHash_6 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_7() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_7)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_7() const { return ___m_TrackableProperty_7; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_7() { return &___m_TrackableProperty_7; }
	inline void set_m_TrackableProperty_7(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_7), value);
	}

	inline static int32_t get_offset_of_m_Trigger_8() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_8)); }
	inline int32_t get_m_Trigger_8() const { return ___m_Trigger_8; }
	inline int32_t* get_address_of_m_Trigger_8() { return &___m_Trigger_8; }
	inline void set_m_Trigger_8(int32_t value)
	{
		___m_Trigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef ABSTRACTTARGETFOLLOWER_T1919708159_H
#define ABSTRACTTARGETFOLLOWER_T1919708159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.AbstractTargetFollower
struct  AbstractTargetFollower_t1919708159  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::m_Target
	Transform_t3600365921 * ___m_Target_4;
	// System.Boolean UnityStandardAssets.Cameras.AbstractTargetFollower::m_AutoTargetPlayer
	bool ___m_AutoTargetPlayer_5;
	// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType UnityStandardAssets.Cameras.AbstractTargetFollower::m_UpdateType
	int32_t ___m_UpdateType_6;
	// UnityEngine.Rigidbody UnityStandardAssets.Cameras.AbstractTargetFollower::targetRigidbody
	Rigidbody_t3916780224 * ___targetRigidbody_7;

public:
	inline static int32_t get_offset_of_m_Target_4() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___m_Target_4)); }
	inline Transform_t3600365921 * get_m_Target_4() const { return ___m_Target_4; }
	inline Transform_t3600365921 ** get_address_of_m_Target_4() { return &___m_Target_4; }
	inline void set_m_Target_4(Transform_t3600365921 * value)
	{
		___m_Target_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_4), value);
	}

	inline static int32_t get_offset_of_m_AutoTargetPlayer_5() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___m_AutoTargetPlayer_5)); }
	inline bool get_m_AutoTargetPlayer_5() const { return ___m_AutoTargetPlayer_5; }
	inline bool* get_address_of_m_AutoTargetPlayer_5() { return &___m_AutoTargetPlayer_5; }
	inline void set_m_AutoTargetPlayer_5(bool value)
	{
		___m_AutoTargetPlayer_5 = value;
	}

	inline static int32_t get_offset_of_m_UpdateType_6() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___m_UpdateType_6)); }
	inline int32_t get_m_UpdateType_6() const { return ___m_UpdateType_6; }
	inline int32_t* get_address_of_m_UpdateType_6() { return &___m_UpdateType_6; }
	inline void set_m_UpdateType_6(int32_t value)
	{
		___m_UpdateType_6 = value;
	}

	inline static int32_t get_offset_of_targetRigidbody_7() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___targetRigidbody_7)); }
	inline Rigidbody_t3916780224 * get_targetRigidbody_7() const { return ___targetRigidbody_7; }
	inline Rigidbody_t3916780224 ** get_address_of_targetRigidbody_7() { return &___targetRigidbody_7; }
	inline void set_targetRigidbody_7(Rigidbody_t3916780224 * value)
	{
		___targetRigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetRigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTARGETFOLLOWER_T1919708159_H
#ifndef PROTECTCAMERAFROMWALLCLIP_T303409715_H
#define PROTECTCAMERAFROMWALLCLIP_T303409715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.ProtectCameraFromWallClip
struct  ProtectCameraFromWallClip_t303409715  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::clipMoveTime
	float ___clipMoveTime_4;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::returnTime
	float ___returnTime_5;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::sphereCastRadius
	float ___sphereCastRadius_6;
	// System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::visualiseInEditor
	bool ___visualiseInEditor_7;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::closestDistance
	float ___closestDistance_8;
	// System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::<protecting>k__BackingField
	bool ___U3CprotectingU3Ek__BackingField_9;
	// System.String UnityStandardAssets.Cameras.ProtectCameraFromWallClip::dontClipTag
	String_t* ___dontClipTag_10;
	// UnityEngine.Transform UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Cam
	Transform_t3600365921 * ___m_Cam_11;
	// UnityEngine.Transform UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Pivot
	Transform_t3600365921 * ___m_Pivot_12;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_OriginalDist
	float ___m_OriginalDist_13;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_MoveVelocity
	float ___m_MoveVelocity_14;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_CurrentDist
	float ___m_CurrentDist_15;
	// UnityEngine.Ray UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Ray
	Ray_t3785851493  ___m_Ray_16;
	// UnityEngine.RaycastHit[] UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Hits
	RaycastHitU5BU5D_t1690781147* ___m_Hits_17;
	// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_RayHitComparer
	RayHitComparer_t2205555946 * ___m_RayHitComparer_18;

public:
	inline static int32_t get_offset_of_clipMoveTime_4() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___clipMoveTime_4)); }
	inline float get_clipMoveTime_4() const { return ___clipMoveTime_4; }
	inline float* get_address_of_clipMoveTime_4() { return &___clipMoveTime_4; }
	inline void set_clipMoveTime_4(float value)
	{
		___clipMoveTime_4 = value;
	}

	inline static int32_t get_offset_of_returnTime_5() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___returnTime_5)); }
	inline float get_returnTime_5() const { return ___returnTime_5; }
	inline float* get_address_of_returnTime_5() { return &___returnTime_5; }
	inline void set_returnTime_5(float value)
	{
		___returnTime_5 = value;
	}

	inline static int32_t get_offset_of_sphereCastRadius_6() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___sphereCastRadius_6)); }
	inline float get_sphereCastRadius_6() const { return ___sphereCastRadius_6; }
	inline float* get_address_of_sphereCastRadius_6() { return &___sphereCastRadius_6; }
	inline void set_sphereCastRadius_6(float value)
	{
		___sphereCastRadius_6 = value;
	}

	inline static int32_t get_offset_of_visualiseInEditor_7() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___visualiseInEditor_7)); }
	inline bool get_visualiseInEditor_7() const { return ___visualiseInEditor_7; }
	inline bool* get_address_of_visualiseInEditor_7() { return &___visualiseInEditor_7; }
	inline void set_visualiseInEditor_7(bool value)
	{
		___visualiseInEditor_7 = value;
	}

	inline static int32_t get_offset_of_closestDistance_8() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___closestDistance_8)); }
	inline float get_closestDistance_8() const { return ___closestDistance_8; }
	inline float* get_address_of_closestDistance_8() { return &___closestDistance_8; }
	inline void set_closestDistance_8(float value)
	{
		___closestDistance_8 = value;
	}

	inline static int32_t get_offset_of_U3CprotectingU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___U3CprotectingU3Ek__BackingField_9)); }
	inline bool get_U3CprotectingU3Ek__BackingField_9() const { return ___U3CprotectingU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CprotectingU3Ek__BackingField_9() { return &___U3CprotectingU3Ek__BackingField_9; }
	inline void set_U3CprotectingU3Ek__BackingField_9(bool value)
	{
		___U3CprotectingU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_dontClipTag_10() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___dontClipTag_10)); }
	inline String_t* get_dontClipTag_10() const { return ___dontClipTag_10; }
	inline String_t** get_address_of_dontClipTag_10() { return &___dontClipTag_10; }
	inline void set_dontClipTag_10(String_t* value)
	{
		___dontClipTag_10 = value;
		Il2CppCodeGenWriteBarrier((&___dontClipTag_10), value);
	}

	inline static int32_t get_offset_of_m_Cam_11() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Cam_11)); }
	inline Transform_t3600365921 * get_m_Cam_11() const { return ___m_Cam_11; }
	inline Transform_t3600365921 ** get_address_of_m_Cam_11() { return &___m_Cam_11; }
	inline void set_m_Cam_11(Transform_t3600365921 * value)
	{
		___m_Cam_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_11), value);
	}

	inline static int32_t get_offset_of_m_Pivot_12() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Pivot_12)); }
	inline Transform_t3600365921 * get_m_Pivot_12() const { return ___m_Pivot_12; }
	inline Transform_t3600365921 ** get_address_of_m_Pivot_12() { return &___m_Pivot_12; }
	inline void set_m_Pivot_12(Transform_t3600365921 * value)
	{
		___m_Pivot_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pivot_12), value);
	}

	inline static int32_t get_offset_of_m_OriginalDist_13() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_OriginalDist_13)); }
	inline float get_m_OriginalDist_13() const { return ___m_OriginalDist_13; }
	inline float* get_address_of_m_OriginalDist_13() { return &___m_OriginalDist_13; }
	inline void set_m_OriginalDist_13(float value)
	{
		___m_OriginalDist_13 = value;
	}

	inline static int32_t get_offset_of_m_MoveVelocity_14() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_MoveVelocity_14)); }
	inline float get_m_MoveVelocity_14() const { return ___m_MoveVelocity_14; }
	inline float* get_address_of_m_MoveVelocity_14() { return &___m_MoveVelocity_14; }
	inline void set_m_MoveVelocity_14(float value)
	{
		___m_MoveVelocity_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentDist_15() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_CurrentDist_15)); }
	inline float get_m_CurrentDist_15() const { return ___m_CurrentDist_15; }
	inline float* get_address_of_m_CurrentDist_15() { return &___m_CurrentDist_15; }
	inline void set_m_CurrentDist_15(float value)
	{
		___m_CurrentDist_15 = value;
	}

	inline static int32_t get_offset_of_m_Ray_16() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Ray_16)); }
	inline Ray_t3785851493  get_m_Ray_16() const { return ___m_Ray_16; }
	inline Ray_t3785851493 * get_address_of_m_Ray_16() { return &___m_Ray_16; }
	inline void set_m_Ray_16(Ray_t3785851493  value)
	{
		___m_Ray_16 = value;
	}

	inline static int32_t get_offset_of_m_Hits_17() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Hits_17)); }
	inline RaycastHitU5BU5D_t1690781147* get_m_Hits_17() const { return ___m_Hits_17; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_m_Hits_17() { return &___m_Hits_17; }
	inline void set_m_Hits_17(RaycastHitU5BU5D_t1690781147* value)
	{
		___m_Hits_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hits_17), value);
	}

	inline static int32_t get_offset_of_m_RayHitComparer_18() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_RayHitComparer_18)); }
	inline RayHitComparer_t2205555946 * get_m_RayHitComparer_18() const { return ___m_RayHitComparer_18; }
	inline RayHitComparer_t2205555946 ** get_address_of_m_RayHitComparer_18() { return &___m_RayHitComparer_18; }
	inline void set_m_RayHitComparer_18(RayHitComparer_t2205555946 * value)
	{
		___m_RayHitComparer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_RayHitComparer_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTCAMERAFROMWALLCLIP_T303409715_H
#ifndef FIRSTPERSONCONTROLLER_T2020989554_H
#define FIRSTPERSONCONTROLLER_T2020989554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.FirstPersonController
struct  FirstPersonController_t2020989554  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_IsWalking
	bool ___m_IsWalking_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_WalkSpeed
	float ___m_WalkSpeed_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunSpeed
	float ___m_RunSpeed_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunstepLenghten
	float ___m_RunstepLenghten_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSpeed
	float ___m_JumpSpeed_8;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StickToGroundForce
	float ___m_StickToGroundForce_9;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_GravityMultiplier
	float ___m_GravityMultiplier_10;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MouseLook
	MouseLook_t2859678661 * ___m_MouseLook_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseFovKick
	bool ___m_UseFovKick_12;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FovKick
	FOVKick_t120370150 * ___m_FovKick_13;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseHeadBob
	bool ___m_UseHeadBob_14;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_HeadBob
	CurveControlledBob_t2679313829 * ___m_HeadBob_15;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpBob
	LerpControlledBob_t1895875871 * ___m_JumpBob_16;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepInterval
	float ___m_StepInterval_17;
	// UnityEngine.AudioClip[] UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FootstepSounds
	AudioClipU5BU5D_t143221404* ___m_FootstepSounds_18;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSound
	AudioClip_t3680889665 * ___m_JumpSound_19;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_LandSound
	AudioClip_t3680889665 * ___m_LandSound_20;
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Camera
	Camera_t4157153871 * ___m_Camera_21;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jump
	bool ___m_Jump_22;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_YRotation
	float ___m_YRotation_23;
	// UnityEngine.Vector2 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Input
	Vector2_t2156229523  ___m_Input_24;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MoveDir
	Vector3_t3722313464  ___m_MoveDir_25;
	// UnityEngine.CharacterController UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CharacterController
	CharacterController_t1138636865 * ___m_CharacterController_26;
	// UnityEngine.CollisionFlags UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CollisionFlags
	int32_t ___m_CollisionFlags_27;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_28;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_29;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepCycle
	float ___m_StepCycle_30;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_NextStep
	float ___m_NextStep_31;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jumping
	bool ___m_Jumping_32;
	// UnityEngine.AudioSource UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_AudioSource
	AudioSource_t3935305588 * ___m_AudioSource_33;

public:
	inline static int32_t get_offset_of_m_IsWalking_4() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_IsWalking_4)); }
	inline bool get_m_IsWalking_4() const { return ___m_IsWalking_4; }
	inline bool* get_address_of_m_IsWalking_4() { return &___m_IsWalking_4; }
	inline void set_m_IsWalking_4(bool value)
	{
		___m_IsWalking_4 = value;
	}

	inline static int32_t get_offset_of_m_WalkSpeed_5() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_WalkSpeed_5)); }
	inline float get_m_WalkSpeed_5() const { return ___m_WalkSpeed_5; }
	inline float* get_address_of_m_WalkSpeed_5() { return &___m_WalkSpeed_5; }
	inline void set_m_WalkSpeed_5(float value)
	{
		___m_WalkSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_RunSpeed_6() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_RunSpeed_6)); }
	inline float get_m_RunSpeed_6() const { return ___m_RunSpeed_6; }
	inline float* get_address_of_m_RunSpeed_6() { return &___m_RunSpeed_6; }
	inline void set_m_RunSpeed_6(float value)
	{
		___m_RunSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_RunstepLenghten_7() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_RunstepLenghten_7)); }
	inline float get_m_RunstepLenghten_7() const { return ___m_RunstepLenghten_7; }
	inline float* get_address_of_m_RunstepLenghten_7() { return &___m_RunstepLenghten_7; }
	inline void set_m_RunstepLenghten_7(float value)
	{
		___m_RunstepLenghten_7 = value;
	}

	inline static int32_t get_offset_of_m_JumpSpeed_8() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpSpeed_8)); }
	inline float get_m_JumpSpeed_8() const { return ___m_JumpSpeed_8; }
	inline float* get_address_of_m_JumpSpeed_8() { return &___m_JumpSpeed_8; }
	inline void set_m_JumpSpeed_8(float value)
	{
		___m_JumpSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_StickToGroundForce_9() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StickToGroundForce_9)); }
	inline float get_m_StickToGroundForce_9() const { return ___m_StickToGroundForce_9; }
	inline float* get_address_of_m_StickToGroundForce_9() { return &___m_StickToGroundForce_9; }
	inline void set_m_StickToGroundForce_9(float value)
	{
		___m_StickToGroundForce_9 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_10() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_GravityMultiplier_10)); }
	inline float get_m_GravityMultiplier_10() const { return ___m_GravityMultiplier_10; }
	inline float* get_address_of_m_GravityMultiplier_10() { return &___m_GravityMultiplier_10; }
	inline void set_m_GravityMultiplier_10(float value)
	{
		___m_GravityMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_m_MouseLook_11() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_MouseLook_11)); }
	inline MouseLook_t2859678661 * get_m_MouseLook_11() const { return ___m_MouseLook_11; }
	inline MouseLook_t2859678661 ** get_address_of_m_MouseLook_11() { return &___m_MouseLook_11; }
	inline void set_m_MouseLook_11(MouseLook_t2859678661 * value)
	{
		___m_MouseLook_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseLook_11), value);
	}

	inline static int32_t get_offset_of_m_UseFovKick_12() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_UseFovKick_12)); }
	inline bool get_m_UseFovKick_12() const { return ___m_UseFovKick_12; }
	inline bool* get_address_of_m_UseFovKick_12() { return &___m_UseFovKick_12; }
	inline void set_m_UseFovKick_12(bool value)
	{
		___m_UseFovKick_12 = value;
	}

	inline static int32_t get_offset_of_m_FovKick_13() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_FovKick_13)); }
	inline FOVKick_t120370150 * get_m_FovKick_13() const { return ___m_FovKick_13; }
	inline FOVKick_t120370150 ** get_address_of_m_FovKick_13() { return &___m_FovKick_13; }
	inline void set_m_FovKick_13(FOVKick_t120370150 * value)
	{
		___m_FovKick_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_FovKick_13), value);
	}

	inline static int32_t get_offset_of_m_UseHeadBob_14() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_UseHeadBob_14)); }
	inline bool get_m_UseHeadBob_14() const { return ___m_UseHeadBob_14; }
	inline bool* get_address_of_m_UseHeadBob_14() { return &___m_UseHeadBob_14; }
	inline void set_m_UseHeadBob_14(bool value)
	{
		___m_UseHeadBob_14 = value;
	}

	inline static int32_t get_offset_of_m_HeadBob_15() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_HeadBob_15)); }
	inline CurveControlledBob_t2679313829 * get_m_HeadBob_15() const { return ___m_HeadBob_15; }
	inline CurveControlledBob_t2679313829 ** get_address_of_m_HeadBob_15() { return &___m_HeadBob_15; }
	inline void set_m_HeadBob_15(CurveControlledBob_t2679313829 * value)
	{
		___m_HeadBob_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_HeadBob_15), value);
	}

	inline static int32_t get_offset_of_m_JumpBob_16() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpBob_16)); }
	inline LerpControlledBob_t1895875871 * get_m_JumpBob_16() const { return ___m_JumpBob_16; }
	inline LerpControlledBob_t1895875871 ** get_address_of_m_JumpBob_16() { return &___m_JumpBob_16; }
	inline void set_m_JumpBob_16(LerpControlledBob_t1895875871 * value)
	{
		___m_JumpBob_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpBob_16), value);
	}

	inline static int32_t get_offset_of_m_StepInterval_17() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StepInterval_17)); }
	inline float get_m_StepInterval_17() const { return ___m_StepInterval_17; }
	inline float* get_address_of_m_StepInterval_17() { return &___m_StepInterval_17; }
	inline void set_m_StepInterval_17(float value)
	{
		___m_StepInterval_17 = value;
	}

	inline static int32_t get_offset_of_m_FootstepSounds_18() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_FootstepSounds_18)); }
	inline AudioClipU5BU5D_t143221404* get_m_FootstepSounds_18() const { return ___m_FootstepSounds_18; }
	inline AudioClipU5BU5D_t143221404** get_address_of_m_FootstepSounds_18() { return &___m_FootstepSounds_18; }
	inline void set_m_FootstepSounds_18(AudioClipU5BU5D_t143221404* value)
	{
		___m_FootstepSounds_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_FootstepSounds_18), value);
	}

	inline static int32_t get_offset_of_m_JumpSound_19() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpSound_19)); }
	inline AudioClip_t3680889665 * get_m_JumpSound_19() const { return ___m_JumpSound_19; }
	inline AudioClip_t3680889665 ** get_address_of_m_JumpSound_19() { return &___m_JumpSound_19; }
	inline void set_m_JumpSound_19(AudioClip_t3680889665 * value)
	{
		___m_JumpSound_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpSound_19), value);
	}

	inline static int32_t get_offset_of_m_LandSound_20() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_LandSound_20)); }
	inline AudioClip_t3680889665 * get_m_LandSound_20() const { return ___m_LandSound_20; }
	inline AudioClip_t3680889665 ** get_address_of_m_LandSound_20() { return &___m_LandSound_20; }
	inline void set_m_LandSound_20(AudioClip_t3680889665 * value)
	{
		___m_LandSound_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_LandSound_20), value);
	}

	inline static int32_t get_offset_of_m_Camera_21() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Camera_21)); }
	inline Camera_t4157153871 * get_m_Camera_21() const { return ___m_Camera_21; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_21() { return &___m_Camera_21; }
	inline void set_m_Camera_21(Camera_t4157153871 * value)
	{
		___m_Camera_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_21), value);
	}

	inline static int32_t get_offset_of_m_Jump_22() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Jump_22)); }
	inline bool get_m_Jump_22() const { return ___m_Jump_22; }
	inline bool* get_address_of_m_Jump_22() { return &___m_Jump_22; }
	inline void set_m_Jump_22(bool value)
	{
		___m_Jump_22 = value;
	}

	inline static int32_t get_offset_of_m_YRotation_23() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_YRotation_23)); }
	inline float get_m_YRotation_23() const { return ___m_YRotation_23; }
	inline float* get_address_of_m_YRotation_23() { return &___m_YRotation_23; }
	inline void set_m_YRotation_23(float value)
	{
		___m_YRotation_23 = value;
	}

	inline static int32_t get_offset_of_m_Input_24() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Input_24)); }
	inline Vector2_t2156229523  get_m_Input_24() const { return ___m_Input_24; }
	inline Vector2_t2156229523 * get_address_of_m_Input_24() { return &___m_Input_24; }
	inline void set_m_Input_24(Vector2_t2156229523  value)
	{
		___m_Input_24 = value;
	}

	inline static int32_t get_offset_of_m_MoveDir_25() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_MoveDir_25)); }
	inline Vector3_t3722313464  get_m_MoveDir_25() const { return ___m_MoveDir_25; }
	inline Vector3_t3722313464 * get_address_of_m_MoveDir_25() { return &___m_MoveDir_25; }
	inline void set_m_MoveDir_25(Vector3_t3722313464  value)
	{
		___m_MoveDir_25 = value;
	}

	inline static int32_t get_offset_of_m_CharacterController_26() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_CharacterController_26)); }
	inline CharacterController_t1138636865 * get_m_CharacterController_26() const { return ___m_CharacterController_26; }
	inline CharacterController_t1138636865 ** get_address_of_m_CharacterController_26() { return &___m_CharacterController_26; }
	inline void set_m_CharacterController_26(CharacterController_t1138636865 * value)
	{
		___m_CharacterController_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterController_26), value);
	}

	inline static int32_t get_offset_of_m_CollisionFlags_27() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_CollisionFlags_27)); }
	inline int32_t get_m_CollisionFlags_27() const { return ___m_CollisionFlags_27; }
	inline int32_t* get_address_of_m_CollisionFlags_27() { return &___m_CollisionFlags_27; }
	inline void set_m_CollisionFlags_27(int32_t value)
	{
		___m_CollisionFlags_27 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_28() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_PreviouslyGrounded_28)); }
	inline bool get_m_PreviouslyGrounded_28() const { return ___m_PreviouslyGrounded_28; }
	inline bool* get_address_of_m_PreviouslyGrounded_28() { return &___m_PreviouslyGrounded_28; }
	inline void set_m_PreviouslyGrounded_28(bool value)
	{
		___m_PreviouslyGrounded_28 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_29() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_OriginalCameraPosition_29)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_29() const { return ___m_OriginalCameraPosition_29; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_29() { return &___m_OriginalCameraPosition_29; }
	inline void set_m_OriginalCameraPosition_29(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_29 = value;
	}

	inline static int32_t get_offset_of_m_StepCycle_30() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StepCycle_30)); }
	inline float get_m_StepCycle_30() const { return ___m_StepCycle_30; }
	inline float* get_address_of_m_StepCycle_30() { return &___m_StepCycle_30; }
	inline void set_m_StepCycle_30(float value)
	{
		___m_StepCycle_30 = value;
	}

	inline static int32_t get_offset_of_m_NextStep_31() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_NextStep_31)); }
	inline float get_m_NextStep_31() const { return ___m_NextStep_31; }
	inline float* get_address_of_m_NextStep_31() { return &___m_NextStep_31; }
	inline void set_m_NextStep_31(float value)
	{
		___m_NextStep_31 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_32() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Jumping_32)); }
	inline bool get_m_Jumping_32() const { return ___m_Jumping_32; }
	inline bool* get_address_of_m_Jumping_32() { return &___m_Jumping_32; }
	inline void set_m_Jumping_32(bool value)
	{
		___m_Jumping_32 = value;
	}

	inline static int32_t get_offset_of_m_AudioSource_33() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_AudioSource_33)); }
	inline AudioSource_t3935305588 * get_m_AudioSource_33() const { return ___m_AudioSource_33; }
	inline AudioSource_t3935305588 ** get_address_of_m_AudioSource_33() { return &___m_AudioSource_33; }
	inline void set_m_AudioSource_33(AudioSource_t3935305588 * value)
	{
		___m_AudioSource_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioSource_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTPERSONCONTROLLER_T2020989554_H
#ifndef HEADBOB_T3275031667_H
#define HEADBOB_T3275031667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.HeadBob
struct  HeadBob_t3275031667  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.HeadBob::Camera
	Camera_t4157153871 * ___Camera_4;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::motionBob
	CurveControlledBob_t2679313829 * ___motionBob_5;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::jumpAndLandingBob
	LerpControlledBob_t1895875871 * ___jumpAndLandingBob_6;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController UnityStandardAssets.Characters.FirstPerson.HeadBob::rigidbodyFirstPersonController
	RigidbodyFirstPersonController_t1207297146 * ___rigidbodyFirstPersonController_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::StrideInterval
	float ___StrideInterval_8;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::RunningStrideLengthen
	float ___RunningStrideLengthen_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.HeadBob::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_10;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.HeadBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_11;

public:
	inline static int32_t get_offset_of_Camera_4() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___Camera_4)); }
	inline Camera_t4157153871 * get_Camera_4() const { return ___Camera_4; }
	inline Camera_t4157153871 ** get_address_of_Camera_4() { return &___Camera_4; }
	inline void set_Camera_4(Camera_t4157153871 * value)
	{
		___Camera_4 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_4), value);
	}

	inline static int32_t get_offset_of_motionBob_5() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___motionBob_5)); }
	inline CurveControlledBob_t2679313829 * get_motionBob_5() const { return ___motionBob_5; }
	inline CurveControlledBob_t2679313829 ** get_address_of_motionBob_5() { return &___motionBob_5; }
	inline void set_motionBob_5(CurveControlledBob_t2679313829 * value)
	{
		___motionBob_5 = value;
		Il2CppCodeGenWriteBarrier((&___motionBob_5), value);
	}

	inline static int32_t get_offset_of_jumpAndLandingBob_6() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___jumpAndLandingBob_6)); }
	inline LerpControlledBob_t1895875871 * get_jumpAndLandingBob_6() const { return ___jumpAndLandingBob_6; }
	inline LerpControlledBob_t1895875871 ** get_address_of_jumpAndLandingBob_6() { return &___jumpAndLandingBob_6; }
	inline void set_jumpAndLandingBob_6(LerpControlledBob_t1895875871 * value)
	{
		___jumpAndLandingBob_6 = value;
		Il2CppCodeGenWriteBarrier((&___jumpAndLandingBob_6), value);
	}

	inline static int32_t get_offset_of_rigidbodyFirstPersonController_7() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___rigidbodyFirstPersonController_7)); }
	inline RigidbodyFirstPersonController_t1207297146 * get_rigidbodyFirstPersonController_7() const { return ___rigidbodyFirstPersonController_7; }
	inline RigidbodyFirstPersonController_t1207297146 ** get_address_of_rigidbodyFirstPersonController_7() { return &___rigidbodyFirstPersonController_7; }
	inline void set_rigidbodyFirstPersonController_7(RigidbodyFirstPersonController_t1207297146 * value)
	{
		___rigidbodyFirstPersonController_7 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbodyFirstPersonController_7), value);
	}

	inline static int32_t get_offset_of_StrideInterval_8() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___StrideInterval_8)); }
	inline float get_StrideInterval_8() const { return ___StrideInterval_8; }
	inline float* get_address_of_StrideInterval_8() { return &___StrideInterval_8; }
	inline void set_StrideInterval_8(float value)
	{
		___StrideInterval_8 = value;
	}

	inline static int32_t get_offset_of_RunningStrideLengthen_9() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___RunningStrideLengthen_9)); }
	inline float get_RunningStrideLengthen_9() const { return ___RunningStrideLengthen_9; }
	inline float* get_address_of_RunningStrideLengthen_9() { return &___RunningStrideLengthen_9; }
	inline void set_RunningStrideLengthen_9(float value)
	{
		___RunningStrideLengthen_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_10() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___m_PreviouslyGrounded_10)); }
	inline bool get_m_PreviouslyGrounded_10() const { return ___m_PreviouslyGrounded_10; }
	inline bool* get_address_of_m_PreviouslyGrounded_10() { return &___m_PreviouslyGrounded_10; }
	inline void set_m_PreviouslyGrounded_10(bool value)
	{
		___m_PreviouslyGrounded_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_11() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___m_OriginalCameraPosition_11)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_11() const { return ___m_OriginalCameraPosition_11; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_11() { return &___m_OriginalCameraPosition_11; }
	inline void set_m_OriginalCameraPosition_11(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADBOB_T3275031667_H
#ifndef RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#define RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct  RigidbodyFirstPersonController_t1207297146  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::cam
	Camera_t4157153871 * ___cam_4;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::movementSettings
	MovementSettings_t1096092444 * ___movementSettings_5;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::mouseLook
	MouseLook_t2859678661 * ___mouseLook_6;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::advancedSettings
	AdvancedSettings_t778418834 * ___advancedSettings_7;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_RigidBody
	Rigidbody_t3916780224 * ___m_RigidBody_8;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Capsule
	CapsuleCollider_t197597763 * ___m_Capsule_9;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_YRotation
	float ___m_YRotation_10;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_GroundContactNormal
	Vector3_t3722313464  ___m_GroundContactNormal_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jump
	bool ___m_Jump_12;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_13;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jumping
	bool ___m_Jumping_14;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_IsGrounded
	bool ___m_IsGrounded_15;

public:
	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___cam_4)); }
	inline Camera_t4157153871 * get_cam_4() const { return ___cam_4; }
	inline Camera_t4157153871 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Camera_t4157153871 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier((&___cam_4), value);
	}

	inline static int32_t get_offset_of_movementSettings_5() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___movementSettings_5)); }
	inline MovementSettings_t1096092444 * get_movementSettings_5() const { return ___movementSettings_5; }
	inline MovementSettings_t1096092444 ** get_address_of_movementSettings_5() { return &___movementSettings_5; }
	inline void set_movementSettings_5(MovementSettings_t1096092444 * value)
	{
		___movementSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___movementSettings_5), value);
	}

	inline static int32_t get_offset_of_mouseLook_6() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___mouseLook_6)); }
	inline MouseLook_t2859678661 * get_mouseLook_6() const { return ___mouseLook_6; }
	inline MouseLook_t2859678661 ** get_address_of_mouseLook_6() { return &___mouseLook_6; }
	inline void set_mouseLook_6(MouseLook_t2859678661 * value)
	{
		___mouseLook_6 = value;
		Il2CppCodeGenWriteBarrier((&___mouseLook_6), value);
	}

	inline static int32_t get_offset_of_advancedSettings_7() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___advancedSettings_7)); }
	inline AdvancedSettings_t778418834 * get_advancedSettings_7() const { return ___advancedSettings_7; }
	inline AdvancedSettings_t778418834 ** get_address_of_advancedSettings_7() { return &___advancedSettings_7; }
	inline void set_advancedSettings_7(AdvancedSettings_t778418834 * value)
	{
		___advancedSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___advancedSettings_7), value);
	}

	inline static int32_t get_offset_of_m_RigidBody_8() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_RigidBody_8)); }
	inline Rigidbody_t3916780224 * get_m_RigidBody_8() const { return ___m_RigidBody_8; }
	inline Rigidbody_t3916780224 ** get_address_of_m_RigidBody_8() { return &___m_RigidBody_8; }
	inline void set_m_RigidBody_8(Rigidbody_t3916780224 * value)
	{
		___m_RigidBody_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_RigidBody_8), value);
	}

	inline static int32_t get_offset_of_m_Capsule_9() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Capsule_9)); }
	inline CapsuleCollider_t197597763 * get_m_Capsule_9() const { return ___m_Capsule_9; }
	inline CapsuleCollider_t197597763 ** get_address_of_m_Capsule_9() { return &___m_Capsule_9; }
	inline void set_m_Capsule_9(CapsuleCollider_t197597763 * value)
	{
		___m_Capsule_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_9), value);
	}

	inline static int32_t get_offset_of_m_YRotation_10() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_YRotation_10)); }
	inline float get_m_YRotation_10() const { return ___m_YRotation_10; }
	inline float* get_address_of_m_YRotation_10() { return &___m_YRotation_10; }
	inline void set_m_YRotation_10(float value)
	{
		___m_YRotation_10 = value;
	}

	inline static int32_t get_offset_of_m_GroundContactNormal_11() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_GroundContactNormal_11)); }
	inline Vector3_t3722313464  get_m_GroundContactNormal_11() const { return ___m_GroundContactNormal_11; }
	inline Vector3_t3722313464 * get_address_of_m_GroundContactNormal_11() { return &___m_GroundContactNormal_11; }
	inline void set_m_GroundContactNormal_11(Vector3_t3722313464  value)
	{
		___m_GroundContactNormal_11 = value;
	}

	inline static int32_t get_offset_of_m_Jump_12() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Jump_12)); }
	inline bool get_m_Jump_12() const { return ___m_Jump_12; }
	inline bool* get_address_of_m_Jump_12() { return &___m_Jump_12; }
	inline void set_m_Jump_12(bool value)
	{
		___m_Jump_12 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_13() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_PreviouslyGrounded_13)); }
	inline bool get_m_PreviouslyGrounded_13() const { return ___m_PreviouslyGrounded_13; }
	inline bool* get_address_of_m_PreviouslyGrounded_13() { return &___m_PreviouslyGrounded_13; }
	inline void set_m_PreviouslyGrounded_13(bool value)
	{
		___m_PreviouslyGrounded_13 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_14() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Jumping_14)); }
	inline bool get_m_Jumping_14() const { return ___m_Jumping_14; }
	inline bool* get_address_of_m_Jumping_14() { return &___m_Jumping_14; }
	inline void set_m_Jumping_14(bool value)
	{
		___m_Jumping_14 = value;
	}

	inline static int32_t get_offset_of_m_IsGrounded_15() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_IsGrounded_15)); }
	inline bool get_m_IsGrounded_15() const { return ___m_IsGrounded_15; }
	inline bool* get_address_of_m_IsGrounded_15() { return &___m_IsGrounded_15; }
	inline void set_m_IsGrounded_15(bool value)
	{
		___m_IsGrounded_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#ifndef AICHARACTERCONTROL_T2972373937_H
#define AICHARACTERCONTROL_T2972373937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.AICharacterControl
struct  AICharacterControl_t2972373937  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<agent>k__BackingField
	NavMeshAgent_t1276799816 * ___U3CagentU3Ek__BackingField_4;
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<character>k__BackingField
	ThirdPersonCharacter_t1711070432 * ___U3CcharacterU3Ek__BackingField_5;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::target
	Transform_t3600365921 * ___target_6;

public:
	inline static int32_t get_offset_of_U3CagentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___U3CagentU3Ek__BackingField_4)); }
	inline NavMeshAgent_t1276799816 * get_U3CagentU3Ek__BackingField_4() const { return ___U3CagentU3Ek__BackingField_4; }
	inline NavMeshAgent_t1276799816 ** get_address_of_U3CagentU3Ek__BackingField_4() { return &___U3CagentU3Ek__BackingField_4; }
	inline void set_U3CagentU3Ek__BackingField_4(NavMeshAgent_t1276799816 * value)
	{
		___U3CagentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CagentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcharacterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___U3CcharacterU3Ek__BackingField_5)); }
	inline ThirdPersonCharacter_t1711070432 * get_U3CcharacterU3Ek__BackingField_5() const { return ___U3CcharacterU3Ek__BackingField_5; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_U3CcharacterU3Ek__BackingField_5() { return &___U3CcharacterU3Ek__BackingField_5; }
	inline void set_U3CcharacterU3Ek__BackingField_5(ThirdPersonCharacter_t1711070432 * value)
	{
		___U3CcharacterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AICHARACTERCONTROL_T2972373937_H
#ifndef THIRDPERSONCHARACTER_T1711070432_H
#define THIRDPERSONCHARACTER_T1711070432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct  ThirdPersonCharacter_t1711070432  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MovingTurnSpeed
	float ___m_MovingTurnSpeed_4;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_StationaryTurnSpeed
	float ___m_StationaryTurnSpeed_5;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_JumpPower
	float ___m_JumpPower_6;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GravityMultiplier
	float ___m_GravityMultiplier_7;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_RunCycleLegOffset
	float ___m_RunCycleLegOffset_8;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MoveSpeedMultiplier
	float ___m_MoveSpeedMultiplier_9;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_AnimSpeedMultiplier
	float ___m_AnimSpeedMultiplier_10;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundCheckDistance
	float ___m_GroundCheckDistance_11;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_12;
	// UnityEngine.Animator UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Animator
	Animator_t434523843 * ___m_Animator_13;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_IsGrounded
	bool ___m_IsGrounded_14;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_OrigGroundCheckDistance
	float ___m_OrigGroundCheckDistance_15;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_TurnAmount
	float ___m_TurnAmount_17;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_ForwardAmount
	float ___m_ForwardAmount_18;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundNormal
	Vector3_t3722313464  ___m_GroundNormal_19;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleHeight
	float ___m_CapsuleHeight_20;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleCenter
	Vector3_t3722313464  ___m_CapsuleCenter_21;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Capsule
	CapsuleCollider_t197597763 * ___m_Capsule_22;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Crouching
	bool ___m_Crouching_23;

public:
	inline static int32_t get_offset_of_m_MovingTurnSpeed_4() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_MovingTurnSpeed_4)); }
	inline float get_m_MovingTurnSpeed_4() const { return ___m_MovingTurnSpeed_4; }
	inline float* get_address_of_m_MovingTurnSpeed_4() { return &___m_MovingTurnSpeed_4; }
	inline void set_m_MovingTurnSpeed_4(float value)
	{
		___m_MovingTurnSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_StationaryTurnSpeed_5() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_StationaryTurnSpeed_5)); }
	inline float get_m_StationaryTurnSpeed_5() const { return ___m_StationaryTurnSpeed_5; }
	inline float* get_address_of_m_StationaryTurnSpeed_5() { return &___m_StationaryTurnSpeed_5; }
	inline void set_m_StationaryTurnSpeed_5(float value)
	{
		___m_StationaryTurnSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_6() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_JumpPower_6)); }
	inline float get_m_JumpPower_6() const { return ___m_JumpPower_6; }
	inline float* get_address_of_m_JumpPower_6() { return &___m_JumpPower_6; }
	inline void set_m_JumpPower_6(float value)
	{
		___m_JumpPower_6 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_7() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GravityMultiplier_7)); }
	inline float get_m_GravityMultiplier_7() const { return ___m_GravityMultiplier_7; }
	inline float* get_address_of_m_GravityMultiplier_7() { return &___m_GravityMultiplier_7; }
	inline void set_m_GravityMultiplier_7(float value)
	{
		___m_GravityMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_RunCycleLegOffset_8() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_RunCycleLegOffset_8)); }
	inline float get_m_RunCycleLegOffset_8() const { return ___m_RunCycleLegOffset_8; }
	inline float* get_address_of_m_RunCycleLegOffset_8() { return &___m_RunCycleLegOffset_8; }
	inline void set_m_RunCycleLegOffset_8(float value)
	{
		___m_RunCycleLegOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeedMultiplier_9() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_MoveSpeedMultiplier_9)); }
	inline float get_m_MoveSpeedMultiplier_9() const { return ___m_MoveSpeedMultiplier_9; }
	inline float* get_address_of_m_MoveSpeedMultiplier_9() { return &___m_MoveSpeedMultiplier_9; }
	inline void set_m_MoveSpeedMultiplier_9(float value)
	{
		___m_MoveSpeedMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_m_AnimSpeedMultiplier_10() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_AnimSpeedMultiplier_10)); }
	inline float get_m_AnimSpeedMultiplier_10() const { return ___m_AnimSpeedMultiplier_10; }
	inline float* get_address_of_m_AnimSpeedMultiplier_10() { return &___m_AnimSpeedMultiplier_10; }
	inline void set_m_AnimSpeedMultiplier_10(float value)
	{
		___m_AnimSpeedMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheckDistance_11() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GroundCheckDistance_11)); }
	inline float get_m_GroundCheckDistance_11() const { return ___m_GroundCheckDistance_11; }
	inline float* get_address_of_m_GroundCheckDistance_11() { return &___m_GroundCheckDistance_11; }
	inline void set_m_GroundCheckDistance_11(float value)
	{
		___m_GroundCheckDistance_11 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_12() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Rigidbody_12)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_12() const { return ___m_Rigidbody_12; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_12() { return &___m_Rigidbody_12; }
	inline void set_m_Rigidbody_12(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_12), value);
	}

	inline static int32_t get_offset_of_m_Animator_13() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Animator_13)); }
	inline Animator_t434523843 * get_m_Animator_13() const { return ___m_Animator_13; }
	inline Animator_t434523843 ** get_address_of_m_Animator_13() { return &___m_Animator_13; }
	inline void set_m_Animator_13(Animator_t434523843 * value)
	{
		___m_Animator_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_13), value);
	}

	inline static int32_t get_offset_of_m_IsGrounded_14() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_IsGrounded_14)); }
	inline bool get_m_IsGrounded_14() const { return ___m_IsGrounded_14; }
	inline bool* get_address_of_m_IsGrounded_14() { return &___m_IsGrounded_14; }
	inline void set_m_IsGrounded_14(bool value)
	{
		___m_IsGrounded_14 = value;
	}

	inline static int32_t get_offset_of_m_OrigGroundCheckDistance_15() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_OrigGroundCheckDistance_15)); }
	inline float get_m_OrigGroundCheckDistance_15() const { return ___m_OrigGroundCheckDistance_15; }
	inline float* get_address_of_m_OrigGroundCheckDistance_15() { return &___m_OrigGroundCheckDistance_15; }
	inline void set_m_OrigGroundCheckDistance_15(float value)
	{
		___m_OrigGroundCheckDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_TurnAmount_17() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_TurnAmount_17)); }
	inline float get_m_TurnAmount_17() const { return ___m_TurnAmount_17; }
	inline float* get_address_of_m_TurnAmount_17() { return &___m_TurnAmount_17; }
	inline void set_m_TurnAmount_17(float value)
	{
		___m_TurnAmount_17 = value;
	}

	inline static int32_t get_offset_of_m_ForwardAmount_18() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_ForwardAmount_18)); }
	inline float get_m_ForwardAmount_18() const { return ___m_ForwardAmount_18; }
	inline float* get_address_of_m_ForwardAmount_18() { return &___m_ForwardAmount_18; }
	inline void set_m_ForwardAmount_18(float value)
	{
		___m_ForwardAmount_18 = value;
	}

	inline static int32_t get_offset_of_m_GroundNormal_19() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GroundNormal_19)); }
	inline Vector3_t3722313464  get_m_GroundNormal_19() const { return ___m_GroundNormal_19; }
	inline Vector3_t3722313464 * get_address_of_m_GroundNormal_19() { return &___m_GroundNormal_19; }
	inline void set_m_GroundNormal_19(Vector3_t3722313464  value)
	{
		___m_GroundNormal_19 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleHeight_20() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_CapsuleHeight_20)); }
	inline float get_m_CapsuleHeight_20() const { return ___m_CapsuleHeight_20; }
	inline float* get_address_of_m_CapsuleHeight_20() { return &___m_CapsuleHeight_20; }
	inline void set_m_CapsuleHeight_20(float value)
	{
		___m_CapsuleHeight_20 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleCenter_21() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_CapsuleCenter_21)); }
	inline Vector3_t3722313464  get_m_CapsuleCenter_21() const { return ___m_CapsuleCenter_21; }
	inline Vector3_t3722313464 * get_address_of_m_CapsuleCenter_21() { return &___m_CapsuleCenter_21; }
	inline void set_m_CapsuleCenter_21(Vector3_t3722313464  value)
	{
		___m_CapsuleCenter_21 = value;
	}

	inline static int32_t get_offset_of_m_Capsule_22() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Capsule_22)); }
	inline CapsuleCollider_t197597763 * get_m_Capsule_22() const { return ___m_Capsule_22; }
	inline CapsuleCollider_t197597763 ** get_address_of_m_Capsule_22() { return &___m_Capsule_22; }
	inline void set_m_Capsule_22(CapsuleCollider_t197597763 * value)
	{
		___m_Capsule_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_22), value);
	}

	inline static int32_t get_offset_of_m_Crouching_23() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Crouching_23)); }
	inline bool get_m_Crouching_23() const { return ___m_Crouching_23; }
	inline bool* get_address_of_m_Crouching_23() { return &___m_Crouching_23; }
	inline void set_m_Crouching_23(bool value)
	{
		___m_Crouching_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCHARACTER_T1711070432_H
#ifndef THIRDPERSONUSERCONTROL_T1527285130_H
#define THIRDPERSONUSERCONTROL_T1527285130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl
struct  ThirdPersonUserControl_t1527285130  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Character
	ThirdPersonCharacter_t1711070432 * ___m_Character_4;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Cam
	Transform_t3600365921 * ___m_Cam_5;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_CamForward
	Vector3_t3722313464  ___m_CamForward_6;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Move
	Vector3_t3722313464  ___m_Move_7;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Jump
	bool ___m_Jump_8;

public:
	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Character_4)); }
	inline ThirdPersonCharacter_t1711070432 * get_m_Character_4() const { return ___m_Character_4; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(ThirdPersonCharacter_t1711070432 * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_4), value);
	}

	inline static int32_t get_offset_of_m_Cam_5() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Cam_5)); }
	inline Transform_t3600365921 * get_m_Cam_5() const { return ___m_Cam_5; }
	inline Transform_t3600365921 ** get_address_of_m_Cam_5() { return &___m_Cam_5; }
	inline void set_m_Cam_5(Transform_t3600365921 * value)
	{
		___m_Cam_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_5), value);
	}

	inline static int32_t get_offset_of_m_CamForward_6() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_CamForward_6)); }
	inline Vector3_t3722313464  get_m_CamForward_6() const { return ___m_CamForward_6; }
	inline Vector3_t3722313464 * get_address_of_m_CamForward_6() { return &___m_CamForward_6; }
	inline void set_m_CamForward_6(Vector3_t3722313464  value)
	{
		___m_CamForward_6 = value;
	}

	inline static int32_t get_offset_of_m_Move_7() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Move_7)); }
	inline Vector3_t3722313464  get_m_Move_7() const { return ___m_Move_7; }
	inline Vector3_t3722313464 * get_address_of_m_Move_7() { return &___m_Move_7; }
	inline void set_m_Move_7(Vector3_t3722313464  value)
	{
		___m_Move_7 = value;
	}

	inline static int32_t get_offset_of_m_Jump_8() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Jump_8)); }
	inline bool get_m_Jump_8() const { return ___m_Jump_8; }
	inline bool* get_address_of_m_Jump_8() { return &___m_Jump_8; }
	inline void set_m_Jump_8(bool value)
	{
		___m_Jump_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONUSERCONTROL_T1527285130_H
#ifndef AXISTOUCHBUTTON_T3522881333_H
#define AXISTOUCHBUTTON_T3522881333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t3522881333  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_7;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t3522881333 * ___m_PairedWith_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_t4087348596 * ___m_Axis_9;

public:
	inline static int32_t get_offset_of_axisName_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisName_4)); }
	inline String_t* get_axisName_4() const { return ___axisName_4; }
	inline String_t** get_address_of_axisName_4() { return &___axisName_4; }
	inline void set_axisName_4(String_t* value)
	{
		___axisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_4), value);
	}

	inline static int32_t get_offset_of_axisValue_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisValue_5)); }
	inline float get_axisValue_5() const { return ___axisValue_5; }
	inline float* get_address_of_axisValue_5() { return &___axisValue_5; }
	inline void set_axisValue_5(float value)
	{
		___axisValue_5 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___responseSpeed_6)); }
	inline float get_responseSpeed_6() const { return ___responseSpeed_6; }
	inline float* get_address_of_responseSpeed_6() { return &___responseSpeed_6; }
	inline void set_responseSpeed_6(float value)
	{
		___responseSpeed_6 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___returnToCentreSpeed_7)); }
	inline float get_returnToCentreSpeed_7() const { return ___returnToCentreSpeed_7; }
	inline float* get_address_of_returnToCentreSpeed_7() { return &___returnToCentreSpeed_7; }
	inline void set_returnToCentreSpeed_7(float value)
	{
		___returnToCentreSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_8() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_PairedWith_8)); }
	inline AxisTouchButton_t3522881333 * get_m_PairedWith_8() const { return ___m_PairedWith_8; }
	inline AxisTouchButton_t3522881333 ** get_address_of_m_PairedWith_8() { return &___m_PairedWith_8; }
	inline void set_m_PairedWith_8(AxisTouchButton_t3522881333 * value)
	{
		___m_PairedWith_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_8), value);
	}

	inline static int32_t get_offset_of_m_Axis_9() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_Axis_9)); }
	inline VirtualAxis_t4087348596 * get_m_Axis_9() const { return ___m_Axis_9; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_Axis_9() { return &___m_Axis_9; }
	inline void set_m_Axis_9(VirtualAxis_t4087348596 * value)
	{
		___m_Axis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T3522881333_H
#ifndef BUTTONHANDLER_T823762219_H
#define BUTTONHANDLER_T823762219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_t823762219  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_4;

public:
	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(ButtonHandler_t823762219, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_T823762219_H
#ifndef INPUTAXISSCROLLBAR_T457958266_H
#define INPUTAXISSCROLLBAR_T457958266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t457958266  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_4;

public:
	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t457958266, ___axis_4)); }
	inline String_t* get_axis_4() const { return ___axis_4; }
	inline String_t** get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(String_t* value)
	{
		___axis_4 = value;
		Il2CppCodeGenWriteBarrier((&___axis_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T457958266_H
#ifndef JOYSTICK_T2204371675_H
#define JOYSTICK_T2204371675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t2204371675  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_4;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_5;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_t3722313464  ___m_StartPos_8;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_9;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_10;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_11;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_12;

public:
	inline static int32_t get_offset_of_MovementRange_4() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___MovementRange_4)); }
	inline int32_t get_MovementRange_4() const { return ___MovementRange_4; }
	inline int32_t* get_address_of_MovementRange_4() { return &___MovementRange_4; }
	inline void set_MovementRange_4(int32_t value)
	{
		___MovementRange_4 = value;
	}

	inline static int32_t get_offset_of_axesToUse_5() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___axesToUse_5)); }
	inline int32_t get_axesToUse_5() const { return ___axesToUse_5; }
	inline int32_t* get_address_of_axesToUse_5() { return &___axesToUse_5; }
	inline void set_axesToUse_5(int32_t value)
	{
		___axesToUse_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_StartPos_8)); }
	inline Vector3_t3722313464  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_t3722313464  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_UseX_9() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseX_9)); }
	inline bool get_m_UseX_9() const { return ___m_UseX_9; }
	inline bool* get_address_of_m_UseX_9() { return &___m_UseX_9; }
	inline void set_m_UseX_9(bool value)
	{
		___m_UseX_9 = value;
	}

	inline static int32_t get_offset_of_m_UseY_10() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseY_10)); }
	inline bool get_m_UseY_10() const { return ___m_UseY_10; }
	inline bool* get_address_of_m_UseY_10() { return &___m_UseY_10; }
	inline void set_m_UseY_10(bool value)
	{
		___m_UseY_10 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_11() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_HorizontalVirtualAxis_11)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_11() const { return ___m_HorizontalVirtualAxis_11; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_11() { return &___m_HorizontalVirtualAxis_11; }
	inline void set_m_HorizontalVirtualAxis_11(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_12() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_VerticalVirtualAxis_12)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_12() const { return ___m_VerticalVirtualAxis_12; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_12() { return &___m_VerticalVirtualAxis_12; }
	inline void set_m_VerticalVirtualAxis_12(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T2204371675_H
#ifndef MOBILECONTROLRIG_T1964600252_H
#define MOBILECONTROLRIG_T1964600252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_t1964600252  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_T1964600252_H
#ifndef TILTINPUT_T1639936653_H
#define TILTINPUT_T1639936653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t1639936653  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t3982445645 * ___mapping_4;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_7;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_t4087348596 * ___m_SteerAxis_8;

public:
	inline static int32_t get_offset_of_mapping_4() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___mapping_4)); }
	inline AxisMapping_t3982445645 * get_mapping_4() const { return ___mapping_4; }
	inline AxisMapping_t3982445645 ** get_address_of_mapping_4() { return &___mapping_4; }
	inline void set_mapping_4(AxisMapping_t3982445645 * value)
	{
		___mapping_4 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_4), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_5() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___tiltAroundAxis_5)); }
	inline int32_t get_tiltAroundAxis_5() const { return ___tiltAroundAxis_5; }
	inline int32_t* get_address_of_tiltAroundAxis_5() { return &___tiltAroundAxis_5; }
	inline void set_tiltAroundAxis_5(int32_t value)
	{
		___tiltAroundAxis_5 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_6() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___fullTiltAngle_6)); }
	inline float get_fullTiltAngle_6() const { return ___fullTiltAngle_6; }
	inline float* get_address_of_fullTiltAngle_6() { return &___fullTiltAngle_6; }
	inline void set_fullTiltAngle_6(float value)
	{
		___fullTiltAngle_6 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_7() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___centreAngleOffset_7)); }
	inline float get_centreAngleOffset_7() const { return ___centreAngleOffset_7; }
	inline float* get_address_of_centreAngleOffset_7() { return &___centreAngleOffset_7; }
	inline void set_centreAngleOffset_7(float value)
	{
		___centreAngleOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_8() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___m_SteerAxis_8)); }
	inline VirtualAxis_t4087348596 * get_m_SteerAxis_8() const { return ___m_SteerAxis_8; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_SteerAxis_8() { return &___m_SteerAxis_8; }
	inline void set_m_SteerAxis_8(VirtualAxis_t4087348596 * value)
	{
		___m_SteerAxis_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T1639936653_H
#ifndef TOUCHPAD_T539039257_H
#define TOUCHPAD_T539039257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_t539039257  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_4;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_5;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_7;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_8;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_t3722313464  ___m_StartPos_10;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_t2156229523  ___m_PreviousDelta_11;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_t3722313464  ___m_JoytickOutput_12;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_13;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_14;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_15;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_16;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_17;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_18;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_t2156229523  ___m_PreviousTouchPos_19;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_t3722313464  ___m_Center_20;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t2670269651 * ___m_Image_21;

public:
	inline static int32_t get_offset_of_axesToUse_4() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___axesToUse_4)); }
	inline int32_t get_axesToUse_4() const { return ___axesToUse_4; }
	inline int32_t* get_address_of_axesToUse_4() { return &___axesToUse_4; }
	inline void set_axesToUse_4(int32_t value)
	{
		___axesToUse_4 = value;
	}

	inline static int32_t get_offset_of_controlStyle_5() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___controlStyle_5)); }
	inline int32_t get_controlStyle_5() const { return ___controlStyle_5; }
	inline int32_t* get_address_of_controlStyle_5() { return &___controlStyle_5; }
	inline void set_controlStyle_5(int32_t value)
	{
		___controlStyle_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_8() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Xsensitivity_8)); }
	inline float get_Xsensitivity_8() const { return ___Xsensitivity_8; }
	inline float* get_address_of_Xsensitivity_8() { return &___Xsensitivity_8; }
	inline void set_Xsensitivity_8(float value)
	{
		___Xsensitivity_8 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_9() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Ysensitivity_9)); }
	inline float get_Ysensitivity_9() const { return ___Ysensitivity_9; }
	inline float* get_address_of_Ysensitivity_9() { return &___Ysensitivity_9; }
	inline void set_Ysensitivity_9(float value)
	{
		___Ysensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_10() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_StartPos_10)); }
	inline Vector3_t3722313464  get_m_StartPos_10() const { return ___m_StartPos_10; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_10() { return &___m_StartPos_10; }
	inline void set_m_StartPos_10(Vector3_t3722313464  value)
	{
		___m_StartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_11() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousDelta_11)); }
	inline Vector2_t2156229523  get_m_PreviousDelta_11() const { return ___m_PreviousDelta_11; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousDelta_11() { return &___m_PreviousDelta_11; }
	inline void set_m_PreviousDelta_11(Vector2_t2156229523  value)
	{
		___m_PreviousDelta_11 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_12() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_JoytickOutput_12)); }
	inline Vector3_t3722313464  get_m_JoytickOutput_12() const { return ___m_JoytickOutput_12; }
	inline Vector3_t3722313464 * get_address_of_m_JoytickOutput_12() { return &___m_JoytickOutput_12; }
	inline void set_m_JoytickOutput_12(Vector3_t3722313464  value)
	{
		___m_JoytickOutput_12 = value;
	}

	inline static int32_t get_offset_of_m_UseX_13() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseX_13)); }
	inline bool get_m_UseX_13() const { return ___m_UseX_13; }
	inline bool* get_address_of_m_UseX_13() { return &___m_UseX_13; }
	inline void set_m_UseX_13(bool value)
	{
		___m_UseX_13 = value;
	}

	inline static int32_t get_offset_of_m_UseY_14() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseY_14)); }
	inline bool get_m_UseY_14() const { return ___m_UseY_14; }
	inline bool* get_address_of_m_UseY_14() { return &___m_UseY_14; }
	inline void set_m_UseY_14(bool value)
	{
		___m_UseY_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_15() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_HorizontalVirtualAxis_15)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_15() const { return ___m_HorizontalVirtualAxis_15; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_15() { return &___m_HorizontalVirtualAxis_15; }
	inline void set_m_HorizontalVirtualAxis_15(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_15), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_16() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_VerticalVirtualAxis_16)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_16() const { return ___m_VerticalVirtualAxis_16; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_16() { return &___m_VerticalVirtualAxis_16; }
	inline void set_m_VerticalVirtualAxis_16(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_16), value);
	}

	inline static int32_t get_offset_of_m_Dragging_17() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Dragging_17)); }
	inline bool get_m_Dragging_17() const { return ___m_Dragging_17; }
	inline bool* get_address_of_m_Dragging_17() { return &___m_Dragging_17; }
	inline void set_m_Dragging_17(bool value)
	{
		___m_Dragging_17 = value;
	}

	inline static int32_t get_offset_of_m_Id_18() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Id_18)); }
	inline int32_t get_m_Id_18() const { return ___m_Id_18; }
	inline int32_t* get_address_of_m_Id_18() { return &___m_Id_18; }
	inline void set_m_Id_18(int32_t value)
	{
		___m_Id_18 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_19() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousTouchPos_19)); }
	inline Vector2_t2156229523  get_m_PreviousTouchPos_19() const { return ___m_PreviousTouchPos_19; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousTouchPos_19() { return &___m_PreviousTouchPos_19; }
	inline void set_m_PreviousTouchPos_19(Vector2_t2156229523  value)
	{
		___m_PreviousTouchPos_19 = value;
	}

	inline static int32_t get_offset_of_m_Center_20() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Center_20)); }
	inline Vector3_t3722313464  get_m_Center_20() const { return ___m_Center_20; }
	inline Vector3_t3722313464 * get_address_of_m_Center_20() { return &___m_Center_20; }
	inline void set_m_Center_20(Vector3_t3722313464  value)
	{
		___m_Center_20 = value;
	}

	inline static int32_t get_offset_of_m_Image_21() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Image_21)); }
	inline Image_t2670269651 * get_m_Image_21() const { return ___m_Image_21; }
	inline Image_t2670269651 ** get_address_of_m_Image_21() { return &___m_Image_21; }
	inline void set_m_Image_21(Image_t2670269651 * value)
	{
		___m_Image_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T539039257_H
#ifndef AFTERBURNERPHYSICSFORCE_T498893161_H
#define AFTERBURNERPHYSICSFORCE_T498893161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.AfterburnerPhysicsForce
struct  AfterburnerPhysicsForce_t498893161  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectAngle
	float ___effectAngle_4;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectWidth
	float ___effectWidth_5;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectDistance
	float ___effectDistance_6;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::force
	float ___force_7;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Cols
	ColliderU5BU5D_t4234922487* ___m_Cols_8;
	// UnityEngine.SphereCollider UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Sphere
	SphereCollider_t2077223608 * ___m_Sphere_9;

public:
	inline static int32_t get_offset_of_effectAngle_4() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___effectAngle_4)); }
	inline float get_effectAngle_4() const { return ___effectAngle_4; }
	inline float* get_address_of_effectAngle_4() { return &___effectAngle_4; }
	inline void set_effectAngle_4(float value)
	{
		___effectAngle_4 = value;
	}

	inline static int32_t get_offset_of_effectWidth_5() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___effectWidth_5)); }
	inline float get_effectWidth_5() const { return ___effectWidth_5; }
	inline float* get_address_of_effectWidth_5() { return &___effectWidth_5; }
	inline void set_effectWidth_5(float value)
	{
		___effectWidth_5 = value;
	}

	inline static int32_t get_offset_of_effectDistance_6() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___effectDistance_6)); }
	inline float get_effectDistance_6() const { return ___effectDistance_6; }
	inline float* get_address_of_effectDistance_6() { return &___effectDistance_6; }
	inline void set_effectDistance_6(float value)
	{
		___effectDistance_6 = value;
	}

	inline static int32_t get_offset_of_force_7() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___force_7)); }
	inline float get_force_7() const { return ___force_7; }
	inline float* get_address_of_force_7() { return &___force_7; }
	inline void set_force_7(float value)
	{
		___force_7 = value;
	}

	inline static int32_t get_offset_of_m_Cols_8() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___m_Cols_8)); }
	inline ColliderU5BU5D_t4234922487* get_m_Cols_8() const { return ___m_Cols_8; }
	inline ColliderU5BU5D_t4234922487** get_address_of_m_Cols_8() { return &___m_Cols_8; }
	inline void set_m_Cols_8(ColliderU5BU5D_t4234922487* value)
	{
		___m_Cols_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cols_8), value);
	}

	inline static int32_t get_offset_of_m_Sphere_9() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___m_Sphere_9)); }
	inline SphereCollider_t2077223608 * get_m_Sphere_9() const { return ___m_Sphere_9; }
	inline SphereCollider_t2077223608 ** get_address_of_m_Sphere_9() { return &___m_Sphere_9; }
	inline void set_m_Sphere_9(SphereCollider_t2077223608 * value)
	{
		___m_Sphere_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sphere_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFTERBURNERPHYSICSFORCE_T498893161_H
#ifndef EXPLOSIONFIREANDDEBRIS_T2411343565_H
#define EXPLOSIONFIREANDDEBRIS_T2411343565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct  ExplosionFireAndDebris_t2411343565  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform[] UnityStandardAssets.Effects.ExplosionFireAndDebris::debrisPrefabs
	TransformU5BU5D_t807237628* ___debrisPrefabs_4;
	// UnityEngine.Transform UnityStandardAssets.Effects.ExplosionFireAndDebris::firePrefab
	Transform_t3600365921 * ___firePrefab_5;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numDebrisPieces
	int32_t ___numDebrisPieces_6;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numFires
	int32_t ___numFires_7;

public:
	inline static int32_t get_offset_of_debrisPrefabs_4() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___debrisPrefabs_4)); }
	inline TransformU5BU5D_t807237628* get_debrisPrefabs_4() const { return ___debrisPrefabs_4; }
	inline TransformU5BU5D_t807237628** get_address_of_debrisPrefabs_4() { return &___debrisPrefabs_4; }
	inline void set_debrisPrefabs_4(TransformU5BU5D_t807237628* value)
	{
		___debrisPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___debrisPrefabs_4), value);
	}

	inline static int32_t get_offset_of_firePrefab_5() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___firePrefab_5)); }
	inline Transform_t3600365921 * get_firePrefab_5() const { return ___firePrefab_5; }
	inline Transform_t3600365921 ** get_address_of_firePrefab_5() { return &___firePrefab_5; }
	inline void set_firePrefab_5(Transform_t3600365921 * value)
	{
		___firePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___firePrefab_5), value);
	}

	inline static int32_t get_offset_of_numDebrisPieces_6() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___numDebrisPieces_6)); }
	inline int32_t get_numDebrisPieces_6() const { return ___numDebrisPieces_6; }
	inline int32_t* get_address_of_numDebrisPieces_6() { return &___numDebrisPieces_6; }
	inline void set_numDebrisPieces_6(int32_t value)
	{
		___numDebrisPieces_6 = value;
	}

	inline static int32_t get_offset_of_numFires_7() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___numFires_7)); }
	inline int32_t get_numFires_7() const { return ___numFires_7; }
	inline int32_t* get_address_of_numFires_7() { return &___numFires_7; }
	inline void set_numFires_7(int32_t value)
	{
		___numFires_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONFIREANDDEBRIS_T2411343565_H
#ifndef EXPLOSIONPHYSICSFORCE_T3982641844_H
#define EXPLOSIONPHYSICSFORCE_T3982641844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct  ExplosionPhysicsForce_t3982641844  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.ExplosionPhysicsForce::explosionForce
	float ___explosionForce_4;

public:
	inline static int32_t get_offset_of_explosionForce_4() { return static_cast<int32_t>(offsetof(ExplosionPhysicsForce_t3982641844, ___explosionForce_4)); }
	inline float get_explosionForce_4() const { return ___explosionForce_4; }
	inline float* get_address_of_explosionForce_4() { return &___explosionForce_4; }
	inline void set_explosionForce_4(float value)
	{
		___explosionForce_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONPHYSICSFORCE_T3982641844_H
#ifndef EXPLOSIVE_T792321375_H
#define EXPLOSIVE_T792321375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Explosive
struct  Explosive_t792321375  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Effects.Explosive::explosionPrefab
	Transform_t3600365921 * ___explosionPrefab_4;
	// System.Single UnityStandardAssets.Effects.Explosive::detonationImpactVelocity
	float ___detonationImpactVelocity_5;
	// System.Single UnityStandardAssets.Effects.Explosive::sizeMultiplier
	float ___sizeMultiplier_6;
	// System.Boolean UnityStandardAssets.Effects.Explosive::reset
	bool ___reset_7;
	// System.Single UnityStandardAssets.Effects.Explosive::resetTimeDelay
	float ___resetTimeDelay_8;
	// System.Boolean UnityStandardAssets.Effects.Explosive::m_Exploded
	bool ___m_Exploded_9;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Effects.Explosive::m_ObjectResetter
	ObjectResetter_t639177103 * ___m_ObjectResetter_10;

public:
	inline static int32_t get_offset_of_explosionPrefab_4() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___explosionPrefab_4)); }
	inline Transform_t3600365921 * get_explosionPrefab_4() const { return ___explosionPrefab_4; }
	inline Transform_t3600365921 ** get_address_of_explosionPrefab_4() { return &___explosionPrefab_4; }
	inline void set_explosionPrefab_4(Transform_t3600365921 * value)
	{
		___explosionPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___explosionPrefab_4), value);
	}

	inline static int32_t get_offset_of_detonationImpactVelocity_5() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___detonationImpactVelocity_5)); }
	inline float get_detonationImpactVelocity_5() const { return ___detonationImpactVelocity_5; }
	inline float* get_address_of_detonationImpactVelocity_5() { return &___detonationImpactVelocity_5; }
	inline void set_detonationImpactVelocity_5(float value)
	{
		___detonationImpactVelocity_5 = value;
	}

	inline static int32_t get_offset_of_sizeMultiplier_6() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___sizeMultiplier_6)); }
	inline float get_sizeMultiplier_6() const { return ___sizeMultiplier_6; }
	inline float* get_address_of_sizeMultiplier_6() { return &___sizeMultiplier_6; }
	inline void set_sizeMultiplier_6(float value)
	{
		___sizeMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_reset_7() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___reset_7)); }
	inline bool get_reset_7() const { return ___reset_7; }
	inline bool* get_address_of_reset_7() { return &___reset_7; }
	inline void set_reset_7(bool value)
	{
		___reset_7 = value;
	}

	inline static int32_t get_offset_of_resetTimeDelay_8() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___resetTimeDelay_8)); }
	inline float get_resetTimeDelay_8() const { return ___resetTimeDelay_8; }
	inline float* get_address_of_resetTimeDelay_8() { return &___resetTimeDelay_8; }
	inline void set_resetTimeDelay_8(float value)
	{
		___resetTimeDelay_8 = value;
	}

	inline static int32_t get_offset_of_m_Exploded_9() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___m_Exploded_9)); }
	inline bool get_m_Exploded_9() const { return ___m_Exploded_9; }
	inline bool* get_address_of_m_Exploded_9() { return &___m_Exploded_9; }
	inline void set_m_Exploded_9(bool value)
	{
		___m_Exploded_9 = value;
	}

	inline static int32_t get_offset_of_m_ObjectResetter_10() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___m_ObjectResetter_10)); }
	inline ObjectResetter_t639177103 * get_m_ObjectResetter_10() const { return ___m_ObjectResetter_10; }
	inline ObjectResetter_t639177103 ** get_address_of_m_ObjectResetter_10() { return &___m_ObjectResetter_10; }
	inline void set_m_ObjectResetter_10(ObjectResetter_t639177103 * value)
	{
		___m_ObjectResetter_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectResetter_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIVE_T792321375_H
#ifndef EXTINGUISHABLEPARTICLESYSTEM_T4259708998_H
#define EXTINGUISHABLEPARTICLESYSTEM_T4259708998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExtinguishableParticleSystem
struct  ExtinguishableParticleSystem_t4259708998  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.ExtinguishableParticleSystem::multiplier
	float ___multiplier_4;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.ExtinguishableParticleSystem::m_Systems
	ParticleSystemU5BU5D_t3089334924* ___m_Systems_5;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_t4259708998, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_Systems_5() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_t4259708998, ___m_Systems_5)); }
	inline ParticleSystemU5BU5D_t3089334924* get_m_Systems_5() const { return ___m_Systems_5; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_m_Systems_5() { return &___m_Systems_5; }
	inline void set_m_Systems_5(ParticleSystemU5BU5D_t3089334924* value)
	{
		___m_Systems_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Systems_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTINGUISHABLEPARTICLESYSTEM_T4259708998_H
#ifndef FIRELIGHT_T2068143130_H
#define FIRELIGHT_T2068143130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.FireLight
struct  FireLight_t2068143130  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.FireLight::m_Rnd
	float ___m_Rnd_4;
	// System.Boolean UnityStandardAssets.Effects.FireLight::m_Burning
	bool ___m_Burning_5;
	// UnityEngine.Light UnityStandardAssets.Effects.FireLight::m_Light
	Light_t3756812086 * ___m_Light_6;

public:
	inline static int32_t get_offset_of_m_Rnd_4() { return static_cast<int32_t>(offsetof(FireLight_t2068143130, ___m_Rnd_4)); }
	inline float get_m_Rnd_4() const { return ___m_Rnd_4; }
	inline float* get_address_of_m_Rnd_4() { return &___m_Rnd_4; }
	inline void set_m_Rnd_4(float value)
	{
		___m_Rnd_4 = value;
	}

	inline static int32_t get_offset_of_m_Burning_5() { return static_cast<int32_t>(offsetof(FireLight_t2068143130, ___m_Burning_5)); }
	inline bool get_m_Burning_5() const { return ___m_Burning_5; }
	inline bool* get_address_of_m_Burning_5() { return &___m_Burning_5; }
	inline void set_m_Burning_5(bool value)
	{
		___m_Burning_5 = value;
	}

	inline static int32_t get_offset_of_m_Light_6() { return static_cast<int32_t>(offsetof(FireLight_t2068143130, ___m_Light_6)); }
	inline Light_t3756812086 * get_m_Light_6() const { return ___m_Light_6; }
	inline Light_t3756812086 ** get_address_of_m_Light_6() { return &___m_Light_6; }
	inline void set_m_Light_6(Light_t3756812086 * value)
	{
		___m_Light_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Light_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRELIGHT_T2068143130_H
#ifndef HOSE_T3016386068_H
#define HOSE_T3016386068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Hose
struct  Hose_t3016386068  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.Hose::maxPower
	float ___maxPower_4;
	// System.Single UnityStandardAssets.Effects.Hose::minPower
	float ___minPower_5;
	// System.Single UnityStandardAssets.Effects.Hose::changeSpeed
	float ___changeSpeed_6;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.Hose::hoseWaterSystems
	ParticleSystemU5BU5D_t3089334924* ___hoseWaterSystems_7;
	// UnityEngine.Renderer UnityStandardAssets.Effects.Hose::systemRenderer
	Renderer_t2627027031 * ___systemRenderer_8;
	// System.Single UnityStandardAssets.Effects.Hose::m_Power
	float ___m_Power_9;

public:
	inline static int32_t get_offset_of_maxPower_4() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___maxPower_4)); }
	inline float get_maxPower_4() const { return ___maxPower_4; }
	inline float* get_address_of_maxPower_4() { return &___maxPower_4; }
	inline void set_maxPower_4(float value)
	{
		___maxPower_4 = value;
	}

	inline static int32_t get_offset_of_minPower_5() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___minPower_5)); }
	inline float get_minPower_5() const { return ___minPower_5; }
	inline float* get_address_of_minPower_5() { return &___minPower_5; }
	inline void set_minPower_5(float value)
	{
		___minPower_5 = value;
	}

	inline static int32_t get_offset_of_changeSpeed_6() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___changeSpeed_6)); }
	inline float get_changeSpeed_6() const { return ___changeSpeed_6; }
	inline float* get_address_of_changeSpeed_6() { return &___changeSpeed_6; }
	inline void set_changeSpeed_6(float value)
	{
		___changeSpeed_6 = value;
	}

	inline static int32_t get_offset_of_hoseWaterSystems_7() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___hoseWaterSystems_7)); }
	inline ParticleSystemU5BU5D_t3089334924* get_hoseWaterSystems_7() const { return ___hoseWaterSystems_7; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_hoseWaterSystems_7() { return &___hoseWaterSystems_7; }
	inline void set_hoseWaterSystems_7(ParticleSystemU5BU5D_t3089334924* value)
	{
		___hoseWaterSystems_7 = value;
		Il2CppCodeGenWriteBarrier((&___hoseWaterSystems_7), value);
	}

	inline static int32_t get_offset_of_systemRenderer_8() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___systemRenderer_8)); }
	inline Renderer_t2627027031 * get_systemRenderer_8() const { return ___systemRenderer_8; }
	inline Renderer_t2627027031 ** get_address_of_systemRenderer_8() { return &___systemRenderer_8; }
	inline void set_systemRenderer_8(Renderer_t2627027031 * value)
	{
		___systemRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Power_9() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___m_Power_9)); }
	inline float get_m_Power_9() const { return ___m_Power_9; }
	inline float* get_address_of_m_Power_9() { return &___m_Power_9; }
	inline void set_m_Power_9(float value)
	{
		___m_Power_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSE_T3016386068_H
#ifndef PARTICLESYSTEMMULTIPLIER_T2770350653_H
#define PARTICLESYSTEMMULTIPLIER_T2770350653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct  ParticleSystemMultiplier_t2770350653  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.ParticleSystemMultiplier::multiplier
	float ___multiplier_4;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ParticleSystemMultiplier_t2770350653, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMMULTIPLIER_T2770350653_H
#ifndef SMOKEPARTICLES_T494565528_H
#define SMOKEPARTICLES_T494565528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.SmokeParticles
struct  SmokeParticles_t494565528  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip[] UnityStandardAssets.Effects.SmokeParticles::extinguishSounds
	AudioClipU5BU5D_t143221404* ___extinguishSounds_4;

public:
	inline static int32_t get_offset_of_extinguishSounds_4() { return static_cast<int32_t>(offsetof(SmokeParticles_t494565528, ___extinguishSounds_4)); }
	inline AudioClipU5BU5D_t143221404* get_extinguishSounds_4() const { return ___extinguishSounds_4; }
	inline AudioClipU5BU5D_t143221404** get_address_of_extinguishSounds_4() { return &___extinguishSounds_4; }
	inline void set_extinguishSounds_4(AudioClipU5BU5D_t143221404* value)
	{
		___extinguishSounds_4 = value;
		Il2CppCodeGenWriteBarrier((&___extinguishSounds_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOKEPARTICLES_T494565528_H
#ifndef BALL_T2378314638_H
#define BALL_T2378314638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.Ball
struct  Ball_t2378314638  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MovePower
	float ___m_MovePower_4;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.Ball::m_UseTorque
	bool ___m_UseTorque_5;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MaxAngularVelocity
	float ___m_MaxAngularVelocity_6;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_JumpPower
	float ___m_JumpPower_7;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Ball.Ball::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_9;

public:
	inline static int32_t get_offset_of_m_MovePower_4() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_MovePower_4)); }
	inline float get_m_MovePower_4() const { return ___m_MovePower_4; }
	inline float* get_address_of_m_MovePower_4() { return &___m_MovePower_4; }
	inline void set_m_MovePower_4(float value)
	{
		___m_MovePower_4 = value;
	}

	inline static int32_t get_offset_of_m_UseTorque_5() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_UseTorque_5)); }
	inline bool get_m_UseTorque_5() const { return ___m_UseTorque_5; }
	inline bool* get_address_of_m_UseTorque_5() { return &___m_UseTorque_5; }
	inline void set_m_UseTorque_5(bool value)
	{
		___m_UseTorque_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxAngularVelocity_6() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_MaxAngularVelocity_6)); }
	inline float get_m_MaxAngularVelocity_6() const { return ___m_MaxAngularVelocity_6; }
	inline float* get_address_of_m_MaxAngularVelocity_6() { return &___m_MaxAngularVelocity_6; }
	inline void set_m_MaxAngularVelocity_6(float value)
	{
		___m_MaxAngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_7() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_JumpPower_7)); }
	inline float get_m_JumpPower_7() const { return ___m_JumpPower_7; }
	inline float* get_address_of_m_JumpPower_7() { return &___m_JumpPower_7; }
	inline void set_m_JumpPower_7(float value)
	{
		___m_JumpPower_7 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_9() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_Rigidbody_9)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_9() const { return ___m_Rigidbody_9; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_9() { return &___m_Rigidbody_9; }
	inline void set_m_Rigidbody_9(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T2378314638_H
#ifndef BALLUSERCONTROL_T2574698008_H
#define BALLUSERCONTROL_T2574698008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.BallUserControl
struct  BallUserControl_t2574698008  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Ball.Ball UnityStandardAssets.Vehicles.Ball.BallUserControl::ball
	Ball_t2378314638 * ___ball_4;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::move
	Vector3_t3722313464  ___move_5;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Ball.BallUserControl::cam
	Transform_t3600365921 * ___cam_6;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::camForward
	Vector3_t3722313464  ___camForward_7;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.BallUserControl::jump
	bool ___jump_8;

public:
	inline static int32_t get_offset_of_ball_4() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___ball_4)); }
	inline Ball_t2378314638 * get_ball_4() const { return ___ball_4; }
	inline Ball_t2378314638 ** get_address_of_ball_4() { return &___ball_4; }
	inline void set_ball_4(Ball_t2378314638 * value)
	{
		___ball_4 = value;
		Il2CppCodeGenWriteBarrier((&___ball_4), value);
	}

	inline static int32_t get_offset_of_move_5() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___move_5)); }
	inline Vector3_t3722313464  get_move_5() const { return ___move_5; }
	inline Vector3_t3722313464 * get_address_of_move_5() { return &___move_5; }
	inline void set_move_5(Vector3_t3722313464  value)
	{
		___move_5 = value;
	}

	inline static int32_t get_offset_of_cam_6() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___cam_6)); }
	inline Transform_t3600365921 * get_cam_6() const { return ___cam_6; }
	inline Transform_t3600365921 ** get_address_of_cam_6() { return &___cam_6; }
	inline void set_cam_6(Transform_t3600365921 * value)
	{
		___cam_6 = value;
		Il2CppCodeGenWriteBarrier((&___cam_6), value);
	}

	inline static int32_t get_offset_of_camForward_7() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___camForward_7)); }
	inline Vector3_t3722313464  get_camForward_7() const { return ___camForward_7; }
	inline Vector3_t3722313464 * get_address_of_camForward_7() { return &___camForward_7; }
	inline void set_camForward_7(Vector3_t3722313464  value)
	{
		___camForward_7 = value;
	}

	inline static int32_t get_offset_of_jump_8() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___jump_8)); }
	inline bool get_jump_8() const { return ___jump_8; }
	inline bool* get_address_of_jump_8() { return &___jump_8; }
	inline void set_jump_8(bool value)
	{
		___jump_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLUSERCONTROL_T2574698008_H
#ifndef DISPLACE_T1352130581_H
#define DISPLACE_T1352130581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Displace
struct  Displace_t1352130581  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLACE_T1352130581_H
#ifndef PLANARREFLECTION_T439636033_H
#define PLANARREFLECTION_T439636033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.PlanarReflection
struct  PlanarReflection_t439636033  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LayerMask UnityStandardAssets.Water.PlanarReflection::reflectionMask
	LayerMask_t3493934918  ___reflectionMask_4;
	// System.Boolean UnityStandardAssets.Water.PlanarReflection::reflectSkybox
	bool ___reflectSkybox_5;
	// UnityEngine.Color UnityStandardAssets.Water.PlanarReflection::clearColor
	Color_t2555686324  ___clearColor_6;
	// System.String UnityStandardAssets.Water.PlanarReflection::reflectionSampler
	String_t* ___reflectionSampler_7;
	// System.Single UnityStandardAssets.Water.PlanarReflection::clipPlaneOffset
	float ___clipPlaneOffset_8;
	// UnityEngine.Vector3 UnityStandardAssets.Water.PlanarReflection::m_Oldpos
	Vector3_t3722313464  ___m_Oldpos_9;
	// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::m_ReflectionCamera
	Camera_t4157153871 * ___m_ReflectionCamera_10;
	// UnityEngine.Material UnityStandardAssets.Water.PlanarReflection::m_SharedMaterial
	Material_t340375123 * ___m_SharedMaterial_11;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean> UnityStandardAssets.Water.PlanarReflection::m_HelperCameras
	Dictionary_2_t310742658 * ___m_HelperCameras_12;

public:
	inline static int32_t get_offset_of_reflectionMask_4() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectionMask_4)); }
	inline LayerMask_t3493934918  get_reflectionMask_4() const { return ___reflectionMask_4; }
	inline LayerMask_t3493934918 * get_address_of_reflectionMask_4() { return &___reflectionMask_4; }
	inline void set_reflectionMask_4(LayerMask_t3493934918  value)
	{
		___reflectionMask_4 = value;
	}

	inline static int32_t get_offset_of_reflectSkybox_5() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectSkybox_5)); }
	inline bool get_reflectSkybox_5() const { return ___reflectSkybox_5; }
	inline bool* get_address_of_reflectSkybox_5() { return &___reflectSkybox_5; }
	inline void set_reflectSkybox_5(bool value)
	{
		___reflectSkybox_5 = value;
	}

	inline static int32_t get_offset_of_clearColor_6() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___clearColor_6)); }
	inline Color_t2555686324  get_clearColor_6() const { return ___clearColor_6; }
	inline Color_t2555686324 * get_address_of_clearColor_6() { return &___clearColor_6; }
	inline void set_clearColor_6(Color_t2555686324  value)
	{
		___clearColor_6 = value;
	}

	inline static int32_t get_offset_of_reflectionSampler_7() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectionSampler_7)); }
	inline String_t* get_reflectionSampler_7() const { return ___reflectionSampler_7; }
	inline String_t** get_address_of_reflectionSampler_7() { return &___reflectionSampler_7; }
	inline void set_reflectionSampler_7(String_t* value)
	{
		___reflectionSampler_7 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionSampler_7), value);
	}

	inline static int32_t get_offset_of_clipPlaneOffset_8() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___clipPlaneOffset_8)); }
	inline float get_clipPlaneOffset_8() const { return ___clipPlaneOffset_8; }
	inline float* get_address_of_clipPlaneOffset_8() { return &___clipPlaneOffset_8; }
	inline void set_clipPlaneOffset_8(float value)
	{
		___clipPlaneOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_Oldpos_9() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_Oldpos_9)); }
	inline Vector3_t3722313464  get_m_Oldpos_9() const { return ___m_Oldpos_9; }
	inline Vector3_t3722313464 * get_address_of_m_Oldpos_9() { return &___m_Oldpos_9; }
	inline void set_m_Oldpos_9(Vector3_t3722313464  value)
	{
		___m_Oldpos_9 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCamera_10() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_ReflectionCamera_10)); }
	inline Camera_t4157153871 * get_m_ReflectionCamera_10() const { return ___m_ReflectionCamera_10; }
	inline Camera_t4157153871 ** get_address_of_m_ReflectionCamera_10() { return &___m_ReflectionCamera_10; }
	inline void set_m_ReflectionCamera_10(Camera_t4157153871 * value)
	{
		___m_ReflectionCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCamera_10), value);
	}

	inline static int32_t get_offset_of_m_SharedMaterial_11() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_SharedMaterial_11)); }
	inline Material_t340375123 * get_m_SharedMaterial_11() const { return ___m_SharedMaterial_11; }
	inline Material_t340375123 ** get_address_of_m_SharedMaterial_11() { return &___m_SharedMaterial_11; }
	inline void set_m_SharedMaterial_11(Material_t340375123 * value)
	{
		___m_SharedMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_SharedMaterial_11), value);
	}

	inline static int32_t get_offset_of_m_HelperCameras_12() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_HelperCameras_12)); }
	inline Dictionary_2_t310742658 * get_m_HelperCameras_12() const { return ___m_HelperCameras_12; }
	inline Dictionary_2_t310742658 ** get_address_of_m_HelperCameras_12() { return &___m_HelperCameras_12; }
	inline void set_m_HelperCameras_12(Dictionary_2_t310742658 * value)
	{
		___m_HelperCameras_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_HelperCameras_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANARREFLECTION_T439636033_H
#ifndef SPECULARLIGHTING_T2163114238_H
#define SPECULARLIGHTING_T2163114238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.SpecularLighting
struct  SpecularLighting_t2163114238  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Water.SpecularLighting::specularLight
	Transform_t3600365921 * ___specularLight_4;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.SpecularLighting::m_WaterBase
	WaterBase_t3883863317 * ___m_WaterBase_5;

public:
	inline static int32_t get_offset_of_specularLight_4() { return static_cast<int32_t>(offsetof(SpecularLighting_t2163114238, ___specularLight_4)); }
	inline Transform_t3600365921 * get_specularLight_4() const { return ___specularLight_4; }
	inline Transform_t3600365921 ** get_address_of_specularLight_4() { return &___specularLight_4; }
	inline void set_specularLight_4(Transform_t3600365921 * value)
	{
		___specularLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___specularLight_4), value);
	}

	inline static int32_t get_offset_of_m_WaterBase_5() { return static_cast<int32_t>(offsetof(SpecularLighting_t2163114238, ___m_WaterBase_5)); }
	inline WaterBase_t3883863317 * get_m_WaterBase_5() const { return ___m_WaterBase_5; }
	inline WaterBase_t3883863317 ** get_address_of_m_WaterBase_5() { return &___m_WaterBase_5; }
	inline void set_m_WaterBase_5(WaterBase_t3883863317 * value)
	{
		___m_WaterBase_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaterBase_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECULARLIGHTING_T2163114238_H
#ifndef WATER_T936588004_H
#define WATER_T936588004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water
struct  Water_t936588004  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::waterMode
	int32_t ___waterMode_4;
	// System.Boolean UnityStandardAssets.Water.Water::disablePixelLights
	bool ___disablePixelLights_5;
	// System.Int32 UnityStandardAssets.Water.Water::textureSize
	int32_t ___textureSize_6;
	// System.Single UnityStandardAssets.Water.Water::clipPlaneOffset
	float ___clipPlaneOffset_7;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::reflectLayers
	LayerMask_t3493934918  ___reflectLayers_8;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::refractLayers
	LayerMask_t3493934918  ___refractLayers_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_ReflectionCameras
	Dictionary_2_t75641268 * ___m_ReflectionCameras_10;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_RefractionCameras
	Dictionary_2_t75641268 * ___m_RefractionCameras_11;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_ReflectionTexture
	RenderTexture_t2108887433 * ___m_ReflectionTexture_12;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_RefractionTexture
	RenderTexture_t2108887433 * ___m_RefractionTexture_13;
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_14;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_15;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_16;

public:
	inline static int32_t get_offset_of_waterMode_4() { return static_cast<int32_t>(offsetof(Water_t936588004, ___waterMode_4)); }
	inline int32_t get_waterMode_4() const { return ___waterMode_4; }
	inline int32_t* get_address_of_waterMode_4() { return &___waterMode_4; }
	inline void set_waterMode_4(int32_t value)
	{
		___waterMode_4 = value;
	}

	inline static int32_t get_offset_of_disablePixelLights_5() { return static_cast<int32_t>(offsetof(Water_t936588004, ___disablePixelLights_5)); }
	inline bool get_disablePixelLights_5() const { return ___disablePixelLights_5; }
	inline bool* get_address_of_disablePixelLights_5() { return &___disablePixelLights_5; }
	inline void set_disablePixelLights_5(bool value)
	{
		___disablePixelLights_5 = value;
	}

	inline static int32_t get_offset_of_textureSize_6() { return static_cast<int32_t>(offsetof(Water_t936588004, ___textureSize_6)); }
	inline int32_t get_textureSize_6() const { return ___textureSize_6; }
	inline int32_t* get_address_of_textureSize_6() { return &___textureSize_6; }
	inline void set_textureSize_6(int32_t value)
	{
		___textureSize_6 = value;
	}

	inline static int32_t get_offset_of_clipPlaneOffset_7() { return static_cast<int32_t>(offsetof(Water_t936588004, ___clipPlaneOffset_7)); }
	inline float get_clipPlaneOffset_7() const { return ___clipPlaneOffset_7; }
	inline float* get_address_of_clipPlaneOffset_7() { return &___clipPlaneOffset_7; }
	inline void set_clipPlaneOffset_7(float value)
	{
		___clipPlaneOffset_7 = value;
	}

	inline static int32_t get_offset_of_reflectLayers_8() { return static_cast<int32_t>(offsetof(Water_t936588004, ___reflectLayers_8)); }
	inline LayerMask_t3493934918  get_reflectLayers_8() const { return ___reflectLayers_8; }
	inline LayerMask_t3493934918 * get_address_of_reflectLayers_8() { return &___reflectLayers_8; }
	inline void set_reflectLayers_8(LayerMask_t3493934918  value)
	{
		___reflectLayers_8 = value;
	}

	inline static int32_t get_offset_of_refractLayers_9() { return static_cast<int32_t>(offsetof(Water_t936588004, ___refractLayers_9)); }
	inline LayerMask_t3493934918  get_refractLayers_9() const { return ___refractLayers_9; }
	inline LayerMask_t3493934918 * get_address_of_refractLayers_9() { return &___refractLayers_9; }
	inline void set_refractLayers_9(LayerMask_t3493934918  value)
	{
		___refractLayers_9 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_10() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_ReflectionCameras_10)); }
	inline Dictionary_2_t75641268 * get_m_ReflectionCameras_10() const { return ___m_ReflectionCameras_10; }
	inline Dictionary_2_t75641268 ** get_address_of_m_ReflectionCameras_10() { return &___m_ReflectionCameras_10; }
	inline void set_m_ReflectionCameras_10(Dictionary_2_t75641268 * value)
	{
		___m_ReflectionCameras_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCameras_10), value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_11() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_RefractionCameras_11)); }
	inline Dictionary_2_t75641268 * get_m_RefractionCameras_11() const { return ___m_RefractionCameras_11; }
	inline Dictionary_2_t75641268 ** get_address_of_m_RefractionCameras_11() { return &___m_RefractionCameras_11; }
	inline void set_m_RefractionCameras_11(Dictionary_2_t75641268 * value)
	{
		___m_RefractionCameras_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionCameras_11), value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_12() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_ReflectionTexture_12)); }
	inline RenderTexture_t2108887433 * get_m_ReflectionTexture_12() const { return ___m_ReflectionTexture_12; }
	inline RenderTexture_t2108887433 ** get_address_of_m_ReflectionTexture_12() { return &___m_ReflectionTexture_12; }
	inline void set_m_ReflectionTexture_12(RenderTexture_t2108887433 * value)
	{
		___m_ReflectionTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTexture_12), value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_13() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_RefractionTexture_13)); }
	inline RenderTexture_t2108887433 * get_m_RefractionTexture_13() const { return ___m_RefractionTexture_13; }
	inline RenderTexture_t2108887433 ** get_address_of_m_RefractionTexture_13() { return &___m_RefractionTexture_13; }
	inline void set_m_RefractionTexture_13(RenderTexture_t2108887433 * value)
	{
		___m_RefractionTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionTexture_13), value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_14() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_HardwareWaterSupport_14)); }
	inline int32_t get_m_HardwareWaterSupport_14() const { return ___m_HardwareWaterSupport_14; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_14() { return &___m_HardwareWaterSupport_14; }
	inline void set_m_HardwareWaterSupport_14(int32_t value)
	{
		___m_HardwareWaterSupport_14 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_15() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_OldReflectionTextureSize_15)); }
	inline int32_t get_m_OldReflectionTextureSize_15() const { return ___m_OldReflectionTextureSize_15; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_15() { return &___m_OldReflectionTextureSize_15; }
	inline void set_m_OldReflectionTextureSize_15(int32_t value)
	{
		___m_OldReflectionTextureSize_15 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_16() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_OldRefractionTextureSize_16)); }
	inline int32_t get_m_OldRefractionTextureSize_16() const { return ___m_OldRefractionTextureSize_16; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_16() { return &___m_OldRefractionTextureSize_16; }
	inline void set_m_OldRefractionTextureSize_16(int32_t value)
	{
		___m_OldRefractionTextureSize_16 = value;
	}
};

struct Water_t936588004_StaticFields
{
public:
	// System.Boolean UnityStandardAssets.Water.Water::s_InsideWater
	bool ___s_InsideWater_17;

public:
	inline static int32_t get_offset_of_s_InsideWater_17() { return static_cast<int32_t>(offsetof(Water_t936588004_StaticFields, ___s_InsideWater_17)); }
	inline bool get_s_InsideWater_17() const { return ___s_InsideWater_17; }
	inline bool* get_address_of_s_InsideWater_17() { return &___s_InsideWater_17; }
	inline void set_s_InsideWater_17(bool value)
	{
		___s_InsideWater_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T936588004_H
#ifndef WATERBASE_T3883863317_H
#define WATERBASE_T3883863317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterBase
struct  WaterBase_t3883863317  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material UnityStandardAssets.Water.WaterBase::sharedMaterial
	Material_t340375123 * ___sharedMaterial_4;
	// UnityStandardAssets.Water.WaterQuality UnityStandardAssets.Water.WaterBase::waterQuality
	int32_t ___waterQuality_5;
	// System.Boolean UnityStandardAssets.Water.WaterBase::edgeBlend
	bool ___edgeBlend_6;

public:
	inline static int32_t get_offset_of_sharedMaterial_4() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___sharedMaterial_4)); }
	inline Material_t340375123 * get_sharedMaterial_4() const { return ___sharedMaterial_4; }
	inline Material_t340375123 ** get_address_of_sharedMaterial_4() { return &___sharedMaterial_4; }
	inline void set_sharedMaterial_4(Material_t340375123 * value)
	{
		___sharedMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterial_4), value);
	}

	inline static int32_t get_offset_of_waterQuality_5() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___waterQuality_5)); }
	inline int32_t get_waterQuality_5() const { return ___waterQuality_5; }
	inline int32_t* get_address_of_waterQuality_5() { return &___waterQuality_5; }
	inline void set_waterQuality_5(int32_t value)
	{
		___waterQuality_5 = value;
	}

	inline static int32_t get_offset_of_edgeBlend_6() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___edgeBlend_6)); }
	inline bool get_edgeBlend_6() const { return ___edgeBlend_6; }
	inline bool* get_address_of_edgeBlend_6() { return &___edgeBlend_6; }
	inline void set_edgeBlend_6(bool value)
	{
		___edgeBlend_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERBASE_T3883863317_H
#ifndef WATERBASIC_T418026961_H
#define WATERBASIC_T418026961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterBasic
struct  WaterBasic_t418026961  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERBASIC_T418026961_H
#ifndef WATERTILE_T2536246541_H
#define WATERTILE_T2536246541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterTile
struct  WaterTile_t2536246541  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Water.PlanarReflection UnityStandardAssets.Water.WaterTile::reflection
	PlanarReflection_t439636033 * ___reflection_4;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.WaterTile::waterBase
	WaterBase_t3883863317 * ___waterBase_5;

public:
	inline static int32_t get_offset_of_reflection_4() { return static_cast<int32_t>(offsetof(WaterTile_t2536246541, ___reflection_4)); }
	inline PlanarReflection_t439636033 * get_reflection_4() const { return ___reflection_4; }
	inline PlanarReflection_t439636033 ** get_address_of_reflection_4() { return &___reflection_4; }
	inline void set_reflection_4(PlanarReflection_t439636033 * value)
	{
		___reflection_4 = value;
		Il2CppCodeGenWriteBarrier((&___reflection_4), value);
	}

	inline static int32_t get_offset_of_waterBase_5() { return static_cast<int32_t>(offsetof(WaterTile_t2536246541, ___waterBase_5)); }
	inline WaterBase_t3883863317 * get_waterBase_5() const { return ___waterBase_5; }
	inline WaterBase_t3883863317 ** get_address_of_waterBase_5() { return &___waterBase_5; }
	inline void set_waterBase_5(WaterBase_t3883863317 * value)
	{
		___waterBase_5 = value;
		Il2CppCodeGenWriteBarrier((&___waterBase_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERTILE_T2536246541_H
#ifndef CAMERA2DFOLLOW_T3335230098_H
#define CAMERA2DFOLLOW_T3335230098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.Camera2DFollow
struct  Camera2DFollow_t3335230098  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets._2D.Camera2DFollow::target
	Transform_t3600365921 * ___target_4;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::damping
	float ___damping_5;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadFactor
	float ___lookAheadFactor_6;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadReturnSpeed
	float ___lookAheadReturnSpeed_7;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadMoveThreshold
	float ___lookAheadMoveThreshold_8;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::m_OffsetZ
	float ___m_OffsetZ_9;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_LastTargetPosition
	Vector3_t3722313464  ___m_LastTargetPosition_10;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_CurrentVelocity
	Vector3_t3722313464  ___m_CurrentVelocity_11;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_LookAheadPos
	Vector3_t3722313464  ___m_LookAheadPos_12;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_damping_5() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___damping_5)); }
	inline float get_damping_5() const { return ___damping_5; }
	inline float* get_address_of_damping_5() { return &___damping_5; }
	inline void set_damping_5(float value)
	{
		___damping_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadFactor_6() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___lookAheadFactor_6)); }
	inline float get_lookAheadFactor_6() const { return ___lookAheadFactor_6; }
	inline float* get_address_of_lookAheadFactor_6() { return &___lookAheadFactor_6; }
	inline void set_lookAheadFactor_6(float value)
	{
		___lookAheadFactor_6 = value;
	}

	inline static int32_t get_offset_of_lookAheadReturnSpeed_7() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___lookAheadReturnSpeed_7)); }
	inline float get_lookAheadReturnSpeed_7() const { return ___lookAheadReturnSpeed_7; }
	inline float* get_address_of_lookAheadReturnSpeed_7() { return &___lookAheadReturnSpeed_7; }
	inline void set_lookAheadReturnSpeed_7(float value)
	{
		___lookAheadReturnSpeed_7 = value;
	}

	inline static int32_t get_offset_of_lookAheadMoveThreshold_8() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___lookAheadMoveThreshold_8)); }
	inline float get_lookAheadMoveThreshold_8() const { return ___lookAheadMoveThreshold_8; }
	inline float* get_address_of_lookAheadMoveThreshold_8() { return &___lookAheadMoveThreshold_8; }
	inline void set_lookAheadMoveThreshold_8(float value)
	{
		___lookAheadMoveThreshold_8 = value;
	}

	inline static int32_t get_offset_of_m_OffsetZ_9() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_OffsetZ_9)); }
	inline float get_m_OffsetZ_9() const { return ___m_OffsetZ_9; }
	inline float* get_address_of_m_OffsetZ_9() { return &___m_OffsetZ_9; }
	inline void set_m_OffsetZ_9(float value)
	{
		___m_OffsetZ_9 = value;
	}

	inline static int32_t get_offset_of_m_LastTargetPosition_10() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_LastTargetPosition_10)); }
	inline Vector3_t3722313464  get_m_LastTargetPosition_10() const { return ___m_LastTargetPosition_10; }
	inline Vector3_t3722313464 * get_address_of_m_LastTargetPosition_10() { return &___m_LastTargetPosition_10; }
	inline void set_m_LastTargetPosition_10(Vector3_t3722313464  value)
	{
		___m_LastTargetPosition_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentVelocity_11() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_CurrentVelocity_11)); }
	inline Vector3_t3722313464  get_m_CurrentVelocity_11() const { return ___m_CurrentVelocity_11; }
	inline Vector3_t3722313464 * get_address_of_m_CurrentVelocity_11() { return &___m_CurrentVelocity_11; }
	inline void set_m_CurrentVelocity_11(Vector3_t3722313464  value)
	{
		___m_CurrentVelocity_11 = value;
	}

	inline static int32_t get_offset_of_m_LookAheadPos_12() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_LookAheadPos_12)); }
	inline Vector3_t3722313464  get_m_LookAheadPos_12() const { return ___m_LookAheadPos_12; }
	inline Vector3_t3722313464 * get_address_of_m_LookAheadPos_12() { return &___m_LookAheadPos_12; }
	inline void set_m_LookAheadPos_12(Vector3_t3722313464  value)
	{
		___m_LookAheadPos_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA2DFOLLOW_T3335230098_H
#ifndef CAMERAFOLLOW_T1399352937_H
#define CAMERAFOLLOW_T1399352937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.CameraFollow
struct  CameraFollow_t1399352937  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets._2D.CameraFollow::xMargin
	float ___xMargin_4;
	// System.Single UnityStandardAssets._2D.CameraFollow::yMargin
	float ___yMargin_5;
	// System.Single UnityStandardAssets._2D.CameraFollow::xSmooth
	float ___xSmooth_6;
	// System.Single UnityStandardAssets._2D.CameraFollow::ySmooth
	float ___ySmooth_7;
	// UnityEngine.Vector2 UnityStandardAssets._2D.CameraFollow::maxXAndY
	Vector2_t2156229523  ___maxXAndY_8;
	// UnityEngine.Vector2 UnityStandardAssets._2D.CameraFollow::minXAndY
	Vector2_t2156229523  ___minXAndY_9;
	// UnityEngine.Transform UnityStandardAssets._2D.CameraFollow::m_Player
	Transform_t3600365921 * ___m_Player_10;

public:
	inline static int32_t get_offset_of_xMargin_4() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___xMargin_4)); }
	inline float get_xMargin_4() const { return ___xMargin_4; }
	inline float* get_address_of_xMargin_4() { return &___xMargin_4; }
	inline void set_xMargin_4(float value)
	{
		___xMargin_4 = value;
	}

	inline static int32_t get_offset_of_yMargin_5() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___yMargin_5)); }
	inline float get_yMargin_5() const { return ___yMargin_5; }
	inline float* get_address_of_yMargin_5() { return &___yMargin_5; }
	inline void set_yMargin_5(float value)
	{
		___yMargin_5 = value;
	}

	inline static int32_t get_offset_of_xSmooth_6() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___xSmooth_6)); }
	inline float get_xSmooth_6() const { return ___xSmooth_6; }
	inline float* get_address_of_xSmooth_6() { return &___xSmooth_6; }
	inline void set_xSmooth_6(float value)
	{
		___xSmooth_6 = value;
	}

	inline static int32_t get_offset_of_ySmooth_7() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___ySmooth_7)); }
	inline float get_ySmooth_7() const { return ___ySmooth_7; }
	inline float* get_address_of_ySmooth_7() { return &___ySmooth_7; }
	inline void set_ySmooth_7(float value)
	{
		___ySmooth_7 = value;
	}

	inline static int32_t get_offset_of_maxXAndY_8() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___maxXAndY_8)); }
	inline Vector2_t2156229523  get_maxXAndY_8() const { return ___maxXAndY_8; }
	inline Vector2_t2156229523 * get_address_of_maxXAndY_8() { return &___maxXAndY_8; }
	inline void set_maxXAndY_8(Vector2_t2156229523  value)
	{
		___maxXAndY_8 = value;
	}

	inline static int32_t get_offset_of_minXAndY_9() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___minXAndY_9)); }
	inline Vector2_t2156229523  get_minXAndY_9() const { return ___minXAndY_9; }
	inline Vector2_t2156229523 * get_address_of_minXAndY_9() { return &___minXAndY_9; }
	inline void set_minXAndY_9(Vector2_t2156229523  value)
	{
		___minXAndY_9 = value;
	}

	inline static int32_t get_offset_of_m_Player_10() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___m_Player_10)); }
	inline Transform_t3600365921 * get_m_Player_10() const { return ___m_Player_10; }
	inline Transform_t3600365921 ** get_address_of_m_Player_10() { return &___m_Player_10; }
	inline void set_m_Player_10(Transform_t3600365921 * value)
	{
		___m_Player_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Player_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOLLOW_T1399352937_H
#ifndef PLATFORMER2DUSERCONTROL_T4130129562_H
#define PLATFORMER2DUSERCONTROL_T4130129562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.Platformer2DUserControl
struct  Platformer2DUserControl_t4130129562  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets._2D.PlatformerCharacter2D UnityStandardAssets._2D.Platformer2DUserControl::m_Character
	PlatformerCharacter2D_t675295753 * ___m_Character_4;
	// System.Boolean UnityStandardAssets._2D.Platformer2DUserControl::m_Jump
	bool ___m_Jump_5;

public:
	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(Platformer2DUserControl_t4130129562, ___m_Character_4)); }
	inline PlatformerCharacter2D_t675295753 * get_m_Character_4() const { return ___m_Character_4; }
	inline PlatformerCharacter2D_t675295753 ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(PlatformerCharacter2D_t675295753 * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_4), value);
	}

	inline static int32_t get_offset_of_m_Jump_5() { return static_cast<int32_t>(offsetof(Platformer2DUserControl_t4130129562, ___m_Jump_5)); }
	inline bool get_m_Jump_5() const { return ___m_Jump_5; }
	inline bool* get_address_of_m_Jump_5() { return &___m_Jump_5; }
	inline void set_m_Jump_5(bool value)
	{
		___m_Jump_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMER2DUSERCONTROL_T4130129562_H
#ifndef PLATFORMERCHARACTER2D_T675295753_H
#define PLATFORMERCHARACTER2D_T675295753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.PlatformerCharacter2D
struct  PlatformerCharacter2D_t675295753  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets._2D.PlatformerCharacter2D::m_MaxSpeed
	float ___m_MaxSpeed_4;
	// System.Single UnityStandardAssets._2D.PlatformerCharacter2D::m_JumpForce
	float ___m_JumpForce_5;
	// System.Single UnityStandardAssets._2D.PlatformerCharacter2D::m_CrouchSpeed
	float ___m_CrouchSpeed_6;
	// System.Boolean UnityStandardAssets._2D.PlatformerCharacter2D::m_AirControl
	bool ___m_AirControl_7;
	// UnityEngine.LayerMask UnityStandardAssets._2D.PlatformerCharacter2D::m_WhatIsGround
	LayerMask_t3493934918  ___m_WhatIsGround_8;
	// UnityEngine.Transform UnityStandardAssets._2D.PlatformerCharacter2D::m_GroundCheck
	Transform_t3600365921 * ___m_GroundCheck_9;
	// System.Boolean UnityStandardAssets._2D.PlatformerCharacter2D::m_Grounded
	bool ___m_Grounded_11;
	// UnityEngine.Transform UnityStandardAssets._2D.PlatformerCharacter2D::m_CeilingCheck
	Transform_t3600365921 * ___m_CeilingCheck_12;
	// UnityEngine.Animator UnityStandardAssets._2D.PlatformerCharacter2D::m_Anim
	Animator_t434523843 * ___m_Anim_14;
	// UnityEngine.Rigidbody2D UnityStandardAssets._2D.PlatformerCharacter2D::m_Rigidbody2D
	Rigidbody2D_t939494601 * ___m_Rigidbody2D_15;
	// System.Boolean UnityStandardAssets._2D.PlatformerCharacter2D::m_FacingRight
	bool ___m_FacingRight_16;

public:
	inline static int32_t get_offset_of_m_MaxSpeed_4() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_MaxSpeed_4)); }
	inline float get_m_MaxSpeed_4() const { return ___m_MaxSpeed_4; }
	inline float* get_address_of_m_MaxSpeed_4() { return &___m_MaxSpeed_4; }
	inline void set_m_MaxSpeed_4(float value)
	{
		___m_MaxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_JumpForce_5() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_JumpForce_5)); }
	inline float get_m_JumpForce_5() const { return ___m_JumpForce_5; }
	inline float* get_address_of_m_JumpForce_5() { return &___m_JumpForce_5; }
	inline void set_m_JumpForce_5(float value)
	{
		___m_JumpForce_5 = value;
	}

	inline static int32_t get_offset_of_m_CrouchSpeed_6() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_CrouchSpeed_6)); }
	inline float get_m_CrouchSpeed_6() const { return ___m_CrouchSpeed_6; }
	inline float* get_address_of_m_CrouchSpeed_6() { return &___m_CrouchSpeed_6; }
	inline void set_m_CrouchSpeed_6(float value)
	{
		___m_CrouchSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_AirControl_7() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_AirControl_7)); }
	inline bool get_m_AirControl_7() const { return ___m_AirControl_7; }
	inline bool* get_address_of_m_AirControl_7() { return &___m_AirControl_7; }
	inline void set_m_AirControl_7(bool value)
	{
		___m_AirControl_7 = value;
	}

	inline static int32_t get_offset_of_m_WhatIsGround_8() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_WhatIsGround_8)); }
	inline LayerMask_t3493934918  get_m_WhatIsGround_8() const { return ___m_WhatIsGround_8; }
	inline LayerMask_t3493934918 * get_address_of_m_WhatIsGround_8() { return &___m_WhatIsGround_8; }
	inline void set_m_WhatIsGround_8(LayerMask_t3493934918  value)
	{
		___m_WhatIsGround_8 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheck_9() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_GroundCheck_9)); }
	inline Transform_t3600365921 * get_m_GroundCheck_9() const { return ___m_GroundCheck_9; }
	inline Transform_t3600365921 ** get_address_of_m_GroundCheck_9() { return &___m_GroundCheck_9; }
	inline void set_m_GroundCheck_9(Transform_t3600365921 * value)
	{
		___m_GroundCheck_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroundCheck_9), value);
	}

	inline static int32_t get_offset_of_m_Grounded_11() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_Grounded_11)); }
	inline bool get_m_Grounded_11() const { return ___m_Grounded_11; }
	inline bool* get_address_of_m_Grounded_11() { return &___m_Grounded_11; }
	inline void set_m_Grounded_11(bool value)
	{
		___m_Grounded_11 = value;
	}

	inline static int32_t get_offset_of_m_CeilingCheck_12() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_CeilingCheck_12)); }
	inline Transform_t3600365921 * get_m_CeilingCheck_12() const { return ___m_CeilingCheck_12; }
	inline Transform_t3600365921 ** get_address_of_m_CeilingCheck_12() { return &___m_CeilingCheck_12; }
	inline void set_m_CeilingCheck_12(Transform_t3600365921 * value)
	{
		___m_CeilingCheck_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CeilingCheck_12), value);
	}

	inline static int32_t get_offset_of_m_Anim_14() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_Anim_14)); }
	inline Animator_t434523843 * get_m_Anim_14() const { return ___m_Anim_14; }
	inline Animator_t434523843 ** get_address_of_m_Anim_14() { return &___m_Anim_14; }
	inline void set_m_Anim_14(Animator_t434523843 * value)
	{
		___m_Anim_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Anim_14), value);
	}

	inline static int32_t get_offset_of_m_Rigidbody2D_15() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_Rigidbody2D_15)); }
	inline Rigidbody2D_t939494601 * get_m_Rigidbody2D_15() const { return ___m_Rigidbody2D_15; }
	inline Rigidbody2D_t939494601 ** get_address_of_m_Rigidbody2D_15() { return &___m_Rigidbody2D_15; }
	inline void set_m_Rigidbody2D_15(Rigidbody2D_t939494601 * value)
	{
		___m_Rigidbody2D_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody2D_15), value);
	}

	inline static int32_t get_offset_of_m_FacingRight_16() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_FacingRight_16)); }
	inline bool get_m_FacingRight_16() const { return ___m_FacingRight_16; }
	inline bool* get_address_of_m_FacingRight_16() { return &___m_FacingRight_16; }
	inline void set_m_FacingRight_16(bool value)
	{
		___m_FacingRight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMERCHARACTER2D_T675295753_H
#ifndef RESTARTER_T269523250_H
#define RESTARTER_T269523250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.Restarter
struct  Restarter_t269523250  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTARTER_T269523250_H
#ifndef LOOKATTARGET_T3260877718_H
#define LOOKATTARGET_T3260877718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.LookatTarget
struct  LookatTarget_t3260877718  : public AbstractTargetFollower_t1919708159
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Cameras.LookatTarget::m_RotationRange
	Vector2_t2156229523  ___m_RotationRange_8;
	// System.Single UnityStandardAssets.Cameras.LookatTarget::m_FollowSpeed
	float ___m_FollowSpeed_9;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.LookatTarget::m_FollowAngles
	Vector3_t3722313464  ___m_FollowAngles_10;
	// UnityEngine.Quaternion UnityStandardAssets.Cameras.LookatTarget::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_11;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.LookatTarget::m_FollowVelocity
	Vector3_t3722313464  ___m_FollowVelocity_12;

public:
	inline static int32_t get_offset_of_m_RotationRange_8() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_RotationRange_8)); }
	inline Vector2_t2156229523  get_m_RotationRange_8() const { return ___m_RotationRange_8; }
	inline Vector2_t2156229523 * get_address_of_m_RotationRange_8() { return &___m_RotationRange_8; }
	inline void set_m_RotationRange_8(Vector2_t2156229523  value)
	{
		___m_RotationRange_8 = value;
	}

	inline static int32_t get_offset_of_m_FollowSpeed_9() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_FollowSpeed_9)); }
	inline float get_m_FollowSpeed_9() const { return ___m_FollowSpeed_9; }
	inline float* get_address_of_m_FollowSpeed_9() { return &___m_FollowSpeed_9; }
	inline void set_m_FollowSpeed_9(float value)
	{
		___m_FollowSpeed_9 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_10() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_FollowAngles_10)); }
	inline Vector3_t3722313464  get_m_FollowAngles_10() const { return ___m_FollowAngles_10; }
	inline Vector3_t3722313464 * get_address_of_m_FollowAngles_10() { return &___m_FollowAngles_10; }
	inline void set_m_FollowAngles_10(Vector3_t3722313464  value)
	{
		___m_FollowAngles_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_11() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_OriginalRotation_11)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_11() const { return ___m_OriginalRotation_11; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_11() { return &___m_OriginalRotation_11; }
	inline void set_m_OriginalRotation_11(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_11 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_12() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_FollowVelocity_12)); }
	inline Vector3_t3722313464  get_m_FollowVelocity_12() const { return ___m_FollowVelocity_12; }
	inline Vector3_t3722313464 * get_address_of_m_FollowVelocity_12() { return &___m_FollowVelocity_12; }
	inline void set_m_FollowVelocity_12(Vector3_t3722313464  value)
	{
		___m_FollowVelocity_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKATTARGET_T3260877718_H
#ifndef PIVOTBASEDCAMERARIG_T3786953582_H
#define PIVOTBASEDCAMERARIG_T3786953582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.PivotBasedCameraRig
struct  PivotBasedCameraRig_t3786953582  : public AbstractTargetFollower_t1919708159
{
public:
	// UnityEngine.Transform UnityStandardAssets.Cameras.PivotBasedCameraRig::m_Cam
	Transform_t3600365921 * ___m_Cam_8;
	// UnityEngine.Transform UnityStandardAssets.Cameras.PivotBasedCameraRig::m_Pivot
	Transform_t3600365921 * ___m_Pivot_9;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.PivotBasedCameraRig::m_LastTargetPosition
	Vector3_t3722313464  ___m_LastTargetPosition_10;

public:
	inline static int32_t get_offset_of_m_Cam_8() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t3786953582, ___m_Cam_8)); }
	inline Transform_t3600365921 * get_m_Cam_8() const { return ___m_Cam_8; }
	inline Transform_t3600365921 ** get_address_of_m_Cam_8() { return &___m_Cam_8; }
	inline void set_m_Cam_8(Transform_t3600365921 * value)
	{
		___m_Cam_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_8), value);
	}

	inline static int32_t get_offset_of_m_Pivot_9() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t3786953582, ___m_Pivot_9)); }
	inline Transform_t3600365921 * get_m_Pivot_9() const { return ___m_Pivot_9; }
	inline Transform_t3600365921 ** get_address_of_m_Pivot_9() { return &___m_Pivot_9; }
	inline void set_m_Pivot_9(Transform_t3600365921 * value)
	{
		___m_Pivot_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pivot_9), value);
	}

	inline static int32_t get_offset_of_m_LastTargetPosition_10() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t3786953582, ___m_LastTargetPosition_10)); }
	inline Vector3_t3722313464  get_m_LastTargetPosition_10() const { return ___m_LastTargetPosition_10; }
	inline Vector3_t3722313464 * get_address_of_m_LastTargetPosition_10() { return &___m_LastTargetPosition_10; }
	inline void set_m_LastTargetPosition_10(Vector3_t3722313464  value)
	{
		___m_LastTargetPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOTBASEDCAMERARIG_T3786953582_H
#ifndef TARGETFIELDOFVIEW_T3060904718_H
#define TARGETFIELDOFVIEW_T3060904718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.TargetFieldOfView
struct  TargetFieldOfView_t3060904718  : public AbstractTargetFollower_t1919708159
{
public:
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_FovAdjustTime
	float ___m_FovAdjustTime_8;
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_ZoomAmountMultiplier
	float ___m_ZoomAmountMultiplier_9;
	// System.Boolean UnityStandardAssets.Cameras.TargetFieldOfView::m_IncludeEffectsInSize
	bool ___m_IncludeEffectsInSize_10;
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_BoundSize
	float ___m_BoundSize_11;
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_FovAdjustVelocity
	float ___m_FovAdjustVelocity_12;
	// UnityEngine.Camera UnityStandardAssets.Cameras.TargetFieldOfView::m_Cam
	Camera_t4157153871 * ___m_Cam_13;
	// UnityEngine.Transform UnityStandardAssets.Cameras.TargetFieldOfView::m_LastTarget
	Transform_t3600365921 * ___m_LastTarget_14;

public:
	inline static int32_t get_offset_of_m_FovAdjustTime_8() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_FovAdjustTime_8)); }
	inline float get_m_FovAdjustTime_8() const { return ___m_FovAdjustTime_8; }
	inline float* get_address_of_m_FovAdjustTime_8() { return &___m_FovAdjustTime_8; }
	inline void set_m_FovAdjustTime_8(float value)
	{
		___m_FovAdjustTime_8 = value;
	}

	inline static int32_t get_offset_of_m_ZoomAmountMultiplier_9() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_ZoomAmountMultiplier_9)); }
	inline float get_m_ZoomAmountMultiplier_9() const { return ___m_ZoomAmountMultiplier_9; }
	inline float* get_address_of_m_ZoomAmountMultiplier_9() { return &___m_ZoomAmountMultiplier_9; }
	inline void set_m_ZoomAmountMultiplier_9(float value)
	{
		___m_ZoomAmountMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_m_IncludeEffectsInSize_10() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_IncludeEffectsInSize_10)); }
	inline bool get_m_IncludeEffectsInSize_10() const { return ___m_IncludeEffectsInSize_10; }
	inline bool* get_address_of_m_IncludeEffectsInSize_10() { return &___m_IncludeEffectsInSize_10; }
	inline void set_m_IncludeEffectsInSize_10(bool value)
	{
		___m_IncludeEffectsInSize_10 = value;
	}

	inline static int32_t get_offset_of_m_BoundSize_11() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_BoundSize_11)); }
	inline float get_m_BoundSize_11() const { return ___m_BoundSize_11; }
	inline float* get_address_of_m_BoundSize_11() { return &___m_BoundSize_11; }
	inline void set_m_BoundSize_11(float value)
	{
		___m_BoundSize_11 = value;
	}

	inline static int32_t get_offset_of_m_FovAdjustVelocity_12() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_FovAdjustVelocity_12)); }
	inline float get_m_FovAdjustVelocity_12() const { return ___m_FovAdjustVelocity_12; }
	inline float* get_address_of_m_FovAdjustVelocity_12() { return &___m_FovAdjustVelocity_12; }
	inline void set_m_FovAdjustVelocity_12(float value)
	{
		___m_FovAdjustVelocity_12 = value;
	}

	inline static int32_t get_offset_of_m_Cam_13() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_Cam_13)); }
	inline Camera_t4157153871 * get_m_Cam_13() const { return ___m_Cam_13; }
	inline Camera_t4157153871 ** get_address_of_m_Cam_13() { return &___m_Cam_13; }
	inline void set_m_Cam_13(Camera_t4157153871 * value)
	{
		___m_Cam_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_13), value);
	}

	inline static int32_t get_offset_of_m_LastTarget_14() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_LastTarget_14)); }
	inline Transform_t3600365921 * get_m_LastTarget_14() const { return ___m_LastTarget_14; }
	inline Transform_t3600365921 ** get_address_of_m_LastTarget_14() { return &___m_LastTarget_14; }
	inline void set_m_LastTarget_14(Transform_t3600365921 * value)
	{
		___m_LastTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFIELDOFVIEW_T3060904718_H
#ifndef GERSTNERDISPLACE_T2537102777_H
#define GERSTNERDISPLACE_T2537102777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.GerstnerDisplace
struct  GerstnerDisplace_t2537102777  : public Displace_t1352130581
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GERSTNERDISPLACE_T2537102777_H
#ifndef AUTOCAM_T137911967_H
#define AUTOCAM_T137911967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.AutoCam
struct  AutoCam_t137911967  : public PivotBasedCameraRig_t3786953582
{
public:
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_MoveSpeed
	float ___m_MoveSpeed_11;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TurnSpeed
	float ___m_TurnSpeed_12;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_RollSpeed
	float ___m_RollSpeed_13;
	// System.Boolean UnityStandardAssets.Cameras.AutoCam::m_FollowVelocity
	bool ___m_FollowVelocity_14;
	// System.Boolean UnityStandardAssets.Cameras.AutoCam::m_FollowTilt
	bool ___m_FollowTilt_15;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_SpinTurnLimit
	float ___m_SpinTurnLimit_16;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TargetVelocityLowerLimit
	float ___m_TargetVelocityLowerLimit_17;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_SmoothTurnTime
	float ___m_SmoothTurnTime_18;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_LastFlatAngle
	float ___m_LastFlatAngle_19;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_CurrentTurnAmount
	float ___m_CurrentTurnAmount_20;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TurnSpeedVelocityChange
	float ___m_TurnSpeedVelocityChange_21;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.AutoCam::m_RollUp
	Vector3_t3722313464  ___m_RollUp_22;

public:
	inline static int32_t get_offset_of_m_MoveSpeed_11() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_MoveSpeed_11)); }
	inline float get_m_MoveSpeed_11() const { return ___m_MoveSpeed_11; }
	inline float* get_address_of_m_MoveSpeed_11() { return &___m_MoveSpeed_11; }
	inline void set_m_MoveSpeed_11(float value)
	{
		___m_MoveSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeed_12() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_TurnSpeed_12)); }
	inline float get_m_TurnSpeed_12() const { return ___m_TurnSpeed_12; }
	inline float* get_address_of_m_TurnSpeed_12() { return &___m_TurnSpeed_12; }
	inline void set_m_TurnSpeed_12(float value)
	{
		___m_TurnSpeed_12 = value;
	}

	inline static int32_t get_offset_of_m_RollSpeed_13() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_RollSpeed_13)); }
	inline float get_m_RollSpeed_13() const { return ___m_RollSpeed_13; }
	inline float* get_address_of_m_RollSpeed_13() { return &___m_RollSpeed_13; }
	inline void set_m_RollSpeed_13(float value)
	{
		___m_RollSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_14() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_FollowVelocity_14)); }
	inline bool get_m_FollowVelocity_14() const { return ___m_FollowVelocity_14; }
	inline bool* get_address_of_m_FollowVelocity_14() { return &___m_FollowVelocity_14; }
	inline void set_m_FollowVelocity_14(bool value)
	{
		___m_FollowVelocity_14 = value;
	}

	inline static int32_t get_offset_of_m_FollowTilt_15() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_FollowTilt_15)); }
	inline bool get_m_FollowTilt_15() const { return ___m_FollowTilt_15; }
	inline bool* get_address_of_m_FollowTilt_15() { return &___m_FollowTilt_15; }
	inline void set_m_FollowTilt_15(bool value)
	{
		___m_FollowTilt_15 = value;
	}

	inline static int32_t get_offset_of_m_SpinTurnLimit_16() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_SpinTurnLimit_16)); }
	inline float get_m_SpinTurnLimit_16() const { return ___m_SpinTurnLimit_16; }
	inline float* get_address_of_m_SpinTurnLimit_16() { return &___m_SpinTurnLimit_16; }
	inline void set_m_SpinTurnLimit_16(float value)
	{
		___m_SpinTurnLimit_16 = value;
	}

	inline static int32_t get_offset_of_m_TargetVelocityLowerLimit_17() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_TargetVelocityLowerLimit_17)); }
	inline float get_m_TargetVelocityLowerLimit_17() const { return ___m_TargetVelocityLowerLimit_17; }
	inline float* get_address_of_m_TargetVelocityLowerLimit_17() { return &___m_TargetVelocityLowerLimit_17; }
	inline void set_m_TargetVelocityLowerLimit_17(float value)
	{
		___m_TargetVelocityLowerLimit_17 = value;
	}

	inline static int32_t get_offset_of_m_SmoothTurnTime_18() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_SmoothTurnTime_18)); }
	inline float get_m_SmoothTurnTime_18() const { return ___m_SmoothTurnTime_18; }
	inline float* get_address_of_m_SmoothTurnTime_18() { return &___m_SmoothTurnTime_18; }
	inline void set_m_SmoothTurnTime_18(float value)
	{
		___m_SmoothTurnTime_18 = value;
	}

	inline static int32_t get_offset_of_m_LastFlatAngle_19() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_LastFlatAngle_19)); }
	inline float get_m_LastFlatAngle_19() const { return ___m_LastFlatAngle_19; }
	inline float* get_address_of_m_LastFlatAngle_19() { return &___m_LastFlatAngle_19; }
	inline void set_m_LastFlatAngle_19(float value)
	{
		___m_LastFlatAngle_19 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTurnAmount_20() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_CurrentTurnAmount_20)); }
	inline float get_m_CurrentTurnAmount_20() const { return ___m_CurrentTurnAmount_20; }
	inline float* get_address_of_m_CurrentTurnAmount_20() { return &___m_CurrentTurnAmount_20; }
	inline void set_m_CurrentTurnAmount_20(float value)
	{
		___m_CurrentTurnAmount_20 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeedVelocityChange_21() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_TurnSpeedVelocityChange_21)); }
	inline float get_m_TurnSpeedVelocityChange_21() const { return ___m_TurnSpeedVelocityChange_21; }
	inline float* get_address_of_m_TurnSpeedVelocityChange_21() { return &___m_TurnSpeedVelocityChange_21; }
	inline void set_m_TurnSpeedVelocityChange_21(float value)
	{
		___m_TurnSpeedVelocityChange_21 = value;
	}

	inline static int32_t get_offset_of_m_RollUp_22() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_RollUp_22)); }
	inline Vector3_t3722313464  get_m_RollUp_22() const { return ___m_RollUp_22; }
	inline Vector3_t3722313464 * get_address_of_m_RollUp_22() { return &___m_RollUp_22; }
	inline void set_m_RollUp_22(Vector3_t3722313464  value)
	{
		___m_RollUp_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCAM_T137911967_H
#ifndef FREELOOKCAM_T2000732766_H
#define FREELOOKCAM_T2000732766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.FreeLookCam
struct  FreeLookCam_t2000732766  : public PivotBasedCameraRig_t3786953582
{
public:
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_MoveSpeed
	float ___m_MoveSpeed_11;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TurnSpeed
	float ___m_TurnSpeed_12;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TurnSmoothing
	float ___m_TurnSmoothing_13;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TiltMax
	float ___m_TiltMax_14;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TiltMin
	float ___m_TiltMin_15;
	// System.Boolean UnityStandardAssets.Cameras.FreeLookCam::m_LockCursor
	bool ___m_LockCursor_16;
	// System.Boolean UnityStandardAssets.Cameras.FreeLookCam::m_VerticalAutoReturn
	bool ___m_VerticalAutoReturn_17;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_LookAngle
	float ___m_LookAngle_18;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TiltAngle
	float ___m_TiltAngle_19;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.FreeLookCam::m_PivotEulers
	Vector3_t3722313464  ___m_PivotEulers_21;
	// UnityEngine.Quaternion UnityStandardAssets.Cameras.FreeLookCam::m_PivotTargetRot
	Quaternion_t2301928331  ___m_PivotTargetRot_22;
	// UnityEngine.Quaternion UnityStandardAssets.Cameras.FreeLookCam::m_TransformTargetRot
	Quaternion_t2301928331  ___m_TransformTargetRot_23;

public:
	inline static int32_t get_offset_of_m_MoveSpeed_11() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_MoveSpeed_11)); }
	inline float get_m_MoveSpeed_11() const { return ___m_MoveSpeed_11; }
	inline float* get_address_of_m_MoveSpeed_11() { return &___m_MoveSpeed_11; }
	inline void set_m_MoveSpeed_11(float value)
	{
		___m_MoveSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeed_12() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TurnSpeed_12)); }
	inline float get_m_TurnSpeed_12() const { return ___m_TurnSpeed_12; }
	inline float* get_address_of_m_TurnSpeed_12() { return &___m_TurnSpeed_12; }
	inline void set_m_TurnSpeed_12(float value)
	{
		___m_TurnSpeed_12 = value;
	}

	inline static int32_t get_offset_of_m_TurnSmoothing_13() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TurnSmoothing_13)); }
	inline float get_m_TurnSmoothing_13() const { return ___m_TurnSmoothing_13; }
	inline float* get_address_of_m_TurnSmoothing_13() { return &___m_TurnSmoothing_13; }
	inline void set_m_TurnSmoothing_13(float value)
	{
		___m_TurnSmoothing_13 = value;
	}

	inline static int32_t get_offset_of_m_TiltMax_14() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TiltMax_14)); }
	inline float get_m_TiltMax_14() const { return ___m_TiltMax_14; }
	inline float* get_address_of_m_TiltMax_14() { return &___m_TiltMax_14; }
	inline void set_m_TiltMax_14(float value)
	{
		___m_TiltMax_14 = value;
	}

	inline static int32_t get_offset_of_m_TiltMin_15() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TiltMin_15)); }
	inline float get_m_TiltMin_15() const { return ___m_TiltMin_15; }
	inline float* get_address_of_m_TiltMin_15() { return &___m_TiltMin_15; }
	inline void set_m_TiltMin_15(float value)
	{
		___m_TiltMin_15 = value;
	}

	inline static int32_t get_offset_of_m_LockCursor_16() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_LockCursor_16)); }
	inline bool get_m_LockCursor_16() const { return ___m_LockCursor_16; }
	inline bool* get_address_of_m_LockCursor_16() { return &___m_LockCursor_16; }
	inline void set_m_LockCursor_16(bool value)
	{
		___m_LockCursor_16 = value;
	}

	inline static int32_t get_offset_of_m_VerticalAutoReturn_17() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_VerticalAutoReturn_17)); }
	inline bool get_m_VerticalAutoReturn_17() const { return ___m_VerticalAutoReturn_17; }
	inline bool* get_address_of_m_VerticalAutoReturn_17() { return &___m_VerticalAutoReturn_17; }
	inline void set_m_VerticalAutoReturn_17(bool value)
	{
		___m_VerticalAutoReturn_17 = value;
	}

	inline static int32_t get_offset_of_m_LookAngle_18() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_LookAngle_18)); }
	inline float get_m_LookAngle_18() const { return ___m_LookAngle_18; }
	inline float* get_address_of_m_LookAngle_18() { return &___m_LookAngle_18; }
	inline void set_m_LookAngle_18(float value)
	{
		___m_LookAngle_18 = value;
	}

	inline static int32_t get_offset_of_m_TiltAngle_19() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TiltAngle_19)); }
	inline float get_m_TiltAngle_19() const { return ___m_TiltAngle_19; }
	inline float* get_address_of_m_TiltAngle_19() { return &___m_TiltAngle_19; }
	inline void set_m_TiltAngle_19(float value)
	{
		___m_TiltAngle_19 = value;
	}

	inline static int32_t get_offset_of_m_PivotEulers_21() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_PivotEulers_21)); }
	inline Vector3_t3722313464  get_m_PivotEulers_21() const { return ___m_PivotEulers_21; }
	inline Vector3_t3722313464 * get_address_of_m_PivotEulers_21() { return &___m_PivotEulers_21; }
	inline void set_m_PivotEulers_21(Vector3_t3722313464  value)
	{
		___m_PivotEulers_21 = value;
	}

	inline static int32_t get_offset_of_m_PivotTargetRot_22() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_PivotTargetRot_22)); }
	inline Quaternion_t2301928331  get_m_PivotTargetRot_22() const { return ___m_PivotTargetRot_22; }
	inline Quaternion_t2301928331 * get_address_of_m_PivotTargetRot_22() { return &___m_PivotTargetRot_22; }
	inline void set_m_PivotTargetRot_22(Quaternion_t2301928331  value)
	{
		___m_PivotTargetRot_22 = value;
	}

	inline static int32_t get_offset_of_m_TransformTargetRot_23() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TransformTargetRot_23)); }
	inline Quaternion_t2301928331  get_m_TransformTargetRot_23() const { return ___m_TransformTargetRot_23; }
	inline Quaternion_t2301928331 * get_address_of_m_TransformTargetRot_23() { return &___m_TransformTargetRot_23; }
	inline void set_m_TransformTargetRot_23(Quaternion_t2301928331  value)
	{
		___m_TransformTargetRot_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FREELOOKCAM_T2000732766_H
#ifndef HANDHELDCAM_T450595784_H
#define HANDHELDCAM_T450595784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.HandHeldCam
struct  HandHeldCam_t450595784  : public LookatTarget_t3260877718
{
public:
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_SwaySpeed
	float ___m_SwaySpeed_13;
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_BaseSwayAmount
	float ___m_BaseSwayAmount_14;
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_TrackingSwayAmount
	float ___m_TrackingSwayAmount_15;
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_TrackingBias
	float ___m_TrackingBias_16;

public:
	inline static int32_t get_offset_of_m_SwaySpeed_13() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_SwaySpeed_13)); }
	inline float get_m_SwaySpeed_13() const { return ___m_SwaySpeed_13; }
	inline float* get_address_of_m_SwaySpeed_13() { return &___m_SwaySpeed_13; }
	inline void set_m_SwaySpeed_13(float value)
	{
		___m_SwaySpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_BaseSwayAmount_14() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_BaseSwayAmount_14)); }
	inline float get_m_BaseSwayAmount_14() const { return ___m_BaseSwayAmount_14; }
	inline float* get_address_of_m_BaseSwayAmount_14() { return &___m_BaseSwayAmount_14; }
	inline void set_m_BaseSwayAmount_14(float value)
	{
		___m_BaseSwayAmount_14 = value;
	}

	inline static int32_t get_offset_of_m_TrackingSwayAmount_15() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_TrackingSwayAmount_15)); }
	inline float get_m_TrackingSwayAmount_15() const { return ___m_TrackingSwayAmount_15; }
	inline float* get_address_of_m_TrackingSwayAmount_15() { return &___m_TrackingSwayAmount_15; }
	inline void set_m_TrackingSwayAmount_15(float value)
	{
		___m_TrackingSwayAmount_15 = value;
	}

	inline static int32_t get_offset_of_m_TrackingBias_16() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_TrackingBias_16)); }
	inline float get_m_TrackingBias_16() const { return ___m_TrackingBias_16; }
	inline float* get_address_of_m_TrackingBias_16() { return &___m_TrackingBias_16; }
	inline void set_m_TrackingBias_16(float value)
	{
		___m_TrackingBias_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDHELDCAM_T450595784_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2100[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_4(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2109[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2111[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2115[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_6(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_7(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2117[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2120[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2121[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2122[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2123[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (Camera2DFollow_t3335230098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[9] = 
{
	Camera2DFollow_t3335230098::get_offset_of_target_4(),
	Camera2DFollow_t3335230098::get_offset_of_damping_5(),
	Camera2DFollow_t3335230098::get_offset_of_lookAheadFactor_6(),
	Camera2DFollow_t3335230098::get_offset_of_lookAheadReturnSpeed_7(),
	Camera2DFollow_t3335230098::get_offset_of_lookAheadMoveThreshold_8(),
	Camera2DFollow_t3335230098::get_offset_of_m_OffsetZ_9(),
	Camera2DFollow_t3335230098::get_offset_of_m_LastTargetPosition_10(),
	Camera2DFollow_t3335230098::get_offset_of_m_CurrentVelocity_11(),
	Camera2DFollow_t3335230098::get_offset_of_m_LookAheadPos_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (CameraFollow_t1399352937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[7] = 
{
	CameraFollow_t1399352937::get_offset_of_xMargin_4(),
	CameraFollow_t1399352937::get_offset_of_yMargin_5(),
	CameraFollow_t1399352937::get_offset_of_xSmooth_6(),
	CameraFollow_t1399352937::get_offset_of_ySmooth_7(),
	CameraFollow_t1399352937::get_offset_of_maxXAndY_8(),
	CameraFollow_t1399352937::get_offset_of_minXAndY_9(),
	CameraFollow_t1399352937::get_offset_of_m_Player_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (Platformer2DUserControl_t4130129562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[2] = 
{
	Platformer2DUserControl_t4130129562::get_offset_of_m_Character_4(),
	Platformer2DUserControl_t4130129562::get_offset_of_m_Jump_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (PlatformerCharacter2D_t675295753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[13] = 
{
	PlatformerCharacter2D_t675295753::get_offset_of_m_MaxSpeed_4(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_JumpForce_5(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_CrouchSpeed_6(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_AirControl_7(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_WhatIsGround_8(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_GroundCheck_9(),
	0,
	PlatformerCharacter2D_t675295753::get_offset_of_m_Grounded_11(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_CeilingCheck_12(),
	0,
	PlatformerCharacter2D_t675295753::get_offset_of_m_Anim_14(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_Rigidbody2D_15(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_FacingRight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (Restarter_t269523250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (AbstractTargetFollower_t1919708159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[4] = 
{
	AbstractTargetFollower_t1919708159::get_offset_of_m_Target_4(),
	AbstractTargetFollower_t1919708159::get_offset_of_m_AutoTargetPlayer_5(),
	AbstractTargetFollower_t1919708159::get_offset_of_m_UpdateType_6(),
	AbstractTargetFollower_t1919708159::get_offset_of_targetRigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (UpdateType_t2449601881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	UpdateType_t2449601881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (AutoCam_t137911967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[12] = 
{
	AutoCam_t137911967::get_offset_of_m_MoveSpeed_11(),
	AutoCam_t137911967::get_offset_of_m_TurnSpeed_12(),
	AutoCam_t137911967::get_offset_of_m_RollSpeed_13(),
	AutoCam_t137911967::get_offset_of_m_FollowVelocity_14(),
	AutoCam_t137911967::get_offset_of_m_FollowTilt_15(),
	AutoCam_t137911967::get_offset_of_m_SpinTurnLimit_16(),
	AutoCam_t137911967::get_offset_of_m_TargetVelocityLowerLimit_17(),
	AutoCam_t137911967::get_offset_of_m_SmoothTurnTime_18(),
	AutoCam_t137911967::get_offset_of_m_LastFlatAngle_19(),
	AutoCam_t137911967::get_offset_of_m_CurrentTurnAmount_20(),
	AutoCam_t137911967::get_offset_of_m_TurnSpeedVelocityChange_21(),
	AutoCam_t137911967::get_offset_of_m_RollUp_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (FreeLookCam_t2000732766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[13] = 
{
	FreeLookCam_t2000732766::get_offset_of_m_MoveSpeed_11(),
	FreeLookCam_t2000732766::get_offset_of_m_TurnSpeed_12(),
	FreeLookCam_t2000732766::get_offset_of_m_TurnSmoothing_13(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltMax_14(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltMin_15(),
	FreeLookCam_t2000732766::get_offset_of_m_LockCursor_16(),
	FreeLookCam_t2000732766::get_offset_of_m_VerticalAutoReturn_17(),
	FreeLookCam_t2000732766::get_offset_of_m_LookAngle_18(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltAngle_19(),
	0,
	FreeLookCam_t2000732766::get_offset_of_m_PivotEulers_21(),
	FreeLookCam_t2000732766::get_offset_of_m_PivotTargetRot_22(),
	FreeLookCam_t2000732766::get_offset_of_m_TransformTargetRot_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (HandHeldCam_t450595784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[4] = 
{
	HandHeldCam_t450595784::get_offset_of_m_SwaySpeed_13(),
	HandHeldCam_t450595784::get_offset_of_m_BaseSwayAmount_14(),
	HandHeldCam_t450595784::get_offset_of_m_TrackingSwayAmount_15(),
	HandHeldCam_t450595784::get_offset_of_m_TrackingBias_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (LookatTarget_t3260877718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[5] = 
{
	LookatTarget_t3260877718::get_offset_of_m_RotationRange_8(),
	LookatTarget_t3260877718::get_offset_of_m_FollowSpeed_9(),
	LookatTarget_t3260877718::get_offset_of_m_FollowAngles_10(),
	LookatTarget_t3260877718::get_offset_of_m_OriginalRotation_11(),
	LookatTarget_t3260877718::get_offset_of_m_FollowVelocity_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (PivotBasedCameraRig_t3786953582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[3] = 
{
	PivotBasedCameraRig_t3786953582::get_offset_of_m_Cam_8(),
	PivotBasedCameraRig_t3786953582::get_offset_of_m_Pivot_9(),
	PivotBasedCameraRig_t3786953582::get_offset_of_m_LastTargetPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (ProtectCameraFromWallClip_t303409715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[15] = 
{
	ProtectCameraFromWallClip_t303409715::get_offset_of_clipMoveTime_4(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_returnTime_5(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_sphereCastRadius_6(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_visualiseInEditor_7(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_closestDistance_8(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_U3CprotectingU3Ek__BackingField_9(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_dontClipTag_10(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Cam_11(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Pivot_12(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_OriginalDist_13(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_MoveVelocity_14(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_CurrentDist_15(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Ray_16(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Hits_17(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_RayHitComparer_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (RayHitComparer_t2205555946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (TargetFieldOfView_t3060904718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[7] = 
{
	TargetFieldOfView_t3060904718::get_offset_of_m_FovAdjustTime_8(),
	TargetFieldOfView_t3060904718::get_offset_of_m_ZoomAmountMultiplier_9(),
	TargetFieldOfView_t3060904718::get_offset_of_m_IncludeEffectsInSize_10(),
	TargetFieldOfView_t3060904718::get_offset_of_m_BoundSize_11(),
	TargetFieldOfView_t3060904718::get_offset_of_m_FovAdjustVelocity_12(),
	TargetFieldOfView_t3060904718::get_offset_of_m_Cam_13(),
	TargetFieldOfView_t3060904718::get_offset_of_m_LastTarget_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (FirstPersonController_t2020989554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[30] = 
{
	FirstPersonController_t2020989554::get_offset_of_m_IsWalking_4(),
	FirstPersonController_t2020989554::get_offset_of_m_WalkSpeed_5(),
	FirstPersonController_t2020989554::get_offset_of_m_RunSpeed_6(),
	FirstPersonController_t2020989554::get_offset_of_m_RunstepLenghten_7(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpSpeed_8(),
	FirstPersonController_t2020989554::get_offset_of_m_StickToGroundForce_9(),
	FirstPersonController_t2020989554::get_offset_of_m_GravityMultiplier_10(),
	FirstPersonController_t2020989554::get_offset_of_m_MouseLook_11(),
	FirstPersonController_t2020989554::get_offset_of_m_UseFovKick_12(),
	FirstPersonController_t2020989554::get_offset_of_m_FovKick_13(),
	FirstPersonController_t2020989554::get_offset_of_m_UseHeadBob_14(),
	FirstPersonController_t2020989554::get_offset_of_m_HeadBob_15(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpBob_16(),
	FirstPersonController_t2020989554::get_offset_of_m_StepInterval_17(),
	FirstPersonController_t2020989554::get_offset_of_m_FootstepSounds_18(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpSound_19(),
	FirstPersonController_t2020989554::get_offset_of_m_LandSound_20(),
	FirstPersonController_t2020989554::get_offset_of_m_Camera_21(),
	FirstPersonController_t2020989554::get_offset_of_m_Jump_22(),
	FirstPersonController_t2020989554::get_offset_of_m_YRotation_23(),
	FirstPersonController_t2020989554::get_offset_of_m_Input_24(),
	FirstPersonController_t2020989554::get_offset_of_m_MoveDir_25(),
	FirstPersonController_t2020989554::get_offset_of_m_CharacterController_26(),
	FirstPersonController_t2020989554::get_offset_of_m_CollisionFlags_27(),
	FirstPersonController_t2020989554::get_offset_of_m_PreviouslyGrounded_28(),
	FirstPersonController_t2020989554::get_offset_of_m_OriginalCameraPosition_29(),
	FirstPersonController_t2020989554::get_offset_of_m_StepCycle_30(),
	FirstPersonController_t2020989554::get_offset_of_m_NextStep_31(),
	FirstPersonController_t2020989554::get_offset_of_m_Jumping_32(),
	FirstPersonController_t2020989554::get_offset_of_m_AudioSource_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (HeadBob_t3275031667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[8] = 
{
	HeadBob_t3275031667::get_offset_of_Camera_4(),
	HeadBob_t3275031667::get_offset_of_motionBob_5(),
	HeadBob_t3275031667::get_offset_of_jumpAndLandingBob_6(),
	HeadBob_t3275031667::get_offset_of_rigidbodyFirstPersonController_7(),
	HeadBob_t3275031667::get_offset_of_StrideInterval_8(),
	HeadBob_t3275031667::get_offset_of_RunningStrideLengthen_9(),
	HeadBob_t3275031667::get_offset_of_m_PreviouslyGrounded_10(),
	HeadBob_t3275031667::get_offset_of_m_OriginalCameraPosition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (MouseLook_t2859678661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[11] = 
{
	MouseLook_t2859678661::get_offset_of_XSensitivity_0(),
	MouseLook_t2859678661::get_offset_of_YSensitivity_1(),
	MouseLook_t2859678661::get_offset_of_clampVerticalRotation_2(),
	MouseLook_t2859678661::get_offset_of_MinimumX_3(),
	MouseLook_t2859678661::get_offset_of_MaximumX_4(),
	MouseLook_t2859678661::get_offset_of_smooth_5(),
	MouseLook_t2859678661::get_offset_of_smoothTime_6(),
	MouseLook_t2859678661::get_offset_of_lockCursor_7(),
	MouseLook_t2859678661::get_offset_of_m_CharacterTargetRot_8(),
	MouseLook_t2859678661::get_offset_of_m_CameraTargetRot_9(),
	MouseLook_t2859678661::get_offset_of_m_cursorIsLocked_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (RigidbodyFirstPersonController_t1207297146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[12] = 
{
	RigidbodyFirstPersonController_t1207297146::get_offset_of_cam_4(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_movementSettings_5(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_mouseLook_6(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_advancedSettings_7(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_RigidBody_8(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Capsule_9(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_YRotation_10(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_GroundContactNormal_11(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Jump_12(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_PreviouslyGrounded_13(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Jumping_14(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_IsGrounded_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (MovementSettings_t1096092444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[8] = 
{
	MovementSettings_t1096092444::get_offset_of_ForwardSpeed_0(),
	MovementSettings_t1096092444::get_offset_of_BackwardSpeed_1(),
	MovementSettings_t1096092444::get_offset_of_StrafeSpeed_2(),
	MovementSettings_t1096092444::get_offset_of_RunMultiplier_3(),
	MovementSettings_t1096092444::get_offset_of_RunKey_4(),
	MovementSettings_t1096092444::get_offset_of_JumpForce_5(),
	MovementSettings_t1096092444::get_offset_of_SlopeCurveModifier_6(),
	MovementSettings_t1096092444::get_offset_of_CurrentTargetSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (AdvancedSettings_t778418834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[5] = 
{
	AdvancedSettings_t778418834::get_offset_of_groundCheckDistance_0(),
	AdvancedSettings_t778418834::get_offset_of_stickToGroundHelperDistance_1(),
	AdvancedSettings_t778418834::get_offset_of_slowDownRate_2(),
	AdvancedSettings_t778418834::get_offset_of_airControl_3(),
	AdvancedSettings_t778418834::get_offset_of_shellOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (Ball_t2378314638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[6] = 
{
	Ball_t2378314638::get_offset_of_m_MovePower_4(),
	Ball_t2378314638::get_offset_of_m_UseTorque_5(),
	Ball_t2378314638::get_offset_of_m_MaxAngularVelocity_6(),
	Ball_t2378314638::get_offset_of_m_JumpPower_7(),
	0,
	Ball_t2378314638::get_offset_of_m_Rigidbody_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (BallUserControl_t2574698008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[5] = 
{
	BallUserControl_t2574698008::get_offset_of_ball_4(),
	BallUserControl_t2574698008::get_offset_of_move_5(),
	BallUserControl_t2574698008::get_offset_of_cam_6(),
	BallUserControl_t2574698008::get_offset_of_camForward_7(),
	BallUserControl_t2574698008::get_offset_of_jump_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (AICharacterControl_t2972373937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[3] = 
{
	AICharacterControl_t2972373937::get_offset_of_U3CagentU3Ek__BackingField_4(),
	AICharacterControl_t2972373937::get_offset_of_U3CcharacterU3Ek__BackingField_5(),
	AICharacterControl_t2972373937::get_offset_of_target_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (ThirdPersonCharacter_t1711070432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[20] = 
{
	ThirdPersonCharacter_t1711070432::get_offset_of_m_MovingTurnSpeed_4(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_StationaryTurnSpeed_5(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_JumpPower_6(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GravityMultiplier_7(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_RunCycleLegOffset_8(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_MoveSpeedMultiplier_9(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_AnimSpeedMultiplier_10(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GroundCheckDistance_11(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Rigidbody_12(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Animator_13(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_IsGrounded_14(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_OrigGroundCheckDistance_15(),
	0,
	ThirdPersonCharacter_t1711070432::get_offset_of_m_TurnAmount_17(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_ForwardAmount_18(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GroundNormal_19(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_CapsuleHeight_20(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_CapsuleCenter_21(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Capsule_22(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Crouching_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (ThirdPersonUserControl_t1527285130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[5] = 
{
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Character_4(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Cam_5(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_CamForward_6(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Move_7(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Jump_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (AxisTouchButton_t3522881333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[6] = 
{
	AxisTouchButton_t3522881333::get_offset_of_axisName_4(),
	AxisTouchButton_t3522881333::get_offset_of_axisValue_5(),
	AxisTouchButton_t3522881333::get_offset_of_responseSpeed_6(),
	AxisTouchButton_t3522881333::get_offset_of_returnToCentreSpeed_7(),
	AxisTouchButton_t3522881333::get_offset_of_m_PairedWith_8(),
	AxisTouchButton_t3522881333::get_offset_of_m_Axis_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (ButtonHandler_t823762219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[1] = 
{
	ButtonHandler_t823762219::get_offset_of_Name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (CrossPlatformInputManager_t191731427), -1, sizeof(CrossPlatformInputManager_t191731427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2159[3] = 
{
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (ActiveInputMethod_t139315314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[3] = 
{
	ActiveInputMethod_t139315314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (VirtualAxis_t4087348596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[3] = 
{
	VirtualAxis_t4087348596::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t4087348596::get_offset_of_m_Value_1(),
	VirtualAxis_t4087348596::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (VirtualButton_t2756566330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[5] = 
{
	VirtualButton_t2756566330::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2756566330::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2756566330::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2756566330::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2756566330::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (InputAxisScrollbar_t457958266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[1] = 
{
	InputAxisScrollbar_t457958266::get_offset_of_axis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (Joystick_t2204371675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[9] = 
{
	Joystick_t2204371675::get_offset_of_MovementRange_4(),
	Joystick_t2204371675::get_offset_of_axesToUse_5(),
	Joystick_t2204371675::get_offset_of_horizontalAxisName_6(),
	Joystick_t2204371675::get_offset_of_verticalAxisName_7(),
	Joystick_t2204371675::get_offset_of_m_StartPos_8(),
	Joystick_t2204371675::get_offset_of_m_UseX_9(),
	Joystick_t2204371675::get_offset_of_m_UseY_10(),
	Joystick_t2204371675::get_offset_of_m_HorizontalVirtualAxis_11(),
	Joystick_t2204371675::get_offset_of_m_VerticalVirtualAxis_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (AxisOption_t3128671669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2165[4] = 
{
	AxisOption_t3128671669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (MobileControlRig_t1964600252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (MobileInput_t2025745297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (StandaloneInput_t1343950252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (TiltInput_t1639936653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[5] = 
{
	TiltInput_t1639936653::get_offset_of_mapping_4(),
	TiltInput_t1639936653::get_offset_of_tiltAroundAxis_5(),
	TiltInput_t1639936653::get_offset_of_fullTiltAngle_6(),
	TiltInput_t1639936653::get_offset_of_centreAngleOffset_7(),
	TiltInput_t1639936653::get_offset_of_m_SteerAxis_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (AxisOptions_t3101732129)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2170[3] = 
{
	AxisOptions_t3101732129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (AxisMapping_t3982445645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[2] = 
{
	AxisMapping_t3982445645::get_offset_of_type_0(),
	AxisMapping_t3982445645::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (MappingType_t2039944511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2172[5] = 
{
	MappingType_t2039944511::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (TouchPad_t539039257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[18] = 
{
	TouchPad_t539039257::get_offset_of_axesToUse_4(),
	TouchPad_t539039257::get_offset_of_controlStyle_5(),
	TouchPad_t539039257::get_offset_of_horizontalAxisName_6(),
	TouchPad_t539039257::get_offset_of_verticalAxisName_7(),
	TouchPad_t539039257::get_offset_of_Xsensitivity_8(),
	TouchPad_t539039257::get_offset_of_Ysensitivity_9(),
	TouchPad_t539039257::get_offset_of_m_StartPos_10(),
	TouchPad_t539039257::get_offset_of_m_PreviousDelta_11(),
	TouchPad_t539039257::get_offset_of_m_JoytickOutput_12(),
	TouchPad_t539039257::get_offset_of_m_UseX_13(),
	TouchPad_t539039257::get_offset_of_m_UseY_14(),
	TouchPad_t539039257::get_offset_of_m_HorizontalVirtualAxis_15(),
	TouchPad_t539039257::get_offset_of_m_VerticalVirtualAxis_16(),
	TouchPad_t539039257::get_offset_of_m_Dragging_17(),
	TouchPad_t539039257::get_offset_of_m_Id_18(),
	TouchPad_t539039257::get_offset_of_m_PreviousTouchPos_19(),
	TouchPad_t539039257::get_offset_of_m_Center_20(),
	TouchPad_t539039257::get_offset_of_m_Image_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (AxisOption_t1372819835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2174[4] = 
{
	AxisOption_t1372819835::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ControlStyle_t1372986211)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2175[4] = 
{
	ControlStyle_t1372986211::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (VirtualInput_t2597455733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[4] = 
{
	VirtualInput_t2597455733::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t2597455733::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (WaterBasic_t418026961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (Displace_t1352130581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (GerstnerDisplace_t2537102777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (MeshContainer_t140041298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[3] = 
{
	MeshContainer_t140041298::get_offset_of_mesh_0(),
	MeshContainer_t140041298::get_offset_of_vertices_1(),
	MeshContainer_t140041298::get_offset_of_normals_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (PlanarReflection_t439636033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[9] = 
{
	PlanarReflection_t439636033::get_offset_of_reflectionMask_4(),
	PlanarReflection_t439636033::get_offset_of_reflectSkybox_5(),
	PlanarReflection_t439636033::get_offset_of_clearColor_6(),
	PlanarReflection_t439636033::get_offset_of_reflectionSampler_7(),
	PlanarReflection_t439636033::get_offset_of_clipPlaneOffset_8(),
	PlanarReflection_t439636033::get_offset_of_m_Oldpos_9(),
	PlanarReflection_t439636033::get_offset_of_m_ReflectionCamera_10(),
	PlanarReflection_t439636033::get_offset_of_m_SharedMaterial_11(),
	PlanarReflection_t439636033::get_offset_of_m_HelperCameras_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (SpecularLighting_t2163114238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[2] = 
{
	SpecularLighting_t2163114238::get_offset_of_specularLight_4(),
	SpecularLighting_t2163114238::get_offset_of_m_WaterBase_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (Water_t936588004), -1, sizeof(Water_t936588004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2183[14] = 
{
	Water_t936588004::get_offset_of_waterMode_4(),
	Water_t936588004::get_offset_of_disablePixelLights_5(),
	Water_t936588004::get_offset_of_textureSize_6(),
	Water_t936588004::get_offset_of_clipPlaneOffset_7(),
	Water_t936588004::get_offset_of_reflectLayers_8(),
	Water_t936588004::get_offset_of_refractLayers_9(),
	Water_t936588004::get_offset_of_m_ReflectionCameras_10(),
	Water_t936588004::get_offset_of_m_RefractionCameras_11(),
	Water_t936588004::get_offset_of_m_ReflectionTexture_12(),
	Water_t936588004::get_offset_of_m_RefractionTexture_13(),
	Water_t936588004::get_offset_of_m_HardwareWaterSupport_14(),
	Water_t936588004::get_offset_of_m_OldReflectionTextureSize_15(),
	Water_t936588004::get_offset_of_m_OldRefractionTextureSize_16(),
	Water_t936588004_StaticFields::get_offset_of_s_InsideWater_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (WaterMode_t293960580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2184[4] = 
{
	WaterMode_t293960580::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (WaterQuality_t1541080576)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2185[4] = 
{
	WaterQuality_t1541080576::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (WaterBase_t3883863317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[3] = 
{
	WaterBase_t3883863317::get_offset_of_sharedMaterial_4(),
	WaterBase_t3883863317::get_offset_of_waterQuality_5(),
	WaterBase_t3883863317::get_offset_of_edgeBlend_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (WaterTile_t2536246541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[2] = 
{
	WaterTile_t2536246541::get_offset_of_reflection_4(),
	WaterTile_t2536246541::get_offset_of_waterBase_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (AfterburnerPhysicsForce_t498893161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[6] = 
{
	AfterburnerPhysicsForce_t498893161::get_offset_of_effectAngle_4(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_effectWidth_5(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_effectDistance_6(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_force_7(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_m_Cols_8(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_m_Sphere_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (ExplosionFireAndDebris_t2411343565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[4] = 
{
	ExplosionFireAndDebris_t2411343565::get_offset_of_debrisPrefabs_4(),
	ExplosionFireAndDebris_t2411343565::get_offset_of_firePrefab_5(),
	ExplosionFireAndDebris_t2411343565::get_offset_of_numDebrisPieces_6(),
	ExplosionFireAndDebris_t2411343565::get_offset_of_numFires_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (U3CStartU3Ec__Iterator0_t150375094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[10] = 
{
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U3CmultiplierU3E__0_0(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U3CrU3E__0_1(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U3CcolsU3E__0_2(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U24locvar0_3(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U24locvar1_4(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U3CtestRU3E__0_5(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t150375094::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (ExplosionPhysicsForce_t3982641844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[1] = 
{
	ExplosionPhysicsForce_t3982641844::get_offset_of_explosionForce_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (U3CStartU3Ec__Iterator0_t3271890998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[11] = 
{
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U3CmultiplierU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U3CrU3E__0_1(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U3CcolsU3E__0_2(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U3CrigidbodiesU3E__0_3(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U24locvar0_4(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U24locvar1_5(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U24locvar2_6(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U24this_7(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U24current_8(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U24disposing_9(),
	U3CStartU3Ec__Iterator0_t3271890998::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (Explosive_t792321375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[7] = 
{
	Explosive_t792321375::get_offset_of_explosionPrefab_4(),
	Explosive_t792321375::get_offset_of_detonationImpactVelocity_5(),
	Explosive_t792321375::get_offset_of_sizeMultiplier_6(),
	Explosive_t792321375::get_offset_of_reset_7(),
	Explosive_t792321375::get_offset_of_resetTimeDelay_8(),
	Explosive_t792321375::get_offset_of_m_Exploded_9(),
	Explosive_t792321375::get_offset_of_m_ObjectResetter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (U3COnCollisionEnterU3Ec__Iterator0_t2695961835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[5] = 
{
	U3COnCollisionEnterU3Ec__Iterator0_t2695961835::get_offset_of_col_0(),
	U3COnCollisionEnterU3Ec__Iterator0_t2695961835::get_offset_of_U24this_1(),
	U3COnCollisionEnterU3Ec__Iterator0_t2695961835::get_offset_of_U24current_2(),
	U3COnCollisionEnterU3Ec__Iterator0_t2695961835::get_offset_of_U24disposing_3(),
	U3COnCollisionEnterU3Ec__Iterator0_t2695961835::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (ExtinguishableParticleSystem_t4259708998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[2] = 
{
	ExtinguishableParticleSystem_t4259708998::get_offset_of_multiplier_4(),
	ExtinguishableParticleSystem_t4259708998::get_offset_of_m_Systems_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (FireLight_t2068143130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[3] = 
{
	FireLight_t2068143130::get_offset_of_m_Rnd_4(),
	FireLight_t2068143130::get_offset_of_m_Burning_5(),
	FireLight_t2068143130::get_offset_of_m_Light_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (Hose_t3016386068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	Hose_t3016386068::get_offset_of_maxPower_4(),
	Hose_t3016386068::get_offset_of_minPower_5(),
	Hose_t3016386068::get_offset_of_changeSpeed_6(),
	Hose_t3016386068::get_offset_of_hoseWaterSystems_7(),
	Hose_t3016386068::get_offset_of_systemRenderer_8(),
	Hose_t3016386068::get_offset_of_m_Power_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (ParticleSystemMultiplier_t2770350653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[1] = 
{
	ParticleSystemMultiplier_t2770350653::get_offset_of_multiplier_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (SmokeParticles_t494565528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[1] = 
{
	SmokeParticles_t494565528::get_offset_of_extinguishSounds_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
